package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class Recode_taocai extends Activity {
	
	Button	BTN_SAVE;
	Button	BTN_EXIT;
	Button	BTN_MORE;
	Button	BTN_LESS;

	Button	BTN_NEW;
	Button	BTN_UPDATA;
	Button	BTN_DEL;
	Button	BTN_CAIDEL1;
	Button	BTN_CAIDEL2;
	
	ListView	LS_TAOCAI;
	ListView	LS_TAOCAI_CAI_1,LS_TAOCAI_CAI_2;
	ListView	LS_ALLCAI;
	ListView	LS_BIGCLASS;
	
	EditText	CAI_NAME;
	EditText	CAI_CODE;
	EditText	CAI_PINYIN;
	EditText	CAI_PRICE;
	EditText	CAI_YOUHUI;
	EditText	CAI_OTHER;
	
	TextView	ALL_PRICE_1,ALL_PRICE_2;
	Spinner		CAI_SHUXING;
	Spinner		CAI_FENLEI;
	
	LinearLayout	LAYOUT_PART2;
	LinearLayout	LAYOUT_PART3;

	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;
    private ArrayList<HashMap<String,String>> list_cai=null;
    private HashMap<String,String>map_cai=null;
	SimpleAdapter adapter_cai;
    private ArrayList<HashMap<String,String>> list_bigclass=null;
    private HashMap<String,String>map_bigclass=null;
	SimpleAdapter adapter_bigclass;
    private ArrayList<HashMap<String,String>> list_allcai=null;
    private HashMap<String,String>map_allcai=null;
	SimpleAdapter adapter_allcai;
	int	list_all=0;
    int	list_position=0; 
    int big_class_num;
    
    private ArrayAdapter adapter_cai_shuxing;  
    int		cai_shuxing;
    View	TAO_Cai,BIG_ITem,Cai_ITem;
    String	TaoCai_Code,TaoCai_Name,  CaiCode;
    
    private String[] m,big_class;
    private ArrayAdapter<String> adapter_fenlei;  
    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_recode_taocai);
        
        find_view();
        key_things();
        //初始化数据库 显示在listview
 	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
  		String[] refer = {"1"}; 
 	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   Cursor  cursor = db.rawQuery("select * from lfcy_base_dishinf where cIfTaocai = ?", refer);
 	   fill_hashmap(cursor);
  	   inflateList(cursor);	
 	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   cursor = db.rawQuery("select * from lfcy_base_bigsort", null);
 	   fill_hashmap_bigclass(cursor);
  	   inflateList_bigclass(cursor);	
 	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   cursor = db.rawQuery("select * from lfcy_base_dishinf ", null);
 	   fill_hashmap_allcai(cursor);
  	   inflateList_allcai(cursor);	
  	   
  	   get_fenlei();
       init_view();
    }

    
    
	private void get_fenlei() {
		// TODO Auto-generated method stub
		Cursor   cursor = db.rawQuery("select * from lfcy_base_bigsort", null);
		int i = 0;
		while(cursor.moveToNext()) {
            map_bigclass = new HashMap<String,String>();
            
            map_bigclass.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map_bigclass.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
            map_bigclass.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));

            list_bigclass.add(map_bigclass);
			big_class[i] = cursor.getString(cursor.getColumnIndex("cCode"));				
			m[i] = cursor.getString(cursor.getColumnIndex("cName"));

			i++;
		}		
	}



	private void init_view() {
		// TODO Auto-generated method stub
        //将可选内容与ArrayAdapter连接起来   
		adapter_cai_shuxing= ArrayAdapter.createFromResource(this, R.array.cai_shuxing, android.R.layout.simple_spinner_item); 
 	    //设置下拉列表的风格   
		adapter_cai_shuxing.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		CAI_SHUXING.setAdapter(adapter_cai_shuxing);    
		
		
		//分类的下拉框初始化
	
        //将可选内容与ArrayAdapter连接起来  
	    adapter_fenlei = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,m);           
        //设置下拉列表的风格  
        adapter_fenlei.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);      
        //将adapter 添加到spinner中  
        CAI_FENLEI.setAdapter(adapter_fenlei);  
        //添加事件Spinner事件监听    
        CAI_FENLEI.setOnItemSelectedListener(new SpinnerSelectedListener());  
        //设置默认值  
        CAI_FENLEI.setVisibility(View.VISIBLE); 
	}

    //使用数组形式操作  
    class SpinnerSelectedListener implements OnItemSelectedListener{  
  
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,  
                long arg3) {  
//            CAI_YOUHUI.setText("："+m[arg2]);  
        }  
  
        public void onNothingSelected(AdapterView<?> arg0) {  
        }  
    } 
/************** 套菜列表 开始**********************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            map.put("_num", 	""+i);           
            map.put("_id", 		 cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cDishName", cursor.getString(cursor.getColumnIndex("cDishName")));
            map.put("cDishCode", cursor.getString(cursor.getColumnIndex("cDishCode")));
            map.put("cDishPinYin",  cursor.getString(cursor.getColumnIndex("cDishPinYin")));
            map.put("cDishClass1",  cursor.getString(cursor.getColumnIndex("cDishClass1")));
            map.put("fDishPrice", cursor.getString(cursor.getColumnIndex("fDishPrice")));
            map.put("iDishStyle", cursor.getString(cursor.getColumnIndex("iDishStyle")));
		
			list.add(map);
		}
		list_all = i;
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.i800_cai_list 
				, new String[]{	"_num" , "cDishName"  ,"cDishCode" , 
        				"fDishPrice"   ,"fTaocaiPrice", "cDishPinYin",
        				"cDishClass1"}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3,
        				R.id.table_list_4,R.id.table_list_5,R.id.table_list_6,
        				R.id.table_list_7 }
     );	
		//显示数据
		LS_TAOCAI.setAdapter(adapter);
	}
	/************** 套菜列表  结束**********************************************/
	
	/************** 菜品列表 开始**********************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap_allcai(Cursor cursor) {
	    list_allcai=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map_allcai=new HashMap<String,String>();
            map_allcai.put("_num", ""+i);            
            map_allcai.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map_allcai.put("cDishName", cursor.getString(cursor.getColumnIndex("cDishName")));
            map_allcai.put("cDishCode", cursor.getString(cursor.getColumnIndex("cDishCode")));
            map_allcai.put("cDishPinYin",  cursor.getString(cursor.getColumnIndex("cDishPinYin")));
            map_allcai.put("fDishPrice", cursor.getString(cursor.getColumnIndex("fDishPrice")));
            map_allcai.put("iDishStyle", cursor.getString(cursor.getColumnIndex("iDishStyle")));
		
            list_allcai.add(map_allcai);
			i++;
		}
		list_all = i;
		return list_allcai;
	}
	
	private void inflateList_allcai(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter_allcai = new SimpleAdapter(
        		this, 
        		list_allcai, 
				R.layout.imx_table_list 
				, new String[]{	"_num" 		  , "cDishName" 	  ,"cDishCode" 	    , "cDishPinYin"  ,"fDishPrice"  ,"iDishStyle"	 }
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3,R.id.table_list_4,R.id.table_list_5 }
        );	
		//显示数据
		LS_ALLCAI.setAdapter(adapter_allcai);
	}
	/************** 套品列表  结束**********************************************/
	

	
	/************** 套菜内菜品列表 开始**********************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap_cai(Cursor cursor) {
	    list_cai=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map_cai=new HashMap<String,String>();
            
            map_cai.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map_cai.put("cTaoCaiCode", cursor.getString(cursor.getColumnIndex("cTaoCaiCode")));
            map_cai.put("cTaoCaiName", cursor.getString(cursor.getColumnIndex("cTaoCaiName")));
            map_cai.put("cCaiCode",  cursor.getString(cursor.getColumnIndex("cCaiCode")));
            map_cai.put("cCaiName",  cursor.getString(cursor.getColumnIndex("cCaiName")));
            map_cai.put("cCaiPrice",  cursor.getString(cursor.getColumnIndex("cCaiPrice")));
		
            list_cai.add(map_cai);
		}
		list_all = i;
		return list_cai;
	}
	
	private void inflateList_cai(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter_cai = new SimpleAdapter(
        		this, 
        		list_cai, 
				R.layout.i800_usual_item 
				, new String[]{	"cCaiName" 	  ,"cCaiPrice" 	}
				, new int[]{R.id.item_list_1 , R.id.item_list_2 }
        );	
		//显示数据
		LS_TAOCAI_CAI_1.setAdapter(adapter_cai);
		LS_TAOCAI_CAI_2.setAdapter(adapter_cai);
	}
	/************** 套菜内菜品列表  结束**********************************************/
	
	
	/************** 菜品大类列表 开始**********************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap_bigclass(Cursor cursor) {
	    list_bigclass=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
            map_bigclass = new HashMap<String,String>();
            
            map_bigclass.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map_bigclass.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
            map_bigclass.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));

            list_bigclass.add(map_bigclass);
			i++;
		}
		big_class_num = i;
	    big_class = new String[i];
	    m =new String[i];

		list_all = i;
		return list_cai;
	}
	
	private void inflateList_bigclass(Cursor cursor)
	{
		//将数据与adapter集合起来
		adapter_bigclass = new SimpleAdapter(
        		this, 
        		list_bigclass, 
				R.layout.i800_bigclass_list 
				, new String[]{	"cName" 	  }
				, new int[]{R.id.table_list_1  }
        );	
		//显示数据
		LS_BIGCLASS.setAdapter(adapter_bigclass);
	}
	/************** 菜品大类列表  结束**********************************************/
	

	
	
	private	void my_AnimationSet( int mycase){
		AnimationSet animationSet_in = new AnimationSet(true); 
		TranslateAnimation translateAnimation_in = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation_in.setStartOffset(400);
		translateAnimation_in.setDuration(400); 
		animationSet_in.addAnimation(translateAnimation_in); 	
		
		AnimationSet animationSet_out = new AnimationSet(true); 
		TranslateAnimation translateAnimation_out = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation_out.setDuration(400); 
		animationSet_out.addAnimation(translateAnimation_out); 		
		
		switch (mycase) {
		case 1:		//首页出   尾页进
			LAYOUT_PART2.startAnimation(animationSet_out);				
			LAYOUT_PART2.setVisibility(View.GONE);			
			LAYOUT_PART3.setVisibility(View.VISIBLE);			
			LAYOUT_PART3.startAnimation(animationSet_in);				
			break;
		case 2:		//首页进  尾页出 
			LAYOUT_PART3.startAnimation(animationSet_out);				
			LAYOUT_PART3.setVisibility(View.GONE);			
			LAYOUT_PART2.setVisibility(View.VISIBLE);			
			LAYOUT_PART2.startAnimation(animationSet_in);				
			break;
		default:
			break;
		}
	}
	private void key_things() {
		// TODO Auto-generated method stub
		
		BTN_NEW.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		CAI_NAME.setText("");
		   		CAI_CODE.setText("");
		   		CAI_PINYIN.setText("");
		   		CAI_PRICE.setText("");
		   		CAI_YOUHUI.setText("");
		   		CAI_OTHER.setText("");	
                
		   		String[] refer = {"0"};
           	    Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode = ?", refer);
           	    fill_hashmap_cai(cursor);
        	    inflateList_cai(cursor);	  
    			ALL_PRICE_1.setText(""+0);			
    			ALL_PRICE_2.setText(""+0);	
    			TaoCai_Code = null;
    		}
	    });
		
		BTN_MORE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		my_AnimationSet(1);
		   	}
	    });	
		BTN_LESS.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		my_AnimationSet(2);			
		   	}
	    });	
		
		BTN_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		String	name	=	CAI_NAME.getText().toString();
		   		String	code	=	CAI_CODE.getText().toString();
		   		String	pinyinma=	CAI_PINYIN.getText().toString();
		   		String	price	=	CAI_PRICE.getText().toString();
		   		int     style   = 	CAI_SHUXING.getSelectedItemPosition();
		   		int     bigclass= 	CAI_FENLEI.getSelectedItemPosition();
		   		String  class_code = big_class[bigclass];
		   		String	price_all  = ALL_PRICE_1.getText().toString();
		   		
   				String Activity_Table = "lfcy_base_dishinf";
   				sql_control sqlc = new sql_control(); 
				boolean if_only_one = sqlc.sql_item_only_one(db, Activity_Table, "cDishCode", code);
				if (if_only_one) {
					insert_DishData(db, name,name,code,pinyinma,pinyinma			,class_code,"2",null,""+style,"0",price ,"1",price_all );					
					LS_TAOCAI.setSelection(adapter.getCount()); 
				} else {
			   		//修改数据库
		   			ContentValues cv = new ContentValues();
		   		   	//存储修改的数据
		   		   	cv.put("cDishName" 		, name);
		   		   	cv.put("cIfTaocai" 		, "1");
		   		   	cv.put("cDishCode" 		, code);
		   		   	cv.put("cDishPinYin"	, pinyinma);
		   		   	cv.put("cDishClass1"	, big_class[bigclass]);
		   		   	cv.put("iDishStyle"		, ""+style);
		   		   	cv.put("fDishPrice"		, price );
		   		   	cv.put("fTaocaiPrice"	, price_all);
		   		 
		   		   	String[] contion = {code}; 
				   	db.update("lfcy_base_dishinf",cv,"cDishCode = ? ",contion);
				}
				
		  		
				String[] refer = {"1"}; //为了区别她是套菜
		  	    //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
		  	    Cursor  cursor = db.rawQuery("select * from lfcy_base_dishinf where cIfTaocai = ?", refer);
		 	    fill_hashmap(cursor);
		 	    inflateList(cursor);	
		   	}
	    });		
		
		BTN_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
		
		CAI_SHUXING.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	cai_shuxing = postion;
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });

		
		BTN_DEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
			   		//获取listview行数
	 				HashMap<String,String> map = (HashMap<String,String>)LS_TAOCAI.getItemAtPosition(list_position);
	                String id =	String.valueOf(map.get("_id"));		   		//根据行数得到该行的id值
			   		//删除对应id的那道菜
			   		remove_cai(db , id);

			  		String[] refer = {"1"}; 
			  	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
			  	   Cursor  cursor = db.rawQuery("select * from lfcy_base_dishinf where cIfTaocai = ?", refer);
			  	   fill_hashmap(cursor);
			  	   inflateList(cursor);					
		   		} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Recode_taocai.this, "删除失败！", Toast.LENGTH_LONG).show();
				}

		   	}

	    });	
		
		LS_TAOCAI.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
               list_position = position;
			   HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
               String	cDishName 	=	String.valueOf(map.get("cDishName"));
               String	cDishCode  	=	String.valueOf(map.get("cDishCode"));
               String	cDishPinYin =	String.valueOf(map.get("cDishPinYin"));
               String	fDishPrice 	=	String.valueOf(map.get("fDishPrice"));
               String	iDishStyle  =	String.valueOf(map.get("iDishStyle"));
               String	cDishClass1  =	String.valueOf(map.get("cDishClass1"));
              
//               CAI_YOUHUI.setText(cDishClass1);
               int i;
               try {
                   Integer intObj = new Integer(iDishStyle);
                   i = intObj.intValue();				
               } catch (Exception e) {
				// TODO: handle exception
            	   i= 5;
               }
               
               int j;
               try {
            	   for(j=0;j<=big_class_num;j++)
            	   {                	   
            		   if (big_class[j].trim().equals(cDishClass1))
            			   break;
            	   }	   			
               } catch (Exception e) {
				// TODO: handle exception
            	   j = 1;
               }
               System.out.println(j);
				try {
					TAO_Cai.setBackgroundColor(Color.argb(255, 236, 236, 236)); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				TAO_Cai = v;
				TAO_Cai.setBackgroundColor(Color.argb(255, 22, 175, 251)); 	

               CAI_NAME.setText(cDishName);
               CAI_CODE.setText(cDishCode);
               CAI_PINYIN.setText(cDishPinYin);
               CAI_PRICE.setText(fDishPrice);
               System.out.println("CAI_SHUXING = "+i);
               CAI_SHUXING.setSelection(i-1); 
               CAI_FENLEI.setSelection(j); 
               
               TaoCai_Code = cDishCode;
               TaoCai_Name = cDishName;
               String[] refer = {cDishCode};
         	   Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode = ?", refer);
         	   fill_hashmap_cai(cursor);
          	   inflateList_cai(cursor);	    	   
          	   Total_Money();		   	
            }  
        }); 
	
		LS_BIGCLASS.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
            	String bigclass;
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
 				bigclass =	String.valueOf(map.get("cCode"));   
 				System.out.println("bigclass == "+ bigclass);

 				String[] nima = {bigclass};

 		 	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 			   Cursor cursor = db.rawQuery("select * from lfcy_base_dishinf where cDishClass1 = ?", nima);
 		 	   fill_hashmap_allcai(cursor);
 		  	   inflateList_allcai(cursor);	

 				try {
					BIG_ITem.setBackgroundColor(Color.argb(255, 211, 209, 209)); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				BIG_ITem = v;
				BIG_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
            }  
        });  
		
		LS_ALLCAI.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {  
            	try {
                	String bigclass;
     				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
                    String	cDishName 	=	String.valueOf(map.get("cDishName"));
                    String	cDishCode  	=	String.valueOf(map.get("cDishCode"));
                    String	fDishPrice 	=	String.valueOf(map.get("fDishPrice"));

    				insert_TaoCaiDishData(db, TaoCai_Code,TaoCai_Name,	cDishCode,cDishName,fDishPrice );

                   String[] refer = {TaoCai_Code};
              	   Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode = ?", refer);
              	   fill_hashmap_cai(cursor);
               	   inflateList_cai(cursor);	  
               	   Total_Money();					
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Recode_taocai.this, "请先保存套餐编码，再选择菜品", Toast.LENGTH_SHORT).show();
				}          	   
            }  
        });  
		
		LS_TAOCAI_CAI_1.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
                CaiCode 	=	String.valueOf(map.get("_id"));

 				try {
					Cai_ITem.setBackgroundColor(Color.argb(255, 211, 209, 209)); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				Cai_ITem = v;
				Cai_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251));  	
		   		BTN_CAIDEL1.setVisibility(View.VISIBLE);
            }  
        }); 
		LS_TAOCAI_CAI_2.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
                CaiCode 	=	String.valueOf(map.get("_id"));

 				try {
					Cai_ITem.setBackgroundColor(Color.argb(255, 211, 209, 209)); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				Cai_ITem = v;
				Cai_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251));  	
		   		BTN_CAIDEL2.setVisibility(View.VISIBLE);
           }  
        }); 

		BTN_CAIDEL1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		remove_tao_cai(db,CaiCode);
		   		
		   		BTN_CAIDEL1.setVisibility(View.INVISIBLE);
               String[] refer = {TaoCai_Code};
          	   Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode = ?", refer);
          	   fill_hashmap_cai(cursor);
           	   inflateList_cai(cursor);	 		   		
           	   Total_Money();		   	
		   	}
	    });	
		BTN_CAIDEL2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		remove_tao_cai(db,CaiCode);
		   		BTN_CAIDEL2.setVisibility(View.INVISIBLE);
               String[] refer = {TaoCai_Code};
          	   Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode = ?", refer);
          	   fill_hashmap_cai(cursor);
           	   inflateList_cai(cursor);	 
           	   Total_Money();		   	
           	 }
	    });			
		
	}

	
	protected void remove_tao_cai(SQLiteDatabase db, String	id) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_taocai_cailist", " _id =?", new String[]{id});
   			} catch (Exception e) {
					
   		}
	}
	
	protected void remove_cai(SQLiteDatabase db, String	id) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_base_dishinf", " _id =?", new String[]{id});
   			} catch (Exception e) {
					
   		}
	}

	private void insert_TaoCaiDishData(SQLiteDatabase db, 
			String cTaoCaiCode,	String cTaoCaiName, 
			String cCaiCode,	String cCaiName, 
			String cCaiPrice ) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_taocai_cailist values(null,?,?,?,?,?)", 
					new String[]{
				cTaoCaiCode ,cTaoCaiName ,	cCaiCode ,cCaiName ,cCaiPrice });		
	}
	
	public void insert_DishData(SQLiteDatabase db, String cDishName, String cDishName2,
			String cDishCode, String cDishPinYin, String cDishHelpCode, String cDishClass1,
			String cDishClass2, String cDishOther, String iDishStyle,String iDishDian, String fDishPrice,
			String cIfTaocai , String fTaocaiPrice) {

		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_dishinf values(null,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
					new String[]{
				cDishName ,cDishName2 ,	cDishCode ,cDishPinYin ,
				cDishHelpCode , cDishClass1 , cDishClass2  ,cDishOther , 
				iDishStyle ,iDishDian ,	fDishPrice ,cIfTaocai ,fTaocaiPrice });
	}
	
	protected void  Total_Money() {
		// TODO Auto-generated method stub
		try {
			String[] sell_number = {TaoCai_Code};
			Cursor  cursor = db.rawQuery("select * from lfcy_taocai_cailist where cTaoCaiCode= ?", sell_number);
			int	Total_All = 0;
			while(cursor.moveToNext()) 
			{
				String Sprice = cursor.getString(cursor.getColumnIndex("cCaiPrice"));
				Integer intObj = new Integer(Sprice);
			    int i = intObj.intValue();	
			    Total_All = Total_All + i;
			}		
			ALL_PRICE_1.setText(""+Total_All);			
			ALL_PRICE_2.setText(""+Total_All);			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}	
	private void find_view() {
		// TODO Auto-generated method stub
		BTN_SAVE	=	(Button) findViewById(R.id.recode_taocai_save);
		BTN_EXIT	=	(Button) findViewById(R.id.recode_taocai_exit);
		BTN_MORE	=	(Button) findViewById(R.id.recode_taocai_more);
		BTN_LESS	=	(Button) findViewById(R.id.recode_taocai_less);

		BTN_NEW	  	= (Button) findViewById(R.id.recode_taocai_new );
		BTN_UPDATA	= (Button) findViewById(R.id.recode_taocai_update);
		BTN_DEL	  	= (Button) findViewById(R.id.recode_taocai_del);
		BTN_CAIDEL1	  	= (Button) findViewById(R.id.recode_taocai_del1);
		BTN_CAIDEL2	  	= (Button) findViewById(R.id.recode_taocai_del2);
		
		LS_TAOCAI 	= (ListView) findViewById(R.id.recode_taocai_list);
		LS_BIGCLASS	= (ListView) findViewById(R.id.recode_bigclass_list);
		LS_TAOCAI_CAI_1	= (ListView) findViewById(R.id.recode_cai_list_1);
		LS_TAOCAI_CAI_2	= (ListView) findViewById(R.id.recode_cai_list_2);
		LS_ALLCAI 	= (ListView) findViewById(R.id.recode_allcai_list);
		
		CAI_NAME	=	(EditText) findViewById(R.id.recode_taocai_name);
		CAI_YOUHUI	=	(EditText) findViewById(R.id.recode_taocai_youhuijia);
		CAI_PRICE	=	(EditText) findViewById(R.id.recode_taocai_price);
		CAI_CODE	=	(EditText) findViewById(R.id.recode_taocai_code);
		CAI_PINYIN	=	(EditText) findViewById(R.id.recode_taocai_pinyin);
		CAI_OTHER	=	(EditText) findViewById(R.id.recode_taocai_other);
		ALL_PRICE_1	=	(TextView) findViewById(R.id.recode_cai_list_price);
		ALL_PRICE_2	=	(TextView) findViewById(R.id.recode_cai_list_price_2);
		CAI_SHUXING = (Spinner) findViewById(R.id.recode_taocai_spinner_shuxing1);
		CAI_FENLEI  = (Spinner) findViewById(R.id.recode_taocai_spinner_fenlei);
		
	
		LAYOUT_PART2 =	(LinearLayout) findViewById(R.id.part_2);
		LAYOUT_PART3 =	(LinearLayout) findViewById(R.id.part_3);
	}
}