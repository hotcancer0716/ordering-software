package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class User_Setup extends Activity {
    /** Called when the activity is first created. */
	Button		USER_SETUP_EXIT;
	ListView	USER_LS;
	TextView	USER_SETUP_STATE;
	EditText	USER_SETUP_NAME;
	Spinner		USER_SETUP_STYLE;
	EditText	USER_SETUP_CODE;
	EditText	USER_SETUP_PASSWORD;
	
	Button		USER_SETUP_SAVE;
	Button		USER_SETUP_CANCEL;
	Button		USER_SETUP_UPDATA;
	Button		USER_SETUP_NEW;
	Button		USER_SETUP_DEL;

	LinearLayout	PART_2;
	View		BIG_ITem;
	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

    private ArrayAdapter adapter_spinnerl;  

    boolean	save_or_updata;
    boolean	new_or_cancel;
    boolean	NAME_PASSWORD;	//现在焦点在桌号还是在容纳人数上
    int	list_position=0; 
    int	list_all=0;
    String  ID;
    String	Table_Num,Person_Num;
    final String[] chiwei = {"销售员","收款员","经理"};
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_user_setup);
        
       find_id();   
       key_things();
       intit_view();
       USER_SETUP_NEW.setVisibility(View.VISIBLE);
       init_spinner();
       //初始化数据库 显示在listview
 	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
 		
 	   //刷新列表
 	   refech_list(db);
	}

	private void refech_list(SQLiteDatabase db) {
 	   //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
	   Cursor  cursor = db.rawQuery("select * from lfcy_user_inf", null);
	   fill_hashmap(cursor);
	   inflateList(cursor);			
	}

	private void intit_view() {
		// TODO Auto-generated method stub	       
		USER_SETUP_STATE.setText("操作员信息设置");
		USER_SETUP_NAME.setEnabled(false);
		USER_SETUP_STYLE.setEnabled(false);
		USER_SETUP_CODE.setEnabled(false);
		USER_SETUP_PASSWORD.setEnabled(false);
		   		
		USER_SETUP_SAVE.setVisibility(View.GONE);
		USER_SETUP_CANCEL.setVisibility(View.GONE);
		USER_SETUP_UPDATA.setVisibility(View.GONE);
		USER_SETUP_DEL.setVisibility(View.GONE);
		USER_SETUP_NEW.setVisibility(View.GONE);
	}

	private void init_spinner() {
		// TODO Auto-generated method stub
		/**********************************************************************************/		        
		adapter_spinnerl= ArrayAdapter.createFromResource(this, R.array.user_manage, android.R.layout.simple_spinner_item); 
		adapter_spinnerl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		USER_SETUP_STYLE.setAdapter(adapter_spinnerl);    
	}

	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            map.put("xuhao", ""+i);

            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cUserName", cursor.getString(cursor.getColumnIndex("cUserName")));
            map.put("cUserCode", cursor.getString(cursor.getColumnIndex("cUserCode")));
            map.put("iUserPassword",  cursor.getString(cursor.getColumnIndex("iUserPassword")));
            map.put("cUserManager", cursor.getString(cursor.getColumnIndex("cUserManager")));
            map.put("iUserLimit", cursor.getString(cursor.getColumnIndex("iUserLimit")));
		
			list.add(map);
		}
		list_all = i;
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_table_list 
				, new String[]{	"xuhao" 		  , "cUserName" ,"cUserCode" 	, "iUserPassword" , "cUserManager"	}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3,R.id.table_list_4,R.id.table_list_5 }
        );
		
		//显示数据
        USER_LS.setAdapter(adapter);
	}
	
	private void key_things() {
		// TODO Auto-generated method stub
		USER_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		db.close();
		   		finish();				
		   	}
	    });	
		
		USER_SETUP_CANCEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		USER_SETUP_NAME.setText("");
		   		USER_SETUP_CODE.setText("");
		   		USER_SETUP_PASSWORD.setText("");
	   			intit_view();
	   			USER_SETUP_NEW.setVisibility(View.VISIBLE);

				AnimationSet animationSet = new AnimationSet(true); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
				translateAnimation.setDuration(200); 

				animationSet.addAnimation(translateAnimation); 
				
		   		PART_2.startAnimation(animationSet);
		   	}
	    });	
		
		USER_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
				/***************动画效果******************************/
				AnimationSet animationSet = new AnimationSet(true); 
			
				ScaleAnimation myAnimation_Scale =new ScaleAnimation(1.0f, 0.1f, 1.0f, 0.1f, 
			             Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				myAnimation_Scale.setDuration(200); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -0.6f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.45f); 
				translateAnimation.setDuration(200); 

				animationSet.addAnimation(myAnimation_Scale); 
				animationSet.addAnimation(translateAnimation); 				
				/***************以上是动画效果******************************/
				
		   		//先判断操作员是否为空
		   		if (USER_SETUP_NAME.getText().toString() == null || USER_SETUP_NAME.getText().toString().length() <= 0 ||
		   			USER_SETUP_CODE.getText().toString() == null || USER_SETUP_CODE.getText().toString().length() <= 0 ||
		   			USER_SETUP_PASSWORD.getText().toString() == null || USER_SETUP_PASSWORD.getText().toString().length() <= 0	) 
		   		{
					//桌号为空
		   			Toast.makeText(User_Setup.this, "请完整填写上述操作员相关资料", Toast.LENGTH_SHORT).show();
				} 
		   		else 
		   		{
		   			sql_control sqlc = new sql_control();					
					boolean if_only_one = sqlc.sql_item_only_one(db, "lfcy_user_inf", "cUserCode", USER_SETUP_CODE.getText().toString());
						
			   		//根据条件 判断是添加还是修改
			   		if (save_or_updata) {
			   			if (if_only_one) {
							//添加数据库
				   			sqlc.insert_table_user_info(db, 
				   					USER_SETUP_NAME.getText().toString() , 
				   					USER_SETUP_CODE.getText().toString(), 
				   					USER_SETUP_PASSWORD.getText().toString(), 
				   					chiwei[USER_SETUP_STYLE.getSelectedItemPosition()],
				   					""+(USER_SETUP_STYLE.getSelectedItemPosition()+1)
				   					);

				   	 	    refech_list(db);

				   			USER_LS.setSelection(adapter.getCount());  //显示ListView的最后一条		    

				   			intit_view();
				   			USER_SETUP_NEW.setVisibility(View.VISIBLE);
							PART_2.startAnimation(animationSet);							
						} else {
							Toast.makeText(User_Setup.this, "操作员编码不能重复", Toast.LENGTH_LONG).show();
						}

						
					} else {
						//更新数据库
						sqlc.updata_table_user_info(db, ID, 
			   					USER_SETUP_NAME.getText().toString() , 
			   					USER_SETUP_CODE.getText().toString(), 
			   					USER_SETUP_PASSWORD.getText().toString(),
			   					chiwei[USER_SETUP_STYLE.getSelectedItemPosition()],
			   					""+(USER_SETUP_STYLE.getSelectedItemPosition()+1)
								);
						
				 	    refech_list(db);

				 	    //显示ListView的最后一条
			   			USER_LS.setSelection(adapter.getCount());   			    
			   			
			   			intit_view();
			   			USER_SETUP_NEW.setVisibility(View.VISIBLE);
						PART_2.startAnimation(animationSet);
					}						
				}
		   		

		   	}
	    });	
		USER_SETUP_UPDATA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		intit_view();
			   
		   		USER_SETUP_SAVE.setVisibility(View.VISIBLE);
		   		USER_SETUP_CANCEL.setVisibility(View.VISIBLE);
		   		
		   		USER_SETUP_STYLE.setEnabled(true);
		   		USER_SETUP_NAME.setEnabled(true);
		   		USER_SETUP_CODE.setEnabled(true);
		   		USER_SETUP_PASSWORD.setEnabled(true);

	   			NAME_PASSWORD = true;
	   			save_or_updata = false;
		   	}
	    });			
		USER_SETUP_NEW.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		intit_view();
				AnimationSet animationSet = new AnimationSet(true); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
				translateAnimation.setDuration(200); 
				animationSet.addAnimation(translateAnimation); 
			   		   		
				USER_SETUP_SAVE.setVisibility(View.VISIBLE);
				USER_SETUP_CANCEL.setVisibility(View.VISIBLE);
			    
				USER_SETUP_NAME.setText("");
				USER_SETUP_CODE.setText("");		   		
				USER_SETUP_PASSWORD.setText("");		   		
				USER_SETUP_STYLE.setEnabled(true);
				USER_SETUP_NAME.setEnabled(true);
				USER_SETUP_CODE.setEnabled(true);
				USER_SETUP_PASSWORD.setEnabled(true);

		   		PART_2.startAnimation(animationSet);
		   		save_or_updata = true;
		   	}
	    });	
		USER_SETUP_DEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
				/***************动画效果******************************/
				AnimationSet animationSet = new AnimationSet(true); 
				
				ScaleAnimation myAnimation_Scale =new ScaleAnimation(1.0f, 0.1f, 1.0f, 0.1f, 
			             Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				myAnimation_Scale.setDuration(200); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -0.6f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.45f); 
				translateAnimation.setDuration(200); 

				animationSet.addAnimation(myAnimation_Scale); 
				animationSet.addAnimation(translateAnimation); 				
				/***************以上是动画效果******************************/
				
		   		//根据操作员编码删除
		   		sql_control sqlc = new sql_control();
		   		sqlc.remove_table_user_info(db,USER_SETUP_CODE.getText().toString());
		   
		  	   refech_list(db);

	   			USER_LS.setSelection(adapter.getCount());  //显示ListView的最后一条		    

	   			intit_view();
		   		USER_SETUP_NAME.setText("");
		   		USER_SETUP_CODE.setText("");
		   		USER_SETUP_PASSWORD.setText("");
		   		
	   			USER_SETUP_NEW.setVisibility(View.VISIBLE);
				PART_2.startAnimation(animationSet);
		   	}
	    });					
		USER_LS.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
            	list_position = position;
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
                String	iTableNumber =	String.valueOf(map.get("cUserName"));
                String	iTableClass  =	String.valueOf(map.get("cUserCode"));
                String	iTableClass2 =	String.valueOf(map.get("iUserPassword"));
                String	iTablePerson =	String.valueOf(map.get("iUserLimit"));
                ID =	String.valueOf(map.get("_id")); 
                Integer intObj = new Integer(iTablePerson);
                int i = intObj.intValue();	

		   		intit_view();

		   		USER_SETUP_NAME.setText(iTableNumber);
		   		USER_SETUP_STYLE.setSelection(i-1); 
		   		USER_SETUP_CODE.setText(iTableClass);
		   		USER_SETUP_PASSWORD.setText(iTableClass2);

		   		USER_SETUP_STYLE.setEnabled(false);
		   		USER_SETUP_NAME.setEnabled(false);
		   		USER_SETUP_CODE.setEnabled(false);
		   		USER_SETUP_PASSWORD.setEnabled(false);

		   		USER_SETUP_NEW.setVisibility(View.VISIBLE);
		   		USER_SETUP_UPDATA.setVisibility(View.VISIBLE);
		   		USER_SETUP_DEL.setVisibility(View.VISIBLE);
		   		
                try {
    				BIG_ITem.setBackgroundColor(Color.argb(236, 236, 236, 251)); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				BIG_ITem = v;
				BIG_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 

             }  
         }); 
		
	}

	private void find_id() {
		// TODO Auto-generated method stub
		USER_SETUP_EXIT		=	(Button) findViewById(R.id.pos_setup_exit);
		USER_SETUP_STATE	=	(TextView) findViewById(R.id.pos_setup_state);
		USER_LS				=	(ListView) findViewById(R.id.pos_setup_list);
		USER_SETUP_NAME		=	(EditText) findViewById(R.id.pos_setup_name);
		USER_SETUP_CODE		=	(EditText) findViewById(R.id.pos_setup_code);
		USER_SETUP_PASSWORD	=	(EditText) findViewById(R.id.pos_setup_password);
		USER_SETUP_STYLE	=	(Spinner)  findViewById(R.id.pos_setup_spinner1);
		USER_SETUP_SAVE		=	(Button) findViewById(R.id.pos_setup_save);
		USER_SETUP_CANCEL	=	(Button) findViewById(R.id.pos_setup_cancel);
		USER_SETUP_UPDATA	=	(Button) findViewById(R.id.pos_setup_updata);
		USER_SETUP_NEW		=	(Button) findViewById(R.id.pos_setup_new);
		USER_SETUP_DEL		=	(Button) findViewById(R.id.pos_setup_del);
		
		PART_2				=	(LinearLayout) findViewById(R.id.part_2);
	}
	
	
}