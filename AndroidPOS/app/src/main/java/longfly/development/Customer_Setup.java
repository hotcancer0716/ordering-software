package longfly.development;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import casio.serial.SerialPort;
import longfly.development.Print_Setup.insert_Thread;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class Customer_Setup extends Activity {
	
	Button	CUSTOMER_SETUP_EXIT;
	
	EditText	CUSTOMER_EDIT_1;
	Button	CUSTOMER_SETUP_SENT_1;
	Button	CUSTOMER_SETUP_SENT_2;

  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;

// 	final String  TTY_DEV = "/dev/ttymxc1";
 	final String  TTY_DEV = "/dev/ttymxc4";

	private   SerialPort   mSerialPort = null;		//串口设备描述
	protected OutputStream mOutputStream;		//串口输出描述

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_customer_setup);
        
        find_id();
        key_things();       
   }



	private void key_things() {
		CUSTOMER_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });

		CUSTOMER_SETUP_SENT_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		SerialPort mSerialPort = null;		//串口设备描述
		   		  
		        try {
					mSerialPort = new SerialPort(new File(TTY_DEV), 115200, 0);					
					mOutputStream = mSerialPort.getOutputStream();						
					Sent_Customer_Clear();
				} catch (Exception e) {
					// TODO: handle exception
				}	
		   	}

	    });	
		CUSTOMER_SETUP_SENT_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		SerialPort mSerialPort = null;		//串口设备描述
		   		  
		        try {
					mSerialPort = new SerialPort(new File(TTY_DEV), 115200, 0);					
					mOutputStream = mSerialPort.getOutputStream();	
					String contenr = CUSTOMER_EDIT_1.getText().toString();
					Sent_Customer_Clear(contenr);	   				
				} catch (Exception e) {
					// TODO: handle exception
				}	
		   	}
	    });		
	}


	private void Sent_Customer_Clear(String word) {
		// TODO Auto-generated method stub
   		byte[] cancel_to_normal = {0x1b, 0x51, 0x41 };//清除客显
   		print_String(cancel_to_normal , word);			   		
	}
	
	private void Sent_Customer_Clear() {
		// TODO Auto-generated method stub
   		byte[] cancel_to_normal = {0x1b, 0x40 };//清除客显
   		print_String(cancel_to_normal);			   		
	}

	//发送客显清除
	protected void print_String(byte[] prt_code_buffer) {
		try {
			byte[] buffer = prt_code_buffer;//发送客显清除;
			mOutputStream.write(buffer);
		} catch (IOException e) {
		}		
	}
	
	//发送客显显示
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//发送头;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
			byte[] buffere = {0x0d};//发送尾;
			mOutputStream.write(buffere);			
		} catch (IOException e) {
		}		
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		CUSTOMER_SETUP_EXIT	=	(Button) findViewById(R.id.pos_setup_exit);
		CUSTOMER_EDIT_1		=	(EditText) findViewById(R.id.customer_setup_edittext);
		CUSTOMER_SETUP_SENT_2	=	(Button) findViewById(R.id.customer_setup_sent_2);
		CUSTOMER_SETUP_SENT_1	=	(Button) findViewById(R.id.customer_setup_sent_1);
	}
  
}