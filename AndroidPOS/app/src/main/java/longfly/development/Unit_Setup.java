package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class Unit_Setup extends Activity {
	TextView	TX1,TX2,TX3,TX4,TX5,TX6,TX7,TX8;
	
	Button		TASTE_EXIT;	
	ListView	TASTE_LIST;
	EditText	TASTE_EDIT_NAME;
	EditText	TASTE_EDIT_CODE;
	Button		TASTE_BTN_NEW;
	Button		TASTE_BTN_EDIT;
	Button		TASTE_BTN_DEL;
	Button		TASTE_BTN_SAVE;
	Button		TASTE_BTN_CANCEL;
	View		ITem;
	
	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

    int	list_position=0; 
    boolean new_add;
    String ID;
    final	String Activity_Table = "lfcy_base_unit";
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_taste_setup);
        
        find_id();
        init_View();
        key_things();
		ViewSetVis(3);
		EditToWrite(false);

        //初始化数据库 显示在listview
  	    db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
 	   
    	init_taset_list();
   }

	private void init_View() {
		// TODO Auto-generated method stub
		TX1.setText("单位设置");
		TX2.setText("单位列表");
		TX3.setText("行");
		TX4.setText("单位名称");
		TX5.setText("单位编码");
		TX6.setText("单位管理");
		TX7.setText("单位名称");
		TX8.setText("单位编码");
		TASTE_EDIT_NAME.setHint("请输入单位名称");
		TASTE_EDIT_CODE.setHint("请输入单位编码");		
	}

	/*******************大类列表开始*****************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;            
			map=new HashMap<String,String>();
            map.put("num", ""+i);            
            map.put("_id", 	 cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
            map.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));
            map.put("cMemo", cursor.getString(cursor.getColumnIndex("cMemo")));
		
			list.add(map);
		}
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_class_cai_list 
				, new String[]{	"num" 		  , "cName" ,"cCode" 		}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3 }
        );
		
		//显示数据
        TASTE_LIST.setAdapter(adapter);
	}
	
	private void init_taset_list() {
  		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   Cursor  cursor = db.rawQuery("select * from "+ Activity_Table, null);
 	   fill_hashmap(cursor);
 	   inflateList(cursor);			
	}
	/*******************大类列表结束*****************************************/



	private void key_things() {
		TASTE_EXIT.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	

		TASTE_LIST.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
            	list_position = position;
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
				String	cName  =	String.valueOf(map.get("cName"));
                String	cCode  =	String.valueOf(map.get("cCode"));
                ID =	String.valueOf(map.get("_id")); 
                try {
                    ITem.setBackgroundColor(Color.argb(255, 235, 235, 235));              
				} catch (Exception e) {
					// TODO: handle exception
				}
                ITem = v;
                ITem.setBackgroundColor(Color.argb(255, 22, 175, 251));              
        		
        		TASTE_EDIT_NAME.setText(cName);
        		TASTE_EDIT_CODE.setText(cCode);
        		
        		ViewSetVis(1);
            }  
        }); 
		
		TASTE_BTN_NEW.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				//编辑框清空
				Edit_Clear();
				//编辑框可写
				EditToWrite(true);
				//取消和保存 显示
				ViewSetVis(2);
				new_add = true;				
			}
		});
		TASTE_BTN_EDIT.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//编辑框可写
				EditToWrite(true);
				//取消和保存 显示				
				ViewSetVis(2);
				new_add= false;
			}
		});	
		TASTE_BTN_DEL.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditToWrite(false);
				//数据库根据分类编码删除一条
		   		//根据操作员编码删除
		   		sql_control sqlc = new sql_control();
		   		sqlc.remove_table_data(db, Activity_Table, "cCode",
		   				TASTE_EDIT_CODE.getText().toString());
		   
		   		init_taset_list();
				//刷新数据库
				ViewSetVis(1);
				Edit_Clear();
			}
		});		
		TASTE_BTN_SAVE.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
	   			sql_control sqlc = new sql_control();					
				
				//判断编辑框是否为空	
				if ( TASTE_EDIT_NAME.getText().toString() == null || TASTE_EDIT_NAME.getText().toString().length() <= 0 ||
					 TASTE_EDIT_CODE.getText().toString() == null || TASTE_EDIT_CODE.getText().toString().length() <= 0) 
				{
					Toast.makeText(Unit_Setup.this, "单位名称和单位编码不能为空 ", Toast.LENGTH_SHORT).show();				
				} 
				else 
				{
					//判断是新增的还是修改的
					if (new_add) {	
						//判断分类编码是否有重复	
						boolean if_only_one = sqlc.sql_item_only_one(db, Activity_Table, "cCode", TASTE_EDIT_CODE.getText().toString());
			   			if (if_only_one) {
							//添加数据库
				   			sqlc.insert_table_info(db, Activity_Table ,
				   					TASTE_EDIT_NAME.getText().toString() , 
				   					TASTE_EDIT_CODE.getText().toString() ,
				   					null
				   					);
				   			init_taset_list();
				   	 	    TASTE_LIST.setSelection(adapter.getCount());  //显示ListView的最后一条		    							
							EditToWrite(false);
							Edit_Clear();
			   			} else {
							Toast.makeText(Unit_Setup.this, "单位编码不能重复", Toast.LENGTH_LONG).show();
						}
					} else {
						//修改    编辑   更新数据库
						sqlc.updata_table_info(db,Activity_Table ,
								TASTE_EDIT_CODE.getText().toString(), 
								TASTE_EDIT_NAME.getText().toString(), 
								TASTE_EDIT_CODE.getText().toString(),
								null
								);
						
						init_taset_list();
						EditToWrite(false);
						Edit_Clear();
					}	
				}
				//新增，修改，删除 显示
				ViewSetVis(1);
			}
		});

		TASTE_BTN_CANCEL.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditToWrite(false);
				//新增，修改，删除 显示
				ViewSetVis(3);
			}
		});
	}
	
	

	protected void Edit_Clear() {
		// TODO Auto-generated method stub
		TASTE_EDIT_NAME.setText("");
		TASTE_EDIT_CODE.setText("");
	}
	protected void EditToWrite(boolean bool) {
		// TODO Auto-generated method stub
		TASTE_EDIT_NAME.setEnabled(bool);
		TASTE_EDIT_CODE.setEnabled(bool);
	}
	protected void ViewSetVis(int value) {
		TASTE_BTN_NEW.setVisibility(View.GONE);		
		TASTE_BTN_EDIT.setVisibility(View.GONE);	
		TASTE_BTN_DEL.setVisibility(View.GONE);	
		TASTE_BTN_SAVE.setVisibility(View.GONE);
		TASTE_BTN_CANCEL.setVisibility(View.GONE);
		
		switch (value) {
		case 1:
			TASTE_BTN_NEW.setVisibility(View.VISIBLE);		
			TASTE_BTN_EDIT.setVisibility(View.VISIBLE);	
			TASTE_BTN_DEL.setVisibility(View.VISIBLE);				
			break;
		case 2:
			TASTE_BTN_SAVE.setVisibility(View.VISIBLE);
			TASTE_BTN_CANCEL.setVisibility(View.VISIBLE);			
			break;			
		case 3:
			TASTE_BTN_NEW.setVisibility(View.VISIBLE);
			break;	
		default:
			break;
		}	
	}

	private void find_id() {
		// TODO Auto-generated method stub
		TASTE_EXIT		= (Button) findViewById(R.id.taste_exit);
		TASTE_BTN_NEW 	= (Button) findViewById(R.id.taste_btn_add);		
		TASTE_BTN_EDIT	= (Button) findViewById(R.id.taste_btn_edit);		
		TASTE_BTN_DEL 	= (Button) findViewById(R.id.taste_btn_del);	
		TASTE_BTN_SAVE 	= (Button) findViewById(R.id.taste_btn_save);		
		TASTE_BTN_CANCEL= (Button) findViewById(R.id.taste_btn_cancel);		
		TASTE_LIST  	= (ListView) findViewById(R.id.taste_list);
		
		TASTE_EDIT_NAME = (EditText) findViewById(R.id.taste_name);
		TASTE_EDIT_CODE = (EditText) findViewById(R.id.taste_code);	
		
		TX1 = (TextView) findViewById(R.id.share_text_1);
		TX2 = (TextView) findViewById(R.id.share_text_2);
		TX3 = (TextView) findViewById(R.id.share_text_3);
		TX4 = (TextView) findViewById(R.id.share_text_4);
		TX5 = (TextView) findViewById(R.id.share_text_5);
		TX6 = (TextView) findViewById(R.id.share_text_6);
		TX7 = (TextView) findViewById(R.id.share_text_7);
		TX8 = (TextView) findViewById(R.id.share_text_8);
	}
   
}