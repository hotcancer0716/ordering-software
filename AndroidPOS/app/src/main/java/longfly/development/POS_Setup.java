package longfly.development;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class POS_Setup extends Activity {
	
	Button	POS_SETUP_SAVE;
	Button	POS_SETUP_EXIT;
	
	EditText	POS_SETUP_SHOP_NUM;
	EditText	POS_SETUP_MACHINE_NUM;
	EditText	POS_SETUP_SHOP_NAME;
	EditText	POS_SETUP_SERVERIP;
	EditText	POS_SETUP_SERVERNAME;
	EditText	POS_SETUP_SERVERPASSWORD;

	
  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;
	String	SPF_Pos_ShopNum		=	"SPF_Pos_ShopNum";
	String	SPF_Pos_Machine		=	"SPF_Pos_Machine";
	String	SPF_Pos_ShopName	=	"SPF_Pos_ShopName";
	String	SPF_Pos_ServerIp	=	"SPF_Pos_ServerIp";
	String	SPF_Pos_ServerName	=	"SPF_Pos_ServerName";
	String	SPF_Pos_ServerPassword	=	"SPF_Pos_ServerPassword";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_pos_setup);
        
        find_id();
        key_things();
        
 	    settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
        get_SharePerference();    
   }


	private void get_SharePerference() {
		// TODO Auto-generated method stub
		POS_SETUP_SHOP_NUM.setText(  settings.getString(SPF_Pos_ShopNum, null) );
		POS_SETUP_MACHINE_NUM.setText(  settings.getString(SPF_Pos_Machine, null) );
		POS_SETUP_SHOP_NAME.setText(  settings.getString(SPF_Pos_ShopName, null) );
		POS_SETUP_SERVERIP.setText(  settings.getString(SPF_Pos_ServerIp, null) );
		POS_SETUP_SERVERNAME.setText(  settings.getString(SPF_Pos_ServerName, null) );
		POS_SETUP_SERVERPASSWORD.setText(  settings.getString(SPF_Pos_ServerPassword, null) );
	}


	private void key_things() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(POS_Setup.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		POS_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
	}


	

	private void find_id() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE	=	(Button) findViewById(R.id.pos_setup_save);
		POS_SETUP_EXIT	=	(Button) findViewById(R.id.pos_setup_exit);
		
		POS_SETUP_SHOP_NUM			=	(EditText) findViewById(R.id.pos_setup_shopnum);
		POS_SETUP_MACHINE_NUM		=	(EditText) findViewById(R.id.pos_setup_machinenum);
		POS_SETUP_SHOP_NAME			=	(EditText) findViewById(R.id.pos_setup_shopname);
		POS_SETUP_SERVERIP			=	(EditText) findViewById(R.id.pos_setup_serverip);
		POS_SETUP_SERVERNAME		=	(EditText) findViewById(R.id.pos_setup_servername);
		POS_SETUP_SERVERPASSWORD	=	(EditText) findViewById(R.id.pos_setup_serverpassword);
	}
	
	
	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(POS_Setup.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据     
	        handler.sendEmptyMessage(5);
	        editor.putString(SPF_Pos_ShopNum, String.valueOf( POS_SETUP_SHOP_NUM.getText().toString()));  
	        editor.putString(SPF_Pos_Machine, String.valueOf( POS_SETUP_MACHINE_NUM.getText().toString()));  
	        editor.putString(SPF_Pos_ShopName, String.valueOf( POS_SETUP_SHOP_NAME.getText().toString()));   
	        handler.sendEmptyMessage(25);
	        editor.putString(SPF_Pos_ServerIp, String.valueOf( POS_SETUP_SERVERIP.getText().toString()));  
	        editor.putString(SPF_Pos_ServerName, String.valueOf( POS_SETUP_SERVERNAME.getText().toString()));  
	        editor.putString(SPF_Pos_ServerPassword, String.valueOf( POS_SETUP_SERVERPASSWORD.getText().toString()));   
	        handler.sendEmptyMessage(85);
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}