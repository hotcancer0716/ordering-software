package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class Recode_cai extends Activity {
	
	Button	BTN_SAVE;
	Button	BTN_EXIT;

	Button	BTN_NEW;
	Button	BTN_UPDATA;
	Button	BTN_DEL;
	
	ListView	LS_CAI;
	
	EditText	CAI_NAME;
	EditText	CAI_CODE;
	EditText	CAI_PINYIN;
	EditText	CAI_CHENGBEN;
	EditText	CAI_PRICE;
	EditText	CAI_YOUHUI;
	EditText	CAI_OTHER;
	
	Spinner		CAI_DANWEI;
	Spinner		CAI_SHUXING;
	Spinner		CAI_BIGCLASS;
	
	Button		BTN_KOUWEI;
	Button		BTN_MAKE;
	Button		BTN_STYLE;
	Button		BTN_SHICAI;

	LinearLayout	LAYOUT_KOUWEI;
	LinearLayout	LAYOUT_MAKE;
	LinearLayout	LAYOUT_STYLE;
	LinearLayout	LAYOUT_PIC;

	GridView	RECODE_TESTE,RECODE_MAKEWAY;
	/**************高级设置开始***************/
	ImageView	CAI_IMG;
	ImageView	CAI_STYLE;
	TextView	CAI_STYLE_NAME,CAI_AD_NAME,CAI_AD_CODE,CAI_AD_PRICE;
	TextView	PIC_PATH;	
	private static int RESULT_LOAD_IMAGE = 1;
	String 		picturePath;
	/**************高级设置结束***************/
	
	SQLiteDatabase db;
	String[] Big_Class_Name ,Big_Class_Code;
	String[] Unit_Name ,Unit_Code;
	View	BIG_ITem;
	private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;
    int	list_position=0; 
    boolean	New_Updata;
    private ArrayAdapter adapter_cai_shuxing,adapter_spinner_bigclass,adapter_spinner_danwei;  
    int		cai_shuxing;
    String ID;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_recode_cai);
        
       find_view();
       key_things();
  	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
       init_view();
  	   refch_list();
 	   viewToWrite(false);
    }

    
    
	private void init_view() {
		//获取菜品大类的编码
 	    Cursor  cursor = db.rawQuery("select * from lfcy_base_bigsort", null);
		Big_Class_Name = new String[cursor.getCount()];
		Big_Class_Code = new String[cursor.getCount()];
 	    int i=0;
		while(cursor.moveToNext()) {
			Big_Class_Name[i] = cursor.getString(cursor.getColumnIndex("cName"));		
			Big_Class_Code[i] = cursor.getString(cursor.getColumnIndex("cCode"));		
			i++;
		}
		adapter_spinner_bigclass = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Big_Class_Name); 
 	    //设置下拉列表的风格   
		adapter_spinner_bigclass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		CAI_BIGCLASS.setAdapter(adapter_spinner_bigclass); 
		//组成数组
		//传给Spinner
		
		// TODO Auto-generated method stub
        //将可选内容与ArrayAdapter连接起来   
		adapter_cai_shuxing= ArrayAdapter.createFromResource(this, R.array.cai_shuxing, android.R.layout.simple_spinner_item); 
 	    //设置下拉列表的风格   
		adapter_cai_shuxing.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		CAI_SHUXING.setAdapter(adapter_cai_shuxing);    
		
		//获取菜品单位的组
 	    Cursor  cursor_dw = db.rawQuery("select * from lfcy_base_unit", null);
		Unit_Name = new String[cursor_dw.getCount()];
		Unit_Code = new String[cursor_dw.getCount()];
 	    i=0;
		while(cursor_dw.moveToNext()) {
			Unit_Name[i] = cursor_dw.getString(cursor_dw.getColumnIndex("cName"));		
			Unit_Code[i] = cursor_dw.getString(cursor_dw.getColumnIndex("cCode"));		
			i++;
		}
		adapter_spinner_danwei = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Unit_Name); 
 	    //设置下拉列表的风格   
		adapter_spinner_danwei.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		CAI_DANWEI.setAdapter(adapter_spinner_danwei); 		
	}



	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            map.put("num", ""+i);            
            map.put("_id", 		 	cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cDishName", 	cursor.getString(cursor.getColumnIndex("cDishName")));
            map.put("cDishCode", 	cursor.getString(cursor.getColumnIndex("cDishCode")));
            map.put("cDishPinYin",  cursor.getString(cursor.getColumnIndex("cDishPinYin")));
            int num = Traversal(Big_Class_Code,cursor.getString(cursor.getColumnIndex("cDishClass1")));
            map.put("cDishClass1", Big_Class_Name[num]);

            int num1 = Traversal(Unit_Code,cursor.getString(cursor.getColumnIndex("cDishOther")));
            map.put("cDishOther", Unit_Name[num1]);
            
            map.put("fTaocaiPrice", cursor.getString(cursor.getColumnIndex("fTaocaiPrice")));           
            map.put("fDishPrice", 	cursor.getString(cursor.getColumnIndex("fDishPrice")));
            map.put("iDishStyle", 	cursor.getString(cursor.getColumnIndex("iDishStyle")));
            map.put("cDishName2", 	cursor.getString(cursor.getColumnIndex("cDishName2")));
		
			list.add(map);
		}
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.i800_cai_list 
				, new String[]{	"num" , "cDishName"  ,"cDishCode" , 
        				"fDishPrice"   ,"fTaocaiPrice", "cDishPinYin",
        				"cDishClass1"}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3,
        				R.id.table_list_4,R.id.table_list_5,R.id.table_list_6,
        				R.id.table_list_7 }
        );	
		//显示数据
		LS_CAI.setAdapter(adapter);
	}
	private int Traversal(String[] Table_Where_Code, String string) {
		// TODO Auto-generated method stub
		int i = 0;
		for (int j = 0; j < Table_Where_Code.length; j++) {
			if( Table_Where_Code[j].trim().equals(string) )
			{
				i = j;
	    		Log.i("info", "i  = "+ i);  
			}	
		}
		return i;			
	}	
	private void init_btn(int state){
		BTN_KOUWEI.setBackgroundResource(R.drawable.table_number_up);
		BTN_MAKE.setBackgroundResource(R.drawable.table_number_up);
		BTN_STYLE.setBackgroundResource(R.drawable.table_number_up);
		BTN_SHICAI.setBackgroundResource(R.drawable.table_number_up);
		
		LAYOUT_KOUWEI.setVisibility(View.GONE);
		LAYOUT_MAKE.setVisibility(View.GONE);
		LAYOUT_STYLE.setVisibility(View.GONE);
		LAYOUT_PIC.setVisibility(View.GONE);
		
		AnimationSet animationSet = new AnimationSet(true); 
		TranslateAnimation translateAnimation = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation.setDuration(200); 
		animationSet.addAnimation(translateAnimation); 
		
		switch (state) {
		case 1:
			BTN_KOUWEI.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYOUT_KOUWEI.setVisibility(View.VISIBLE);			
			LAYOUT_KOUWEI.startAnimation(animationSet);			
			break;
		case 2:
			BTN_MAKE.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYOUT_MAKE.setVisibility(View.VISIBLE);			
			LAYOUT_MAKE.startAnimation(animationSet);			
			break;
		case 3:
			BTN_STYLE.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYOUT_STYLE.setVisibility(View.VISIBLE);			
			LAYOUT_STYLE.startAnimation(animationSet);			
			break;
		case 4:
			BTN_SHICAI.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYOUT_PIC.setVisibility(View.VISIBLE);			
			LAYOUT_PIC.startAnimation(animationSet);			
			break;		
		default:
			break;
		}
		
	}

	private void key_things() {
		BTN_KOUWEI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_btn(1);
		   	}
	    });	
		BTN_MAKE.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_btn(2);
		   	}
	    });	
		BTN_STYLE.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_btn(3);
		   	}
	    });		
		
		BTN_SHICAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_btn(4);
		   		BTN_SHICAI.setFocusableInTouchMode(true);
		   		CAI_NAME.setFocusableInTouchMode(false);
		   		CAI_CODE.setFocusableInTouchMode(false);
		   		CAI_PINYIN.setFocusableInTouchMode(false);
		   		CAI_CHENGBEN.setFocusableInTouchMode(false);
		   		CAI_PRICE.setFocusableInTouchMode(false);
		   		CAI_YOUHUI.setFocusableInTouchMode(false);
		   		CAI_OTHER.setFocusableInTouchMode(false);
		   	}
	    });		
		
		BTN_SAVE.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		String	name	=	CAI_NAME.getText().toString();
		   		String	code	=	CAI_CODE.getText().toString();
		   		String	pinyinma=	CAI_PINYIN.getText().toString();
		   		String	price	=	CAI_PRICE.getText().toString();
		   		String	chengben=	CAI_CHENGBEN.getText().toString();
		   		String	youhui	=	CAI_YOUHUI.getText().toString();
		   		int     style   = 	CAI_SHUXING.getSelectedItemPosition();
		   		int     bigclass= 	CAI_BIGCLASS.getSelectedItemPosition();
		   		int     danwei  = 	CAI_DANWEI.getSelectedItemPosition();

		   		//判断必填项有没有空白的
		   		if (name == null || name.length() <= 0 ||
		   			code == null || code.length() <= 0 ||
		   			price== null ||	price.length()<= 0) 
		   		{
					Toast.makeText(Recode_cai.this, "菜品名称，菜品编码不能为空", Toast.LENGTH_SHORT).show();			
				} 
		   		else 
		   		{
			   		//判断是新增还是修改
		   			if (New_Updata) {
						//判断分类编码是否有重复	
		   				sql_control	sqlc = new sql_control();
		   				String Activity_Table = "lfcy_base_dishinf";
						boolean if_only_one = sqlc.sql_item_only_one(db, Activity_Table, "cDishCode", code);
						
						if (if_only_one) {
							//新增		   		
							insert_DishData(db, name,picturePath,code,pinyinma,pinyinma,
									Big_Class_Code[bigclass],"2",Unit_Code[danwei],""+style,"0",price);
						
							refch_list();
					 	    //显示ListView的最后一条
					 	    LS_CAI.setSelection(adapter.getCount());  							
					  	    viewToWrite(false);		
						} else {
							Toast.makeText(Recode_cai.this, "菜品编码不能重复", Toast.LENGTH_SHORT).show();
						}

		   			} else {
				   		//修改数据库
			   			ContentValues cv = new ContentValues();
			   		   	//存储修改的数据
			   		   	cv.put("cDishName" 		, name);
			   		   	cv.put("cDishName2" 	, picturePath);
			   		   	cv.put("cDishCode" 		, code);
			   		   	cv.put("cDishPinYin"	, pinyinma);
			   		   	cv.put("cDishClass1"	, Big_Class_Code[bigclass]);
			   		   	cv.put("cDishOther"		, Unit_Code[danwei]);
			   		   	cv.put("iDishStyle"		, ""+(style+1));
			   		   	cv.put("fDishPrice"		, price);
			   		   	String[] contion = {ID}; 
					   	db.update("lfcy_base_dishinf",cv,"_id = ? ",contion);
						refch_list();
						int i;
						try {
		                   Integer intObj = new Integer(ID);
		                   i = intObj.intValue();				
						} catch (Exception e) {
		            	   i= 1;
						}
						try {
					 	    LS_CAI.setSelection(i-5);  														
							   BIG_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
						} catch (Exception e) {
							// TODO: handle exception
						}
						
				  	    viewToWrite(false);		
					}

				}

		   	}
	    });		
		
		BTN_EXIT.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
		
		CAI_SHUXING.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	cai_shuxing = postion;
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });

		
		BTN_NEW.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				viewToWrite(true);
				viewToNew();
				New_Updata = true;
			}
		});
		
		BTN_UPDATA.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				viewToWrite(true);	
				New_Updata = false;
			}
		});
		
		BTN_DEL.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
			   		//获取listview行数
	 				HashMap<String,String> map = (HashMap<String,String>)LS_CAI.getItemAtPosition(list_position);
	                String id =	String.valueOf(map.get("_id"));		   		//根据行数得到该行的id值
			   		//删除对应id的那道菜
	                sql_control sqlc = new sql_control();
	                sqlc.remove_table_data(db, "lfcy_base_dishinf", "_id", id);
	                refch_list();
	          	    viewToWrite(false);
		   		} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Recode_cai.this, "删除失败！", Toast.LENGTH_LONG).show();
				}
		   	}
	    });	


		LS_CAI.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
            	list_position = position;
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
				String	cDishName 	=	String.valueOf(map.get("cDishName"));
				String	cDishCode  	=	String.valueOf(map.get("cDishCode"));
				String	cDishPinYin =	String.valueOf(map.get("cDishPinYin"));
				String	fDishPrice 	=	String.valueOf(map.get("fDishPrice"));
				String	iDishStyle  =	String.valueOf(map.get("iDishStyle"));
				String	cDishClass1 =	String.valueOf(map.get("cDishClass1"));
				String	cDishOther =	String.valueOf(map.get("cDishOther"));
                String	cDishName2  =   String.valueOf(map.get("cDishName2"));
				ID = String.valueOf(map.get("_id"));
				int i;
				try {
                   Integer intObj = new Integer(iDishStyle);
                   i = intObj.intValue();				
				} catch (Exception e) {
            	   i= 5;
				}
	
               int j = Traversal(Big_Class_Name,cDishClass1);
               int k = Traversal(Unit_Name,cDishOther);

               CAI_NAME.setText(cDishName);
               CAI_CODE.setText(cDishCode);
               CAI_PINYIN.setText(cDishPinYin);
               CAI_PRICE.setText(fDishPrice);
               CAI_SHUXING.setSelection(i-1); 
               CAI_BIGCLASS.setSelection(j); 
               CAI_DANWEI.setSelection(k); 
               /*********高级设置开始*************/
               picturePath = cDishName2;
   			   CAI_IMG.setImageBitmap(BitmapFactory.decodeFile(picturePath));
   			   PIC_PATH.setText("路径是 ："+ picturePath);
           	   switch (i) {
					case 1:
			               CAI_STYLE_NAME.setText("推荐");
			               CAI_STYLE.setImageResource(R.drawable.griditem_cai_style_1);						
						break;
					case 2:
			               CAI_STYLE_NAME.setText("特价");
			               CAI_STYLE.setImageResource(R.drawable.griditem_cai_style_2);												
						break;
					case 3:
			               CAI_STYLE_NAME.setText("新品");
			               CAI_STYLE.setImageResource(R.drawable.griditem_cai_style_3);						
						break;						
					case 4:
			               CAI_STYLE_NAME.setText("招牌");
			               CAI_STYLE.setImageResource(R.drawable.griditem_cai_style_4);						
						break;						
					default:
						break;
					}
           	   CAI_AD_NAME.setText(cDishName);
           	   CAI_AD_CODE.setText(cDishCode);
           	   CAI_AD_PRICE.setText(fDishPrice);
           	   
           	   //菜品口味GridView
           	   init_Taste_Grid(cDishCode);
           	   init_MakeWay_Grid(cDishCode);
           	   
               /*********高级设置结束始*************/
               try {
   					BIG_ITem.setBackgroundColor(Color.argb(236, 236, 236, 251)); 
			   } catch (Exception e) {
					// TODO: handle exception
			   }
			   BIG_ITem = v;
			   BIG_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
		 	   viewToWrite(false);
		 	   
		   		BTN_SHICAI.setFocusableInTouchMode(true);
		   		CAI_NAME.setFocusableInTouchMode(false);
		   		CAI_CODE.setFocusableInTouchMode(false);
		   		CAI_PINYIN.setFocusableInTouchMode(false);
		   		CAI_CHENGBEN.setFocusableInTouchMode(false);
		   		CAI_PRICE.setFocusableInTouchMode(false);
		   		CAI_YOUHUI.setFocusableInTouchMode(false);
		   		CAI_OTHER.setFocusableInTouchMode(false);
            }
        }); 
		
		CAI_IMG.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				CAI_IMG.setFocusableInTouchMode(true);
		   		CAI_NAME.setFocusableInTouchMode(false);
		   		CAI_CODE.setFocusableInTouchMode(false);
		   		CAI_PINYIN.setFocusableInTouchMode(false);
		   		CAI_CHENGBEN.setFocusableInTouchMode(false);
		   		CAI_PRICE.setFocusableInTouchMode(false);
		   		CAI_YOUHUI.setFocusableInTouchMode(false);
		   		CAI_OTHER.setFocusableInTouchMode(false);
		   		
				Intent i = new Intent(
						Intent.ACTION_PICK,
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				
				startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
 
		CAI_IMG.setFocusableInTouchMode(true);
   		CAI_NAME.setFocusableInTouchMode(false);
   		CAI_CODE.setFocusableInTouchMode(false);
   		CAI_PINYIN.setFocusableInTouchMode(false);
   		CAI_CHENGBEN.setFocusableInTouchMode(false);
   		CAI_PRICE.setFocusableInTouchMode(false);
   		CAI_YOUHUI.setFocusableInTouchMode(false);
   		CAI_OTHER.setFocusableInTouchMode(false);
   		
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			picturePath = cursor.getString(columnIndex);
			cursor.close();
			
			CAI_IMG.setImageBitmap(BitmapFactory.decodeFile(picturePath));
			PIC_PATH.setText("路径是 ："+ picturePath);
		}
    }
    
	protected void viewToWrite(boolean bool) {
		// TODO Auto-generated method stub
		CAI_NAME.setEnabled(bool);
		CAI_CODE.setEnabled(bool);
		CAI_PINYIN.setEnabled(bool);
		CAI_CHENGBEN.setEnabled(bool);
		CAI_PRICE.setEnabled(bool);
		CAI_YOUHUI.setEnabled(bool);
		CAI_OTHER.setEnabled(bool);
		CAI_SHUXING.setEnabled(bool);
		CAI_BIGCLASS.setEnabled(bool);
	}

	protected void viewToNew() {
		// TODO Auto-generated method stub
		CAI_NAME.setText("");
		CAI_CODE.setText("");
		CAI_PINYIN.setText("");
		CAI_CHENGBEN.setText("");
		CAI_PRICE.setText("");
		CAI_YOUHUI.setText("");
		CAI_OTHER.setText("");
	}


	private void refch_list() {
		// TODO Auto-generated method stub
  		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
  	    Cursor  cursor = db.rawQuery("select * from lfcy_base_dishinf", null);
  	    fill_hashmap(cursor);
  	    inflateList(cursor);					
	}
	
	//这里有问题！要增加菜品单位名称和编码的插入！
	private void insert_DishData(SQLiteDatabase db, String cDishName, String cDishName2,
			String cDishCode, String cDishPinYin, String cDishHelpCode, String cDishClass1,
			String cDishClass2, String cDishOther, String iDishStyle,String iDishDian, String fDishPrice) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_dishinf values(null,?,?,?,?,?,?,?,?,?,?,?,null,null)", 
					new String[]{
				cDishName ,cDishName2 ,	cDishCode ,cDishPinYin ,
				cDishHelpCode , cDishClass1 , cDishClass2  ,cDishOther , 
				iDishStyle ,iDishDian ,	fDishPrice });
	}
	
	
	private void find_view() {
		// TODO Auto-generated method stub
		BTN_SAVE	= (Button) findViewById(R.id.recode_cai_save);
		BTN_EXIT	= (Button) findViewById(R.id.recode_cai_exit);
		BTN_NEW	  	= (Button) findViewById(R.id.recode_cai_new);
		BTN_UPDATA	= (Button) findViewById(R.id.recode_cai_update);
		BTN_DEL	  	= (Button) findViewById(R.id.recode_cai_del);
		
		LS_CAI 		= (ListView) findViewById(R.id.recode_cai_list);
		
		CAI_NAME	= (EditText) findViewById(R.id.recode_cai_name);
		CAI_CODE	= (EditText) findViewById(R.id.recode_cai_code);
		CAI_PINYIN	= (EditText) findViewById(R.id.recode_cai_pinyin);
		CAI_CHENGBEN= (EditText) findViewById(R.id.recode_cai_chengben);
		CAI_PRICE	= (EditText) findViewById(R.id.recode_cai_price);
		CAI_YOUHUI	= (EditText) findViewById(R.id.recode_cai_youhuijia);
		CAI_SHUXING = (Spinner)  findViewById(R.id.recode_cai_spinner_shuxing1);
		CAI_DANWEI  = (Spinner)  findViewById(R.id.recode_cai_spinner_danwei);		
		CAI_BIGCLASS= (Spinner)  findViewById(R.id.recode_cai_spinner_bigclass);
		CAI_OTHER	= (EditText) findViewById(R.id.recode_cai_other);
		
		
		BTN_KOUWEI	= (Button) findViewById(R.id.recode_cai_btn_kouwei);
		BTN_MAKE	= (Button) findViewById(R.id.recode_cai_btn_make);
		BTN_STYLE	= (Button) findViewById(R.id.recode_cai_btn_style);
		BTN_SHICAI	= (Button) findViewById(R.id.recode_cai_btn_shicai);
		BTN_SHICAI.setFocusable(true);
		
		LAYOUT_KOUWEI =	(LinearLayout) findViewById(R.id.recode_cai_layout_kouwei);
		LAYOUT_MAKE	  =	(LinearLayout) findViewById(R.id.recode_cai_layout_make);
		LAYOUT_STYLE  =	(LinearLayout) findViewById(R.id.recode_cai_layout_style);
		LAYOUT_PIC 	  =	(LinearLayout) findViewById(R.id.recode_cai_layout_shicai);
		RECODE_TESTE  = (GridView) findViewById(R.id.recodecai_gridview_taste);
		RECODE_MAKEWAY= (GridView) findViewById(R.id.recodecai_gridview_makeway);
		PIC_PATH 	= (TextView) findViewById(R.id.recode_cai_pic_path);
		CAI_IMG 	= (ImageView) findViewById(R.id.recode_cai_img);	
		CAI_IMG.setFocusable(true);
		CAI_STYLE 	= (ImageView) findViewById(R.id.grid_cai_style_pic);	
		CAI_STYLE_NAME = (TextView) findViewById(R.id.grid_cai_style);
		CAI_AD_NAME  = (TextView) findViewById(R.id.grid_cai_name);
		CAI_AD_CODE  = (TextView) findViewById(R.id.grid_cai_code);
		CAI_AD_PRICE = (TextView) findViewById(R.id.grid_cai_price);
	}
	
	/******************************/
	private void init_Taste_Grid(String cDishCode) {
		// TODO Auto-generated method stub
		try {
    		Cursor  cursor = db.rawQuery( "select * from lfcy_base_taste" , null);
			inflateList_taste(cursor);	
			fill_hashmap_taste(cursor);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void init_MakeWay_Grid(String cDishCode) {
		// TODO Auto-generated method stub
		try {
    		Cursor  cursor = db.rawQuery( "select * from lfcy_base_dishtaste" , null);
			inflateList_taste(cursor);	
			adapter_taste = new SimpleAdapter(
		      		this, 
		      		list_taste, 
					R.layout.imx_redioitem 
					, new String[]{"cName"}
					, new int[]{R.id.item_1 }
				);
				
				//显示数据
				RECODE_MAKEWAY.setAdapter(adapter_taste);	
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private ArrayList<HashMap<String,String>> list_taste=null;
    private HashMap<String,String>map_taste=null;
	SimpleAdapter adapter_taste;
	
	private ArrayList inflateList_taste(Cursor cursor) {
		// TODO Auto-generated method stub
		list_taste=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) 
		{
		   i++;
           map_taste=new HashMap<String,String>();
          
           map_taste.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
           map_taste.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
           map_taste.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));
			
           list_taste.add(map_taste);
		}
	   System.out.println("i =" + i);
		
		return list_taste;					
	}

	private void fill_hashmap_taste(Cursor cursor) {
		// TODO Auto-generated method stub
		 //将数据与adapter集合起来
		adapter_taste = new SimpleAdapter(
      		this, 
      		list_taste, 
			R.layout.imx_redioitem 
			, new String[]{"cName"}
			, new int[]{R.id.item_1 }
		);
		
		//显示数据
		RECODE_TESTE.setAdapter(adapter_taste);				
	}  
	/*****************************/
}