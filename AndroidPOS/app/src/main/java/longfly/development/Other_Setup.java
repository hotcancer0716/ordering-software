package longfly.development;

import java.io.DataOutputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Other_Setup extends Activity {
	
	Button	POS_SETUP_SAVE;
	Button	POS_SETUP_EXIT;
	Button	OTHER_RETOUCH;
	EditText	OPEN_ORDER;

	String	SPF_OTHER_ED_OPEN	=	"SPF_OTHER_ED_OPEN";

  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_other_setup);
        
        find_id();
        key_things();
        
 	    settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
        get_SharePerference();    
   }


	private void get_SharePerference() {
		// TODO Auto-generated method stub
		OPEN_ORDER.setText(  settings.getString(SPF_OTHER_ED_OPEN, null) );
	}


	private void key_things() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(Other_Setup.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		POS_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	

		OTHER_RETOUCH.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
/*		   		
		   		execShell("rm /data/system/calibration");
//		   		execShell("stop");
		   		execShell("echo 0,0 > /sys/class/graphics/fb0/pan");
		   		execShell("ts_calibrator");
//		   		execShell("start"); 
 */
		   	}
	    });	
	}


	public void execShell(String cmd){
    	try{  
            //权限设置
            Process p = Runtime.getRuntime().exec("su");  
            //获取输出流
            OutputStream outputStream = p.getOutputStream();
            DataOutputStream dataOutputStream=new DataOutputStream(outputStream);
            //将命令写入
            dataOutputStream.writeBytes(cmd);
            //提交命令
            dataOutputStream.flush();
            //关闭流操作
            dataOutputStream.close();
            outputStream.close();
       }  
       catch(Throwable t)  
       {  
             t.printStackTrace();  
       } 
    }	

	private void find_id() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE	=	(Button) findViewById(R.id.other_setup_save);
		POS_SETUP_EXIT	=	(Button) findViewById(R.id.other_setup_exit);
		OTHER_RETOUCH	=	(Button) findViewById(R.id.other_retouch);
		OPEN_ORDER		=	(EditText) findViewById(R.id.other_dev_setup_edit);
	}
	
	
	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Other_Setup.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据     
	        handler.sendEmptyMessage(5);
	        editor.putString(SPF_OTHER_ED_OPEN, String.valueOf( OPEN_ORDER.getText().toString()));  
	        handler.sendEmptyMessage(85);
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}