package longfly.development;

import java.util.Calendar;

import longfly.development.About.insert_Thread;

import org.w3c.dom.Text;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class POS_Database extends Activity {
    /** Called when the activity is first created. */
	Button		INIFO_EXIT,INFO_BTN_2,INFO_BTN_3;
	TextView	INFO_TITLE,INFO_PART_1,INFO_PART_2,INFO_PART_3;
	TextView	INFO_PART_1_TEXT,INFO_PART_2_TEXT,INFO_PART_3_TEXT;
	TextView	INFO_PART_2_1_TEXT,INFO_PART_3_1_TEXT;
	boolean		Save_or_Repace;
  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	String	SPF_Pos_SaveDatabase_Time	=	"SPF_Pos_SaveDatabase_Time";
	String	SPF_Pos_RepDatabase_Time	=	"SPF_Pos_RepDatabase_Time";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_info);
        
        find_id();
        key_things();
        init_view();
 	    settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
       //刷新上次备份/还原时间
        init_last_check_time();
    }

	private void init_last_check_time() {
		// TODO Auto-generated method stub
        get_SharePerference();   
	}
	private void get_SharePerference() {
		if (settings.getString(SPF_Pos_SaveDatabase_Time, null).equals(null)) {
			INFO_PART_2_TEXT.setText(  "未做过日结" );			
		} else {
			INFO_PART_2_1_TEXT.setText(settings.getString(SPF_Pos_SaveDatabase_Time, null));
			INFO_PART_2_1_TEXT.setVisibility(View.VISIBLE);
		}

		if (settings.getString(SPF_Pos_SaveDatabase_Time, null).equals(null)) {
			INFO_PART_3_TEXT.setText(  "未做过月结" );			
		} else {
			INFO_PART_3_1_TEXT.setText(settings.getString(SPF_Pos_RepDatabase_Time, null));
			INFO_PART_3_1_TEXT.setVisibility(View.VISIBLE);
		}		

	}
	private void init_view() {
		// TODO Auto-generated method stub
		INFO_TITLE.setText("数据库管理");
		INFO_PART_1.setVisibility(View.GONE);
		INFO_PART_2.setText("数据库备份");
		INFO_PART_3.setText("数据库还原");
		INFO_PART_1_TEXT.setVisibility(View.GONE);
		INFO_PART_2_TEXT.setText("上次备份时间");
		INFO_PART_3_TEXT.setText("上次还原时间");
		INFO_BTN_2.setText("数据库备份");	
		INFO_BTN_3.setText("数据库还原");
	}

	private void key_things() {
		// TODO Auto-generated method stub
		INIFO_EXIT.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				finish();
			}
		});
		INFO_BTN_2.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				Save_or_Repace = true;
				Start_DataBase_Setup();
			}
		});
		INFO_BTN_3.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				Save_or_Repace = false;
				Start_DataBase_Setup();
			}
		});
	}

	private void find_id() {
		// TODO Auto-generated method stub
		INIFO_EXIT = (Button) findViewById(R.id.info_exit);
		INFO_BTN_2 = (Button) findViewById(R.id.info_btn_2);
		INFO_BTN_3 = (Button) findViewById(R.id.info_btn_3);		
		INFO_TITLE = (TextView) findViewById(R.id.info_title);
		INFO_PART_1 = (TextView) findViewById(R.id.info_part_1);
		INFO_PART_2 = (TextView) findViewById(R.id.info_part_2);
		INFO_PART_3 = (TextView) findViewById(R.id.info_part_3);
		INFO_PART_1_TEXT = (TextView) findViewById(R.id.info_part_1_text);
		INFO_PART_2_TEXT = (TextView) findViewById(R.id.info_part_2_text);
		INFO_PART_3_TEXT = (TextView) findViewById(R.id.info_part_3_text);
		INFO_PART_2_1_TEXT = (TextView) findViewById(R.id.info_part_2_1_text);
		INFO_PART_3_1_TEXT = (TextView) findViewById(R.id.info_part_3_1_text);
	}
	
    private String init_time() {
		// TODO Auto-generated method stub
    	int mYear,mMonth,mDay,mHour,mMinute,mSecond;

    	//获取桌台号和即时时间    
   		final Calendar c = 	Calendar.getInstance();
        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
        mMonth 	= c.get(Calendar.MONTH)+ 1;	//获取当前月份
        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
        mSecond = c.get(Calendar.SECOND);	//获取当前的秒钟数		   		

   		String mmHour,mmMinute,mmSecond;

   		if(mHour < 10)
   			mmHour = "0"+mHour;
   		else
   			mmHour = ""+mHour;

   		if(mMinute < 10)
   			mmMinute = "0"+mMinute;
   		else
   			mmMinute = ""+mMinute;

   		if(mSecond < 10)
   			mmSecond = "0"+mSecond;
   		else
   			mmSecond = ""+mSecond;

   		String	system_date	= mYear+"-"+mMonth+"-"+	mDay;
   		String	system_time	= mmHour+":"+mmMinute+":"+mmSecond;

 		return system_date +"  "+ system_time;
	}
    
    
	protected void Start_DataBase_Setup() {
		// TODO Auto-generated method stub
		progressDialog("正在操作数据库 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}	
	
	/***************     以下是一个带进度条的对话框    **************************************/
	ProgressDialog progressDialog;

	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(POS_Database.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){   			  
    			  progressDialog.cancel();
   		       	  init_last_check_time();

    			  if (Save_or_Repace) {
					Toast.makeText(POS_Database.this, "数据库备份完成", Toast.LENGTH_SHORT).show();
				  } else {
					Toast.makeText(POS_Database.this, "数据库还原完成", Toast.LENGTH_SHORT).show();	
				  }
    	      }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
			handler.sendEmptyMessage(1);	//进度条进度	
			//获取时间
        	String Check_Time = init_time();
	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();          	
    	
        	if (Save_or_Repace) {
				//日结
    			handler.sendEmptyMessage(10);	//进度条进度	
   			
    			//把时间写入上次日结时间
    	        editor.putString(SPF_Pos_SaveDatabase_Time, String.valueOf( Check_Time ));  
    			handler.sendEmptyMessage(90);	//进度条进度	           		
			} else {
				//月结
				handler.sendEmptyMessage(10);	//进度条进度	
				
    			//把时间写入上次月结时间				
    	        editor.putString(SPF_Pos_RepDatabase_Time, String.valueOf( Check_Time ));  
				handler.sendEmptyMessage(90);	//进度条进度	   
			}
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
			handler.sendEmptyMessage(100);	//进度条进度	  
        }	
    };
    
	/***************     以上是一个进度条    **************************************/
}