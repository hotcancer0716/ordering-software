package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class Table_Setup extends Activity {
    /** Called when the activity is first created. */
	Button		TABLE_SETUP_EXIT;
	ListView	TABLE_LS;
	TextView	TABLE_SETUP_STATE;
	TextView	TABLE_SETUP_NUM;
	Spinner		TABLE_SETUP_STYLE;
	Spinner		TABLE_SETUP_STYLE2;
	TextView	TABLE_SETUP_PERSON;

	Button		TABLE_SETUP_FRONT;
	Button		TABLE_SETUP_NEXT;
	
	Button		TABLE_SETUP_SAVE;
	Button		TABLE_SETUP_CANCEL;
	Button		TABLE_SETUP_UPDATA;
	Button		TABLE_SETUP_NEW;
	Button		TABLE_SETUP_DEL;

	Button		TABLE_SETUP_1;
	Button		TABLE_SETUP_2;
	Button		TABLE_SETUP_3;
	Button		TABLE_SETUP_4;
	Button		TABLE_SETUP_5;
	Button		TABLE_SETUP_6;
	Button		TABLE_SETUP_7;
	Button		TABLE_SETUP_8;
	Button		TABLE_SETUP_9;
	Button		TABLE_SETUP_0;
	
	Button		TABLE_SETUP_CLEAR;
	Button		TABLE_SETUP_NUMDEL;
	LinearLayout	PART_2;
	View		ITem;
	
	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

    private ArrayAdapter adapter_spinnerl;  
    private ArrayAdapter adapter_spinner2;  

    boolean	new_or_cancel;
    boolean	NAME_PASSWORD;	//现在焦点在桌号还是在容纳人数上
    boolean	NEW_UPDATE;	//是新增桌台还是修改
    int	list_position=0; 
    int	list_all=0;
    String	Table_Num,Person_Num;
    String[] Table_Type_Name ,Table_Type_Code,Table_Quyu_Name ,Table_Quyu_Code;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_table_setup);
        
       find_id();   
       key_things();
 	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
       intit_view();
	   TABLE_SETUP_NEW.setVisibility(View.VISIBLE);
       init_spinner();
       //初始化数据库 显示在listview

	   refch_list();
	   
	}

	private void refch_list() {
 		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
	   Cursor  cursor = db.rawQuery("select * from lfcy_base_tableinf", null);
	   fill_hashmap(cursor);
	   inflateList(cursor);			
	}

	private void intit_view() {
		// TODO Auto-generated method stub	       
  	   TABLE_SETUP_STATE.setText("桌台信息设置");
  	   TABLE_SETUP_NUM.setEnabled(false);
  	   TABLE_SETUP_STYLE.setEnabled(false);
       TABLE_SETUP_STYLE2.setEnabled(false);
  	   TABLE_SETUP_PERSON.setEnabled(false);
	   TABLE_SETUP_NUM.setBackgroundResource(R.drawable.nothing);
	   TABLE_SETUP_PERSON.setBackgroundResource(R.drawable.nothing);			   		
      
//     TABLE_SETUP_FRONT.setVisibility(View.GONE);
//	   TABLE_SETUP_NEXT.setVisibility(View.GONE);
	   TABLE_SETUP_SAVE.setVisibility(View.GONE);
	   TABLE_SETUP_CANCEL.setVisibility(View.GONE);
	   TABLE_SETUP_UPDATA.setVisibility(View.GONE);
	   TABLE_SETUP_DEL.setVisibility(View.GONE);
	   TABLE_SETUP_NEW.setVisibility(View.GONE);
	   
	   TABLE_SETUP_0.setVisibility(View.GONE);
	   TABLE_SETUP_1.setVisibility(View.GONE);
	   TABLE_SETUP_2.setVisibility(View.GONE);
	   TABLE_SETUP_3.setVisibility(View.GONE);
	   TABLE_SETUP_4.setVisibility(View.GONE);
	   TABLE_SETUP_5.setVisibility(View.GONE);
	   TABLE_SETUP_6.setVisibility(View.GONE);
	   TABLE_SETUP_7.setVisibility(View.GONE);
	   TABLE_SETUP_8.setVisibility(View.GONE);
	   TABLE_SETUP_9.setVisibility(View.GONE);
	   TABLE_SETUP_CLEAR.setVisibility(View.GONE);
	   TABLE_SETUP_NUMDEL.setVisibility(View.GONE);
	}

	private void init_spinner() {
		// TODO Auto-generated method stub
 	    Cursor  cursor = db.rawQuery("select * from lfcy_base_tabletype", null);
		Table_Type_Name = new String[cursor.getCount()];
		Table_Type_Code = new String[cursor.getCount()];
 	    int i=0;
		while(cursor.moveToNext()) {
			Table_Type_Name[i] = cursor.getString(cursor.getColumnIndex("cName"));		
			Table_Type_Code[i] = cursor.getString(cursor.getColumnIndex("cCode"));		
			i++;
		}
		adapter_spinnerl = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Table_Type_Name); 
		adapter_spinnerl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		TABLE_SETUP_STYLE.setAdapter(adapter_spinnerl);    

		/**********************************************************************************/		        
 	    cursor = db.rawQuery("select * from lfcy_base_tablearea", null);
		Table_Quyu_Code = new String[cursor.getCount()];
		Table_Quyu_Name = new String[cursor.getCount()];
 	    int j=0;
		while(cursor.moveToNext()) {
			Table_Quyu_Code[j] = cursor.getString(cursor.getColumnIndex("cCode"));		
			Table_Quyu_Name[j] = cursor.getString(cursor.getColumnIndex("cName"));		
			j++;
		}
		adapter_spinner2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Table_Quyu_Name); 
		adapter_spinner2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		TABLE_SETUP_STYLE2.setAdapter(adapter_spinner2); 		
	}

	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            map.put("_num", ""+i);
            
            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("iTableNumber", cursor.getString(cursor.getColumnIndex("iTableNumber")));
            //根据字段iTableClass的值！判断是桌台类型里的第几个
            //再根据第几个，把数组中的第几个分派给map
            int num = Traversal(Table_Type_Code,cursor.getString(cursor.getColumnIndex("iTableClass")));
            map.put("iTableType", Table_Type_Name[num]);
            map.put("iTableClass", cursor.getString(cursor.getColumnIndex("iTableClass")));

            //根据字段iTableClass2的值！判断是桌台类型里的第几个
            //再根据第几个，把数组中的第几个分派给map
            num = Traversal(Table_Quyu_Code,cursor.getString(cursor.getColumnIndex("iTableClass2")));
            map.put("iTableQuyu", Table_Quyu_Name[num]);
            map.put("iTableClass2", cursor.getString(cursor.getColumnIndex("iTableClass2")));
            map.put("iTablePerson", cursor.getString(cursor.getColumnIndex("iTablePerson")));
		
			list.add(map);
		}
		list_all = i;
		return list;
	}
	
	private int Traversal(String[] Table_Where_Code, String string) {
		// TODO Auto-generated method stub
		int i = 0;
		for (int j = 0; j < Table_Where_Code.length; j++) {
			if( Table_Where_Code[j].trim().equals(string) )
			{
				i = j;
	    		Log.i("info", "i  = "+ i);  
			}	
		}
		return i;			
	}

	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_table_setup_list 
				, new String[]{	"_num" 			, "iTableNumber" ,"iTableType" 	, "iTableQuyu" , "iTablePerson"	}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3,R.id.table_list_4,R.id.table_list_5 }
        );
		
		//显示数据
		TABLE_LS.setAdapter(adapter);
	}
	
	
	private void key_things() {
		// TODO Auto-generated method stub
		TABLE_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		db.close();
		   		finish();				
		   	}
	    });	
		TABLE_SETUP_CANCEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		TABLE_SETUP_NUM.setText("");
		   		TABLE_SETUP_PERSON.setText("");
	   			intit_view();
	   		    TABLE_SETUP_NEW.setVisibility(View.VISIBLE);

				AnimationSet animationSet = new AnimationSet(true); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
				translateAnimation.setDuration(200); 

				animationSet.addAnimation(translateAnimation); 
				
		   		PART_2.startAnimation(animationSet);
		   	}
	    });	
		TABLE_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
				/***************动画效果******************************/
				AnimationSet animationSet = new AnimationSet(true); 
				
				ScaleAnimation myAnimation_Scale =new ScaleAnimation(1.0f, 0.1f, 1.0f, 0.1f, 
			             Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				myAnimation_Scale.setDuration(200); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -0.6f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.45f); 
				translateAnimation.setDuration(200); 

				animationSet.addAnimation(myAnimation_Scale); 
				animationSet.addAnimation(translateAnimation); 				
		   		
				PART_2.startAnimation(animationSet);
				/***************以上是动画效果******************************/		   		
		   		//先判断桌号是否为空
		   		if (TABLE_SETUP_NUM.getText().toString() == null || TABLE_SETUP_NUM.getText().toString().length() <= 0) {
					//桌号为空
		   			Toast.makeText(Table_Setup.this, "请设置桌号", Toast.LENGTH_SHORT).show();
				} else {
			   		//根据条件 判断是添加还是修改
			   		if (NEW_UPDATE) {
						//添加数据库
			   			String	Table_number = TABLE_SETUP_NUM.getText().toString();	//桌台号
			   			int		Table_class  = TABLE_SETUP_STYLE.getSelectedItemPosition();	//包厢大厅卡座
			   			int     Table_class2 = TABLE_SETUP_STYLE2.getSelectedItemPosition();
			   			String	Table_person = TABLE_SETUP_PERSON.getText().toString();	//桌台可容纳人数
			   			
	    				insert_TableData(db,Table_number,
	    						Table_Type_Code[Table_class],Table_Quyu_Code[Table_class2],
	    						"1",null,Table_person,null);				
			   			//重新查询
	    				refch_list();
			   		    TABLE_LS.setSelection(adapter.getCount());  //显示ListView的最后一条		    
			   			intit_view();
			   		    TABLE_SETUP_NEW.setVisibility(View.VISIBLE);			   		    
					} else {
				   		//修改数据库
			   			String	Table_number = TABLE_SETUP_NUM.getText().toString();	//桌台号
			   			int		Table_class  = TABLE_SETUP_STYLE.getSelectedItemPosition();	//包厢大厅卡座
			   			int     Table_class2 = TABLE_SETUP_STYLE2.getSelectedItemPosition();
			   			String	Table_person = TABLE_SETUP_PERSON.getText().toString();	//桌台可容纳人数
			   			sql_control sqlc = new sql_control();
			   			sqlc.updata_table_setup_info(db, Table_number, 
			   					Table_Type_Code[Table_class],Table_Quyu_Code[Table_class2], 
			   					Table_person);
			   			//重新查询
	    				refch_list();
			   			intit_view();
			   		    TABLE_SETUP_NEW.setVisibility(View.VISIBLE);							
					}
				}
		   	}
	    });	
		
		TABLE_SETUP_FRONT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		list_position--;
		   		if(list_position < 1)
		   		{
		   			list_position = 0;
		   			Toast.makeText(Table_Setup.this, "已经是第一条了", Toast.LENGTH_SHORT).show();
		   		}	
		   		try {
	 				HashMap<String,String> map = (HashMap<String,String>)TABLE_LS.getItemAtPosition(list_position);
	                String	iTableNumber =	String.valueOf(map.get("iTableNumber"));
	                String	iTableClass  =	String.valueOf(map.get("iTableClass"));
	                String	iTableClass2 =	String.valueOf(map.get("iTableClass2"));
	                String	iTablePerson =	String.valueOf(map.get("iTablePerson"));

	                int i = Traversal(Table_Type_Code,iTableClass);
	                int j = Traversal(Table_Quyu_Code,iTableClass2);
	                
	                TABLE_SETUP_NUM.setText(iTableNumber);
	                TABLE_SETUP_STYLE.setSelection(i); 
	                TABLE_SETUP_STYLE2.setSelection(j); 
	                TABLE_SETUP_PERSON.setText(iTablePerson);					
				} catch (Exception e) {
					// TODO: handle exception
				}

		   	}
	    });		
		TABLE_SETUP_NEXT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		list_position++;
		   		if(list_position > list_all)
		   		{
		   			list_position = list_all;
		   			Toast.makeText(Table_Setup.this, "已经是最后一条了", Toast.LENGTH_SHORT).show();
		   		}
		   		try {
			   		HashMap<String,String> map = (HashMap<String,String>)TABLE_LS.getItemAtPosition(list_position);
	                String	iTableNumber =	String.valueOf(map.get("iTableNumber"));
	                String	iTableClass  =	String.valueOf(map.get("iTableClass"));
	                String	iTableClass2 =	String.valueOf(map.get("iTableClass2"));
	                String	iTablePerson =	String.valueOf(map.get("iTablePerson"));
	                
	                int i = Traversal(Table_Type_Code,iTableClass);
	                int j = Traversal(Table_Quyu_Code,iTableClass2);
	                
	                TABLE_SETUP_NUM.setText(iTableNumber);
	                TABLE_SETUP_STYLE.setSelection(i); 
	                TABLE_SETUP_STYLE2.setSelection(j); 
	                TABLE_SETUP_PERSON.setText(iTablePerson);					
				} catch (Exception e) {
					// TODO: handle exception
				}

		   	}
	    });			
		TABLE_SETUP_UPDATA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		intit_view();
		   		TABLE_SETUP_0.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_1.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_2.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_3.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_4.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_5.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_6.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_7.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_8.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_9.setVisibility(View.VISIBLE);
		 	    TABLE_SETUP_CLEAR.setVisibility(View.VISIBLE);
			    TABLE_SETUP_NUMDEL.setVisibility(View.VISIBLE);
			   
		   		TABLE_SETUP_SAVE.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_CANCEL.setVisibility(View.VISIBLE);
		   		
			    TABLE_SETUP_STYLE.setEnabled(true);
			    TABLE_SETUP_STYLE2.setEnabled(true);		   		
		  	    TABLE_SETUP_NUM.setEnabled(false);
		  	    TABLE_SETUP_PERSON.setEnabled(true);

	   			NAME_PASSWORD = true;
	   			TABLE_SETUP_NUM.setBackgroundResource(R.drawable.edit_style_button_on);
		   		TABLE_SETUP_PERSON.setBackgroundResource(R.drawable.edit_style_button_off);			   		
		   		NEW_UPDATE = false;
		   	}
	    });			
		
		TABLE_SETUP_NEW.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		intit_view();
				AnimationSet animationSet = new AnimationSet(true); 
				TranslateAnimation translateAnimation = new TranslateAnimation( 
						Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
						Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
				translateAnimation.setDuration(200); 
				animationSet.addAnimation(translateAnimation); 
				
		   		TABLE_SETUP_0.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_1.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_2.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_3.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_4.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_5.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_6.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_7.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_8.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_9.setVisibility(View.VISIBLE);
		 	    TABLE_SETUP_CLEAR.setVisibility(View.VISIBLE);
			    TABLE_SETUP_NUMDEL.setVisibility(View.VISIBLE);
			   		   		
		   		TABLE_SETUP_SAVE.setVisibility(View.VISIBLE);
		   		TABLE_SETUP_CANCEL.setVisibility(View.VISIBLE);
			    
			    TABLE_SETUP_NUM.setText("");
			    TABLE_SETUP_PERSON.setText("");		   		
			    TABLE_SETUP_STYLE.setEnabled(true);
			    TABLE_SETUP_STYLE2.setEnabled(true);		   	
		  	    TABLE_SETUP_NUM.setEnabled(true);
		  	    TABLE_SETUP_PERSON.setEnabled(true);

	   			NAME_PASSWORD = true;
	   			TABLE_SETUP_NUM.setBackgroundResource(R.drawable.edit_style_button_on);
		   		TABLE_SETUP_PERSON.setBackgroundResource(R.drawable.edit_style_button_off);			   		

		   		PART_2.startAnimation(animationSet);
		   		NEW_UPDATE = true;
		   	}
	    });	
		
		TABLE_SETUP_DEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		//获取listview行数
	 				HashMap<String,String> map = (HashMap<String,String>)TABLE_LS.getItemAtPosition(list_position);
	                String id =	String.valueOf(map.get("_id"));		   		//根据行数得到该行的id值
			   		//删除对应id的那道菜
			   		remove_cai(db , id);
			   		refch_list();
		   		} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Table_Setup.this, "删除失败！", Toast.LENGTH_LONG).show();
				}
		   	}
	    });		
		
		TABLE_LS.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
            	list_position = position;
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
                String	iTableNumber =	String.valueOf(map.get("iTableNumber"));
                String	iTableClass  =	String.valueOf(map.get("iTableClass"));
                String	iTableClass2 =	String.valueOf(map.get("iTableClass2"));
                String	iTablePerson =	String.valueOf(map.get("iTablePerson"));
                
//                Integer intObj = new Integer(iTableClass);
//                int i = intObj.intValue();	
                int i = Traversal(Table_Type_Code,iTableClass);
//                intObj = new Integer(iTableClass2);
//                int j = intObj.intValue();	
                int j = Traversal(Table_Quyu_Code,iTableClass2);

		   		intit_view();

                TABLE_SETUP_NUM.setText(iTableNumber);
                TABLE_SETUP_STYLE.setSelection(i); 
                TABLE_SETUP_STYLE2.setSelection(j); 
                TABLE_SETUP_PERSON.setText(iTablePerson);

                TABLE_SETUP_STYLE.setEnabled(false);
                TABLE_SETUP_STYLE2.setEnabled(false);
           	    TABLE_SETUP_NUM.setEnabled(false);
          	    TABLE_SETUP_PERSON.setEnabled(false);

        	    TABLE_SETUP_NEW.setVisibility(View.VISIBLE);
        	    TABLE_SETUP_UPDATA.setVisibility(View.VISIBLE);
        	    TABLE_SETUP_DEL.setVisibility(View.VISIBLE);
        	    
                try {
                    ITem.setBackgroundColor(Color.argb(255, 235, 235, 235));              
				} catch (Exception e) {
					// TODO: handle exception
				}
                ITem = v;
                ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
             }  
         }); 
		
		/**********键盘操作************/
		TABLE_SETUP_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		if ( NAME_PASSWORD)
		   		{
			   		Table_Num =  TABLE_SETUP_NUM.getText().toString();
			   		TABLE_SETUP_NUM.setText(Table_Num+"1");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"1");			   			
		   		}	
		   	}
	    });	
		TABLE_SETUP_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"2");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"2");			   			
		   		}	
		   	}
	    });		
		TABLE_SETUP_3.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"3");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"3");			   			
		   		}	
		   	}
	    });			
		TABLE_SETUP_4.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"4");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"4");			   			
		   		}	
		   	}
	    });			
		TABLE_SETUP_5.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"5");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"5");			   			
		   		}	
		   	}
	    });			
		TABLE_SETUP_6.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"6");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"6");			   			
		   		}		
		   	}
	    });			
		
		TABLE_SETUP_7.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"7");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"7");			   			
		   		}	
		   	}
	    });	
		TABLE_SETUP_8.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"8");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"8");			   			
		   		}	
		   	}
	    });		
		TABLE_SETUP_9.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"9");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"9");			   			
		   		}	
		   	}
	    });
		TABLE_SETUP_0.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText(Table_Num+"0");		   			
		   		}
		   		else
		   		{
		   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
		   			TABLE_SETUP_PERSON.setText(Person_Num+"0");			   			
		   		}	
		   	}
	    });	
		
		TABLE_SETUP_NUMDEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String str = null;

		   		try {
			   		if ( NAME_PASSWORD)
			   		{
			   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
			   			str=Table_Num.substring(0,Table_Num.length()-1);
			   			TABLE_SETUP_NUM.setText(str);	   			
			   		}
			   		else
			   		{
			   			Person_Num =  TABLE_SETUP_PERSON.getText().toString();
			   			str=Person_Num.substring(0,Person_Num.length()-1);
			   			TABLE_SETUP_PERSON.setText(str);		   			
			   		}						
				} catch (Exception e) {
					// TODO: handle exception
				}
		   	}
	    });
		
		TABLE_SETUP_CLEAR.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		if ( NAME_PASSWORD)
		   		{
		   			Table_Num =  TABLE_SETUP_NUM.getText().toString();
		   			TABLE_SETUP_NUM.setText("");		   			
		   		}
		   		else
		   		{
			   		Person_Num =  TABLE_SETUP_PERSON.getText().toString();
			   		TABLE_SETUP_PERSON.setText("");			   			
		   		}
		   	}
	    });
		TABLE_SETUP_NUM.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
	   			NAME_PASSWORD = true;
	   			TABLE_SETUP_NUM.setBackgroundResource(R.drawable.edit_style_button_on);
		   		TABLE_SETUP_PERSON.setBackgroundResource(R.drawable.edit_style_button_off);			   		
		   	}
	    });	
		
		TABLE_SETUP_PERSON.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
	   			NAME_PASSWORD = false;
	   			TABLE_SETUP_NUM.setBackgroundResource(R.drawable.edit_style_button_off);
		   		TABLE_SETUP_PERSON.setBackgroundResource(R.drawable.edit_style_button_on);			   		
		   	}
	    });			
		/**********键盘操作************/
	}

	/*
	 * iTableClass是桌台类型（卡座，包厢）
	 * iTableClass是桌台区域（一楼，二楼）
	 * 
	 */
	public void insert_TableData(SQLiteDatabase db2, String iTableNumber,
			String iTableClass, String iTableClass2, String iTableState, String iTabletime,
			String iTablePerson, String  iTable_Person) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_tableinf values(null,?,?,?,?,?,?,?)", 
					new String[]{
				iTableNumber ,iTableClass ,	iTableClass2 ,iTableState ,
				iTabletime , iTablePerson , iTable_Person });		
	};	
	
	
	protected void remove_cai(SQLiteDatabase db, String	id) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_base_tableinf", " _id =?", new String[]{id});
   			} catch (Exception e) {
					
   		}
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		TABLE_SETUP_EXIT	=	(Button) findViewById(R.id.table_setup_exit);
		TABLE_SETUP_STATE	=	(TextView) findViewById(R.id.table_setup_state);
		TABLE_LS			=	(ListView) findViewById(R.id.table_setup_list);
		TABLE_SETUP_NUM		=	(TextView) findViewById(R.id.table_setup_tablenum);
		TABLE_SETUP_STYLE	=	(Spinner) findViewById(R.id.table_setup_spinner1);
		TABLE_SETUP_STYLE2	=	(Spinner) findViewById(R.id.table_setup_spinner2);
		TABLE_SETUP_PERSON	=	(TextView) findViewById(R.id.table_setup_person);
		TABLE_SETUP_FRONT	=	(Button) findViewById(R.id.table_setup_front);
		TABLE_SETUP_NEXT	=	(Button) findViewById(R.id.table_setup_next);
		TABLE_SETUP_SAVE	=	(Button) findViewById(R.id.table_setup_save);
		TABLE_SETUP_CANCEL	=	(Button) findViewById(R.id.table_setup_cancel);
		TABLE_SETUP_UPDATA	=	(Button) findViewById(R.id.table_setup_updata);
		TABLE_SETUP_NEW		=	(Button) findViewById(R.id.table_setup_new);
		TABLE_SETUP_DEL		=	(Button) findViewById(R.id.table_setup_del);
		
		TABLE_SETUP_0		=	(Button) findViewById(R.id.table_setup_0);
		TABLE_SETUP_1		=	(Button) findViewById(R.id.table_setup_1);
		TABLE_SETUP_2		=	(Button) findViewById(R.id.table_setup_2);
		TABLE_SETUP_3		=	(Button) findViewById(R.id.table_setup_3);
		TABLE_SETUP_4		=	(Button) findViewById(R.id.table_setup_4);
		TABLE_SETUP_5		=	(Button) findViewById(R.id.table_setup_5);
		TABLE_SETUP_6		=	(Button) findViewById(R.id.table_setup_6);
		TABLE_SETUP_7		=	(Button) findViewById(R.id.table_setup_7);
		TABLE_SETUP_8		=	(Button) findViewById(R.id.table_setup_8);
		TABLE_SETUP_9		=	(Button) findViewById(R.id.table_setup_9);
		TABLE_SETUP_CLEAR	=	(Button) findViewById(R.id.table_setup_clear);
		TABLE_SETUP_NUMDEL	=	(Button) findViewById(R.id.table_setup_numdel);
		PART_2				=	(LinearLayout) findViewById(R.id.part_2);
	}
	
	
}