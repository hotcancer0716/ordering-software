package longfly.development;
/****
 * 	’base‘－基本资料
 *	‘sys’ －系统管理
 *	‘rep‘ －报表中心
 *	‘vip’－营业卡管理
 *	‘in’－采购管理
 *	‘store’－库存管理
 */
 
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class sql_control {
	
	public void create_table_sample(SQLiteDatabase db ,String sql_string)
	{
		try {
			//临时开台单号表,用于定了台！但是还没点菜的情况！仅在桌台显示开台状态
			db.execSQL( "create table lfcy_base_sample( " +
 					"_id integer primary key autoincrement," +	//自增号  流水号
 					"timestamp DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')) ," +	//时间
					"cSellNo	    int)");		//销售单号（利用开台时间）
			Log.i("info", "lfcy_base_sample success" );  
		} catch (Exception e) {
			// TODO: handle exception
			Log.i("info", "lfcy_base_sample failed" );  
		}		
		
	}
	

	public void insert_table_sample(SQLiteDatabase db ,String sql_string)
	{
   		try {
   			SimpleDateFormat   formatter   =   new   SimpleDateFormat   ("yyyy-MM-dd hh:mm:ss");     
   			Date   curDate   =   new   Date(System.currentTimeMillis());//获取当前时间     
   			String   str   =   formatter.format(curDate); 
		   	//更新数据
		   	db.execSQL("insert into lfcy_base_sample values(?,?,?)", 
					new String[]{
		   			null , str ,sql_string});
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}

	public void insert_table_sample(SQLiteDatabase db,String time ,String sql_string)
	{
   		try {
		   	//更新数据
		   	db.execSQL("insert into lfcy_base_sample values(?,?,?)", 
					new String[]{
		   			null , time ,sql_string});
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}
	
	public void remove_table_sample(SQLiteDatabase db, String	id) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_base_sample", " _id =?", new String[]{id});
   			} catch (Exception e) {
					
   		}
	}
	/***************上面只是练习！下面才是真正的******************************/
	/**********用于Adapter  开始*************/

	public String Traversal_Table_Type(String table_Place_Code) {
		// TODO Auto-generated method stub
		SQLiteDatabase db;
 	   	db = SQLiteDatabase.openOrCreateDatabase("/data/data/longfly.development/files/"+"/Android_POS.db3", null);
 	   	//获取数据 
		Cursor  cursor = db.rawQuery("select * from lfcy_base_tabletype", null);
		String[] Table_Type_Name = new String[cursor.getCount()];
		String[] Table_Type_Code = new String[cursor.getCount()];
 	    int i=0;
 	    //组成数组
		while(cursor.moveToNext()) {
			Table_Type_Name[i] = cursor.getString(cursor.getColumnIndex("cName"));		
			Table_Type_Code[i] = cursor.getString(cursor.getColumnIndex("cCode"));		
			i++;
		}
		//定位
		int ii = 0;
		for (int j = 0; j < Table_Type_Code.length; j++) {
			if( Table_Type_Code[j].trim().equals(table_Place_Code) )
			{
				ii = j;
			}	
		}
		//返回桌台类型名称
		db.close();
		return 	Table_Type_Name[ii];		
	}	
	/**********用于Adapter  结束*************/
	//创建主机的机器常规设置
	public void create_table_machine_setup(SQLiteDatabase db ,String sql_string)
	{
		try {
			//临时开台单号表,用于定了台！但是还没点菜的情况！仅在桌台显示开台状态
			db.execSQL( "create table lfcy_base_machine_setup( " +
 					"_id integer primary key autoincrement," +	//自增号  流水号
 					"cShopNum 		varchar(10)," +		//店号
 					"cMachineNum 	varchar(10)," +		//机号
 					"cShopName 		varchar(10)," +		//店名					
					"cSellNo	    Varchar(20))");		//销售单号（利用开台时间）
		} catch (Exception e) {
			// TODO: handle exception
		}				
	}	
	
	
	//插入一条新增数据给操作员表
	public void insert_table_user_info(SQLiteDatabase db ,String cUserName, String cUserCode ,
			String iUserPassword, String cUserManager, String iUserLimit)
	{
   		try {
   			db.execSQL("insert into lfcy_user_inf values(null,?,?,?,?,?)", 
   					new String[]{
   					cUserName , cUserCode ,iUserPassword , cUserManager , iUserLimit
   			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
		
	public void remove_table_user_info(SQLiteDatabase db, String	refer) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_user_inf", " cUserCode =?", new String[]{refer});
   			} catch (Exception e) {				
   		}
	}  			
   			
	
	public void updata_table_user_info(SQLiteDatabase db, String	refer,
			String	string0 ,String	string1,String	string2,String	string3, String	string4) 
	{
		// TODO Auto-generated method stub
   		try {
   			ContentValues cv = new ContentValues();
   		   	//存储修改的数据
   		   	cv.put("cUserName" 		, string0);
   		   	cv.put("cUserCode" 		, string1);
   		   	cv.put("iUserPassword"	, string2);
   		   	cv.put("cUserManager"	, string3);
   		   	cv.put("iUserLimit"		, string4);
   		   	String[] contion = {refer}; 
		   	db.update("lfcy_user_inf",cv,"_id = ? ",contion);
   		} catch (Exception e) {
					
   		}
	}  



	public void insert_sell_info(SQLiteDatabase db, String string,
			String string2, String string3, String string4, String string5, String string6, 
			String string7, String string8, String string9, String string10, String string11, String string12) {
		// TODO Auto-generated method stub
   		try {
   			db.execSQL("insert into lfcy_base_sellinf_liushui values(null,?,?,?,?,?,?,?,?,?,?,?,?)", 
   					new String[]{
   					 string ,string2 ,string3 ,string4 ,string5 ,
   					string6 ,string7 ,string8 ,string9 ,string10 ,
   					string11 ,string12
   			});
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}


	
	//插入一条新增数据给操作员表
	public void insert_table_bigclass_info(SQLiteDatabase db ,String cName, String cCode)
	{
   		try {
   			db.execSQL("insert into lfcy_base_bigsort values(null,?,?,?)", 
   					new String[]{
   					cName , cCode ,null
   			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}


	public void updata_table_bigclass_info(SQLiteDatabase db, String refer,
			String string, String string1) {
		// TODO Auto-generated method stub
   		try {
   			ContentValues cv = new ContentValues();
   		   	//存储修改的数据
   		   	cv.put("cName" 		, string);
   		   	cv.put("cCode" 		, string1);
   		   	String[] contion = {refer}; 
		   	db.update("lfcy_base_bigsort",cv,"_id = ? ",contion);
   		} catch (Exception e) {
					
   		}		
	}	
	


	public void insert_table_smallclass_info(SQLiteDatabase db,
			String parent_Class_Code, String cName, String cCode) {
   		try {
   			db.execSQL("insert into lfcy_base_smallsort values(null,?,?,?,?)", 
   					new String[]{
   					parent_Class_Code ,cName , cCode ,null
   			});
		} catch (Exception e) {
			// TODO: handle exception
		}	
	}
	
	public void updata_table_smallclass_info(SQLiteDatabase db, String refer,
			String parent_Class_Code, String cName, String cCode) {
		// TODO Auto-generated method stub
   		try {
   			ContentValues cv = new ContentValues();
   		   	//存储修改的数据
   		   	cv.put("cBigCode"	, parent_Class_Code);
   		   	cv.put("cName" 		, cName);
   		   	cv.put("cCode" 		, cCode);
   		   	String[] contion = {refer}; 
		   	db.update("lfcy_base_smallsort",cv,"_id = ? ",contion);
   		} catch (Exception e) {
					
   		}		
	}
	
	
	/*
	 *	修改桌台信息
	 *	refer是桌台号
	 * 	string1 是桌台类型
	 * 	string2是桌台区域
	 * 	string3是桌台人数
	 */
	public void updata_table_setup_info(SQLiteDatabase db, String	refer,
			String	string1 ,String	string2,String	string3) 
	{
		// TODO Auto-generated method stub
   		try {
	   		ContentValues cv = new ContentValues();
		   	//存储修改的数据
		   	cv.put("iTableClass" ,string1);
		   	cv.put("iTableClass2" ,string2);	   		
		   	cv.put("iTablePerson",string3);
		   	String[] contion = {refer}; 
		   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
   		} catch (Exception e) {
			
   		}	
	}  
	/*****************较常复用的************************************/
	public void insert_table_info(SQLiteDatabase db,String table,
			String cName, String cCode ,String cMome) {
   		try {
   			db.execSQL("insert into "+table+" values(null,?,?,?)", 
   					new String[]{
   					cName , cCode ,cMome
   			});
		} catch (Exception e) {
			// TODO: handle exception
		}	
	}
	
	public void updata_table_info(SQLiteDatabase db, String table,String refer,
			String cName, String cCode ,String cMome ) {
		// TODO Auto-generated method stub
   		try {
   			ContentValues cv = new ContentValues();
   		   	//存储修改的数据
   		   	cv.put("cName" 		, cName);
   		   	cv.put("cCode" 		, cCode);
   		   	cv.put("cMemo" 		, cMome);
   		   	String[] contion = {refer}; 
		   	db.update(table,cv,"cCode = ? ",contion);
   		} catch (Exception e) {
					
   		}		
	}
	/***************************************************************/	
/*
 * 删除一条数据
 * table  表名
 * item	     条件字段名
 * refer  字段值
 * */
	public void remove_table_data(SQLiteDatabase db, String	table , String	item ,String	refer) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete(table,  item +"=?", new String[]{refer});
   			} catch (Exception e) {
					
   		}
	}  	
	
	/*************************************************
	 * 
	 * 查询数据库,对比数据
	 * 避免出现重复的字段
	 * table 是表名
	 * item  是字段名
	 * refer 是必须唯一的字段内容
	 * ***********************************************/
	public boolean sql_item_only_one(SQLiteDatabase db,String table , String item ,String refer)
	{
		boolean	Equal = false;
		String name[] = {refer};
		try {
    		Cursor  cursor = db.rawQuery(
    				"select * from "+ table +" where "+ item +" = ?", 
    				name);
    		int num = cursor.getCount();

    		if(num == 0)
    			Equal = true;
    		else
    			Equal = false;			
		} catch (Exception e) {
			Equal = false;			
		}
		
		return Equal;
	}

}
