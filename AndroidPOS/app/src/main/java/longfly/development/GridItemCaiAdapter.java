package longfly.development;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridItemCaiAdapter extends BaseAdapter  
{  
  
    private LayoutInflater inflater;   
    private List<Grid_Item_Cai> gridItemList;   
   
    public GridItemCaiAdapter(int[] cai_style_pic, String[] cai_style ,
    		int[]		cai_pic	,	int[]		cai_number ,
    		String[] cai_name 	,String[] cai_price , 
    		String[] cai_code 	, Context context )   
    {   
        super();   
        gridItemList = new ArrayList<Grid_Item_Cai>();   
        inflater = LayoutInflater.from(context);   
        for (int i = 0; i < cai_pic.length; i++)   
        {   
        	Grid_Item_Cai picture = new Grid_Item_Cai( cai_style_pic[i], cai_style[i] ,
            		cai_pic[i], cai_number[i] ,cai_name[i] ,
            		cai_price[i] , cai_code[i] );   
            
        	gridItemList.add(picture);   
        }   
    }   
    @Override  
    public int getCount( )  
    {  
        if (null != gridItemList)   
        {   
            return gridItemList.size();   
        }   
        else  
        {   
            return 0;   
        }   
    }  
  
    @Override  
    public Object getItem( int position )  
    {  
        return gridItemList.get(position);   
    }  
  
    @Override  
    public long getItemId( int position )  
    {  
        return position;   
    }  
  
    @Override  
    public View getView( int position, View convertView, ViewGroup parent )  
    {  
        ViewHolderCai viewHolder;   
        if (convertView == null)   
        {   
            convertView = inflater.inflate(R.layout.imx_grid_cai_item, null);   
            viewHolder = new ViewHolderCai();   
            viewHolder.CAI_STYLE 	= (TextView) convertView.findViewById(R.id.grid_cai_style);   
            viewHolder.CAI_NAME 	= (TextView) convertView.findViewById(R.id.grid_cai_name);   
            viewHolder.CAI_PRICE 	= (TextView) convertView.findViewById(R.id.grid_cai_price);   
            viewHolder.CAI_CODE 	= (TextView) convertView.findViewById(R.id.grid_cai_code);   
            viewHolder.CAI_BACKGROUND	= (ImageView) convertView.findViewById(R.id.grid_cai_background);      
            viewHolder.CAI_STYLE_IMAGE	= (ImageView) convertView.findViewById(R.id.grid_cai_style_pic);               

            convertView.setTag(viewHolder);   
        } else  
        {   
            viewHolder = (ViewHolderCai) convertView.getTag();   
        }   
        viewHolder.CAI_STYLE.setText(gridItemList.get(position).getStyle());  
        viewHolder.CAI_NAME.setText(gridItemList.get(position).getCaiName());   
        viewHolder.CAI_PRICE.setText(gridItemList.get(position).getCaiPrice());
        viewHolder.CAI_CODE.setText(gridItemList.get(position).getCaiCode());   
        viewHolder.CAI_BACKGROUND.setImageResource(gridItemList.get(position).getPic());   
        viewHolder.CAI_STYLE_IMAGE.setImageResource(gridItemList.get(position).getStylePic());   
        return convertView;   
    } 
}