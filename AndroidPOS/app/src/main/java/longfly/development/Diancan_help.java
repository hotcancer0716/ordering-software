package longfly.development;

import longfly.development.Print_Setup.insert_Thread;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Diancan_help extends Activity {
	
	Button	POS_SETUP_SAVE;
	Button	POS_SETUP_EXIT;
	

  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_diancaihelp);
        
        find_id();
        key_things();
        
 	    settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
        get_SharePerference();    
   }


	private void get_SharePerference() {
		// TODO Auto-generated method stub

	}


	private void key_things() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(Diancan_help.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		POS_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
				overridePendingTransition(R.anim.in_from_top, R.anim.out_to_bottom);	   	
		   	}
	    });	
	}


	

	private void find_id() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE	=	(Button) findViewById(R.id.other_setup_save);
		POS_SETUP_EXIT	=	(Button) findViewById(R.id.other_setup_exit);
	}
	
	
	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Diancan_help.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据     
	        handler.sendEmptyMessage(5);
//	        editor.putString(SPF_Pos_ShopNum, String.valueOf( POS_SETUP_SHOP_NUM.getText().toString()));  
	        handler.sendEmptyMessage(85);
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}