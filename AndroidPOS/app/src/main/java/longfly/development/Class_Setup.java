package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class Class_Setup extends Activity {
	
	Button	CLASS_EXIT;
	
	ListView	CLASS_BIG_LIST;
	ListView	CLASS_SMALL_LIST;

	EditText	CLASS_BIG_NAME;
	EditText	CLASS_BIG_CODE;
	
	Button		CLASS_BIG_NEW;
	Button		CLASS_BIG_EDIT;
	Button		CLASS_BIG_DEL;
	Button		CLASS_BIG_SAVE;
	Button		CLASS_BIG_CANCEL;
	
	Button		CLASS_SMALL_ADD;
	TextView	CLASS_BIG_SMALLTEXT;
	Button		CLASS_SMALL_SAVE;
	Button		CLASS_SMALL_EDIT;
	Button		CLASS_SMALL_CANCEL;
	Button		CLASS_SMALL_DEL;
	EditText	CALSS_SMALL_NAME,CALSS_SMALL_CODE;
	LinearLayout	SMALL_LAYOUT;
	View		ITem,ITemm;
	
	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

    int	list_position=0; 
    boolean new_add ,new_small_add;
    String ID,IDD;
    String Parent_Class_Code;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_class_setup);
        
        find_id();
        key_things();
		ViewSetVis(3);
		EditToWrite(false);

        //初始化数据库 显示在listview
  	    db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
 	   
        init_big_list();
        init_small_list("");        
   }


	/*******************小类列表开始*****************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap_small(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            map.put("num", ""+i);
           
            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cBigCode", cursor.getString(cursor.getColumnIndex("cBigCode")));
            map.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
            map.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));
            map.put("cMemo",  cursor.getString(cursor.getColumnIndex("cMemo")));
		
			list.add(map);
		}
		return list;
	}
	
	private void inflateList_small(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_classmall_cai_list 
				, new String[]{	"num" 			, "cName" ,"cCode" 		}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3 }
        );
		
		//显示数据
        CLASS_SMALL_LIST.setAdapter(adapter);        
	}
	
    private void init_small_list(String parentCode) {
		// TODO Auto-generated method stub
    		String[] refer = {parentCode};
	 	   Cursor  cursor = db.rawQuery("select * from lfcy_base_smallsort where cBigCode = ?", refer);
	 	   fill_hashmap_small(cursor);
	 	   inflateList_small(cursor);				
	}
	/*******************小类列表结束*****************************************/



	/*******************大类列表开始*****************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;            
			map=new HashMap<String,String>();
            map.put("num", ""+i);            
            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
            map.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));
            map.put("cMemo",  cursor.getString(cursor.getColumnIndex("cMemo")));
		
			list.add(map);
		}
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_class_cai_list 
				, new String[]{	"num" 		  , "cName" ,"cCode" 		}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3 }
        );
		
		//显示数据
        CLASS_BIG_LIST.setAdapter(adapter);
	}
	
	private void init_big_list() {
  		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   Cursor  cursor = db.rawQuery("select * from lfcy_base_bigsort", null);
 	   fill_hashmap(cursor);
 	   inflateList(cursor);			
	}
	/*******************大类列表结束*****************************************/



	private void key_things() {
		CLASS_EXIT.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	

		CLASS_BIG_LIST.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
            	list_position = position;
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
				String	cName =	String.valueOf(map.get("cName"));
                String	cCode  =	String.valueOf(map.get("cCode"));
                ID =	String.valueOf(map.get("_id")); 
                Parent_Class_Code = cCode;
                try {
                    ITem.setBackgroundColor(Color.argb(255, 235, 235, 235));              
				} catch (Exception e) {
					// TODO: handle exception
				}
                ITem = v;
                ITem.setBackgroundColor(Color.argb(255, 22, 175, 251));              
        		init_small_list(cCode);      
        		
        		CLASS_BIG_NAME.setText(cName);
        		CLASS_BIG_CODE.setText(cCode);
        		
        		ViewSetVis(1);
				ViewSetSmallVis(1);
            }  
        }); 
		
		CLASS_BIG_NEW.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				//编辑框清空
				Edit_Clear();
				//编辑框可写
				EditToWrite(true);
				//取消和保存 显示
				ViewSetVis(2);
				new_add = true;				
			}
		});
		CLASS_BIG_EDIT.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//编辑框可写
				EditToWrite(true);
				//取消和保存 显示				
				ViewSetVis(2);
				new_add= false;
			}
		});	
		CLASS_BIG_DEL.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditToWrite(false);
				//数据库根据分类编码删除一条
		   		//根据操作员编码删除
		   		sql_control sqlc = new sql_control();
		   		sqlc.remove_table_data(db, "lfcy_base_bigsort", "cCode",
		   				CLASS_BIG_CODE.getText().toString());
		   
		   		init_big_list();

				//刷新数据库
				ViewSetVis(1);
				Edit_Clear();
			}
		});		
		CLASS_BIG_SAVE.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
	   			sql_control sqlc = new sql_control();					
				
				//判断编辑框是否为空	
				if ( CLASS_BIG_NAME.getText().toString() == null || CLASS_BIG_NAME.getText().toString().length() <= 0 ||
					 CLASS_BIG_CODE.getText().toString() == null || CLASS_BIG_CODE.getText().toString().length() <= 0) 
				{
					Toast.makeText(Class_Setup.this, "分类名称和分类编码不能为空 ", Toast.LENGTH_SHORT).show();				
				} 
				else 
				{
					//判断是新增的还是修改的
					if (new_add) {	
						//判断分类编码是否有重复	
						boolean if_only_one = sqlc.sql_item_only_one(db, "lfcy_base_bigsort", "cCode", CLASS_BIG_CODE.getText().toString());
			   			if (if_only_one) {
							//添加数据库
				   			sqlc.insert_table_bigclass_info(db, 
				   					CLASS_BIG_NAME.getText().toString() , 
				   					CLASS_BIG_CODE.getText().toString()
				   					);
				   			init_big_list();
				   	 	    CLASS_BIG_LIST.setSelection(adapter.getCount());  //显示ListView的最后一条		    							
							EditToWrite(false);
							Edit_Clear();
			   			} else {
							Toast.makeText(Class_Setup.this, "分类编码不能重复", Toast.LENGTH_LONG).show();
						}
					} else {
						//修改    编辑   更新数据库
						sqlc.updata_table_bigclass_info(db, ID, 
								CLASS_BIG_NAME.getText().toString(), 
								CLASS_BIG_CODE.getText().toString() 
								);
						
			   			init_big_list();
						EditToWrite(false);
						Edit_Clear();
					}
					
				}

				//新增，修改，删除 显示
				ViewSetVis(1);
			}
		});

		CLASS_BIG_CANCEL.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditToWrite(false);
				//新增，修改，删除 显示
				ViewSetVis(3);
			}
		});
		
		CLASS_SMALL_ADD.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ViewSetSmallVis(2);
				new_small_add = true;
        		CALSS_SMALL_NAME.setEnabled(true);
        		CALSS_SMALL_CODE.setEnabled(true);
				CALSS_SMALL_NAME.setText("");
        		CALSS_SMALL_CODE.setText("");					
			}
		});
		CLASS_SMALL_EDIT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new_small_add = false;
				ViewSetSmallVis(2);
			}
		});
		CLASS_SMALL_DEL.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ViewSetSmallVis(1);
			}
		});
		CLASS_SMALL_LIST.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
				ViewSetSmallVis(3);
            	list_position = position;
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
				String	cName  =	String.valueOf(map.get("cName"));
                String	cCode  =	String.valueOf(map.get("cCode"));
                IDD =	String.valueOf(map.get("_id")); 
                System.out.println(cCode +"  "+cName);
        		Log.i("info", cCode +"  "+cName);  

                try {
                    ITemm.setBackgroundColor(Color.argb(255, 235, 235, 235));              
				} catch (Exception e) {
					// TODO: handle exception
				}
                ITemm = v;
                ITemm.setBackgroundColor(Color.argb(255, 22, 175, 251));              
        		
        		CALSS_SMALL_NAME.setText(cName);
        		CALSS_SMALL_CODE.setText(cCode);				
            }  
        }); 
		
		CLASS_SMALL_CANCEL.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ViewSetSmallVis(1);
			}
		});
		CLASS_SMALL_SAVE.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
	   			sql_control sqlc = new sql_control();					
				
				//判断编辑框是否为空	
				if ( CALSS_SMALL_NAME.getText().toString() == null || CALSS_SMALL_NAME.getText().toString().length() <= 0 ||
						CALSS_SMALL_CODE.getText().toString() == null || CALSS_SMALL_CODE.getText().toString().length() <= 0) 
				{
					Toast.makeText(Class_Setup.this, "小类名称和小类编码不能为空 ", Toast.LENGTH_SHORT).show();				
				} 
				else 
				{
					//判断是新增的还是修改的
					if (new_small_add) {	
						//判断分类编码是否有重复	
						boolean if_only_one = sqlc.sql_item_only_one(db, "lfcy_base_smallsort", "cCode", CALSS_SMALL_CODE.getText().toString());
			   			if (if_only_one) {
							//添加数据库
				   			sqlc.insert_table_smallclass_info(db, 
				   					Parent_Class_Code,
				   					CALSS_SMALL_NAME.getText().toString() , 
				   					CALSS_SMALL_CODE.getText().toString()
				   					);
				   			init_small_list(Parent_Class_Code);
				   			CLASS_SMALL_LIST.setSelection(adapter.getCount());  //显示ListView的最后一条		    							
			        		CALSS_SMALL_NAME.setText("");
			        		CALSS_SMALL_CODE.setText("");
			        		CALSS_SMALL_NAME.setEnabled(false);
			        		CALSS_SMALL_CODE.setEnabled(false);	
							ViewSetSmallVis(1);
			   			} else {
							Toast.makeText(Class_Setup.this, "分类编码不能重复", Toast.LENGTH_LONG).show();
						}
					} else {
						//修改    编辑   更新数据库
						sqlc.updata_table_smallclass_info(db, IDD, 
								Parent_Class_Code,
			   					CALSS_SMALL_NAME.getText().toString() , 
			   					CALSS_SMALL_CODE.getText().toString()
								);
						
			   			init_small_list(Parent_Class_Code);
		        		CALSS_SMALL_NAME.setText("");
		        		CALSS_SMALL_CODE.setText("");
		        		CALSS_SMALL_NAME.setEnabled(false);
		        		CALSS_SMALL_CODE.setEnabled(false);	
						ViewSetSmallVis(1);
					}
				}				
			}
		});
	}
	
	

	protected void Edit_Clear() {
		// TODO Auto-generated method stub
		CLASS_BIG_NAME.setText("");
		CLASS_BIG_CODE.setText("");
	}
	protected void EditToWrite(boolean bool) {
		// TODO Auto-generated method stub
		CLASS_BIG_NAME.setEnabled(bool);
		CLASS_BIG_CODE.setEnabled(bool);
	}
	protected void ViewSetVis(int value) {
		CLASS_BIG_NEW.setVisibility(View.GONE);		
		CLASS_BIG_EDIT.setVisibility(View.GONE);	
		CLASS_BIG_DEL.setVisibility(View.GONE);	
		CLASS_BIG_SAVE.setVisibility(View.GONE);
		CLASS_BIG_CANCEL.setVisibility(View.GONE);
		
		switch (value) {
		case 1:
			CLASS_BIG_NEW.setVisibility(View.VISIBLE);		
			CLASS_BIG_EDIT.setVisibility(View.VISIBLE);	
			CLASS_BIG_DEL.setVisibility(View.VISIBLE);				
			break;
		case 2:
			CLASS_BIG_SAVE.setVisibility(View.VISIBLE);
			CLASS_BIG_CANCEL.setVisibility(View.VISIBLE);			
			break;			
		case 3:
			CLASS_BIG_NEW.setVisibility(View.VISIBLE);
			break;	
		default:
			break;
		}	
	}
	
	protected void ViewSetSmallVis(int value) {
		CLASS_SMALL_ADD.setVisibility(View.GONE);	
		CLASS_SMALL_EDIT.setVisibility(View.GONE);	
		CLASS_SMALL_DEL.setVisibility(View.GONE);	
		SMALL_LAYOUT.setVisibility(View.GONE);	
		
		/*
		 * 状态1就是点击了  左边的大类             应该只显示   增加小类 
		 * 状态2就是点击了右边的增加小类     应该显示Layout  Text变化
		 * 状态3就是点击了右边的小类		应该Text变化     并且显示增加小类，小类修改和删除
		 */
		switch (value) {
		case 1:
			CLASS_SMALL_ADD.setVisibility(View.VISIBLE);				
			CLASS_BIG_SMALLTEXT.setText("菜品小类  分类列表");
			break;
		case 2:
			SMALL_LAYOUT.setVisibility(View.VISIBLE);
			CLASS_BIG_SMALLTEXT.setText("菜品小类  分类管理");
			break;
		case 3:
			CLASS_SMALL_ADD.setVisibility(View.VISIBLE);				
			CLASS_SMALL_EDIT.setVisibility(View.VISIBLE);	
			CLASS_SMALL_DEL.setVisibility(View.VISIBLE);				
			CLASS_BIG_SMALLTEXT.setText("菜品小类  分类管理");			
			break;		
		default:
			break;
		}
	}	

	private void find_id() {
		// TODO Auto-generated method stub
		CLASS_EXIT		= (Button) findViewById(R.id.class_big_exit);
		CLASS_BIG_NEW 	= (Button) findViewById(R.id.class_big_add);		
		CLASS_BIG_EDIT	= (Button) findViewById(R.id.class_big_edit_edit);		
		CLASS_BIG_DEL 	= (Button) findViewById(R.id.class_big_del);	
		CLASS_BIG_SAVE 	= (Button) findViewById(R.id.class_big_save);		
		CLASS_BIG_CANCEL= (Button) findViewById(R.id.class_big_cancel);		
	
		CLASS_SMALL_ADD 	= (Button) findViewById(R.id.class_btn_add_small);
		CLASS_SMALL_SAVE	= (Button) findViewById(R.id.class_btn_save_small);
		CLASS_SMALL_EDIT 	= (Button) findViewById(R.id.class_btn_edit_small);
		CLASS_SMALL_DEL 	= (Button) findViewById(R.id.class_btn_del_small);
		CLASS_SMALL_CANCEL 	= (Button) findViewById(R.id.class_btn_cancel_small);
		CALSS_SMALL_NAME	=	(EditText) findViewById(R.id.class_small_name);
		CALSS_SMALL_CODE	=	(EditText) findViewById(R.id.class_small_code);
		CLASS_BIG_SMALLTEXT = (TextView) findViewById(R.id.class_btn_smalltext);
		SMALL_LAYOUT	=	(LinearLayout) findViewById(R.id.class_layout_small_add);		
		CLASS_BIG_LIST  = (ListView) findViewById(R.id.class_big_list);
		CLASS_SMALL_LIST = (ListView) findViewById(R.id.class_small_list);
		
		CLASS_BIG_NAME = (EditText) findViewById(R.id.class_name);
		CLASS_BIG_CODE = (EditText) findViewById(R.id.class_big_code);	
	}
   
}