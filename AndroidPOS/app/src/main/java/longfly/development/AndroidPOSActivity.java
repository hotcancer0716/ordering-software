package longfly.development;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class AndroidPOSActivity extends Activity {
    /** Called when the activity is first created. */
 
	SQLiteDatabase db;
	ProgressDialog progressDialog;
    private static final int DIALOG =   1;
	private static int i=0;  
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);

		
        init_date();
		Intent intent_page_new = new Intent();
		intent_page_new.setClass(AndroidPOSActivity.this, Login.class);			
		startActivity(intent_page_new);	
		overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
    }

	private void init_date() {
		// TODO Auto-generated method stub
		
		ProgressDialog("下载数据中", "请稍等......");		
     	insert_Thread thread1= new insert_Thread();
     	thread1.start();		
	}
	
	//创建一个带进度条的对话框
	private void ProgressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(AndroidPOSActivity.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	@Override 
    protected Dialog onCreateDialog(int id) {  
        // TODO Auto-generated method stub  
        if(id==1){  
        	return this.buildDialog(AndroidPOSActivity.this);  
        }else{  
            return null;  
        }  
    } 

    @Override 
    protected void onPrepareDialog(int id, Dialog dialog) {  
        // TODO Auto-generated method stub  
        super.onPrepareDialog(DIALOG, dialog);  
    } 	
	
   //弹一个对话框！
    private Dialog buildDialog(Context context) {
		// TODO Auto-generated method stub
        AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setTitle("返回！");
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("要返回到主菜单吗？ ");  
        builder.setCancelable(false);
        builder.setPositiveButton("确定",new DialogInterface.OnClickListener() 
        {  
            @Override 
            public void onClick(DialogInterface dialog, int which) 
            {  
                // TODO Auto-generated method stub  
                setTitle("您单击了对话框上面的正确按钮");  
                finish();
            }  
        });  
          
        return builder.create();  
    } 
	



	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if(	db !=null && db.isOpen() )
		{
			db.close();
		}	
	}
	
	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };	

    
    class insert_Thread extends Thread{
        public void run(){
    		// TODO: handle exception
			handler.sendEmptyMessage(10);

        	try {
				System.out.println("Create start");
				long start_time = Debug.threadCpuTimeNanos();
				db.beginTransaction();
    			//基本菜品信息
    			db.execSQL( "create table lfcy_base_dishinf ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cDishName 		varchar(20)," +		//菜品名称
     					"cDishName2 	Varchar(20)," +		//菜品备用名	!!现在是菜品图片路径！！！
     					"cDishCode		Varchar(20)," +		//菜品编码
     					"cDishPinYin	Varchar(10)," +		//拼音码
     					"cDishHelpCode	Varchar(10)," +		//助记码
     					"cDishClass1	Varchar(10)," +		//大类别
     					"cDishClass2	Varchar(10)," +		//小类别
     					"cDishUnitName	Varchar(10)," +		//单位名称
     					"cDishUnitCode	Varchar(10)," +		//单位编码
     					"cDishOther		Varchar(10)," +		//备注信息
     					"iDishStyle		Varchar(10)," +		//沽清/特价/新品/
     					"iDishDian		int," +				//已点
     					"fDishPrice		Number(16,2),"+		//售价
    					"cIfTaocai		Varchar(10)," +		//套菜属性   “1”是套菜
     					"fTaocaiPrice	Number(16,2))");	//原价（套菜时采用）

            	//套餐所属菜品列表  （一个套餐里最多6个菜品）
    			db.execSQL( "create table lfcy_taocai_cailist ( " +
     					"_id integer primary key autoincrement," +		//自增号  流水号
     					"cTaoCaiCode	varchar(10)," +			//套菜编码
     					"cTaoCaiName	varchar(20)," +			//套菜名称
     					"cCaiCode 		varchar(10)," +			//套菜内菜品编码
     					"cCaiName 		varchar(20)," +			//套菜内菜品名称
     					"cCaiPrice 		Number(16,2))");		//套菜内菜品价格
  
            	//操作者信息
    			db.execSQL( "create table lfcy_user_inf ( " +
     					"_id integer primary key autoincrement," +		//自增号  流水号
     					"cUserName 		varchar(10)," +			//用户名
     					"cUserCode 		varchar(10)," +			//用户编码
     					"iUserPassword 	varchar(10)," +			//用户密码
     					"cUserManager 	varchar(10)," +			//用户职位
     					"iUserLimit     varchar(10))");			//用户权限
   			//桌台信息
    			db.execSQL( "create table lfcy_base_tableinf ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"iTableNumber 	int," +		//桌台号
     					"iTableClass 	int," +		//桌台类型	1.包厢 2.大厅 3.卡座	
     					"iTableClass2 	int," +		//桌台类型	1.A区   2.B区    3.C区	
     					"iTableState 	int," +		//桌台状态	1.空闲2.开台  3.已结账但未清台   4.已点好菜
     					"iTabletime 	Varchar(30)," +		//开台时间
     					"iTablePerson 	int," +		//桌台可容纳人数	
     					"iTable_Person 	int)");		//桌台已容纳人数

    			//菜品大类分类
    			db.execSQL( "create table lfcy_base_bigsort ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//大类名称
     					"cCode 	Varchar(10)," +		//大类编码	
     					"cMemo 	Varchar(30))");		//备注

    			//菜品小类分类
    			db.execSQL( "create table lfcy_base_smallsort ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cBigCode 	Varchar(10)," +		//父类编码	
     					"cName 		Varchar(30)," +		//小类名称
     					"cCode 		Varchar(10)," +		//小类编码	
     					"cMemo 		Varchar(30))");		//备注
   			
    			//菜品口味分类
    			db.execSQL( "create table lfcy_base_taste ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//口味名称
     					"cCode 	Varchar(10),"+		//口味编码	
    					"cMemo 	Varchar(30))");		//备注
    			
    			//菜品做法分类
    			db.execSQL( "create table lfcy_base_dishtaste ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//做法名称
     					"cCode 	Varchar(10)," +		//做法编码	
    					"cMemo 	Varchar(30))");		//备注
    			//桌台区域分类
    			db.execSQL( "create table lfcy_base_tablearea ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//桌台区域名称
     					"cCode 	Varchar(10)," +		//桌台区域编码
						"cMemo 	Varchar(30))");		//备注
 

    			//桌台类型分类
    			db.execSQL( "create table lfcy_base_tabletype ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//桌台类型名称
     					"cCode 	Varchar(10)," +		//桌台类型编码
     					"cMemo 	Varchar(30))");		//备注	

    			//单位分类
    			db.execSQL( "create table lfcy_base_unit ( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cName 	Varchar(30)," +		//单位名称
     					"cCode 	Varchar(10)," +		//单位编码
     					"cMemo 	Varchar(30))");		//备注	
  			
    			//结账信息  收银流水用
    			db.execSQL( "create table  lfcy_base_checkinf( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"iCheckNo 		Varchar(20)," +		//付款单号
     					"cChecktime 	Varchar(30)," +		//付款时间	
//     					"iCheckNoLiuShui Varchar(20)," +	//付款流水号
     					"cUserName 		varchar(30)," +		//收银员
     					"iTableNumber 	int," +				//台号
     					"fCheckPrice	Number(16,2),"+		//消费金额
     					"cTabletime 	Varchar(30)," +		//开台时间
     					"fCheckPriceOut	Number(16,2),"+		//结账找零金额
     					"fCheckPriceIn	Number(16,2),"+		//结账实收金额
     					"cCheck		 	Varchar(30))");		//优惠
//付款单号、付款时间、付款流水号、收银员、台号、消费金额、折扣金额、抹零金额、实收金额
//账单号(开台单号)、桌号、菜品名称、点菜时间   、数量、价格、菜品加价选项、折扣、赠送选项、金额、特价选项、套菜选项、 			
    			//设备信息
    			db.execSQL( "create table lfcy_base_sellinf_liushui( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cOpenTableNo 	Varchar(20),"+	//账单号(开台单号)
     					"iTableNumber 	Varchar(10),"+	//桌号
     					"cDishName 		Varchar(20),"+	//菜品名称
     					"cOpenTableTime Varchar(30),"+	//点菜时间
     					"cOpenTableNum 	Varchar(10),"+	//数量
     					"cOpenTablePrice Number(16,2),"+	//价格
     					"cDishAdd 		Varchar(10),"+	//菜品加价选项
     					"cDishzhekou 	Varchar(10),"+	//折扣
     					"cDishzengsong 	Varchar(10),"+	//赠送选项
     					"cDishPrice 	Number(16,2),"+	//金额
     					"cDishTejia 	Varchar(10),"+	//特价选项
     					"cDishTaocai 	Varchar(10))"	//套菜选项
    					);	
   			//设备信息
    			db.execSQL( "create table lfcy_base_posinf( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"iPosMachineNum   Varchar(10)," +	//机器号
     					"iPosShopNum 	  Varchar(10)," +	//门店号	
     					"iPosShopName 	  Varchar(30),"+	//门店名称	
     					"iPosIp 		  Varchar(20))");	//机器IP
    			
    			//菜品销售信息
    			db.execSQL( "create table lfcy_base_sellinf( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"iTableNumber 	Varchar(10)," +		//桌台号
     					"cSellNo 		Varchar(20)," +		//销售单号（利用开台时间）
     					"cDishName		Varchar(20)," +		//销售菜品名称
     					"cDishCode		Varchar(20)," +		//销售菜品编码
     					"fDishPrice		Number(16,2)," +	//销售菜品单价
     					"cDishQty	    int		   ," +		//菜品销售数量
     					"cDishUnitName  Varchar(20)," +		//菜品单位名称
     					"cDishUnitCode  Varchar(20)," +		//菜品单位编码
						"dXiadantime 	Varchar(30))");		//开台时间

    			//菜品销售信息
    			db.execSQL( "create table lfcy_base_sellinf_new( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"iTableNumber 	Varchar(10)," +		//桌台号
     					"cSellNo 		Varchar(20)," +		//销售单号（利用开台时间）
     					"cDishName		Varchar(20)," +		//销售菜品名称
     					"cDishCode		Varchar(20)," +		//销售菜品编码
     					"fDishPrice		Number(16,2)," +		//销售菜品单价
     					"cDishQty	    int,"+
     					"cDishUnitName  Varchar(20)," +		//菜品单位名称
     					"cDishUnitCode  Varchar(20)," +		//菜品单位编码
						"dXiadantime 	Varchar(30))");		//开台时间
     			
    			//临时开台单号表,用于定了台！但是还没点菜的情况！仅在桌台显示开台状态
    			db.execSQL( "create table lfcy_base_opentableinf( " +
     					"iTableNumber 	Varchar(10)," +		//桌台号
						"cSellNo	    Varchar(20))");		//销售单号（利用开台时间）


    			//桌台结账信息
    			db.execSQL( "create table lfcy_base_tablecheckinf( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"dtCheckDate 		Varchar(10)," +		//结账时间
     					"cUserCode 			varchar(10)," +		//操作员编码
     					"iPosShopNum 	  	Varchar(10)," +		//门店号	
     					"iPosMachineNum  	Varchar(10)," +		//机器号
     					"iTableNumber 		Varchar(10)," +		//桌台号     					
     					"cPayNo 			Varchar(20)," +		//结账单号
     					"cSellNo 			Varchar(20)," +		//销售单号（利用开台时间）
     					"fSaleSum			Number(16,2)," +		//实际消费金额
     					"fSvrSum			Number(16,2)," +		//服务费及加收金额
     					"fReduceSum			Number(16,2)," +		//打折及直减金额
     					"fTradeSum			Number(16,2)," +		//最终应收交易金额     					
     					"fTradeEndSum		Number(16,2)," +		//最终实收交易金额     					
     					"fPayBackSum 		Number(16,2)," +		//找零金额
						"iTable_Person 		int)");				//桌台容纳人数

    			//菜品销售排行信息
    			db.execSQL( "create table lfcy_sell_rank_linshi( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cDishName		Varchar(20)," +		//销售菜品名称
     					"cDishCode		Varchar(20)," +		//销售菜品编码
     					"fDishPrice		Number(16,2)," +	//销售菜品单价
     					"cDishQty	    int		   ," +		//菜品销售数量
     					"cDishUnitName  Varchar(10)," +		//菜品所属单位名称
     					"cDishUnitCode  Varchar(10)," +		//菜品所属单位编码
     					"fDishAllPrice	Number(16,2)," +	//该菜品销售总价
     					"cBigClassCode  Varchar(10)," +		//菜品所属大类
						"dXiadantime 	Varchar(30))");		//开台时间

    			//菜品日结
    			db.execSQL( "create table lfcy_day_check_dish( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cDayCheckData  Varchar(30)," +		//当天日结日期
     					"cDishName		Varchar(20)," +		//销售菜品名称
     					"cDishCode		Varchar(20)," +		//销售菜品编码
     					"fDishPrice		Number(16,2)," +	//销售菜品单价
     					"cDishQty	    int		   ," +		//菜品销售数量
     					"cDishUnitName  Varchar(10)," +		//菜品所属单位名称
     					"cDishUnitCode  Varchar(10)," +		//菜品所属单位编码
     					"fDishAllPrice	Number(16,2)," +    //该菜品销售额
						"cMemo		 	Varchar(10))");		//备注

    			
    			//日结信息
    			db.execSQL( "create table lfcy_day_check( " +
     					"_id integer primary key autoincrement," +	//自增号  流水号
     					"cDayCheckData  Varchar(30)," +		//当天日结日期
     					"fSellAll		Number(16,2)," +	//当天销售营业额
     					"cDishSellAll	Number(16,2)," +	//当天菜品销售额
     					"cPeopleNum 	Varchar(10)," +		//开台客流量
     					"cDayCheck1		Varchar(30)," +		//当天1
     					"cDayCheck2		Varchar(30)," +		//当天2
     					"cDayCheck3		Varchar(30)," +		//当天3     					
     					"cDayCheck4		Varchar(30)," +		//当天4
     					"cDayCheck5		Varchar(30)," +		//当天5
     					"cDayCheck6		Varchar(30)," +		//当天6
						"cDayCheck7 	Varchar(10))");		//当天7

    			
				long create_finiash_time = Debug.threadCpuTimeNanos();
    			
    			handler.sendEmptyMessage(50);
				System.out.println("Create Success ");
    			try {
    				System.out.println("insert start");

    				init_new();
    				init_new2();
    				insert_TableData(db,"01","4003","3001","1",null,"6",null);				
    				insert_TableData(db,"02","4003","3001","1",null,"10",null);				
    				insert_TableData(db,"03","4003","3001","3",null,"8",null);				
    				insert_TableData(db,"04","4003","3001","1",null,"6",null);				  				
    				insert_TableData(db,"05","4003","3001","1",null,"6",null);				
    				insert_TableData(db,"06","4003","3001","1",null,"6",null);				
    				insert_TableData(db,"07","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"08","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"09","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"10","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"11","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"12","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"13","4001","3001","1",null,"6",null);				
    				insert_TableData(db,"14","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"15","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"16","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"17","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"18","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"19","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"20","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"21","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"22","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"23","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"24","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"25","4001","3001","1",null,"6",null);				  				
    				insert_TableData(db,"26","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"27","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"28","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"29","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"30","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"31","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"32","4001","3002","1",null,"6",null);				  				
    				insert_TableData(db,"33","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"34","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"35","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"36","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"37","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"38","4002","3003","1",null,"10",null);				
    				insert_TableData(db,"39","4002","3003","1",null,"10",null);				
	
    				insert_UserData(db, "刘小二","0000","0000", "销售员","01" );
    				insert_UserData(db, "赵铁柱","0001","1001", "销售员","01" );
    				insert_UserData(db, "吴秀娟","0011","1011", "收款员","02" );
    				insert_UserData(db, "王大力","9999","9999", "经理","03" );
    				
    				insert_POSData(db,"0001","0001","Wal-MartStores","192.168.200.119");

    				insert_BigSort(db, "欧陆风味套餐","10001",null );
    				insert_BigSort(db, "浪漫情人套餐","10011",null );
    				insert_BigSort(db, "扒类","10012",null );
    				insert_BigSort(db, "焗出新煮意","10013",null );
    				insert_BigSort(db, "汤","10014",null );
    				insert_BigSort(db, "披萨饼","10015",null );
    				insert_BigSort(db, "粉面","10016",null );
    				insert_BigSort(db, "炒饭","10017",null );
    				insert_BigSort(db, "烩一烩","10018",null );
    				insert_BigSort(db, "三文治及蛋","10029",null );
    				insert_BigSort(db, "风味小食","10020",null );
    				insert_BigSort(db, "饮品","10019",null );
    				insert_BigSort(db, "其他","10021",null );

    				insert_SmallSort(db, "10019","咖啡","0001",null );
    				insert_SmallSort(db, "10019","冰沙","0002",null );
    				insert_SmallSort(db, "10019","鲜榨果汁","0003",null );
    				insert_SmallSort(db, "10019","啤酒","0004",null );
    				insert_SmallSort(db, "10019","红酒","0005",null );
    				
    				insert_SmallSort(db, "10021","小类1","2101",null );
    				insert_SmallSort(db, "10021","小类2","2102",null );
    				
    				insert_DishData(db, "草莓沙司","/mnt/sdcard/picture/cai1.jpg","10001","cmss","caomeishasi"			,"10021","2101","份","2002","","1","0","75.00");
    				insert_DishData(db, "紫薯雪耳","/mnt/sdcard/picture/cai11.jpg","10002","zsxe","zishuxueer"			,"10021","2101","份","2002","",null,"0","62.00");
    				insert_DishData(db, "莲藕豆腐","/mnt/sdcard/picture/cai12.jpg","10003","zsdf","lianoudoufu"			,"10021","2101","份","2002","","3","0","35.00");
    				insert_DishData(db, "紫薯糕",	"/mnt/sdcard/picture/cai13.jpg","10004","zsg","zishugao"				,"10021","2101","份","2002","",null,"1","55.00");
    				insert_DishData(db, "印度蛋糕","/mnt/sdcard/picture/cai14.jpg","10005","yddg","yindudangao"			,"10021","2101","份","2002","","5","0","65.00");
    				insert_DishData(db, "柠檬雪碧","/mnt/sdcard/picture/cai15.jpg","10006","nmxb","ningmengxuebi"			,"10021","2101","份","2002","","2","0","85.00");
    				insert_DishData(db, "清水",		  "/mnt/sdcard/picture/cai2.jpg","10007","qs","qingshui"					,"10021","2101","份","2002","",null,"0","12.00");
    				insert_DishData(db, "巧克力蛋糕","/mnt/sdcard/picture/cai3.jpg","10008","qkldg","qiaokelidangao"	,"10021","2101","份","2002","","3","0","45.00");
    				insert_DishData(db, "清汤煮面","/mnt/sdcard/picture/cai4.jpg","10009","qtzm","qingtangzhumian"		,"10021","2102","份","2002","",null,"1","25.00");
    				insert_DishData(db, "极品海参","/mnt/sdcard/picture/cai5.jpg","10010","jphs","jipinhaishen"			,"10021","2102","份","2002","",null,"0","134.00");
    				insert_DishData(db, "凉拌猪耳","/mnt/sdcard/picture/cai6.jpg","10011","lbze","linagbanzhuer"			,"10021","2102","份","2002","",null,"0","53.00");
    				insert_DishData(db, "蒜泥白肉","/mnt/sdcard/picture/cai7.jpg","10012","snbr","suannibairou"			,"10021","2102","份","2002","",null,"0","73.00");
    				insert_DishData(db, "椒盐虾",	"/mnt/sdcard/picture/cai8.jpg","10013","jyx","jiaoyanxia"				,"10021","2102","份","2002","",null,"0","36.00");
    				insert_DishData(db, "紫薯蛋糕","/mnt/sdcard/picture/cai1.jpg","10014","zsdg","zishudangao"			,"10021","2102","份","2002","","4","0","23.00");
    				insert_DishData(db, "橄榄菜",	"/mnt/sdcard/picture/cai9.jpg","10015","glc","ganlancai"				,"10021","2102","份","2002","","5","0","45.00");
    				insert_DishData(db, "番茄牛肉","/mnt/sdcard/picture/cai1.jpg","10016","fqnr","fanqieniurou"			,"10021","2102","份","2002","","1","0","68.00");
    				insert_DishData(db, "水煮牛肉","/mnt/sdcard/picture/cai12.jpg","10017","fqnr","fanqieniurou"			,"10021","2102","份","2002","","1","0","68.00");
    				insert_DishData(db, "番茄炒蛋","/mnt/sdcard/picture/cai13.jpg","10018","fqnr","fanqieniurou"			,"10021","2102","份","2002","",null,"0","68.00");
    				insert_DishData(db, "京酱肉丝","/mnt/sdcard/picture/cai14.jpg","10019","fqnr","fanqieniurou"			,"10021","2102","份","2002","",null,"0","68.00");
    				insert_DishData(db, "猪腩烧莲藕","/mnt/sdcard/picture/cai16.jpg","10020","fqnr","fanqieniurou"		,"10021","2102","份","2002","",null,"0","68.00");
    				insert_DishData(db, "葱爆牛肉","/mnt/sdcard/picture/cai17.jpg","10021","fqnr","fanqieniurou"			,"10021","2102","份","2002","","2","0","68.00");
    				insert_DishData(db, "牛肉面","/mnt/sdcard/picture/cai11.jpg","10022","fqnr","fanqieniurou"				,"10021","2102","份","2002","","1","0","68.00");

    				insert_DishData(db, "特色大什扒","/mnt/sdcard/picture/cai1.jpg","10031","tsdsp","tesedashipa"				,"10012",null,"份","2002","",null,"0","38.00");
    				insert_DishData(db, "香煎澳洲西冷扒","/mnt/sdcard/picture/cai2.jpg ","10032","xjazxlp","xiangjianaozhouxilengpa"				,"10012",null,"份","2002","","1","0","38.00");
    				insert_DishData(db, "黑椒美国肥牛扒","/mnt/sdcard/picture/cai3.jpg","10033","hjmgfnp","heijiaomeiguofeiniupa"				,"10012",null,"份","2002","","1","0","38.00");
    				insert_DishData(db, "香煎澳洲西冷扒拼鸡扒","/mnt/sdcard/picture/cai4.jpg","10034","xjazxlppjp","xiangjianaozhouxilengpapinjipa"				,"10012",null,"份","2002","","1","0","38.00");
    				insert_DishData(db, "牛仔肋扒","/mnt/sdcard/picture/cai5.jpg","10034","nzlp","niuzaileipa"				,"10012",null,"份","2002","","1","0","38.00");
    				insert_DishData(db, "美国牛仔骨扒","/mnt/sdcard/picture/cai6.jpg","10035","mgnzgp","meiguoniuzaigupa"				,"10012",null,"份","2002","","1","0","48.00");
    				insert_DishData(db, "照烧原汁鲜鱿扒","/mnt/sdcard/picture/cai7.jpg","10036","zsyzxyp","zhaoshaoyuanzhixianyoupa"				,"10012",null,"份","2002","","1","0","28.00");
    				insert_DishData(db, "吉列猪扒","/mnt/sdcard/picture/cai11.jpg","10037","jlzp","jiliezhupa"				,"10012",null,"份","2002","","1","0","28.00");
    				insert_DishData(db, "蒜香鸡扒","/mnt/sdcard/picture/cai12.jpg","10038","sxjp","suanxiangjipa"				,"10012",null,"份","2002","","1","0","30.00");
    				insert_DishData(db, "黑椒牛扒","/mnt/sdcard/picture/cai13.jpg","10039","hjnp","heijiaoniupa"				,"10012",null,"份","2002","","1","0","35.00");
    				insert_DishData(db, "香煎鸡扒 ","/mnt/sdcard/picture/cai14.jpg","10040","xjjp","xiangjianjipa"				,"10012",null,"份","2002","","1","0","40.00");
    				insert_DishData(db, "纽西兰牛仔骨","/mnt/sdcard/picture/cai15.jpg","10041","nxlnzg","niuxilanniuzaigu"				,"10012",null,"份","2002","","1","0","48.00");
   				
    				insert_DishData(db, "腌肉肠仔焗饭 ","/mnt/sdcard/picture/cai16.jpg","10050"," "," "				,"10013",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "芝士海鲜焗饭","/mnt/sdcard/picture/cai17.jpg","10051"," "," "				,"10013",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "葡鸡焗饭","/mnt/sdcard/picture/cai18.jpg","10052"," "," "				,"10013",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "芝士海鲜焗意粉","/mnt/sdcard/picture/cai19.jpg","10053"," "," "				,"10013",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "铁板鳗鱼焗饭","/mnt/sdcard/picture/cai21.jpg","10054"," "," "				,"10013",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "意大利肉酱焗意粉","/mnt/sdcard/picture/cai20.jpg","10055"," "," "				,"10013",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "芝士焗西兰花","/mnt/sdcard/picture/cai22.jpg","10056"," "," "				,"10013",null,"份","2002","","1","0","28.00");
    				insert_DishData(db, "茄汁猪扒焗意粉","/mnt/sdcard/picture/cai23.jpg","10057"," "," "				,"10013",null,"份","2002","","1","0","25.00");
   				
    				insert_DishData(db, "老火靓汤","/mnt/sdcard/picture/cai24.jpg","10060"," "," "				,"10014",null,"份","2002","","1","0","5.00");
    				insert_DishData(db, "罗宋汤 ","/mnt/sdcard/picture/cai25.jpg","10061"," "," "				,"10014",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "泰式冬阴功汤 ","/mnt/sdcard/picture/cai11.jpg","10062"," "," "				,"10014",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "栗米忌廉汤","/mnt/sdcard/picture/cai26.jpg","10063"," "," "				,"10014",null,"份","2002","","1","0","13.00");
    				insert_DishData(db, "鸡茸忌廉汤 ","/mnt/sdcard/picture/cai11.jpg","10064"," "," "				,"10014",null,"份","2002","","1","0","13.00");
    				System.out.println(" insert 1");
    				insert_DishData(db, "牛肉披萨（小） ","/mnt/sdcard/picture/cai27.jpg ","10071"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "海鲜披萨（小） ","/mnt/sdcard/picture/cai28.jpg ","10072"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "鸡肉披萨（小） "," /mnt/sdcard/picture/cai11.jpg","10073"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "牛肉披萨（大） ","/mnt/sdcard/picture/cai10.jpg ","10074"," "," "				,"10015",null,"份","2002","","1","0","30.00");
    				insert_DishData(db, "海鲜披萨（大） ","/mnt/sdcard/picture/cai9.jpg ","10075"," "," "				,"10015",null,"份","2002","","1","0","30.00");
    				insert_DishData(db, "鸡肉披萨（大） ","/mnt/sdcard/picture/cai8.jpg ","10076"," "," "				,"10015",null,"份","2002","","1","0","35.00");
    				insert_DishData(db, "什果披萨（小） ","/mnt/sdcard/picture/cai7.jpg ","10077"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "什果披萨（大） ","/mnt/sdcard/picture/cai6.jpg ","10078"," "," "				,"10015",null,"份","2002","","1","0","30.00");
    				insert_DishData(db, "菠萝海鲜披萨（小） ","/mnt/sdcard/picture/cai5.jpg ","10079"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "菠萝海鲜披萨（大） "," /mnt/sdcard/picture/cai4.jpg","10080"," "," "				,"10015",null,"份","2002","","1","0","30.00");
    				insert_DishData(db, "风味腊肠披萨（小） ","/mnt/sdcard/picture/cai3.jpg ","10081"," "," "				,"10015",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "风味腊肠披萨（大） ","/mnt/sdcard/picture/cai2.jpg ","10082"," "," "				,"10015",null,"份","2002","","1","0","30.00");
    				
    				insert_DishData(db, "黑椒牛柳炒意粉 ","/mnt/sdcard/picture/cai1.jpg ","10100"," "," "				,"10016",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "榨菜肉丝汤米粉 ","/mnt/sdcard/picture/cai11.jpg ","10101"," "," "				,"10016",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "榨菜肉丝汤乌冬面 ","/mnt/sdcard/picture/cai12.jpg ","10102"," "," "				,"10016",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "三丝炒意粉 ","/mnt/sdcard/picture/cai13.jpg ","10103"," "," "				,"10016",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "海鲜炒意粉 ","/mnt/sdcard/picture/cai14.jpg ","10104"," "," "				,"10016",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "星洲炒米粉 ","/mnt/sdcard/picture/cai15.jpg ","10105"," "," "				,"10016",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "日式猪扒汤通心粉 ","/mnt/sdcard/picture/cai16.jpg ","100106"," "," "				,"10016",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "日食海鲜汤乌冬面 ","/mnt/sdcard/picture/cai17.jpg ","100107"," "," "				,"10016",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "猪肉眼扒汤通心粉 ","/mnt/sdcard/picture/cai18.jpg ","100108"," "," "				,"10016",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鲜茄牛肉烩意粉 ","/mnt/sdcard/picture/cai19.jpg ","100109"," "," "				,"10016",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "火腿蛋汤公仔面 ","/mnt/sdcard/picture/cai11.jpg ","100110"," "," "				,"10016",null,"份","2002","","1","0","15.00");

    				insert_DishData(db, "揽菜腌肉炒饭","/mnt/sdcard/picture/cai2.jpg","100200"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鼔椒炒鱿鱼饭 ","/mnt/sdcard/picture/cai3.jpg","100201"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鼔椒炒牛肉饭 ","/mnt/sdcard/picture/cai5.jpg","100202"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "西兰花炒牛肉饭","/mnt/sdcard/picture/cai8.jpg","100203"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "生炒牛肉饭 ","/mnt/sdcard/picture/cai11.jpg","100204"," "," "				,"10017",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "咸鱼鸡粒炒饭","/mnt/sdcard/picture/cai4.jpg","100205"," "," "				,"10017",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "印尼炒饭","/mnt/sdcard/picture/cai9.jpg","100206"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "秦皇炒饭","/mnt/sdcard/picture/cai12.jpg","100207"," "," "				,"10017",null,"份","2002","","1","0","28.00");
    				insert_DishData(db, "扬州炒饭","/mnt/sdcard/picture/cai13.jpg","100208"," "," "				,"10017",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "西式炒饭","/mnt/sdcard/picture/cai14.jpg","100209"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "海鲜炒饭","/mnt/sdcard/picture/cai15.jpg","100210"," "," "				,"10017",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鳗鱼炒饭","/mnt/sdcard/picture/cai16.jpg","100211"," "," "				,"10017",null,"份","2002","","1","0","23.00");

    				insert_DishData(db, "椰香鸡肉饭","/mnt/sdcard/picture/cai11.jpg ","100212"," "," "				,"10018",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "番茄牛肉饭"," /mnt/sdcard/picture/cai12.jpg","100213"," "," "				,"10018",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "黑椒牛肉饭","/mnt/sdcard/picture/cai13.jpg ","100214"," "," "				,"10018",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "鼓汁排骨饭"," /mnt/sdcard/picture/cai1.jpg","100215"," "," "				,"10018",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "冬菇滑鸡饭","/mnt/sdcard/picture/cai14.jpg ","100216"," "," "				,"10018",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "咖喱海鲜饭"," /mnt/sdcard/picture/cai15.jpg","100217"," "," "				,"10018",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "咖喱牛腩饭","/mnt/sdcard/picture/cai16.jpg ","100218"," "," "				,"10018",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "咖喱牛肉饭"," /mnt/sdcard/picture/cai17.jpg","100219"," "," "				,"10018",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "咖喱鸡肉饭"," /mnt/sdcard/picture/cai18.jpg","100220"," "," "				,"10018",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鲜菇牛肉饭","/mnt/sdcard/picture/cai19.jpg ","100221"," "," "				,"10018",null,"份","2002","","1","0","16.00");
   
    				insert_DishData(db, "煎太阳蛋","/mnt/sdcard/picture/cai1.jpg ","100230"," "," "				,"10029",null,"份","2002","","1","0","3.00");
    				insert_DishData(db, "腌肉火腿肠煎双蛋","/mnt/sdcard/picture/cai11.jpg ","100231"," "," "		,"10029",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "牛油多士","/mnt/sdcard/picture/cai12.jpg ","100232"," "," "				,"10029",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "西多士","/mnt/sdcard/picture/cai13.jpg ","100233"," "," "				,"10029",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "火腿蛋三文治"," /mnt/sdcard/picture/cai14.jpg","100234"," "," "				,"10029",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "公司三文治","/mnt/sdcard/picture/cai15.jpg ","100235"," "," "				,"10029",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "牛扒三文治"," /mnt/sdcard/picture/cai16.jpg","100236"," "," "				,"10029",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "猪扒三文治","/mnt/sdcard/picture/cai17.jpg ","100237"," "," "				,"10029",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "腌肉火腿三文治","/mnt/sdcard/picture/cai18.jpg ","100238"," "," "				,"10029",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "鲜蔬三文治","/mnt/sdcard/picture/cai19.jpg ","100239"," "," "				,"10029",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "牛油法包"," /mnt/sdcard/picture/cai21.jpg","100240"," "," "				,"10029",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "草莓酱多士","/mnt/sdcard/picture/cai22.jpg ","100241"," "," "				,"10029",null,"份","2002","","1","0","12.00");
 
    				insert_DishData(db, "美食炸薯条","/mnt/sdcard/picture/cai11.jpg ","100250"," "," "				,"10020",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "金牌鸡中翼","/mnt/sdcard/picture/cai11.jpg ","100251"," "," "				,"10020",null,"份","2002","","1","0","23.00");
    				insert_DishData(db, "美极鱿鱼丝","/mnt/sdcard/picture/cai11.jpg ","100252"," "," "				,"10020",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "凉拌青瓜","/mnt/sdcard/picture/cai11.jpg ","100253"," "," "				,"10020",null,"份","2002","","1","0","13.00");
    				insert_DishData(db, "日式八爪鱼","/mnt/sdcard/picture/cai11.jpg ","100254"," "," "				,"10020",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "椒盐多春鱼","/mnt/sdcard/picture/cai11.jpg ","100255"," "," "				,"10020",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "印尼炸虾片","/mnt/sdcard/picture/cai11.jpg","100256"," "," "				,"10020",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "美极鸭下巴","/mnt/sdcard/picture/cai11.jpg ","100257"," "," "				,"10020",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "孜然牛肉串","/mnt/sdcard/picture/cai11.jpg ","100258"," "," "				,"10020",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "孜然羊肉串","/mnt/sdcard/picture/cai11.jpg ","100259"," "," "				,"10020",null,"份","2002","","1","0","18.00");
    				insert_DishData(db, "椒盐鱿鱼丝","/mnt/sdcard/picture/cai11.jpg ","100260"," "," "				,"10020",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "香煎秋刀鱼","/mnt/sdcard/picture/cai11.jpg ","100261"," "," "				,"10020",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "潮式海鲜青瓜烙","/mnt/sdcard/picture/cai11.jpg ","100262"," "," "			,"10020",null,"份","2002","","1","0","20.00");

    				insert_DishData(db, "柠乐","/mnt/sdcard/picture/cai11.jpg ","100300"," "," "				,"10019",null,"份","2002","","1","0","7.00");
    				insert_DishData(db, "柠七","/mnt/sdcard/picture/cai11.jpg ","100301"," "," "				,"10019",null,"份","2002","","1","0","7.00");
    				insert_DishData(db, "奶茶","/mnt/sdcard/picture/cai11.jpg ","100302"," "," "				,"10019",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "玫瑰奶茶","/mnt/sdcard/picture/cai11.jpg ","100303"," "," "			,"10019",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "拉花奶茶","/mnt/sdcard/picture/cai11.jpg ","100304"," "," "			,"10019",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "香草奶茶","/mnt/sdcard/picture/cai11.jpg ","100305"," "," "			,"10019",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "红豆奶茶","/mnt/sdcard/picture/cai11.jpg ","100306"," "," "			,"10019",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "薄荷奶茶","/mnt/sdcard/picture/cai11.jpg ","100307"," "," "			,"10019",null,"份","2002","","1","0","16.00");
    				insert_DishData(db, "激爽薄荷","/mnt/sdcard/picture/cai11.jpg ","100308"," "," "			,"10019",null,"份","2002","","1","0","9.00");
    				insert_DishData(db, "激爽玫瑰","/mnt/sdcard/picture/cai11.jpg ","100309"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠檬汁","/mnt/sdcard/picture/cai11.jpg ","100310"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠蜜汁","/mnt/sdcard/picture/cai11.jpg ","100311"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠檬红茶","/mnt/sdcard/picture/cai11.jpg ","100312"," "," "			,"10019",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "蜂蜜红茶","/mnt/sdcard/picture/cai11.jpg","100313"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠檬苏打","/mnt/sdcard/picture/cai11.jpg ","100314"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "苏打红/苏打绿","/mnt/sdcard/picture/cai11.jpg ","100315"," "," "	,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠檬梅子","/mnt/sdcard/picture/cai11.jpg ","100316"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "柠檬芦荟","/mnt/sdcard/picture/cai11.jpg ","100317"," "," "			,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "甜鲜奶","/mnt/sdcard/picture/cai11.jpg ","100318"," "," "			,"10019",null,"份","2002","","1","0","10.00");
    				insert_DishData(db, "木瓜鲜奶","/mnt/sdcard/picture/cai11.jpg ","100319"," "," "			,"10019",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "蜂蜜鲜奶","/mnt/sdcard/picture/cai11.jpg ","100320"," "," "			,"10019",null,"份","2002","","1","0","10.00");
    				insert_DishData(db, "红豆鲜奶","/mnt/sdcard/picture/cai11.jpg ","100321"," "," "			,"10019",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "红豆木瓜鲜奶","/mnt/sdcard/picture/cai11.jpg ","100322"," "," "		,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "椰汁西米露","/mnt/sdcard/picture/cai11.jpg ","100323"," "," "		,"10019",null,"份","2002","","1","0","12.00");
    				insert_DishData(db, "椰汁红豆西米露","/mnt/sdcard/picture/cai11.jpg","100324"," "," "	,"10019",null,"份","2002","","1","0","15.00");
    				insert_DishData(db, "木瓜红豆西米露","/mnt/sdcard/picture/cai11.jpg ","100325"," "," "	,"10019",null,"份","2002","","1","0","20.00");
    				insert_DishData(db, "玫瑰花茶","/mnt/sdcard/picture/cai11.jpg ","100326"," "," "			,"10019",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "桂圆红枣润肺茶","/mnt/sdcard/picture/cai11.jpg ","100327"," "," "	,"10019",null,"份","2002","","1","0","25.00");
    				insert_DishData(db, "玫瑰红枣养颜茶","/mnt/sdcard/picture/cai11.jpg ","100328"," "," "	,"10019",null,"份","2002","","1","0","25.00");
    				
    				insert_DishData(db, "雀巢咖啡"," /mnt/sdcard/picture/cai11.jpg","100330"," "," "				,"10019","0001","份","2002","","1","0","10.00");
    				insert_DishData(db, "燕山咖啡","/mnt/sdcard/picture/cai11.jpg ","100331"," "," "				,"10019","0001","份","2002","","1","0","25.00");
    				insert_DishData(db, "冰摩卡"," /mnt/sdcard/picture/cai11.jpg","100332"," "," "				,"10019","0001","份","2002","","1","0","15.00");
    				insert_DishData(db, "冰拿铁","/mnt/sdcard/picture/cai11.jpg ","100333"," "," "				,"10019","0001","份","2002","","1","0","16.00");
    				insert_DishData(db, "卡布奇诺","/mnt/sdcard/picture/cai11.jpg ","100334"," "," "				,"10019","0001","份","2002","","1","0","20.00");
    				insert_DishData(db, "焦糖玛奇朵","/mnt/sdcard/picture/cai11.jpg ","100335"," "," "				,"10019","0001","份","2002","","1","0","20.00");
    				insert_DishData(db, "贵妇人咖啡","/mnt/sdcard/picture/cai11.jpg ","100336"," "," "				,"10019","0001","份","2002","","1","0","20.00");

    				insert_DishData(db, "芒果夏威夷","/mnt/sdcard/picture/cai11.jpg ","100337"," "," "				,"10019","0002","份","2002","","1","0","18.00");
    				insert_DishData(db, "哈密瓜夏威夷"," /mnt/sdcard/picture/cai11.jpg","100338"," "," "				,"10019","0002","份","2002","","1","0","15.00");
    				insert_DishData(db, "柳橙夏威夷","/mnt/sdcard/picture/cai11.jpg ","100339"," "," "				,"10019","0002","份","2002","","1","0","18.00");
    				insert_DishData(db, "红豆夏威夷","/mnt/sdcard/picture/cai11.jpg ","100340"," "," "				,"10019","0002","份","2002","","1","0","18.00");
    				insert_DishData(db, "芒果红豆夏威夷"," /mnt/sdcard/picture/cai11.jpg","100341"," "," "				,"10019","0002","份","2002","","1","0","20.00");
    				insert_DishData(db, "木瓜夏威夷"," /mnt/sdcard/picture/cai11.jpg","100342"," "," "				,"10019","0002","份","2002","","1","0","18.00");
    				insert_DishData(db, "木瓜红豆夏威夷"," /mnt/sdcard/picture/cai11.jpg","100343"," "," "				,"10019","0002","份","2002","","1","0","20.00");

    				insert_DishData(db, "西瓜汁","/mnt/sdcard/picture/cai11.jpg ","100344"," "," "				,"10019","0003","份","2002","","1","0","12.00");
    				insert_DishData(db, "苹果汁","/mnt/sdcard/picture/cai11.jpg ","100345"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    				insert_DishData(db, "哈密瓜汁"," /mnt/sdcard/picture/cai11.jpg","100346"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    				insert_DishData(db, "芒果汁","/mnt/sdcard/picture/cai11.jpg ","100347"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    				insert_DishData(db, "雪梨汁"," /mnt/sdcard/picture/cai11.jpg","100348"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    				insert_DishData(db, "柳橙汁"," /mnt/sdcard/picture/cai11.jpg","100349"," "," "				,"10019","0003","份","2002","","1","0","18.00");
    				insert_DishData(db, "木瓜汁"," /mnt/sdcard/picture/cai11.jpg","100350"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    				insert_DishData(db, "火龙果汁","/mnt/sdcard/picture/cai11.jpg ","100351"," "," "				,"10019","0003","份","2002","","1","0","15.00");
    			
    				insert_DishData(db, "小支哈啤","/mnt/sdcard/picture/cai11.jpg ","100352"," "," "				,"10019","0004","份","2002","","1","0","8.00");
    				insert_DishData(db, "小支百威","/mnt/sdcard/picture/cai11.jpg ","100353"," "," "				,"10019","0004","份","2002","","1","0","12.00");
    				insert_DishData(db, "灌装百威","/mnt/sdcard/picture/cai11.jpg ","100354"," "," "				,"10019","0004","份","2002","","1","0","10.00");
    				insert_DishData(db, "德克拉罐装"," /mnt/sdcard/picture/cai11.jpg","100355"," "," "				,"10019","0004","份","2002","","1","0","15.00");
    				insert_DishData(db, "万德利桶装5L"," /mnt/sdcard/picture/cai11.jpg","100356"," "," "				,"10019","0004","份","2002","","1","0","380.00");
    				
    				insert_DishData(db, "法国长罗红酒","/mnt/sdcard/picture/cai11.jpg ","100357"," "," "				,"10019","0005","份","2002","","1","0","98.00");
    				insert_DishData(db, "中粮长城解百纳"," /mnt/sdcard/picture/cai11.jpg","100358"," "," "				,"10019","0005","份","2002","","1","0","88.00");
    				insert_DishData(db, "中粮长城蛇龙珠","/mnt/sdcard/picture/cai11.jpg ","100359"," "," "				,"10019","0005","份","2002","","1","0","98.00");
    				insert_DishData(db, "金牌葡皇"," /mnt/sdcard/picture/cai11.jpg","100360"," "," "				,"10019",null,"份","2002","","1","0","298.00");

    				insert_DishData(db, "蘑菇牛柳红酒套餐","/mnt/sdcard/picture/cai11.jpg","20011","mgnlhjtc"," "				,"10001",null,null,"1","0","298.00","1","350.00");
    				insert_DishData(db, "百里香纽约大牛排套餐","/mnt/sdcard/picture/cai11.jpg","20012","blxnydnptc"," "				,"10001",null,null,"1","0","198.00","1","250.00");
    				insert_DishData(db, "海陆至尊披萨套餐","/mnt/sdcard/picture/cai11.jpg","20013","hlzzpstc"," "				,"10001",null,null,"1","0","268.00","1","300.00");
    				insert_DishData(db, "果核烤羊排套餐","/mnt/sdcard/picture/cai11.jpg","20014","ghkyptc"," "				,"10011",null,null,"1","0","24.00","1","35.00");
    				insert_DishData(db, "迷你小火锅","/mnt/sdcard/picture/cai11.jpg","20015","mnxhg"," "				,"10011",null,null,"1","0","28.00","1","31.00");
    				insert_DishData(db, "风味腊肠披萨套餐","/mnt/sdcard/picture/cai11.jpg","20016","fwlcpstc"," "				,"10011",null,null,"1","0","23.00","1","31.00");
    				insert_DishData(db, "咖喱牛肉饭套餐","/mnt/sdcard/picture/cai11.jpg","20018","glnrftc"," "				,"10011",null,null,"1","0","28.00","1","50.00");
    				insert_DishData(db, "番茄牛肉饭套餐","/mnt/sdcard/picture/cai11.jpg","20017","fqnrftc"," "				,"10011",null,null,"1","0","29.00","1","60.00");
    				insert_DishData(db, "咖喱鸡肉套餐","/mnt/sdcard/picture/cai11.jpg","20019","gljrftc"," "				,"10011",null,null,"1","0","29.00","1","40.00");
    				System.out.println(" insert 2");

    				insert_TaoCaiDishData(db, "20011","蘑菇牛柳红酒套餐",	"10035","美国牛仔骨扒","48.00");
    				insert_TaoCaiDishData(db, "20011","蘑菇牛柳红酒套餐",	"10060","老火靓汤","5.00" );
    				insert_TaoCaiDishData(db, "20011","蘑菇牛柳红酒套餐",	"100250" , "美食炸薯条","15.00");
    				insert_TaoCaiDishData(db, "20012","百里香纽约大牛排套餐","10031","特色大什扒","38.00");
    				insert_TaoCaiDishData(db, "20012","百里香纽约大牛排套餐","10061","罗宋汤 ","15.00");
    				insert_TaoCaiDishData(db, "20012","百里香纽约大牛排套餐","100258","孜然牛肉串","18.00");

    				insert_TaoCaiDishData(db, "20013","海陆至尊披萨套餐",	"10033","海陆至尊披萨","56.00" );
    				insert_TaoCaiDishData(db, "20013","海陆至尊披萨套餐",	"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20014","果核烤羊排套餐",		"10036","果核烤羊排","74.00" );
    				insert_TaoCaiDishData(db, "20014","果核烤羊排套餐",		"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20015","迷你小火锅",			"10061","罗宋汤","15.00" );
    				insert_TaoCaiDishData(db, "20015","迷你小火锅",			"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20016","风味腊肠披萨套餐",	"10025","风味腊肠披萨","25.00" );
    				insert_TaoCaiDishData(db, "20016","风味腊肠披萨套餐",	"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20018","咖喱牛肉饭套餐",		"10015","咖喱牛肉饭","53.00" );
    				insert_TaoCaiDishData(db, "20018","咖喱牛肉饭套餐",		"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20017","番茄牛肉饭套餐",		"10022","番茄牛肉饭","25.00" );
    				insert_TaoCaiDishData(db, "20017","番茄牛肉饭套餐",		"100344","西瓜汁","12.00" );
    				insert_TaoCaiDishData(db, "20019","咖喱鸡肉饭套餐",		"10011","咖喱鸡肉饭","15.00" );
    				insert_TaoCaiDishData(db, "20019","咖喱鸡肉饭套餐",		"100344","西瓜汁","12.00" );

    			  	//定义SharedPreferences对象  
    				SharedPreferences settings; 
    				settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
    				String	SPF_Pos_DayCheck_Data		=	"SPF_Pos_DayCheck_Data";
    				String	SPF_Pos_MonthCheck_Data		=	"SPF_Pos_MonthCheck_Data";
    		        SharedPreferences.Editor editor = settings.edit();          	
        	        editor.putString(SPF_Pos_DayCheck_Data, String.valueOf( "2000-12-12" ));  
        	        editor.putString(SPF_Pos_MonthCheck_Data, String.valueOf( "2000-12-12" ));  
        	        editor.commit();    	  	

    				
//    				insert_TT_Music_Bar();
   				System.out.println(" insert success");
    				db.setTransactionSuccessful();
    				long insert_finiash_time = Debug.threadCpuTimeNanos();

    				Log.i("info", "创建数据库时间 ： " + (create_finiash_time - start_time));  
    				Log.i("info", "插入数据时间 ： " + (insert_finiash_time - create_finiash_time));  

    			} catch (Exception e) {
    				// TODO: handle exception
    				System.out.println("Failed");
    			}
			} catch (Exception e) {
				// TODO: handle exception
			System.out.println("Create Failed");
    			handler.sendEmptyMessage(80);
			}
			finally{
				db.endTransaction();
			}
			db.close();
			handler.sendEmptyMessage(100);
			finish();
       }
    }

    
	private void insert_TaoCaiDishData(SQLiteDatabase db, 
			String cTaoCaiCode,	String cTaoCaiName, 
			String cCaiCode,	String cCaiName, 
			String cCaiPrice ) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_taocai_cailist values(null,?,?,?,?,?)", 
					new String[]{
				cTaoCaiCode ,cTaoCaiName ,	cCaiCode ,cCaiName ,cCaiPrice });		
	}

	/*******************************/
	public void insert_TT_Music_Bar() {
		//增加用户
		insert_UserData(db, "胡总","9999","9999", "经理","03" );	
		//增加桌台部分
		sql_control	sqlc = new sql_control();
		sqlc.insert_table_info(db, "lfcy_base_tablearea", "一楼大厅", "3001", null);
		sqlc.insert_table_info(db, "lfcy_base_tablearea", "一楼卡座区", "3002", null);
		sqlc.insert_table_info(db, "lfcy_base_tablearea", "二楼包厢区", "3003", null);

		sqlc.insert_table_info(db, "lfcy_base_tabletype", "散台", "4001", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "卡座", "4002", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "包厢", "4003", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "外卖", "4004", null);

		insert_TableData(db,"01","4002","3002","1",null,"10",null);				
		insert_TableData(db,"02","4002","3002","1",null,"10",null);				
		insert_TableData(db,"03","4001","3001","1",null,"8",null);				
		insert_TableData(db,"04","4001","3001","1",null,"6",null);				  				
		insert_TableData(db,"05","4001","3001","1",null,"6",null);				
		insert_TableData(db,"06","4001","3001","1",null,"6",null);				
		insert_TableData(db,"07","4001","3001","1",null,"6",null);				
		insert_TableData(db,"08","4001","3001","1",null,"6",null);				
		insert_TableData(db,"09","4002","3002","1",null,"10",null);				
		insert_TableData(db,"10","4002","3002","1",null,"10",null);	
		
		//增加菜品部分
		sqlc.insert_table_info(db, "lfcy_base_unit", "串", "2001", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "条", "2002", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "个", "2003", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "碟", "2004", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "支", "2005", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "罐", "2006", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "炮", "2007", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "打", "2008", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "半打", "2009", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "包", "2010", null);

		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "煎", "2101", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "炒", "2102", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "烤", "2103", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "炸", "2104", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "蒸", "2105", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "煮", "2106", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "烧", "2107", null);

		sqlc.insert_table_info(db, "lfcy_base_taste", "不辣", "1101", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "少辣", "1102", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "适中", "1103", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "辣",   "1104", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "香辣", "1105", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "麻辣", "1106", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "特辣", "1107", null);	
		
		insert_POSData(db,"0001","0001","Wal-MartStores","192.168.200.119");

		insert_BigSort(db, "烤蔬菜","10001",null );
		insert_BigSort(db, "肉类","10011",null );
		insert_BigSort(db, "饮料类","10012",null );
		insert_BigSort(db, "酒类","10013",null );
		
		insert_DishData(db, "火腿肠","/mnt/sdcard/picture/cai1.jpg","10001","htc","huotuichang","10011","0000","条", "2002"," "," ","0","1.00");
		insert_DishData(db, "豆腐","/mnt/sdcard/picture/cai2.jpg","10002","df"," "				,"10001","0000","串", "2001"," "," ","0","1.00");
		insert_DishData(db, "韭菜","/mnt/sdcard/picture/cai3.jpg","10003","jc"," "				,"10001","0000","串", "2005"," "," ","0","1.00");
		insert_DishData(db, "蒜苔","/mnt/sdcard/picture/cai4.jpg","10004"," "," "				,"10001","0000","串", "2005"," "," ","0","1.50");
		insert_DishData(db, "草菇","/mnt/sdcard/picture/cai5.jpg","10005"," "," "				,"10001","0000","串", "2005"," "," ","0","1.50");
		insert_DishData(db, "热狗","/mnt/sdcard/picture/cai6.jpg","10006"," "," "				,"10011","0000","串", "2005"," "," ","0","2.00");
		insert_DishData(db, "鸡爪","/mnt/sdcard/picture/cai7.jpg","10007"," "," "				,"10011","0000","串", "2005"," ","2","0","3.00");
		insert_DishData(db, "玉米","/mnt/sdcard/picture/cai8.jpg","10008"," "," "				,"10001","0000","串", "2005"," "," ","0","4.00");
		insert_DishData(db, "秋刀鱼","/mnt/sdcard/picture/cai9.jpg","10009"," "," "			,"10011","0000","条", "2002"," "," ","0","7.00");
		insert_DishData(db, "干鱿鱼","/mnt/sdcard/picture/cai10.jpg","10010"," "," "			,"10011","0000","串", "2005"," ","3","0","7.00");
		insert_DishData(db, "排骨","/mnt/sdcard/picture/cai11.jpg","10011"," "," "				,"10011","0000","串", "2005"," "," ","0","7.00");
		insert_DishData(db, "鸡中翅","/mnt/sdcard/picture/cai2.jpg","10012"," "," "					,"10011","0000","串", "2005"," "," ","0","7.00");
		insert_DishData(db, "鸡腿","/mnt/sdcard/picture/cai13.jpg","100013"," "," "					,"10011","0000","串", "2005"," ","2","0","8.00");
		insert_DishData(db, "金针菇","/mnt/sdcard/picture/cai1.jpg","100014"," "," "				,"10001","0000","包", "2005"," "," ","0","10.00");
		insert_DishData(db, "鸡杂","/mnt/sdcard/picture/cai15.jpg","100015"," "," "					,"10011","0000","串", "2005"," "," ","0","10.00");
		insert_DishData(db, "湿鱿鱼","/mnt/sdcard/picture/cai16.jpg","100016"," "," "				,"10011","0000","串", "2005"," "," ","0","15.00");
		insert_DishData(db, "鲜肥牛","/mnt/sdcard/picture/cai17.jpg","100017"," "," "				,"10011","0000","打", "2005"," "," ","0","20.00");
		insert_DishData(db, "鲜肥羊","/mnt/sdcard/picture/cai18.jpg","100018"," "," "				,"10011","0000","打", "2005"," "," ","0","20.00");
		insert_DishData(db, "多春鱼","/mnt/sdcard/picture/cai19.jpg","100019"," "," "				,"10011","0000","碟", "2005"," "," ","0","20.00");
		insert_DishData(db, "生蚝","/mnt/sdcard/picture/cai20.jpg","100020"," "," "					,"10011","0000","碟", "2005"," "," ","0","68.00");
		insert_DishData(db, "莲藕","/mnt/sdcard/picture/cai21.jpg","100021"," "," "					,"10001","0000","串", "2005"," ","1","0","1.50");
		insert_DishData(db, "基围虾","/mnt/sdcard/picture/cai22.jpg","100022"," "," "				,"10011","0000","碟", "2004"," "," ","0","5.00");
		insert_DishData(db, "蚕豆","/mnt/sdcard/picture/cai23.jpg","100023"," "," "					,"10001","0000","碟", "2004"," "," ","0","3.00");
		insert_DishData(db, "花生","/mnt/sdcard/picture/cai24.jpg","100024"," "," "					,"10001","0000","碟", "2004"," "," ","0","3.00");
		insert_DishData(db, "芥辣青瓜","/mnt/sdcard/picture/cai_2.jpg","100025"," "," "				,"10001","0000","碟", "2004"," ","2 ","0","8.00");
		insert_DishData(db, "可口可乐","/mnt/sdcard/picture/cai1.jpg","100026"," "," "				,"10012","0000","罐", "2006"," "," ","0","5.00");
		insert_DishData(db, "雪碧","/mnt/sdcard/picture/cai2.jpg","100027"," "," "					,"10012","0000","罐", "2006"," "," ","0","5.00");
		insert_DishData(db, "椰汁","/mnt/sdcard/picture/cai3.jpg","100028"," "," "					,"10012","0000","罐", "2006"," "," ","0","6.00");
		insert_DishData(db, "王老吉","/mnt/sdcard/picture/cai4.jpg","100029",""," "				,"10012","0000","罐", "2006"," "," ","0","6.00");
		insert_DishData(db, "脉动","/mnt/sdcard/picture/cai11.jpg","100030"," "," "					,"10012","0000","瓶", "2021"," "," ","0","6.00");
		insert_DishData(db, "百威灌装","/mnt/sdcard/picture/cai21.jpg","100031"," "," "				,"10013","0000","打", "2008"," "," ","0","98.00");
		insert_DishData(db, "哈啤灌装","/mnt/sdcard/picture/cai7.jpg","100032"," "," "				,"10013","0000","打", "2008"," "," ","0","78.00");
		insert_DishData(db, "鲜啤","/mnt/sdcard/picture/cai8.jpg","100033"," "," "					,"10013","0000","炮", "2007"," ","2","0","48.00");
		insert_DishData(db, "黑啤(配脉动5支)","/mnt/sdcard/picture/cai9.jpg","100034"," "," "		,"10013","0000","支", "2005"," "," ","0","380.00");
		insert_DishData(db, "芝华士(配脉动5支)","/mnt/sdcard/picture/cai14.jpg","100035"," "," "		,"10013","0000","支", "2005"," "," ","0","380.00");
		insert_DishData(db, "杰克丹尼(配脉动5支)","/mnt/sdcard/picture/cai17.jpg","100036"," "," "	,"10013","0000", "支", "2005"," "," ","0","280.00");
	}
	/*******************************/

	public void init_new2() {
		// TODO Auto-generated method stub
		sql_control	sqlc = new sql_control();
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 12:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 13:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 13:55" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 14:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 15:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 16:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-10 17:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-11 12:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-11 13:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-11 14:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		
		sqlc.insert_sell_info(db, "1 ", "11 ", "111 ",  "2013-11-11 15:25" ,null ,null ,null ,
				null ,null ,null ,null ,null );		

	}

	public void init_new() {
		// TODO Auto-generated method stub
		sql_control	sqlc = new sql_control();
		sqlc.insert_table_info(db, "lfcy_base_unit", "斤", "2001", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "份", "2002", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "碟", "2003", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "套", "2004", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "支", "2005", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "厅", "2006", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "罐", "2007", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "打", "2008", null);
		sqlc.insert_table_info(db, "lfcy_base_unit", "半打", "2009", null);

		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "煎", "2101", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "炒", "2102", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "烹", "2103", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "炸", "2104", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "蒸", "2105", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "煮", "2106", null);
		sqlc.insert_table_info(db, "lfcy_base_dishtaste", "烧", "2107", null);

		sqlc.insert_table_info(db, "lfcy_base_taste", "酸", 	"1101", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "甜", "1102", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "苦", "1103", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "辣", "1104", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "香辣", "1105", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "麻辣", "1106", null);
		sqlc.insert_table_info(db, "lfcy_base_taste", "特辣", "1107", null);

		sqlc.insert_table_info(db, "lfcy_base_tablearea", "一楼大厅", "3001", null);
		sqlc.insert_table_info(db, "lfcy_base_tablearea", "一楼包厢区", "3002", null);
		sqlc.insert_table_info(db, "lfcy_base_tablearea", "二楼包厢区", "3003", null);

		sqlc.insert_table_info(db, "lfcy_base_tabletype", "散台", "4001", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "卡座", "4002", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "包厢", "4003", null);
		sqlc.insert_table_info(db, "lfcy_base_tabletype", "外卖", "4003", null);
	}

	public void insert_DishData(SQLiteDatabase db, String cDishName, String cDishName2,
			String cDishCode, String cDishPinYin, String cDishHelpCode, String cDishClass1,
			String cDishClass2, String cDishUnitName , String cDishUnitCode ,String cDishOther, 
			String iDishStyle,String iDishDian, String fDishPrice) {

		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_dishinf values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,null,null)", 
					new String[]{
				cDishName ,cDishName2 ,	cDishCode ,cDishPinYin ,
				cDishHelpCode , cDishClass1 , cDishClass2  ,cDishUnitName,cDishUnitCode,
				cDishOther ,iDishStyle ,iDishDian ,	fDishPrice });
	}
/*
	//套菜
	public void insert_DishData(SQLiteDatabase db, String cDishName, String cDishName2,
			String cDishCode, String cDishPinYin, String cDishHelpCode, String cDishClass1,
			String cDishClass2, String cDishOther, String iDishStyle,String iDishDian, String fDishPrice,
			String cIfTaocai , String fTaocaiPrice) {

		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_dishinf values(null,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
					new String[]{
				cDishName ,cDishName2 ,	cDishCode ,cDishPinYin ,
				cDishHelpCode , cDishClass1 , cDishClass2  ,cDishOther , 
				iDishStyle ,iDishDian ,	fDishPrice ,cIfTaocai ,fTaocaiPrice});
	}
*/	
	public void insert_BigSort(SQLiteDatabase db, String cName,
			String cCode, String cMemo) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_bigsort values(null,?,?,?)", 
					new String[]{
					cName,cCode,cMemo
				 });	
	}

	public void insert_SmallSort(SQLiteDatabase db, String cBigCode,
			String cName,String cCode, String cMemo) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_smallsort values(null,?,?,?,?)", 
					new String[]{
					cBigCode,cName,cCode,cMemo
				 });	
	}
	
	public void insert_POSData(SQLiteDatabase db2, String iPosMachineNum,
			String iPosShopNum, String iPosShopName, String iPosIp) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_posinf values(null,?,?,?,?)", 
				new String[]{
				iPosMachineNum , iPosShopNum , iPosShopName , iPosIp
		});		
	}

	public void insert_UserData(SQLiteDatabase db2,String cUserName, String cUserCode ,
			String iUserPassword, String cUserManager, String iUserLimit) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_user_inf values(null,?,?,?,?,?)", 
				new String[]{
				cUserName , cUserCode ,iUserPassword , cUserManager , iUserLimit
		});
	}

	public void insert_TableData(SQLiteDatabase db2, String iTableNumber,
			String iTableClass, String iTableClass2, String iTableState, String iTabletime,
			String iTablePerson, String  iTable_Person) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_tableinf values(null,?,?,?,?,?,?,?)", 
					new String[]{
				iTableNumber ,iTableClass ,	iTableClass2 ,iTableState ,
				iTabletime , iTablePerson , iTable_Person });		
			
	};	
}