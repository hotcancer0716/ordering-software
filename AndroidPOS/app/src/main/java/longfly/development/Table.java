package longfly.development;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import casio.serial.SerialPort;
import longfly.development.R.id;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class Table extends Activity {
	
	TextView	ALL_TABLE;
	TextView	ALL_FREE_TABLE;
	GridView	GRIDVIEW_TABLE;
	Button		FRONTPAGE;
	Button		NEXTPAGE;
	TextView	NOWPAGE;
	TextView	ALLPAGE;
	Button		LOGINOUT;
	Button		SHOW_ALLTABLE;
	Button		SHOW_FREETABLE;
	Button		SHOW_SPACETABLE_1;
	Button		SHOW_SPACETABLE_2;

	/***************以下是窗口里的控件***********************/
	Button		NUM_0;
	Button		NUM_1;
	Button		NUM_2;
	Button		NUM_3;
	Button		NUM_4;
	Button		NUM_5;
	Button		NUM_6;
	Button		NUM_7;
	Button		NUM_8;
	Button		NUM_9;
	Button		NUM_DEL;
	Button		NUM_CLEAR;
	Button		NUM_OK;
	
	Button			FIND_TABLE_NUM;
	/*********以下是对话框里的控件***********/
	
	Button	    DAILOG_BTN_KAITAI;	
	Button	    DAILOG_BTN_DIANCAI;	
	Button	    DAILOG_BTN_JIACAI;	
	Button	    DAILOG_BTN_CHACAI;	
	Button	    DAILOG_BTN_JIEZHANG;	
	Button	    DAILOG_BTN_XIAOTAI;	
	Button	    DAILOG_BTN_ZHUANTAI;	
	Button	    DAILOG_BTN_CUICAI;	

	TextView	DAILOG_TEXT_TABLE_NUM;
	TextView	DAILOG_TEXT_TABLE_SELLNUM;
	
	
	String		Share_Table_Num;	
	String		Share_Table_State;		
	String 		num = null ;			
	String[] Table_Type_Name ,Table_Type_Code;
	/*********以上是对话框里的控件***********/
	/**************************************/
	TextView	DAILOG_CHECK_TEXT_TAIHAO;
	TextView	DAILOG_CHECK_TEXT_DANHAO;
	TextView	DAILOG_CHECK_TEXT_KAITAISHIJIAN;
	TextView	DAILOG_CHECK_TEXT_RENSHU;
	TextView	DAILOG_CHECK_TEXT_KAITAIREN;
	TextView	DAILOG_CHECK_TEXT_XIAOFEI;
	TextView	DAILOG_CHECK_TEXT_JIASHOU;
	TextView	DAILOG_CHECK_TEXT_ZHEKOU;
	TextView	DAILOG_CHECK_TEXT_ZONGJI;
	TextView	DAILOG_CHECK_TEXT_YINGSHOU;
	TextView	DAILOG_CHECK_TEXT_XIANSJIN;
	TextView	DAILOG_CHECK_TEXT_ZHAOLING;
	TextView	DAILOG_CHECK_TEXT_KAOHAO;
	TextView	DAILOG_CHECK_TEXT_JIFEN;
	TextView	DAILOG_CHECK_TEXT_KAYUKE;
	TextView	DAILOG_CHECK_TEXT_LEIXING;
	
	Button		DAILOG_BTN_CHECK_SK;
	Button		DAILOG_BTN_CHECK_0 ;	
	Button		DAILOG_BTN_CHECK_1 ;	
	Button		DAILOG_BTN_CHECK_2 ;	
	Button		DAILOG_BTN_CHECK_3 ;	
	Button		DAILOG_BTN_CHECK_4 ;	
	Button		DAILOG_BTN_CHECK_5 ;	
	Button		DAILOG_BTN_CHECK_6 ;	
	Button		DAILOG_BTN_CHECK_7 ;	
	Button		DAILOG_BTN_CHECK_8 ;	
	Button		DAILOG_BTN_CHECK_9 ;	
	Button		DAILOG_BTN_CHECK_CLEAR ;	
	Button		DAILOG_BTN_CHECK_DEL ;	
	Button		DAILOG_BTN_CHECK ;	
	Button		DAILOG_BTN_EXIT ;	
	
	/*********以下是开台对话框里的控件***********/	
	Button	    DAILOG_BTN_KAITAI2;	
	Button	    DAILOG_BTN_DIANCAI2;	
	Button	    DAILOG_BTN_CANCEL;	
	
	Button		DAILOG_BTN_PER_0;
	Button		DAILOG_BTN_PER_1;
	Button		DAILOG_BTN_PER_2;
	Button		DAILOG_BTN_PER_3;
	Button		DAILOG_BTN_PER_4;
	Button		DAILOG_BTN_PER_5;
	Button		DAILOG_BTN_PER_6;
	Button		DAILOG_BTN_PER_7;
	Button		DAILOG_BTN_PER_8;
	Button		DAILOG_BTN_PER_9;
	Button		DAILOG_BTN_PER_DEL;
	
	TextView	DAILOG_TEXT_PERSON_NUM;				
	TextView	DAILOG_TEXT_DANHAO;				
	/*********以上是对话框里的控件***********/
	
	private	int Dispay_widthPixels;
   	private int DIALOG_FIND=101;
   	private int DIALOG_KAITAI=102;
   	private int DIALOG_JIEZHANG=103;
	private List<Grid_Item_Table> mylist;
	private	GridItemTableSQLAdapter	adapterr;
	SQLiteDatabase db;
	String	UserLimit;
	String	UserCode;
	protected OutputStream mOutputStream;		//串口输出描述
    private final int UPDATE_UI = 1;
    boolean	ref_if = true;
    Handler mHandler; 
    int nowpage = 1;
    String  SQL_String;
    private GestureDetector mGestureDetector;      
    private LinkedHashMap<String, Integer> mRecordMap = new LinkedHashMap<String, Integer>();  

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		DisplayMetrics displaysMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displaysMetrics );
        Dispay_widthPixels=displaysMetrics.widthPixels;	//以上是初始化一个UI像素的描述！
        long	startTime =	Debug.threadCpuTimeNanos(); 
		switch (Dispay_widthPixels) {	//根据屏幕的横向像素来加载不通的UI界面！
			case 800:
		        setContentView(R.layout.i800_table2);
		        break;
			case 1024:
		        setContentView(R.layout.i800_table2);
				break;
			default:
				break;
		}          
        long	endXMLTime =	Debug.threadCpuTimeNanos(); 
		Log.i("info", "执行setContentView(R.layout.i800_table)用时"+(endXMLTime - startTime) +"纳秒");  
		db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);

		find_id();
        key_things();
        init_Table_Type_and_Quyu();
        init_grid(1);
        Refresh_State();
        init_data();		//初始化从上一页获取的导入数据
        
        //启动刷新桌台的线程
        mHandler = new MyHandler();//创建Handler 
        time_ref.start();

        long	endLoadTime =	Debug.threadCpuTimeNanos(); 
		Log.i("info", "载入数据用时"+(endLoadTime - endXMLTime) +"纳秒");  
        mGestureDetector = new GestureDetector(this, new DefaultGestureListener());  

    }

	@Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
        init_grid(1);
        Refresh_State();   
        ref_if = true;
        System.out.println("onResume");
    }

    /********************定时刷新桌台的线程开启*********************************************/
	MyThread time_ref = new MyThread();		//接收PC端数据的线程

	//接收PC端数据的线程
	class MyThread extends Thread 
	{  

		public void run() 
		{
			Time_Ref();
		}
	}   
    

	private void Time_Ref() {
		// TODO Auto-generated method stub
		for(;;)
		{
			try {
				Thread.sleep(1000);
		    	if(ref_if)
		    	{
	                //UPDATE是一个自己定义的整数，代表了消息ID 
	                Message msg = mHandler.obtainMessage(UPDATE_UI); 
	                mHandler.sendMessage(msg); 
	                System.out.println(" ref_if = true");
		    	}
		    	else{
		            System.out.println(" ref_if = flash");
		    	}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 			
		}	
	}	
	
    private class MyHandler extends Handler 
    {

        @Override 
        public void handleMessage(Message msg) { 
            // TODO Auto-generated method stub 
            super.handleMessage(msg); 
            switch(msg.what) 
            { 
	            case UPDATE_UI://在收到消息时，对界面进行更新 	            	
	                init_grid(nowpage);
	                Refresh_State();   
	                break; 
            } 
        } 
    }
    /********************定时刷新桌台的线程结束*********************************************/
	private void init_data() {
		// TODO Auto-generated method stub
		Bundle bundle=getIntent().getExtras();
		  //获取Bundle的信息
		UserCode = bundle.getString("UserCode");
		UserLimit  = bundle.getString("UserLimit");
	}
	
    //刷新桌面的空闲台数
	private void Refresh_State() {
		// TODO Auto-generated method stub
		int	All_Table=0,Free_Table=0;
		String[]	Sql_String_condition = { "1" };	

//		db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
		//查全部的桌台
		try {
    		Cursor  cursor = db.rawQuery("select * from lfcy_base_tableinf", null);
			while (cursor.moveToNext()) {
				All_Table ++;
			}
			ALL_TABLE.setText(""+All_Table);
			cursor.close();
		} catch (Exception e) {
			ALL_TABLE.setText("");
		}	

		//空闲部的桌台
		try {
    		Cursor  cursor = db.rawQuery("select * from lfcy_base_tableinf where iTableState = ?", Sql_String_condition);
			while (cursor.moveToNext()) {
				Free_Table ++;
			}
			ALL_FREE_TABLE.setText(""+Free_Table);
			cursor.close();
		} catch (Exception e) {
			ALL_FREE_TABLE.setText("");
		}
	}

	 
    private void init_Table_Type_and_Quyu() {
		// TODO Auto-generated method stub
 	    Cursor  cursor = db.rawQuery("select * from lfcy_base_tabletype", null);
		Table_Type_Name = new String[cursor.getCount()];
		Table_Type_Code = new String[cursor.getCount()];
 	    int i=0;
		while(cursor.moveToNext()) {
			Table_Type_Name[i] = cursor.getString(cursor.getColumnIndex("cName"));		
			Table_Type_Code[i] = cursor.getString(cursor.getColumnIndex("cCode"));		
			i++;
		}		
	}
	private int Traversal(String[] Table_Where_Code, String string) {
		// TODO Auto-generated method stub
		int i = 0;
		for (int j = 0; j < Table_Where_Code.length; j++) {
			if( Table_Where_Code[j].trim().equals(string) )
			{
				i = j;
	    		Log.i("info", "i  = "+ i);  
			}	
		}
		return i;			
	}   
	private void init_grid( int pageID ) {	//当前页数
		mylist = new ArrayList<Grid_Item_Table>();
		String TABLE_NAME = "lfcy_base_tableinf";
		int PageSize = 30 ;
		String sql= "select * from " + TABLE_NAME +     
        " Limit "+String.valueOf(PageSize)+ " Offset " +String.valueOf( (pageID-1)*PageSize); 
		try {
    		Cursor  cursor = db.rawQuery( sql, null);
			while (cursor.moveToNext()) 
			{
				Grid_Item_Table picture = new Grid_Item_Table();
				picture.setImageId(cursor.getInt(cursor
						.getColumnIndex("iTableState")));
				picture.setTableState(cursor.getString(cursor
						.getColumnIndex("iTableState")));	//桌台的状态（空闲还是开台）
				picture.setTime(cursor.getString(cursor
						.getColumnIndex("iTabletime")));	//开台的时间			
				picture.setNumber(cursor.getString(cursor
						.getColumnIndex("iTableNumber")));	//台号
				int num = Traversal(Table_Type_Code,cursor.getString(cursor.getColumnIndex("iTableClass")));				
				picture.setTablePlace(Table_Type_Name[num]);	//桌台类型（大厅还是包厢）
				picture.setTablePersonNumber(cursor.getString(cursor
						.getColumnIndex("iTablePerson")));	//可容纳人数
				picture.setTable_PersonNumber(cursor.getString(cursor
						.getColumnIndex("iTable_Person")));	//可容纳人数

				mylist.add(picture);
			}
			NOWPAGE.setText(""+pageID);
			if (pageID == 1)
				ALLPAGE.setText(   String.valueOf( cursor.getCount()/30 + 1 )   );
			cursor.close();
//			db.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		adapterr = new GridItemTableSQLAdapter(mylist, this , Dispay_widthPixels);// 自定义适配器
		GRIDVIEW_TABLE.setAdapter(adapterr);
	}

	private void key_things() {
		// TODO Auto-generated method stub
		 GRIDVIEW_TABLE.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
                 Share_Table_Num 	=	mylist.get(position).getNumber();
                 Share_Table_State	=	mylist.get(position).getSringTableState();
                 Integer intObj = new Integer( Share_Table_State );
			   	 int i = intObj.intValue();
			   	 if (i == 1) {
	                 showDialog(DIALOG_KAITAI);					
			   	 } else {
	                 showDialog(DIALOG_FIND);
			   	 }   
             }  
         });     
		 
			FRONTPAGE.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		Integer intObj = new Integer( NOWPAGE.getText().toString() );
			   	    int i = intObj.intValue();	
			   	    if(i == 1)
			   	    {
			   	    	Toast.makeText(Table.this, "已经是最前一页了", Toast.LENGTH_SHORT).show();
			   	    }
			   	    else
			   	    {	
			   	    	init_grid( i-1);
			   	    	NOWPAGE.setText(""+(i-1));
			   	    	nowpage = i-1;
			   	    }	
			   	}
		    });	
			NEXTPAGE.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		Integer intObj = new Integer( NOWPAGE.getText().toString() );
			   	    int i = intObj.intValue();	

			   		intObj = new Integer( ALLPAGE.getText().toString() );
			   	    int j = intObj.intValue();	
			   	    
			   	    if(i == j)
			   	    {
			   	    	Toast.makeText(Table.this, "已经是最后一页了", Toast.LENGTH_SHORT).show();
			   	    }
			   	    else
			   	    {	
			   	    	init_grid( i+1);
			   	    	NOWPAGE.setText(""+(i+1));
			   	    	nowpage = i+1;
			   	    }				   	 
			   	}
		    });	
			
			LOGINOUT.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					db.close();
					finish();
				}
			});
			SHOW_ALLTABLE.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
					turn_off_all();
					SHOW_ALLTABLE.setBackgroundResource(R.drawable.tablestyle_choose);			   	
					init_grid( 1);	
			   	}
		    });			
			SHOW_FREETABLE.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
					turn_off_all();
					SHOW_FREETABLE.setBackgroundResource(R.drawable.tablestyle_choose);			   	
					String		Sql_String="select * from lfcy_base_tableinf where iTableState = ?";
					String[]	Sql_String_condition = { "1" };	
					RefreshGrid(Sql_String ,Sql_String_condition);				
				}
		    });				
			SHOW_SPACETABLE_1.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
					turn_off_all();
					SHOW_SPACETABLE_1.setBackgroundResource(R.drawable.tablestyle_choose);			   	
					String		Sql_String="select * from lfcy_base_tableinf where iTableClass = ?";
					String[]	Sql_String_condition = { "3" };	
					RefreshGrid(Sql_String ,Sql_String_condition);				
				}
		    });				
			SHOW_SPACETABLE_2.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
					turn_off_all();
					SHOW_SPACETABLE_2.setBackgroundResource(R.drawable.tablestyle_choose);			   	
					String		Sql_String="select * from lfcy_base_tableinf where iTableClass = ?";
					String[]	Sql_String_condition = { "2" };	
					RefreshGrid(Sql_String ,Sql_String_condition);				
				}
		    });	
			
			NUM_0.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"0");	
				}
		    });		
			NUM_1.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"1");	
				}
		    });				
			NUM_2.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"2");	
				}
		    });				
			NUM_3.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"3");	
				}
		    });				
			NUM_4.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"4");	
				}
		    });				
			NUM_5.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"5");	
				}
		    });				
			NUM_6.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"6");	
				}
		    });				
			NUM_7.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"7");	
				}
		    });				
			NUM_8.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"8");	
				}
		    });				
			NUM_9.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
			   		FIND_TABLE_NUM.setText(num+"9");	
				}
		    });				
			NUM_DEL.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		try {
				   		String str = null;
				   		num =  FIND_TABLE_NUM.getText().toString();
			   			str=num.substring(0,num.length()-1);
				   		FIND_TABLE_NUM.setText(str);						
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
		    });					
			NUM_CLEAR.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		FIND_TABLE_NUM.setText("");	
				}
		    });					
			NUM_OK.setOnClickListener(new OnClickListener()
		    {
			   	@Override
			   	public void onClick(View v)
			   	{	
			   		num =  FIND_TABLE_NUM.getText().toString();
					String		Sql_String="select * from lfcy_base_tableinf where iTableNumber = ?";
					String[]	Sql_String_condition = { ""+num };	
					RefreshGrid(Sql_String ,Sql_String_condition);					
			   		FIND_TABLE_NUM.setText("");	
			   		
			   		if(num.trim().equals("0716"))
			   		{
			   			finish();
			   		}	
			   	}
		    });		

	}
	
	protected void RefreshGrid(String SQL_String , String[] Sql_String_condition) {
		// TODO Auto-generated method stub
		// 用于初始化
//		db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
		mylist = new ArrayList<Grid_Item_Table>();

		try {
    		Cursor  cursor = db.rawQuery(SQL_String, Sql_String_condition);
			while (cursor.moveToNext()) {
				Grid_Item_Table picture = new Grid_Item_Table();
				picture.setImageId(cursor.getInt(cursor
						.getColumnIndex("iTableState")));
				picture.setTableState(cursor.getString(cursor
						.getColumnIndex("iTableState")));	//桌台的状态（空闲还是开台）
				picture.setTime(cursor.getString(cursor
						.getColumnIndex("iTabletime")));	//开台的时间			
				picture.setNumber(cursor.getString(cursor
						.getColumnIndex("iTableNumber")));	//台号
				picture.setTablePlace(cursor.getString(cursor
						.getColumnIndex("iTableClass")));	//桌台类型（大厅还是包厢）
				picture.setTablePersonNumber(cursor.getString(cursor
						.getColumnIndex("iTablePerson")));	//可容纳人数
				picture.setTable_PersonNumber(cursor.getString(cursor
						.getColumnIndex("iTable_Person")));	//可容纳人数

				mylist.add(picture);
			}
			cursor.close();
//			db.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		adapterr = new GridItemTableSQLAdapter(mylist, this , Dispay_widthPixels);// 自定义适配器
		GRIDVIEW_TABLE.setAdapter(adapterr);		
	}

	private void turn_off_all() {
		// TODO Auto-generated method stub
		SHOW_ALLTABLE.setBackgroundResource(R.drawable.tablestyle_unchoose);			   	
		SHOW_FREETABLE.setBackgroundResource(R.drawable.tablestyle_unchoose);			   	
		SHOW_SPACETABLE_1.setBackgroundResource(R.drawable.tablestyle_unchoose);			   	
		SHOW_SPACETABLE_2.setBackgroundResource(R.drawable.tablestyle_unchoose);			   	
	}
	private void find_id() {
		// TODO Auto-generated method stub
		GRIDVIEW_TABLE	= (GridView) findViewById(id.all_tables);
		ALL_TABLE 		= (TextView) findViewById(id.table_all);
		ALL_FREE_TABLE	= (TextView) findViewById(id.table_free);
		FRONTPAGE		= (Button) 	 findViewById(id.table_page_front);
		NEXTPAGE		= (Button) 	 findViewById(id.table_page_next);
		NOWPAGE			= (TextView) findViewById(id.table_page_now);
		ALLPAGE			= (TextView) findViewById(id.table_page_all);
		LOGINOUT		= (Button) 	findViewById(id.table_loginout);
		SHOW_ALLTABLE	= (Button) findViewById(id.table_show_all);
		SHOW_FREETABLE	= (Button) findViewById(id.table_show_free);
		SHOW_SPACETABLE_1	= (Button) findViewById(id.table_show_other_1);
		SHOW_SPACETABLE_2	= (Button) findViewById(id.table_show_other_2);

		SHOW_ALLTABLE.setText("全部");
		SHOW_FREETABLE.setText("空闲");
		SHOW_SPACETABLE_1.setText("卡座");
		SHOW_SPACETABLE_2.setText("包厢");
		
		NUM_0 = (Button) findViewById(id.table_btn_0);
		NUM_1 = (Button) findViewById(id.table_btn_1);
		NUM_2 = (Button) findViewById(id.table_btn_2);
		NUM_3 = (Button) findViewById(id.table_btn_3);
		NUM_4 = (Button) findViewById(id.table_btn_4);
		NUM_5 = (Button) findViewById(id.table_btn_5);
		NUM_6 = (Button) findViewById(id.table_btn_6);
		NUM_7 = (Button) findViewById(id.table_btn_7);
		NUM_8 = (Button) findViewById(id.table_btn_8);
		NUM_9 = (Button) findViewById(id.table_btn_9);
		NUM_DEL		= (Button) findViewById(id.table_btn_del);
		NUM_CLEAR	= (Button) findViewById(id.table_btn_clear);
		NUM_OK		= (Button) findViewById(id.table_btn_ok);
		
		FIND_TABLE_NUM= (Button) findViewById(id.table_edit_num);
	}

	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	

	/***********  以下是结账对话框       *********************/
    private Dialog buildCheckDialog(Context context) {
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("结账消费管理");  
        LayoutInflater inflater = LayoutInflater.from(Table.this);
        View view = null ;
        ref_if = false;
        switch (Dispay_widthPixels) {
			case 1024:
		        view = inflater.inflate(R.layout.i800dialog_cai_check, null);			
				break;
			case 800:
		        view = inflater.inflate(R.layout.i800dialog_cai_check, null);			
				break;
			default:
				break;
		}
        builder.setView(view);
        builder.setCancelable(false);
 
        
        DAILOG_CHECK_TEXT_TAIHAO		=	(TextView) view.findViewById(id.dialog_check_tx_taihao);
        DAILOG_CHECK_TEXT_DANHAO		=	(TextView) view.findViewById(id.dialog_check_tx_danhao);
    	DAILOG_CHECK_TEXT_KAITAISHIJIAN =	(TextView) view.findViewById(id.dialog_check_tx_kaitaishijian);
    	DAILOG_CHECK_TEXT_RENSHU 		=	(TextView) view.findViewById(id.dialog_check_renshu);
    	DAILOG_CHECK_TEXT_KAITAIREN 	=	(TextView) view.findViewById(id.dialog_check_kaitairen);
    	DAILOG_CHECK_TEXT_XIAOFEI 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_xiaofei);
    	DAILOG_CHECK_TEXT_JIASHOU 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_jiashou);
    	DAILOG_CHECK_TEXT_ZHEKOU 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_zhekou);
    	DAILOG_CHECK_TEXT_ZONGJI 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_zongji);
    	DAILOG_CHECK_TEXT_YINGSHOU 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_yingshou);
    	DAILOG_CHECK_TEXT_XIANSJIN 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_xianjin);
    	DAILOG_CHECK_TEXT_ZHAOLING 		=	(TextView) view.findViewById(id.dialog_check_block2_tx_zhaoling);
    	DAILOG_CHECK_TEXT_KAOHAO 		=	(TextView) view.findViewById(id.dialog_check_kahao);
    	DAILOG_CHECK_TEXT_JIFEN 		=	(TextView) view.findViewById(id.dialog_check_jifen);
    	DAILOG_CHECK_TEXT_KAYUKE 		=	(TextView) view.findViewById(id.dialog_check_yue);
        DAILOG_CHECK_TEXT_LEIXING		=	(TextView) view.findViewById(id.dialog_check_leixing);

        DAILOG_BTN_CHECK_0	=	(Button) view.findViewById(id.dialog_check_btn_control_0);
        DAILOG_BTN_CHECK_1	=	(Button) view.findViewById(id.dialog_check_btn_control_1);
        DAILOG_BTN_CHECK_2	=	(Button) view.findViewById(id.dialog_check_btn_control_2);
        DAILOG_BTN_CHECK_3	=	(Button) view.findViewById(id.dialog_check_btn_control_3);
        DAILOG_BTN_CHECK_4	=	(Button) view.findViewById(id.dialog_check_btn_control_4);
        DAILOG_BTN_CHECK_5	=	(Button) view.findViewById(id.dialog_check_btn_control_5);
        DAILOG_BTN_CHECK_6	=	(Button) view.findViewById(id.dialog_check_btn_control_6);
        DAILOG_BTN_CHECK_7	=	(Button) view.findViewById(id.dialog_check_btn_control_7);
        DAILOG_BTN_CHECK_8	=	(Button) view.findViewById(id.dialog_check_btn_control_8);
        DAILOG_BTN_CHECK_9	=	(Button) view.findViewById(id.dialog_check_btn_control_9);
        DAILOG_BTN_CHECK_CLEAR	=	(Button) view.findViewById(id.dialog_check_btn_control_clear);
        DAILOG_BTN_CHECK_DEL	=	(Button) view.findViewById(id.dialog_check_btn_control_del);

        DAILOG_BTN_CHECK_SK	=	(Button) view.findViewById(id.dialog_check_btn_control_card);
        DAILOG_BTN_CHECK	=	(Button) view.findViewById(id.dialog_check_button_enter_casio);
        DAILOG_BTN_EXIT		=	(Button) view.findViewById(id.dialog_check_button_exit);
       
        get_table_message(Share_Table_Num);
    	DAILOG_CHECK_TEXT_TAIHAO.setText(Share_Table_Num);
    	String	Sell_number = get_table_sellnum(Share_Table_Num);	//该桌的销售单号
    	DAILOG_CHECK_TEXT_DANHAO.setText( Sell_number );
    	
    	//根据销售单号查询销售信息 ，菜的总价！人数！应收！加收！等等之类的
		try {
			String[] sell_number = {Sell_number};
			Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf where cSellNo= ?", sell_number);
			int	Total_All = 0;
			while(cursor.moveToNext()) 
			{
				String Sprice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
				Integer intObj = new Integer(Sprice);
			    int i = intObj.intValue();	
			    Total_All = Total_All + i;
			}		
			DAILOG_CHECK_TEXT_XIAOFEI.setText(""+Total_All);			
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    	//操作员
    	DAILOG_CHECK_TEXT_KAITAIREN.setText(UserCode);

	    Integer intObj = new Integer(DAILOG_CHECK_TEXT_XIAOFEI.getText().toString());
	    int tatal = intObj.intValue();	
	    
	    try {
		    intObj = new Integer(DAILOG_CHECK_TEXT_JIASHOU.getText().toString());			
		} catch (Exception e) {
			// TODO: handle exception
		    intObj = new Integer("4");						
		}
	    int jaishou = intObj.intValue();	
	    
    	DAILOG_CHECK_TEXT_ZONGJI.setText(""+(tatal+jaishou));
    	DAILOG_CHECK_TEXT_YINGSHOU.setText(""+(tatal+jaishou));

    	SerialPort mSerialPort = null;		//串口设备描述
   		String TTY_DEV = "/dev/ttymxc1";

		try {
			mSerialPort = new SerialPort(new File(TTY_DEV), 2400, 0);
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}					
		mOutputStream = mSerialPort.getOutputStream();	

		String contenr = DAILOG_CHECK_TEXT_YINGSHOU.getText().toString();
		Sent_Customer_Clear(contenr);
		
//    	DAILOG_CHECK_TEXT_KAOHAO.setText(Sell_Num);
//    	DAILOG_CHECK_TEXT_JIFEN.setText(Sell_Num);
//    	DAILOG_CHECK_TEXT_KAYUKE.setText(Sell_Num);
        
		
		DAILOG_BTN_CHECK_SK.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_JIEZHANG);  //关闭对话框 
		        init_grid(1);
		        Refresh_State();
		   	}
	    });
		
		DAILOG_BTN_EXIT.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_JIEZHANG);  //关闭对话框 
		        init_grid(1);
		        Refresh_State();	
		        ref_if = true;
		        System.out.println("DAILOG_BTN_EXIT");
		    }
	    });
		
		DAILOG_BTN_CHECK.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
				    Integer intObj = new Integer(DAILOG_CHECK_TEXT_YINGSHOU.getText().toString());
				    int tatal = intObj.intValue();	
				    intObj = new Integer(DAILOG_CHECK_TEXT_XIANSJIN.getText().toString());
				    int xianjin = intObj.intValue();	
				    if(xianjin >= tatal )
				    {	
				    	DAILOG_CHECK_TEXT_ZHAOLING.setText(""+( xianjin - tatal));
				   		ContentValues cv = new ContentValues();
					   	//存储修改的数据
					   	cv.put("iTableState" ,3);
					   	cv.put("iTable_Person","");
					   	cv.put("iTabletime","");
					   	String[] contion = {Share_Table_Num}; 
				   		try {
						   	//更新数据
						   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						/**
						 * 根据销售单号，桌号，人数，操作员，销售金额，结账时间，生成结账数据
						 **/
						//查找店号，机号，操作员
			    		Cursor  cursor = db.rawQuery("select * from lfcy_base_posinf ", null);
			    		cursor.moveToFirst();
			    		String iPosMachineNum = cursor.getString(1).toString();	//机器号
			    		String iPosShopNum = cursor.getString(2).toString();	//门店号
			    		
						//获取结账日期和时间
			       		final Calendar c = 
			       			Calendar.getInstance();
			       			        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
			       			        mMonth 	= c.get(Calendar.MONTH)+1;	//获取当前月份
			       			        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
			       			        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
			       			        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
			       							
						//根据店号机号结账日期和流水生成一条结账单号
						String	Check_Code	=	iPosShopNum+iPosMachineNum+mYear+mMonth+mDay+mHour+mMinute;
			       			        
						//增加一条结账数据
     					//结账时间//操作员编码//门店号//机器号//桌台号//结账单号//销售单号（利用开台时间）
						//实际消费金额//服务费及加收金额//打折及直减金额//最终应收交易金额
						//最终实收交易金额  //找零金额//桌台容纳人数
						insert_CheckData(db ,
								mYear+"-"+mMonth+"-"+mDay+" "+mHour+":"+mMinute,  
								UserCode , 
								iPosShopNum ,
								iPosMachineNum , 
								DAILOG_CHECK_TEXT_TAIHAO.getText().toString() , 
								Check_Code  ,
								DAILOG_CHECK_TEXT_DANHAO.getText().toString() ,
								DAILOG_CHECK_TEXT_XIAOFEI.getText().toString(),
								DAILOG_CHECK_TEXT_JIASHOU.getText().toString(),
								DAILOG_CHECK_TEXT_ZHEKOU.getText().toString(),
								DAILOG_CHECK_TEXT_YINGSHOU.getText().toString(),
								DAILOG_CHECK_TEXT_XIANSJIN.getText().toString(),
								DAILOG_CHECK_TEXT_ZHAOLING.getText().toString(),
								DAILOG_CHECK_TEXT_RENSHU.getText().toString()								
							);
				   		
						SerialPort mSerialPort = null;		//串口设备描述
				   		  
				   	    int	   TTY_BPS = 9600;			//串口的初始信息！只在安装后第一次有用！以后会从xml中读出配置信息
				   		String TTY_DEV = "/dev/ttymxc2";


							mSerialPort = new SerialPort(new File(TTY_DEV), TTY_BPS, 0);					
							mOutputStream = mSerialPort.getOutputStream();	
							
					   		byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
					   		
			   				print_String(cancel_to_normal,"     欢迎光临“8090自助餐厅”");
			   				print_String(cancel_to_normal," 桌台：" + DAILOG_CHECK_TEXT_TAIHAO.getText().toString()  );
			   				print_String(cancel_to_normal," 结账单号  "+ Check_Code );
			   				print_String(cancel_to_normal,"===============================");
			   				print_String(cancel_to_normal,"编号  菜品名称   数量   价格");
			   				try {
			   					String[] sell_number = {DAILOG_CHECK_TEXT_DANHAO.getText().toString()};
			   					cursor = db.rawQuery("select * from lfcy_base_sellinf where cSellNo= ?", sell_number);
			   					int number = 0;
			   					while(cursor.moveToNext()) 
			   					{
			   						number ++;
			   						String SdishName = cursor.getString(cursor.getColumnIndex("cDishName"));
			   						String Snum		 = cursor.getString(cursor.getColumnIndex("cDishQty"));
			   						String Sprice 	 = cursor.getString(cursor.getColumnIndex("fDishPrice"));
			   						
			   		   				print_String(cancel_to_normal,number+"    "+ SdishName +"     "+Snum +"    "+Sprice+".00元" );
			   					}		
				   				print_String(cancel_to_normal,"===============================");
		   		   				print_String(cancel_to_normal,"总价：		     "+ DAILOG_CHECK_TEXT_XIAOFEI.getText().toString() +".00元");				   				
		   		   				print_String(cancel_to_normal,"加收：		     "+ DAILOG_CHECK_TEXT_JIASHOU.getText().toString() +".00元");
		   		   				print_String(cancel_to_normal,"应收：		     "+ DAILOG_CHECK_TEXT_YINGSHOU.getText().toString()+".00元" );
		   		   				print_String(cancel_to_normal,"现金：		     "+ DAILOG_CHECK_TEXT_XIANSJIN.getText().toString()+".00元" );
		   		   				print_String(cancel_to_normal,"找零：		     "+ DAILOG_CHECK_TEXT_ZHAOLING.getText().toString()+".00元" );
		   		   				print_String(cancel_to_normal,"操作员："+ UserCode +"  时间:"+mYear+"-"+mMonth+"-"+ mDay +" "+mHour+":"+mMinute );
				   				print_String(cancel_to_normal," ");
				   				print_String(cancel_to_normal," ");
				   				print_String(cancel_to_normal," ");
				   				print_String(cancel_to_normal," ");
				   				print_String(cancel_to_normal," ");
			   					mSerialPort.close();
			   				} catch (Exception e) {
			   					// TODO: handle exception
			   				}
						
						
				    }
				    else
				    	Toast.makeText(Table.this, "请重新输入现金", Toast.LENGTH_SHORT).show();					
				} catch (Exception e) {
			    	Toast.makeText(Table.this, "请输入现金后,再按结账按钮", Toast.LENGTH_SHORT).show();					
				}
				
				new Handler().postDelayed(new Runnable(){   
				    public void run() {   
				    //execute the task   
				    	Toast.makeText(Table.this, "       找零"+ DAILOG_CHECK_TEXT_ZHAOLING.getText().toString()+"元       ", Toast.LENGTH_LONG).show();					
				   		removeDialog(DIALOG_JIEZHANG);  //关闭对话框 
				        init_grid(1);
				        Refresh_State();
				        ref_if = true;
				    }   
				 }, 3000);  
				
		    	
		   	}
	    });
		DAILOG_BTN_CHECK_1.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  1	);		   	
		   	}
	    });
		DAILOG_BTN_CHECK_2.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  2	);		   	
		   	}
	    });
		DAILOG_BTN_CHECK_3.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 3	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_4.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 4	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_5.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 5	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_6.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  6);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_7.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 7	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_8.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  8	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_9.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  9	);		   	
		   	}
	    });	
		DAILOG_BTN_CHECK_0.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  0	);		   	
		   	}
	    });			
		DAILOG_BTN_CHECK_CLEAR.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText("");		   	
		   	}
	    });			
		DAILOG_BTN_CHECK_DEL.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
			   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
			   		String	str =	num.substring(0,num.length()-1);
			   		DAILOG_CHECK_TEXT_XIANSJIN.setText( str	);						
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Table.this, "请输入金额",	Toast.LENGTH_SHORT ).show();
				}
	   	
		   	}
	    });			
		
		return builder.create();  
    } 
	
    protected void insert_CheckData(SQLiteDatabase db, 
    		String dtCheckDate,
			String cUserCode, 
			String iPosShopNum, 
			String iPosMachineNum,
			String iTableNumber, 
			String cPayNo, 
			String cSellNo,
			String fSaleSum,
			String fSvrSum,
			String fReduceSum, 
			String fTradeSum, 
			String fTradeEndSum,
			String fPayBackSum, 
			String iTable_Person) 
    {
			
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_tablecheckinf values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
				new String[]{ 
					dtCheckDate,		//结账时间
					cUserCode ,			//操作员编码
					iPosShopNum , 		//门店号
					iPosMachineNum , 	//机器号
					iTableNumber  ,		//桌台号
					cPayNo , 			//结账单号
					cSellNo,			//销售单号（利用开台时间）
					fSaleSum,			//实际消费金额
					fSvrSum,			//服务费及加收金额
					fReduceSum,			//打折及直减金额
					fTradeSum,			//最终应收交易金额
					fTradeEndSum,		//最终实收交易金额
					fPayBackSum,		//找零金额
					iTable_Person		//桌台容纳人数
		 });
	}

	//根据桌号查询桌台信息     桌的状态，桌的属性（大厅 ，包厢 ，卡座）
	private void get_table_message(String table_Num2) {
		// TODO Auto-generated method stub
		String[] Contind	= {table_Num2};
		try {
			//根据桌号查询桌台消费信息
    		Cursor  cursor = db.rawQuery("select * from lfcy_base_tableinf where iTableNumber = ?", Contind);
    		cursor.moveToFirst();
    		int    TABLE_STYLE = cursor.getInt(2);
    		String iTabletime = cursor.getString(5).toString();
    		String iTable_Person = cursor.getString(7).toString();	
    		
    	    Integer intObj = new Integer(iTable_Person);
    	    int person = intObj.intValue();	
    		if(TABLE_STYLE == 1)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("大厅");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+(person*2));
    		}
    		if(TABLE_STYLE == 2)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("包厢");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+person*2);       	
    		}
    		if(TABLE_STYLE == 3)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("卡座");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+person*5);
    		}
    		DAILOG_CHECK_TEXT_KAITAISHIJIAN.setText(iTabletime);
        	DAILOG_CHECK_TEXT_RENSHU.setText(iTable_Person);

		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	/***********  以上是结账对话框       *********************/
	
	
	/***      初始一个对话框！ 命名为buildDialog      ***/
    private Dialog buildDialog(Context context) {
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("桌台操作");  
        LayoutInflater inflater = LayoutInflater.from(Table.this);
        View view = null ;
        ref_if = false;

        switch (Dispay_widthPixels) {
			case 1024:
//		        view = inflater.inflate(R.layout.dialog_table_control, null);			
		        view = inflater.inflate(R.layout.i800dialog_table_control, null);			
				break;
			case 800:
		        view = inflater.inflate(R.layout.i800dialog_table_control, null);			
				break;
			default:
		        view = inflater.inflate(R.layout.i800dialog_table_control, null);			
				break;
		}
        builder.setView(view);
        builder.setCancelable(false);
        
        
		DAILOG_BTN_KAITAI 	= (Button) view.findViewById(id.diglog_table_kaitai);
		DAILOG_BTN_DIANCAI 	= (Button) view.findViewById(id.diglog_table_diancai);
		DAILOG_BTN_JIACAI 	= (Button) view.findViewById(id.diglog_table_jiacai);
		DAILOG_BTN_CHACAI 	= (Button) view.findViewById(id.diglog_table_chacai);
		DAILOG_BTN_JIEZHANG = (Button) view.findViewById(id.diglog_table_jiezhang);
		DAILOG_BTN_XIAOTAI 	= (Button) view.findViewById(id.diglog_table_xiaotai);
		DAILOG_BTN_ZHUANTAI	= (Button) view.findViewById(id.diglog_table_zhuantai);
		DAILOG_BTN_CUICAI	= (Button) view.findViewById(id.diglog_table_cuicai);
		
		DAILOG_TEXT_TABLE_NUM = (TextView) view.findViewById(id.diglog_table_number);
		DAILOG_TEXT_TABLE_SELLNUM = (TextView) view.findViewById(id.diglog_table_sellnumber);

		DAILOG_TEXT_TABLE_NUM.setText(Share_Table_Num );
		DAILOG_TEXT_TABLE_SELLNUM.setText(get_table_sellnum(Share_Table_Num));
		
		set_table_button_state( Share_Table_State);	//根据所选桌台的状态来设置按钮的是否能被触发
        
		DAILOG_BTN_KAITAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//获取桌台号和即时时间     生成销售单号
		   		
		   		//设置用餐人数
		   		
		   		//开台
		   		
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });

		DAILOG_BTN_DIANCAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   	    Bundle bundle = new Bundle();
		   	    //保存输入的信息
		   	    bundle.putString("table_number", Share_Table_Num);
		   	    bundle.putString("sell_number", get_table_sellnum(Share_Table_Num));
		   	    bundle.putString("UserCode", UserCode);
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(Table.this, Cai.class);	    			
    			intent_page_new.putExtras(bundle);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });
		
		DAILOG_BTN_JIACAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   	    Bundle bundle = new Bundle();
		   	    //保存输入的信息
		   	    bundle.putString("table_number", Share_Table_Num);
		   	    bundle.putString("sell_number", get_table_sellnum(Share_Table_Num));
		   	    bundle.putString("UserCode", UserCode);
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(Table.this, Cai.class);	    			
    			intent_page_new.putExtras(bundle);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });
		
		DAILOG_BTN_CHACAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   	    Bundle bundle = new Bundle();
		   	    //保存输入的信息
		   	    bundle.putString("table_number", Share_Table_Num);
		   	    bundle.putString("sell_number", get_table_sellnum(Share_Table_Num));
		   	    bundle.putString("UserCode", UserCode);
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(Table.this, Cai.class);	    			
    			intent_page_new.putExtras(bundle);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}
	    });		

		DAILOG_BTN_JIEZHANG.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//结账
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
                showDialog(DIALOG_JIEZHANG);
		   	}

	    });		
		
		DAILOG_BTN_XIAOTAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//消台
		   		ContentValues cv = new ContentValues();
			   	//存储修改的数据
			   	cv.put("iTableState" ,1);
			   	cv.put("iTable_Person","");
			   	cv.put("iTabletime","");
			   	String[] contion = {Share_Table_Num}; 
		   		try {
				   	//更新数据
				   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
				} catch (Exception e) {
					// TODO: handle exception
				}
		        init_grid(1);
		        Refresh_State();		        
		   		removeDialog(DIALOG_FIND);  //关闭对话框 			
		        ref_if = true;

		   	}
	    });		

		DAILOG_BTN_ZHUANTAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//转台
		   		//获取转台前的必备数据
		   		//启动点菜界面 并传入数据
		   	    Bundle bundle = new Bundle();
		   	    //保存输入的信息
		   	    bundle.putString("table_number", Share_Table_Num);
		   	    bundle.putString("sell_number", get_table_sellnum(Share_Table_Num));
		   		//启动转台界面		   		
				Intent intent_page_new = new Intent();
				intent_page_new.setClass(Table.this, Table_ZhuanTai.class);			
    			intent_page_new.putExtras(bundle);
				startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   		
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });		

		DAILOG_BTN_CUICAI.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//返回
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		        ref_if = true;
		   	}
	    });
		
        return builder.create();  
    } 
    
    private	String get_table_sellnum(String table_num)
    {
    	String table_sellnum = null;
    	String[]	Stable_num = {table_num};
    	try {
    		Cursor  cursor = db.rawQuery("select * from lfcy_base_opentableinf where iTableNumber = ?", Stable_num);
    		cursor.moveToLast();
    		table_sellnum = cursor.getString(1).toString();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return table_sellnum;
    }
    
	private void set_table_button_state(String share_Table_State2) {
		// TODO Auto-generated method stub
		DAILOG_BTN_XIAOTAI.setVisibility(View.VISIBLE);
		DAILOG_BTN_JIACAI.setVisibility(View.VISIBLE);
		DAILOG_BTN_JIEZHANG.setVisibility(View.VISIBLE);
		DAILOG_BTN_ZHUANTAI.setVisibility(View.VISIBLE);

		Integer intObj = new Integer(share_Table_State2);
	    int i = intObj.intValue();	
		switch (i) {	//空闲的情况
		case 1:
			DAILOG_BTN_KAITAI.setEnabled(true);
			DAILOG_BTN_KAITAI.setTextColor(Color.rgb(255,255,255));
			DAILOG_BTN_DIANCAI.setEnabled(false);
			DAILOG_BTN_DIANCAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_JIACAI.setEnabled(false);
			DAILOG_BTN_JIACAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_CHACAI.setEnabled(false);
			DAILOG_BTN_CHACAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_JIEZHANG.setEnabled(false);
			DAILOG_BTN_JIEZHANG.setTextColor(R.color.lightgray);
			DAILOG_BTN_XIAOTAI.setEnabled(false);			
			DAILOG_BTN_XIAOTAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_ZHUANTAI.setEnabled(false);			
			DAILOG_BTN_ZHUANTAI.setTextColor(R.color.lightgray);
			break;
		case 2: 	//已点的情况
			DAILOG_BTN_KAITAI.setEnabled(false);
			DAILOG_BTN_KAITAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_DIANCAI.setEnabled(false);
			DAILOG_BTN_DIANCAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_JIACAI.setEnabled(true);
			DAILOG_BTN_JIACAI.setTextColor(Color.rgb(255,255,255));
			DAILOG_BTN_CHACAI.setEnabled(true);
			DAILOG_BTN_CHACAI.setTextColor(Color.rgb(255,255,255));
			DAILOG_BTN_JIEZHANG.setEnabled(true);
			DAILOG_BTN_JIEZHANG.setTextColor(Color.rgb(255,255,255));
			DAILOG_BTN_XIAOTAI.setEnabled(false);			
			DAILOG_BTN_XIAOTAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_ZHUANTAI.setEnabled(true);			
			DAILOG_BTN_ZHUANTAI.setTextColor(Color.rgb(255,255,255));	
			DAILOG_BTN_XIAOTAI.setVisibility(View.GONE);
			break;
		case 3:		//结账但未清台的情况
			DAILOG_BTN_KAITAI.setEnabled(false);
			DAILOG_BTN_KAITAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_DIANCAI.setEnabled(false);
			DAILOG_BTN_DIANCAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_JIACAI.setEnabled(false);
			DAILOG_BTN_JIACAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_CHACAI.setEnabled(false);
			DAILOG_BTN_CHACAI.setTextColor(R.color.lightgray);
			DAILOG_BTN_JIEZHANG.setEnabled(false);
			DAILOG_BTN_JIEZHANG.setTextColor(R.color.lightgray);
			DAILOG_BTN_XIAOTAI.setEnabled(true);			
			DAILOG_BTN_XIAOTAI.setTextColor(Color.rgb(255,255,255));
			DAILOG_BTN_ZHUANTAI.setEnabled(false);			
			DAILOG_BTN_ZHUANTAI.setTextColor(R.color.lightgray);	
			DAILOG_BTN_JIACAI.setVisibility(View.GONE);
			DAILOG_BTN_JIEZHANG.setVisibility(View.GONE);
			DAILOG_BTN_ZHUANTAI.setVisibility(View.GONE);
			break;			
		default:
			break;
		}

	}

	@Override 
    protected Dialog onCreateDialog(int id) {  
        // TODO Auto-generated method stub  
        if(id==101){  
        	return this.buildDialog(Table.this);  
        }
        if(id==102){  
        	return this.buildaDialog(Table.this);  
        }
        if(id==103){  
        	return this.buildCheckDialog(Table.this);  
        }
        else{  
            return null;  
        }  
    } 

    @Override 
    protected void onPrepareDialog(int id, Dialog dialog) {  
        // TODO Auto-generated method stub  
        super.onPrepareDialog(DIALOG_FIND, dialog);  
    } 
    /***      以上是初始一个对话框！       ***/	
    
    
    
    
	
	/***      初始一个对话框！ 命名为buildDialog      ***/
	int mYear,mMonth,mDay,mHour,mMinute;
	String mmMonth,mmDay,mmHour,mmMinute;

    private Dialog buildaDialog(Context context) {
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("开台操作");  
        LayoutInflater inflater = LayoutInflater.from(Table.this);
        View view = null ;
        ref_if = false;

        switch (Dispay_widthPixels) {
			case 1024:
//		        view = inflater.inflate(R.layout.dialog_table_control, null);			
		        view = inflater.inflate(R.layout.dailog_table_personnumber, null);			
				break;
			case 800:
		        view = inflater.inflate(R.layout.dailog_table_personnumber, null);			
				break;
			default:
		        view = inflater.inflate(R.layout.dailog_table_personnumber, null);			
				break;
		}
        builder.setView(view);
        builder.setCancelable(false);        

        DAILOG_BTN_PER_0		= (Button)  view.findViewById(id.table_btn_0);
        DAILOG_BTN_PER_1		= (Button)  view.findViewById(id.table_btn_1);
        DAILOG_BTN_PER_2		= (Button)  view.findViewById(id.table_btn_2);
        DAILOG_BTN_PER_3		= (Button)  view.findViewById(id.table_btn_3);
        DAILOG_BTN_PER_4		= (Button)  view.findViewById(id.table_btn_4);
        DAILOG_BTN_PER_5		= (Button)  view.findViewById(id.table_btn_5);
        DAILOG_BTN_PER_6		= (Button)  view.findViewById(id.table_btn_6);
        DAILOG_BTN_PER_7		= (Button)  view.findViewById(id.table_btn_7);
        DAILOG_BTN_PER_8		= (Button)  view.findViewById(id.table_btn_8);
        DAILOG_BTN_PER_9		= (Button)  view.findViewById(id.table_btn_9);
        DAILOG_BTN_PER_DEL		= (Button)  view.findViewById(id.table_btn_del);
        
		DAILOG_BTN_KAITAI2 		= (Button) view.findViewById(id.dialog_num_kaitai);
		DAILOG_BTN_DIANCAI2 	= (Button) view.findViewById(id.dialog_num_diancai);
		DAILOG_BTN_CANCEL 		= (Button) view.findViewById(id.dialog_num_cancel);
		DAILOG_TEXT_PERSON_NUM	=	(TextView) view.findViewById(id.dialog_person_num);
		DAILOG_TEXT_DANHAO		=	(TextView) view.findViewById(id.dialog_text_danhao);

   		//获取桌台号和即时时间    
   		final Calendar c = 
   			Calendar.getInstance();
   			        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
   			        mMonth 	= c.get(Calendar.MONTH)+1;	//获取当前月份
   			        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
   			        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
   			        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
   	
   		if(mMonth<10)
   		{	
   			mmMonth="0"+mMonth;
   		}
   		else
   			mmMonth=""+mMonth;
   			
   		
   		if(mDay<10)
   		{	
   			mmDay="0"+mDay;
   		} 
   		else
   			mmDay=""+mDay;
   		
   		
   		if(mHour<10)
   		{	
   			mmHour="0"+mHour;
   		}   		
   		else
   			mmHour=""+mHour;
   		
   		
   		if(mMinute<10)
   		{	
   			mmMinute="0"+mMinute;
   		}   		
   		else
   			mmMinute=""+mMinute;
   		
   		final String Sell_Number	=	Share_Table_Num+mYear+mmMonth+mmDay+mmHour+mmMinute;
		DAILOG_TEXT_DANHAO.setText(Sell_Number);
		
		DAILOG_BTN_PER_0.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 0	);	
		   	}
	    });			
		DAILOG_BTN_PER_1.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 1	);	
		   	}
	    });	
		DAILOG_BTN_PER_2.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 2	);	
		   	}
	    });			
		DAILOG_BTN_PER_3.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 3	);	
		   	}
	    });	
		DAILOG_BTN_PER_4.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 4	);	
		   	}
	    });			
		DAILOG_BTN_PER_5.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 5	);	
		   	}
	    });			
		DAILOG_BTN_PER_6.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 6	);	
		   	}
	    });			
		DAILOG_BTN_PER_7.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 7	);	
		   	}
	    });			
		DAILOG_BTN_PER_8.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 8	);	
		   	}
	    });			
		DAILOG_BTN_PER_9.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
			   	String	per_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		DAILOG_TEXT_PERSON_NUM.setText(per_num + 9	);	
		   	}
	    });			
		DAILOG_BTN_PER_DEL.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
			   		String str = null;
			   		String per_num =  DAILOG_TEXT_PERSON_NUM.getText().toString();
		   			str=per_num.substring(0,per_num.length()-1);
		   			DAILOG_TEXT_PERSON_NUM.setText(str);						
				} catch (Exception e) {
					// TODO: handle exception
				}
		   	}
	    });				

		
		DAILOG_BTN_KAITAI2.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//设置用餐人数
		   		String person_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		if(person_num.equals("") || person_num == null  || person_num.equals("0"))
		   		{//会抛出异常
		   			Toast.makeText(Table.this, "开台人数不能为空", Toast.LENGTH_LONG).show();
		   		}
		   		else
		   		{	
			   		// 根据桌台号 和 日期合并  生成销售单号
	//		   		String Sell_Number	=	person_num+mYear+mMonth+mDay+mHour+mMinute;
			   		//开台
			   		ContentValues cv = new ContentValues();
				   	//存储修改的数据
				   	cv.put("iTableState" ,2);
				   	cv.put("iTable_Person",person_num);
				   	cv.put("iTabletime",mHour+":"+mMinute);
				   	String[] contion = {Share_Table_Num}; 
			   		try {
					   	//更新数据
					   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
					} catch (Exception e) {
						// TODO: handle exception
					}
			   		try {
					   	//更新数据
					   	db.execSQL("insert into lfcy_base_opentableinf values(?,?)", 
								new String[]{
					   			Share_Table_Num , Sell_Number});
					} catch (Exception e) {
						// TODO: handle exception
					}
					
			        init_grid(1);
			        Refresh_State();
			   		removeDialog(DIALOG_KAITAI);  //关闭对话框 
			        ref_if = true;
		   		}
		   	}
	    });

		DAILOG_BTN_DIANCAI2.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		//设置用餐人数
		   		String person_num	=	DAILOG_TEXT_PERSON_NUM.getText().toString();
		   		if(person_num.equals("") || person_num == null || person_num.equals("0")){//会抛出异常
		   			Toast.makeText(Table.this, "开台人数不能为空", Toast.LENGTH_LONG).show();
		   		}
		   		else
		   		{
			   		// 根据桌台号 和 日期合并  生成销售单号  	//开台
			   		ContentValues cv = new ContentValues();
				   	//存储修改的数据
				   	cv.put("iTableState" ,2);
				   	cv.put("iTable_Person",person_num);
				   	cv.put("iTabletime",mHour+":"+mMinute);
				   	String[] contion = {Share_Table_Num}; 
			   		try {
					   	//更新数据
					   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
					} catch (Exception e) {
						// TODO: handle exception
					}
			   		try {
					   	//更新数据
					   	db.execSQL("insert into lfcy_base_opentableinf values(?,?)", 
								new String[]{
					   			Share_Table_Num , Sell_Number});
					} catch (Exception e) {
						// TODO: handle exception
					}
			        init_grid(1);
			        Refresh_State();		        
			   		removeDialog(DIALOG_KAITAI);  //关闭对话框 
	
			   		//启动点菜界面 并传入数据
			   	    Bundle bundle = new Bundle();
			   	    //保存输入的信息
			   	    bundle.putString("table_number", Share_Table_Num);
			   	    bundle.putString("sell_number", get_table_sellnum(Share_Table_Num));
			   	    bundle.putString("UserCode", UserCode);
		    		Intent intent_page_new = new Intent();
	    			intent_page_new.setClass(Table.this, Cai.class);	    			
	    			intent_page_new.putExtras(bundle);
		    		startActivity(intent_page_new);	
					overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
		   		}
		   	}
	    });
		
		DAILOG_BTN_CANCEL.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_KAITAI);  //关闭对话框 
		        ref_if = true;
		   	}
	    });		

        return builder.create();  
    } 
 
    /***      以上是初始一个对话框！       ***/	
	//发送客显显示
  
    
	private void Sent_Customer_Clear(String word) {
		// TODO Auto-generated method stub
   		byte[] cancel_to_normal = {0x1b, 0x51, 0x41 };//清除客显
   		send_customer  (cancel_to_normal , word);			   		
	
	}
	
	
	protected void send_customer(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//发送头;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
			byte[] buffere = {0x0d};//发送尾;
			mOutputStream.write(buffere);			
		} catch (IOException e) {
			System.out.println("send customer failed");
		}		
	}
	
	
	@Override  
    protected void onRestoreInstanceState(Bundle savedInstanceState) {  
        // TODO Auto-generated method stub  
        super.onRestoreInstanceState(savedInstanceState);  
        Log.i("info", "onRestoreInstanceState");  
    }  
      
    @Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        // TODO Auto-generated method stub  
        super.onActivityResult(requestCode, resultCode, data);  
        Log.i("info", "onActivityResult");  
    }  
      
    @Override  
    protected void onSaveInstanceState(Bundle outState) {  
        // TODO Auto-generated method stub  
        super.onSaveInstanceState(outState);  
        Log.i("info", "onSaveInstanceState");  
    }  
      
    @Override  
    protected void onStart() {  
        // TODO Auto-generated method stub  
        super.onStart();  
        Log.i("info", "onStart");  
    }  
  
    @Override  
    protected void onRestart() {  
        // TODO Auto-generated method stub  
        super.onRestart();  
        Log.i("info", "onRestart");  
    }  
  
 
    @Override  
    protected void onPause() {  
        // TODO Auto-generated method stub  
        super.onPause();  
        ref_if = false;
        Log.i("info", "onPause");  
    }  
  
    @Override  
    protected void onStop() {  
        // TODO Auto-generated method stub  
        super.onStop();  
        ref_if = false;
        Log.i("info", "onStop");  
    }  
  
    @Override  
    protected void onDestroy() {  
        // TODO Auto-generated method stub  
        super.onDestroy();  
        Log.i("info", "onDestroy");  
    }  
  
    @Override  
    public void onLowMemory() {  
        // TODO Auto-generated method stub  
        super.onLowMemory();  
        //当系统内存不足时 才会调用  但不一定会百分百调用  如果能及时调用则会调用  可能在没用调用之前就将程序关闭了。  
        Log.i("info", "onLowMemory");  
    }  
  
    @Override  
    public void onBackPressed() {  
        // TODO Auto-generated method stub  
        super.onBackPressed();  
        
        ref_if = false;
        Log.i("info", "onBackPressed");  
        //  
    }  
  
    @Override  
    public void finish() {  
        // TODO Auto-generated method stub  
        super.finish();  
        //要关闭Activity 所以才要调用finish()方法  
        ref_if = false;
        Log.i("info", "finish");  
    }  
    
    @Override  
    public boolean onTouchEvent(MotionEvent event) {            
        // 按下时清理之前的记录  
        if (event.getAction() == MotionEvent.ACTION_DOWN) {  
            mRecordMap.clear();  
        }            
        return mGestureDetector.onTouchEvent(event);  
    }  
    
    private class DefaultGestureListener extends SimpleOnGestureListener {  
        // 滑动时触发  
        @Override  
        public boolean onScroll(MotionEvent e1, MotionEvent e2,  
                float distanceX, float distanceY) {  
            /*
             * 向上滑 Y 是正数
             * 向下滑 Y 是负数
             * 
             * 向左滑 X 是正数	值要比较大才是大滑动
             * 向右滑 X 是负数
             */  
            ALL_TABLE.setText("distanceX = "+distanceX +"     distanceY = "+ distanceY);
            return super.onScroll(e1, e2, distanceX, distanceY);  
        }  

    }  
}