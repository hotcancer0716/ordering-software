package longfly.development;

import longfly.development.Print_Setup.insert_Thread;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Net_Setup extends Activity {
	
	Button	POS_SETUP_SAVE;
	Button	POS_SETUP_EXIT;
	TextView	NET_SETUP_WIFI_IP;

	EditText	POS_SETUP_IP;
	EditText	POS_SETUP_ZWYM;
	EditText	POS_SETUP_WANGGUAN;
	EditText	POS_SETUP_MAC;
	EditText	POS_SETUP_DNS;
	EditText	POS_SETUP_WWW;
	
	
  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	String	SPF_Pos_Ip		=	"SPF_Pos_Ip";
	String	SPF_Pos_ZWYM	=	"SPF_Pos_ZWYM";
	String	SPF_Pos_WG		=	"SPF_Pos_WG";
	String	SPF_Pos_MAC		=	"SPF_Pos_MAC";
	String	SPF_Pos_DNS		=	"SPF_Pos_DNS";
	String	SPF_Pos_WWW		=	"SPF_Pos_WWW";
	
	ProgressDialog progressDialog;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_net_setup);
        
        find_id();
        key_things();
        
 	    settings = getSharedPreferences("POS_Setup_SharedPreferences", 0);		
        get_SharePerference();    
        
		WifiManager wifimanage =(WifiManager)Net_Setup.this.getSystemService(Context.WIFI_SERVICE);//获取WifiManager  
        
      //检查wifi是否开启  
        
      if(!wifimanage.isWifiEnabled())  {  
        
        wifimanage.setWifiEnabled(true);  
        
      }  
        
      WifiInfo wifiinfo= wifimanage.getConnectionInfo();  
        
      String ip=intToIp(wifiinfo.getIpAddress());  
        
      NET_SETUP_WIFI_IP.setText(ip);
   }

    //将获取的int转为真正的ip地址,参考的网上的，修改了下  
	private String intToIp(int i) {
		// TODO Auto-generated method stub
		String myip =  (i & 0xFF)+ "." + ((i >> 8 ) & 0xFF) + "." + ((i >> 16 ) & 0xFF) +"."+((i >> 24 ) & 0xFF );
		return	myip;
	}


private void get_SharePerference() {
	POS_SETUP_IP.setText(  settings.getString(SPF_Pos_Ip, null) );
	POS_SETUP_ZWYM.setText(  settings.getString(SPF_Pos_ZWYM, null) );
	POS_SETUP_WANGGUAN.setText(  settings.getString(SPF_Pos_WG, null) );
	POS_SETUP_MAC.setText(  settings.getString(SPF_Pos_MAC, null) );
	POS_SETUP_DNS.setText(  settings.getString(SPF_Pos_DNS, null) );
	POS_SETUP_WWW.setText(  settings.getString(SPF_Pos_WWW, null) );
}


	private void key_things() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(Net_Setup.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		POS_SETUP_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
	}


	

	private void find_id() {
		// TODO Auto-generated method stub
		POS_SETUP_SAVE	=	(Button) findViewById(R.id.net_setup_save);
		POS_SETUP_EXIT	=	(Button) findViewById(R.id.net_setup_exit);
		NET_SETUP_WIFI_IP = (TextView) findViewById(R.id.net_setup_wifi_ip);

		POS_SETUP_IP				=	(EditText) findViewById(R.id.pos_setup_ip);
		POS_SETUP_ZWYM				=	(EditText) findViewById(R.id.pos_setup_ziwangyanma);
		POS_SETUP_WANGGUAN			=	(EditText) findViewById(R.id.pos_setup_wangguan);
		POS_SETUP_MAC				=	(EditText) findViewById(R.id.pos_setup_mac);
		POS_SETUP_DNS				=	(EditText) findViewById(R.id.pos_setup_dns);
		POS_SETUP_WWW				=	(EditText) findViewById(R.id.pos_setup_www);

	}
	
	
	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Net_Setup.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据     
	        handler.sendEmptyMessage(5);
	        handler.sendEmptyMessage(55);
	        editor.putString(SPF_Pos_Ip, String.valueOf( POS_SETUP_IP.getText().toString()));  
	        editor.putString(SPF_Pos_ZWYM, String.valueOf( POS_SETUP_ZWYM.getText().toString()));  
	        editor.putString(SPF_Pos_WG, String.valueOf( POS_SETUP_WANGGUAN.getText().toString()));   
	        editor.putString(SPF_Pos_MAC, String.valueOf( POS_SETUP_MAC.getText().toString()));   
	        editor.putString(SPF_Pos_DNS, String.valueOf( POS_SETUP_DNS.getText().toString()));   
	        editor.putString(SPF_Pos_WWW, String.valueOf( POS_SETUP_WWW.getText().toString()));   
	        handler.sendEmptyMessage(85);
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}