package longfly.development;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import casio.serial.SerialPort;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class POS_System extends Activity {
	
	Button	BTN_REF;
	Button	BTN_BASE_SETUP;
	Button	BTN_BASE_FILES;
	Button	BTN_BASE_SELLMANAGE;
	Button	BTN_BASE_BAOBIAO;
	Button	BTN_BASE_VIP;
	Button	BTN_BASE_WAREHOUSE;
	Button	BTN_BASE_SYSTEMMANAGE;
	
	RelativeLayout	PAGE_BASE_SETUP;
	RelativeLayout	PAGE_BASE_FILES;
	RelativeLayout	PAGE_BASE_SELLMANAGE;
	RelativeLayout	PAGE_BASE_BAOBIAO;
	RelativeLayout	PAGE_BASE_VIP;
	RelativeLayout	PAGE_BASE_WAREHOUSE;
	RelativeLayout	PAGE_BASE_SYSTEM;
	
	/*******基本设置*************************************************************/		
	Button	PAGE_BASE_SETUP_BTN_1;	//营业参数设置
	Button	PAGE_BASE_SETUP_BTN_2;	//操作员设置
	Button	PAGE_BASE_SETUP_BTN_3;	//端口设置
	Button	PAGE_BASE_SETUP_BTN_4;	//打印机设置
	Button	PAGE_BASE_SETUP_BTN_5;	//客显设置
	Button	PAGE_BASE_SETUP_BTN_6;	//其他外设设置
	Button	PAGE_BASE_SETUP_BTN_7;	//联网设置
	
	/*******基本档案*************************************************************/		
	Button	PAGE_BASE_FILES_BTN_DISHINF;	//菜品资料
	Button	PAGE_BASE_FILES_BTN_TAODISHINF;	//套菜资料
	Button	PAGE_BASE_FILES_BTN_TABLEING;	//台房设置
	Button	PAGE_BASE_FILES_BTN_FENLEI;		//分类设置
	Button	PAGE_BASE_FILES_BTN_KOUWEI;		//口味设置
	Button	PAGE_BASE_FILES_BTN_ZUOFA;		//做法设置
	Button	PAGE_BASE_FILES_BTN_EAREA;		//桌台区域设置
	Button	PAGE_BASE_FILES_BTN_TABLESTYLE;	//桌台类型设置
	Button	PAGE_BASE_FILES_BTN_DANWEI;		//单位设置	
	Button	PAGE_BASE_FILES_BTN_OTHER;		//其他设置
	/*******销售管理*************************************************************/		
	Button	PAGE_BASE_SELLMANAGE_TIMEPRICE;		//菜品时价		
	Button	PAGE_BASE_SELLMANAGE_GUQING;		//菜品沽清
	Button	PAGE_BASE_SELLMANAGE_CAIPINXIAOSHOULIUSHUI;	//菜品销售流水
	Button	PAGE_BASE_SELLMANAGE_SHOUYINLIUSHUI;	//收银流水
	Button	PAGE_BASE_SELLMANAGE_RIJIE;			//营业日结	
	/*******报表中心*************************************************************/		
	Button	PAGE_BASE_BAOBIAO_XIAOSHOUCHUN;			//菜品销售查询
	Button	PAGE_BASE_BAOBIAO_SHOUYINCHUN;			//收银查询
	Button	PAGE_BASE_BAOBIAO_SHIDUANXIAOFEICHUN;	//时段消费查询
	/*******会员管理*************************************************************/		
	Button	PAGE_BASE_VIP_CANSHU;			//参数设置
	Button	PAGE_BASE_VIP_LEIXING;			//会员类型
	Button	PAGE_BASE_VIP_XIAOFEIBAOBIAO;	//会员消费报表
	Button	PAGE_BASE_VIP_DANGAN;			//会员档案
	Button	PAGE_BASE_VIP_CHONGZHI;			//会员充值
	Button	PAGE_BASE_VIP_ZHUANGTAIGUANLI;	//会员状态管理	
	/*******仓库管理*************************************************************/		

	/*******系统维护*************************************************************/		
	Button	PAGE_BASE_SYSTEM_CHUSHIHUA;		//系统初始化
	Button	PAGE_BASE_SYSTEM_SHUJUKU;		//数据库管理
	Button	PAGE_BASE_SYSTEM_CAOZUOYAUN;	//操作员管理
	Button	PAGE_BASE_SYSTEM_XITONGRIZHI;	//系统日志
	Button	PAGE_BASE_SYSTEM_SHUJUQINGLI;	//数据清理
	Button	PAGE_BASE_SYSTEM_XITONGXINXI;	//系统信息
	
	/*******最底下一栏的操作员/日期/时间*************************************************************/		
	TextView	USER_CODE;
	TextView	SYSTEM_DATE;
	TextView	SYSTEM_TIME;
	
    private Handler handler = new Handler();  
	protected OutputStream mOutputStream;		//串口输出描述

	SerialPort mSerialPort = null;		//串口设备描述
	String TTY_DEV = "/dev/ttymxc1";
		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_manage);
        
        find_id();
        key_things();
        init_btn();
        init_view(1);        
        init_usr();
        /*  Tbox后显显示时间的代码
		try {
			mSerialPort = new SerialPort(new File(TTY_DEV), 2400, 0);
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
        init_time();
        handler.post(myRunnable);
        */
    }

    private void init_usr() {
		// TODO Auto-generated method stub
    	SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
		Bundle bundle=getIntent().getExtras();
		try {
			  //获取Bundle的信息
	   		String[] refer = {bundle.getString("UserCode")};
       	    Cursor  cursor = db.rawQuery("select * from lfcy_user_inf where cUserCode = ?", refer);
	       	cursor.moveToFirst();
			USER_CODE.setText(bundle.getString("UserCode") +"   " +cursor.getString(1).toString() );		
		} catch (Exception e) {
			  //获取Bundle的信息
			USER_CODE.setText("获取操作员失败");	
		}    	
		db.close();
	}

	Runnable myRunnable= new Runnable() {    
        public void run() {                   
            handler.postDelayed(this, 1000);  
            init_time();
        }  
    };
    
    private void init_time() {
		// TODO Auto-generated method stub
    	int mYear,mMonth,mDay,mHour,mMinute,mSecond;

    	//获取桌台号和即时时间    
   		final Calendar c = 	Calendar.getInstance();
        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
        mMonth 	= c.get(Calendar.MONTH)+ 1;	//获取当前月份
        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
        mSecond = c.get(Calendar.SECOND);	//获取当前的秒钟数		   		

   		String mmHour,mmMinute,mmSecond;

   		if(mHour < 10)
   			mmHour = "0"+mHour;
   		else
   			mmHour = ""+mHour;

   		if(mMinute < 10)
   			mmMinute = "0"+mMinute;
   		else
   			mmMinute = ""+mMinute;

   		if(mSecond < 10)
   			mmSecond = "0"+mSecond;
   		else
   			mmSecond = ""+mSecond;

   		String	system_date	= mYear+"-"+mMonth+"-"+	mDay;
   		String	system_time	= mmHour+":"+mmMinute+":"+mmSecond;
   		SYSTEM_DATE.setText(system_date);
   		SYSTEM_TIME.setText(system_time);

		try {
			mOutputStream = mSerialPort.getOutputStream();				
			String contenr = mmHour+"-"+mmMinute+"-"+mmSecond;
			Sent_Customer_Clear(contenr);  	
			
		} catch (Exception e) {
			// TODO: handle exception
		}
 		
	}

	//初始化最上一排按钮的进入动画
	private void init_btn() {
		// TODO Auto-generated method stub
		AnimationSet animationSet = new AnimationSet(true); 
		TranslateAnimation translateAnimation = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, -8.0f, Animation.RELATIVE_TO_SELF, 0.0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation.setDuration(300); 
		animationSet.addAnimation(translateAnimation); 	
		
		BTN_BASE_SYSTEMMANAGE.startAnimation(animationSet);
		BTN_BASE_WAREHOUSE.startAnimation(animationSet);
		BTN_BASE_VIP.startAnimation(animationSet);
		BTN_BASE_BAOBIAO.startAnimation(animationSet);
		BTN_BASE_SELLMANAGE.startAnimation(animationSet);
		BTN_BASE_FILES.startAnimation(animationSet);
		BTN_BASE_SETUP.startAnimation(animationSet);
	}

	//初始化下排按钮！以及按下时的变化
	private void init_view(int state) {
		// TODO Auto-generated method stub
		BTN_BASE_SETUP.setBackgroundResource(R.drawable.btn_blue_up);			;
		BTN_BASE_FILES.setBackgroundResource(R.drawable.btn_blue_up);
		BTN_BASE_SELLMANAGE.setBackgroundResource(R.drawable.btn_blue_up);
		BTN_BASE_BAOBIAO.setBackgroundResource(R.drawable.btn_blue_up);
		BTN_BASE_VIP.setBackgroundResource(R.drawable.btn_blue_up);
		BTN_BASE_WAREHOUSE.setBackgroundResource(R.drawable.btn_blue_up);
		BTN_BASE_SYSTEMMANAGE.setBackgroundResource(R.drawable.btn_blue_up);		
		
		PAGE_BASE_SETUP.setVisibility(View.GONE);
		PAGE_BASE_FILES.setVisibility(View.GONE);
		PAGE_BASE_SELLMANAGE.setVisibility(View.GONE);
		PAGE_BASE_BAOBIAO.setVisibility(View.GONE);
		PAGE_BASE_VIP.setVisibility(View.GONE);
		PAGE_BASE_WAREHOUSE.setVisibility(View.GONE);
		PAGE_BASE_SYSTEM.setVisibility(View.GONE);

		AnimationSet animationSet = new AnimationSet(true); 
		TranslateAnimation translateAnimation = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation.setDuration(200); 
		animationSet.addAnimation(translateAnimation); 
		
		switch (state) {
		case 1:
			BTN_BASE_SETUP.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_SETUP.setVisibility(View.VISIBLE);			
			PAGE_BASE_SETUP.startAnimation(animationSet);
			break;
		case 2:
			BTN_BASE_FILES.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_FILES.setVisibility(View.VISIBLE);	
			PAGE_BASE_FILES.startAnimation(animationSet);
			break;
		case 3:
			BTN_BASE_SELLMANAGE.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_SELLMANAGE.setVisibility(View.VISIBLE);			
			PAGE_BASE_SELLMANAGE.startAnimation(animationSet);
			break;
		case 4:
			BTN_BASE_BAOBIAO.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_BAOBIAO.setVisibility(View.VISIBLE);			
			PAGE_BASE_BAOBIAO.startAnimation(animationSet);
			break;			
		case 5:
			BTN_BASE_VIP.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_VIP.setVisibility(View.VISIBLE);			
			PAGE_BASE_VIP.startAnimation(animationSet);
			break;		
		case 6:
			BTN_BASE_WAREHOUSE.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_WAREHOUSE.setVisibility(View.VISIBLE);			
			PAGE_BASE_WAREHOUSE.startAnimation(animationSet);
			break;
		case 7:
			BTN_BASE_SYSTEMMANAGE.setBackgroundResource(R.drawable.tablestyle_unchoose);			
			PAGE_BASE_SYSTEM.setVisibility(View.VISIBLE);			
			PAGE_BASE_SYSTEM.startAnimation(animationSet);
			break;		
		default:
		
			break;
		}
	}

	private void key_things() {
		// TODO Auto-generated method stub
		BTN_BASE_SETUP.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(1);
		   	}
	    });	
		BTN_BASE_FILES.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(2);
		   	}
	    });	
		BTN_BASE_SELLMANAGE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(3);
		   	}
	    });			
		BTN_BASE_BAOBIAO.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(4);
		   	}
	    });			
		BTN_BASE_VIP.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(5);
		   	}
	    });			
		BTN_BASE_WAREHOUSE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(6);
		   	}
	    });			
		BTN_BASE_SYSTEMMANAGE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		init_view(7);
		   	}
	    });			
		BTN_REF.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		        init_btn();
		        init_view(1);		   	
		    }
	    });	
		/*******基本设置*************************************************************/		
		PAGE_BASE_SETUP_BTN_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, POS_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		PAGE_BASE_SETUP_BTN_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, User_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		PAGE_BASE_SETUP_BTN_3.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, DuanKou_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		PAGE_BASE_SETUP_BTN_4.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Print_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		PAGE_BASE_SETUP_BTN_5.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Customer_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		PAGE_BASE_SETUP_BTN_6.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Other_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	

		PAGE_BASE_SETUP_BTN_7.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Net_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		/*******基本档案*************************************************************/		
		//菜品资料
		PAGE_BASE_FILES_BTN_DISHINF.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Recode_cai.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		PAGE_BASE_FILES_BTN_TAODISHINF.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Recode_taocai.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		
		//台房设置
		PAGE_BASE_FILES_BTN_TABLEING.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Table_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//分类设置
		PAGE_BASE_FILES_BTN_FENLEI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Class_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		
		//口味设置
		PAGE_BASE_FILES_BTN_KOUWEI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Taste_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		
		//单位设置
		PAGE_BASE_FILES_BTN_DANWEI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Unit_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		//其他设置
		PAGE_BASE_FILES_BTN_OTHER.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		
		PAGE_BASE_FILES_BTN_ZUOFA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, MakeWay_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		PAGE_BASE_FILES_BTN_EAREA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, TableArea_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		PAGE_BASE_FILES_BTN_TABLESTYLE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, TableStyle_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });		
		/*******销售管理*************************************************************/		
		//菜品时价				
		PAGE_BASE_SELLMANAGE_TIMEPRICE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, TimePrice_Setup.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//菜品沽清
		PAGE_BASE_SELLMANAGE_GUQING.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });		
		//菜品销售流水
		PAGE_BASE_SELLMANAGE_CAIPINXIAOSHOULIUSHUI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Liushui_Sell.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		//收银流水
		PAGE_BASE_SELLMANAGE_SHOUYINLIUSHUI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Liushui_Check.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });		
		//营业日结	
		PAGE_BASE_SELLMANAGE_RIJIE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Day_Check.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		/*******报表中心*************************************************************/		
		//菜品销售查询
		PAGE_BASE_BAOBIAO_XIAOSHOUCHUN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Check_Sell.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//收银查询
		PAGE_BASE_BAOBIAO_SHOUYINCHUN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Sell_Rank.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//时段消费查询
		PAGE_BASE_BAOBIAO_SHIDUANXIAOFEICHUN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, Sell_Check.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });			
		/*******会员管理*************************************************************/			
		//参数设置
		PAGE_BASE_VIP_CANSHU.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//会员类型
		PAGE_BASE_VIP_LEIXING.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//会员消费报表
		PAGE_BASE_VIP_XIAOFEIBAOBIAO.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });		
		//会员档案
		PAGE_BASE_VIP_DANGAN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });		
		//会员充值
		PAGE_BASE_VIP_CHONGZHI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//会员状态管理
		PAGE_BASE_VIP_ZHUANGTAIGUANLI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		/*******仓库管理*************************************************************/		

		/*******系统维护*************************************************************/			
		//系统初始化
		PAGE_BASE_SYSTEM_CHUSHIHUA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, POS_Init.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });
		//数据库管理
		PAGE_BASE_SYSTEM_SHUJUKU.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, POS_Database.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//操作员管理
		PAGE_BASE_SYSTEM_CAOZUOYAUN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });	
		//系统日志
		PAGE_BASE_SYSTEM_XITONGRIZHI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, About.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	   	
		    }
	    });
		//数据清理
		PAGE_BASE_SYSTEM_SHUJUQINGLI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
//	    		Intent intent_page_new = new Intent();
//    			intent_page_new.setClass(POS_System.this, Service_manage.class);
//	    		startActivity(intent_page_new);	
//				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	
				finish();

		    }
	    });		
		//系统信息
		PAGE_BASE_SYSTEM_XITONGXINXI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(POS_System.this, POS_Info.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.zoomin, R.anim.zoomout);	
		    }
	    });
}

	private void find_id() {
		// TODO Auto-generated method stub
		PAGE_BASE_SETUP			=	(RelativeLayout) findViewById(R.id.manage_page_base_setup);
		PAGE_BASE_FILES			=	(RelativeLayout) findViewById(R.id.manage_page_base_files);
		PAGE_BASE_SELLMANAGE	=	(RelativeLayout) findViewById(R.id.manage_page_base_sell);
		PAGE_BASE_BAOBIAO		=	(RelativeLayout) findViewById(R.id.manage_page_base_baobiao);
		PAGE_BASE_VIP			=	(RelativeLayout) findViewById(R.id.manage_page_base_vip);
		PAGE_BASE_WAREHOUSE		=	(RelativeLayout) findViewById(R.id.manage_page_base_warehouse);
		PAGE_BASE_SYSTEM		=	(RelativeLayout) findViewById(R.id.manage_page_base_system);
		
		BTN_BASE_SETUP			=	(Button) findViewById(R.id.manage_base_setup);
		BTN_BASE_FILES			=	(Button) findViewById(R.id.manage_base_files);
		BTN_BASE_SELLMANAGE 	=	(Button) findViewById(R.id.manage_base_sellmanage);
		BTN_BASE_BAOBIAO		=	(Button) findViewById(R.id.manage_base_baobiaocenter);
		BTN_BASE_VIP			=	(Button) findViewById(R.id.manage_base_vipmanage);
		BTN_BASE_WAREHOUSE		=	(Button) findViewById(R.id.manage_base_warehousemanage);
		BTN_BASE_SYSTEMMANAGE	=	(Button) findViewById(R.id.manage_base_systemmanage);		
		
		BTN_REF	=	(Button) findViewById(R.id.magane_refech);
		
		/*******基本设置*************************************************************/		
		PAGE_BASE_SETUP_BTN_1	=	(Button) findViewById(R.id.manage_page_base_setup_btn_1);	//营业参数设置
		PAGE_BASE_SETUP_BTN_2	=	(Button) findViewById(R.id.manage_page_base_setup_btn_2);	//操作员设置
		PAGE_BASE_SETUP_BTN_3	=	(Button) findViewById(R.id.manage_page_base_setup_btn_3);	//打印机设置
		PAGE_BASE_SETUP_BTN_4	=	(Button) findViewById(R.id.manage_page_base_setup_btn_4);	//客显设置
		PAGE_BASE_SETUP_BTN_5	=	(Button) findViewById(R.id.manage_page_base_setup_btn_5);	//其他外设设置
		PAGE_BASE_SETUP_BTN_6	=	(Button) findViewById(R.id.manage_page_base_setup_btn_6);	//联网设置
		PAGE_BASE_SETUP_BTN_7	=	(Button) findViewById(R.id.manage_page_base_setup_btn_7);	//打印机设置
	
		/*******基本档案*************************************************************/		
		PAGE_BASE_FILES_BTN_DISHINF		=	(Button) findViewById(R.id.manage_page_base_files_btn_1);		//菜品资料
		PAGE_BASE_FILES_BTN_TAODISHINF	=	(Button) findViewById(R.id.manage_page_base_files_btn_2);		//套菜资料
		PAGE_BASE_FILES_BTN_TABLEING	=	(Button) findViewById(R.id.manage_page_base_files_btn_3);		//台房设置
		PAGE_BASE_FILES_BTN_FENLEI		=	(Button) findViewById(R.id.manage_page_base_files_btn_4);		//台房设置
		PAGE_BASE_FILES_BTN_KOUWEI		=	(Button) findViewById(R.id.manage_page_base_files_btn_5);		//台房设置
		PAGE_BASE_FILES_BTN_DANWEI		=	(Button) findViewById(R.id.manage_page_base_files_btn_6);		//台房设置
		PAGE_BASE_FILES_BTN_OTHER		=	(Button) findViewById(R.id.manage_page_base_files_btn_7);		//其他设置
		PAGE_BASE_FILES_BTN_ZUOFA		=	(Button) findViewById(R.id.manage_page_base_files_btn_8);		//做法设置
		PAGE_BASE_FILES_BTN_EAREA		=	(Button) findViewById(R.id.manage_page_base_files_btn_9);		//台房区域设置
		PAGE_BASE_FILES_BTN_TABLESTYLE	=	(Button) findViewById(R.id.manage_page_base_files_btn_10);		//台房类型设置

		/*******销售管理*************************************************************/		
		PAGE_BASE_SELLMANAGE_TIMEPRICE	=	(Button) findViewById(R.id.manage_page_base_sell_btn_1);	//菜品时价		
		PAGE_BASE_SELLMANAGE_GUQING		=	(Button) findViewById(R.id.manage_page_base_sell_btn_2);		//菜品沽清
		PAGE_BASE_SELLMANAGE_CAIPINXIAOSHOULIUSHUI	=	(Button) findViewById(R.id.manage_page_base_sell_btn_3);	//菜品销售流水
		PAGE_BASE_SELLMANAGE_SHOUYINLIUSHUI	=	(Button) findViewById(R.id.manage_page_base_sell_btn_4);	//收银流水
		PAGE_BASE_SELLMANAGE_RIJIE			=	(Button) findViewById(R.id.manage_page_base_sell_btn_5);			//营业日结	
		/*******报表中心*************************************************************/		
		PAGE_BASE_BAOBIAO_XIAOSHOUCHUN			=	(Button) findViewById(R.id.manage_page_base_baobiao_btn_1);		//菜品销售查询
		PAGE_BASE_BAOBIAO_SHOUYINCHUN			=	(Button) findViewById(R.id.manage_page_base_baobiao_btn_2);			//收银查询
		PAGE_BASE_BAOBIAO_SHIDUANXIAOFEICHUN	=	(Button) findViewById(R.id.manage_page_base_baobiao_btn_3);	//时段消费查询
		/*******会员管理*************************************************************/		
		PAGE_BASE_VIP_CANSHU			=	(Button) findViewById(R.id.manage_page_base_vip_btn_1);			//参数设置
		PAGE_BASE_VIP_LEIXING			=	(Button) findViewById(R.id.manage_page_base_vip_btn_2);			//会员类型
		PAGE_BASE_VIP_XIAOFEIBAOBIAO	=	(Button) findViewById(R.id.manage_page_base_vip_btn_3);	//会员消费报表
		PAGE_BASE_VIP_DANGAN			=	(Button) findViewById(R.id.manage_page_base_vip_btn_4);			//会员档案
		PAGE_BASE_VIP_CHONGZHI			=	(Button) findViewById(R.id.manage_page_base_vip_btn_5);			//会员充值
		PAGE_BASE_VIP_ZHUANGTAIGUANLI	=	(Button) findViewById(R.id.manage_page_base_vip_btn_6);	//会员状态管理	
		/*******仓库管理*************************************************************/		

		/*******系统维护*************************************************************/		
		PAGE_BASE_SYSTEM_CHUSHIHUA		=	(Button) findViewById(R.id.manage_page_base_system_btn_1);		//系统初始化
		PAGE_BASE_SYSTEM_SHUJUKU		=	(Button) findViewById(R.id.manage_page_base_system_btn_2);		//数据库管理
		PAGE_BASE_SYSTEM_CAOZUOYAUN		=	(Button) findViewById(R.id.manage_page_base_system_btn_3);	//操作员管理
		PAGE_BASE_SYSTEM_XITONGRIZHI	=	(Button) findViewById(R.id.manage_page_base_system_btn_4);	//系统日志
		PAGE_BASE_SYSTEM_SHUJUQINGLI	=	(Button) findViewById(R.id.manage_page_base_system_btn_5);	//数据清理
		PAGE_BASE_SYSTEM_XITONGXINXI	=	(Button) findViewById(R.id.manage_page_base_system_btn_6);	//系统信息		

		/*******操作员/系统日期/系统时间*************************************************************/		
		USER_CODE   = (TextView) findViewById(R.id.manage_usercode);
		SYSTEM_DATE = (TextView) findViewById(R.id.manage_date);
		SYSTEM_TIME = (TextView) findViewById(R.id.manage_time);
	}
	
	
	//发送客显显示
	private void Sent_Customer_Clear(String word) {
		// TODO Auto-generated method stub
   		byte[] cancel_to_normal = {0x1b, 0x51, 0x41 };//清除客显
   		send_customer  (cancel_to_normal , word);			   		
	
	}
	
	
	protected void send_customer(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//发送头;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
			byte[] buffere = {0x0d};//发送尾;
			mOutputStream.write(buffere);			
		} catch (IOException e) {
			System.out.println("send customer failed");
		}		
	}
}