package longfly.development;

import java.util.List;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class GridItemTableSQLAdapter extends BaseAdapter {

	List<Grid_Item_Table> pictures;
	Context context;
	int Dispay_widthPixels;
	
	public GridItemTableSQLAdapter(List<Grid_Item_Table> pic_list, Context context , int dispay_widthPixels) {
		super();
		this.pictures = pic_list;
		this.context = context;
		this.Dispay_widthPixels	=	dispay_widthPixels;
	}

	@Override
	public int getCount() {
		if (null != pictures) {
			return pictures.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return pictures.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder viewHolder;
		if (convertView == null) {
			switch (Dispay_widthPixels) {
			case 800:
				convertView = inflater.inflate(R.layout.i800_grid_table_item, null);								
				break;
			case 1024:
				convertView = inflater.inflate(R.layout.imx_grid_table_item, null);				
				break;
			default:
				convertView = inflater.inflate(R.layout.i800_grid_table_item, null);								
				break;
			}
            viewHolder = new ViewHolder();   
            viewHolder.TABLE_NUMBER = (TextView) convertView.findViewById(R.id.table_number);   
            viewHolder.TABLE_BACKGROUND = (ImageView) convertView.findViewById(R.id.grid_table_background);  
            viewHolder.START_TIME = (TextView) convertView.findViewById(R.id.table_start_time);   
            viewHolder.START_TIME_IF = (TextView) convertView.findViewById(R.id.table_item_start_time);   
            viewHolder.TABLE_PERSON = (TextView) convertView.findViewById(R.id.table_item_person_busy);   
            viewHolder.TABLE_PERSON_NUMBER = (TextView) convertView.findViewById(R.id.table_item_person_number);   
            viewHolder.TABLE_PLACE = (TextView) convertView.findViewById(R.id.table_item_place);   
            viewHolder.TABLE_PERSON_REN = (TextView) convertView.findViewById(R.id.table_item_person_ren);   
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
        viewHolder.TABLE_NUMBER.setText(pictures.get(position).getNumber());  
//        viewHolder.TABLE_BACKGROUND.setImageResource(pictures.get(position).getTableState()); 
        String Table_State = pictures.get(position).getSringTableState();
        Integer intObj = new Integer(Table_State);
        int i = intObj.intValue();	
        switch (i) {
			case 1:
		        viewHolder.TABLE_BACKGROUND.setImageResource(R.drawable.table_free);
		        viewHolder.TABLE_PERSON.setText("可容纳");
		        viewHolder.TABLE_PERSON_NUMBER.setText(pictures.get(position).getPersonNumber());
		        viewHolder.START_TIME.setText("");   
		        viewHolder.START_TIME_IF.setText("");   
		        viewHolder.TABLE_PERSON_REN.setText("人");		        
		        break;
			case 2:
		        viewHolder.TABLE_BACKGROUND.setImageResource(R.drawable.table_busy);   			
		        viewHolder.TABLE_PERSON.setText("已容纳");
		        viewHolder.TABLE_PERSON_NUMBER.setText(pictures.get(position).setTable_PersonNumber());
		        viewHolder.START_TIME.setText(pictures.get(position).getTime());   
		        viewHolder.START_TIME_IF.setText("开台时间：");   
		        viewHolder.TABLE_PERSON_REN.setText("人");		        
				break;
			case 3:
		        viewHolder.TABLE_BACKGROUND.setImageResource(R.drawable.table_unclear);   			
		        viewHolder.TABLE_PERSON.setText("待清理");
		        viewHolder.TABLE_PERSON_NUMBER.setText("");
		        viewHolder.START_TIME.setText(pictures.get(position).getTime());   
		        viewHolder.START_TIME_IF.setText("开台时间：");   
		        viewHolder.TABLE_PERSON_REN.setText("");		        
				break;			
			default:
				break;
		}

        viewHolder.TABLE_PLACE.setText(pictures.get(position).getPlace());  
       
		return convertView;
	}

}

