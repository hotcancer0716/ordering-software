package longfly.development;

public class Grid_Item_Cai {
 
		private	int		cai_style_pic;	//样式图片
        private String 	cai_style;		//样式内容
        private	int		cai_pic;		//菜品图片
        private int 	cai_number; 	//菜品图片的顺序号
        private String  cai_name;		//菜品名称
        private String  cai_price;		//菜品售价
        private String  cai_code;		//菜品编码
        
        public Grid_Item_Cai() 
        { 
            super(); 
        } 
     
        public Grid_Item_Cai(int cai_style_pic, String cai_style ,
        		int		cai_pic,int		cai_number ,
        		String cai_name ,String cai_price , 
        		String cai_code) 
        { 
            super(); 
            this.cai_style_pic  = cai_style_pic; 
            this.cai_style 		= cai_style; 
            this.cai_pic 		= cai_pic;
            this.cai_number 	= cai_number; 
            this.cai_name 		= cai_name; 
            this.cai_price 		= cai_price; 
            this.cai_code 		= cai_code; 
        } 
     
        public String getStyle( )
        {
            return cai_style;
        }

        public int getNumber() 
        { 
            return cai_number; 
        } 
        
        public int getStylePic() 
        { 
            return cai_style_pic; 
        }
        
        public int getPic() 
        { 
            return cai_pic; 
        } 
        
        public String getCaiName() 
        { 
            return cai_name; 
        } 
        
        public String getCaiPrice() 
        { 
            return cai_price; 
        }    
        
        public String getCaiCode() 
        { 
            return cai_code; 
        }       

	} 