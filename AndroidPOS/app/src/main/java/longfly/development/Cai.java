package longfly.development;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import casio.serial.SerialPort;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Cai extends Activity {
	
	GridView	GRID_VIEW_CAI;
	ListView	LIST_DIANCAI;
	ListView	LIST_NEW;
	LinearLayout		PART_1;
	LinearLayout		PART_2;
	Button		BTN_SWIFT_1;
	Button		BTN_SWIFT_2;
	Button		FRONTPAGE;
	Button		NEXTPAGE;
	Button		ZNDC;	//智能点餐
	AutoCompleteTextView	ACLT_NAME;
	AutoCompleteTextView	ACLT_CODE;
	LinearLayout		LAY_ACLT_PART_NAME;
	LinearLayout		LAY_ACLT_PART_CODE;
	Button		BTN_ACLT_PART_NAME;
	Button		BTN_ACLT_PART_CODE;	
	Button		BTN_ACLT_PART_NAME_OUT;	
	Button		BTN_ACLT_PART_CODE_OUT;	
	LinearLayout		BTN_ACLT_PART_NAME_SWITCH;	
	Button		BTN_ACLT_PART_NAME_SWITCH2;	
	LinearLayout		BTN_ACLT_PART_CODE_SWITCH;	
	Button		BTN_ACLT_PART_CODE_SWITCH2;	
	
	TextView	NOWPAGE;
	TextView	ALLPAGE;
	TextView	TOTAL_ALL;
	TextView	TABLE_NUM;
	TextView	SELL_NUM;
	ListView	BIG_CLASS;
	GridView	SMALL_CLASS;
	
	Button		BTN_STYLE_1;
	Button		BTN_STYLE_2;
	Button		BTN_STYLE_3;
	Button		BTN_STYLE_4;
	Button		BTN_STYLE_5;
	Button		BTN_STYLE_6;
	Button		BTN_STYLE_7;
	Button		BTN_STYLE_8;
	
	Button		BTN_CAI_XIADAN;
	Button		BTN_CAI_GUAQI;
	Button		BTN_CAI_JIEZHANG;
	
	/**************************************/
	String	CheckTitle,CheckTableNum,CheckNum,CheckContent,CheckDanhao,
	CheckXiaoFeiZongJia,CheckJiaShou,CheckYingShou,CheckXianJinShiSHou,
	CheckZhaoLing,CheckUser,CheckDateTime;
	boolean	Check_b1_1,Check_b1_2,Check_b1_3,Check_b1_4,Check_b1_5,Check_b1_6,Check_b1_7;
	boolean	Check_b2_1,Check_b2_2,Check_b2_3,Check_b2_4,Check_b2_5,Check_b2_6,Check_b2_7,Check_b2_8,Check_b2_9,Check_b2_10,Check_b2_11,Check_b2_12;
	boolean	Check_b3_1,Check_b3_2,Check_b3_3,Check_b3_4,Check_b3_5,Check_b3_6,Check_b3_7,Check_b3_8,Check_b3_9,Check_b3_10,Check_b3_11,Check_b3_12;

	TextView	DAILOG_CHECK_TEXT_TAIHAO;
	TextView	DAILOG_CHECK_TEXT_DANHAO;
	TextView	DAILOG_CHECK_TEXT_KAITAISHIJIAN;
	TextView	DAILOG_CHECK_TEXT_RENSHU;
	TextView	DAILOG_CHECK_TEXT_KAITAIREN;
	TextView	DAILOG_CHECK_TEXT_XIAOFEI;
	TextView	DAILOG_CHECK_TEXT_JIASHOU;
	TextView	DAILOG_CHECK_TEXT_ZHEKOU;
	TextView	DAILOG_CHECK_TEXT_ZONGJI;
	TextView	DAILOG_CHECK_TEXT_YINGSHOU;
	TextView	DAILOG_CHECK_TEXT_XIANSJIN;
	TextView	DAILOG_CHECK_TEXT_ZHAOLING;
	TextView	DAILOG_CHECK_TEXT_KAOHAO;
	TextView	DAILOG_CHECK_TEXT_JIFEN;
	TextView	DAILOG_CHECK_TEXT_KAYUKE;
	TextView	DAILOG_CHECK_TEXT_LEIXING;
	
	Button		DAILOG_BTN_CHECK_SK;
	Button		DAILOG_BTN_CHECK_0 ;	
	Button		DAILOG_BTN_CHECK_1 ;	
	Button		DAILOG_BTN_CHECK_2 ;	
	Button		DAILOG_BTN_CHECK_3 ;	
	Button		DAILOG_BTN_CHECK_4 ;	
	Button		DAILOG_BTN_CHECK_5 ;	
	Button		DAILOG_BTN_CHECK_6 ;	
	Button		DAILOG_BTN_CHECK_7 ;	
	Button		DAILOG_BTN_CHECK_8 ;	
	Button		DAILOG_BTN_CHECK_9 ;	
	Button		DAILOG_BTN_CHECK_CLEAR ;	
	Button		DAILOG_BTN_CHECK_DEL ;	
	Button		DAILOG_BTN_CHECK ;	
	Button		dAILOG_BTN_EXIT ;	
	
	
	
	/*********以下是对话框里的控件***********/
	
	Button	    DAILOG_BTN_CUICAI;	
	Button	    DAILOG_BTN_TUICAI;	
	Button	    DAILOG_BTN_HUANCAI;	
	Button	    DAILOG_BTN_SHANCHU;	
	Button	    DAILOG_BTN_GAIJIA;	
	Button	    DAILOG_BTN_ZUOFA;	
	Button	    DAILOG_BTN_SHULIANG;	
	Button	    DAILOG_BTN_FANHUI;	
	Button	    DAILOG_BTN_FRONT;	
	Button	    DAILOG_BTN_NEXT;	
	ImageView	DAILOG_QTY_REDUCE,DAILOG_QTY_ADD;
	EditText	DAILOG_QTY;
	TextView	DAILOG_CAI_NAME;
	
	View		BIG_ITem,SMALL_ITem;
	String		Share_Table_Name,Share_Cai_QTY,Share_Cai_Code,Share_Cai_ALLPrice;	
	String 		num = null ;					
	/*********以上是对话框里的控件***********/
   
	private List<Grid_Item_Cai_SQL> mylist;	//网格控件显示菜品
    private ArrayList<HashMap<String,String>> list=null;	//已点菜品的listview
    private HashMap<String,String>map=null;	//已点菜品
	private	GridItemCaiSQLAdapter	adapterr;	//网格控件和数据库接口
	SQLiteDatabase db;

	protected OutputStream mOutputStream;		//串口输出描述

	int	pageID=1;						//翻页的当前页
   	private int DIALOG_FIND=101;		//菜品操作的首页
   	private int DIALOG_CHECK=102;		//菜品操作的首页
	String Table_Num,Sell_Num,UserCode;		//从前面页面传进来的桌号和销售单号 和操作员
	SimpleAdapter list_adapter;		//已点菜品的数据库和listview接口
	int	Dispay_widthPixels;			//屏幕分辨率
	int	CAI_list_number;			//已点菜品的选中行号
	boolean	SWITCH_yidian = true;
	String all_page;
	String[] CaiDish_Name,CaiDish_Code ;
 	String bigclass;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		DisplayMetrics displaysMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displaysMetrics );
        Dispay_widthPixels=displaysMetrics.widthPixels;	//以上是初始化一个UI像素的描述！

        long	startTime =	Debug.threadCpuTimeNanos(); 
		switch (Dispay_widthPixels) {	//根据屏幕的横向像素来加载不通的UI界面！
			case 800:
		        setContentView(R.layout.i800_cai3);
		        break;
			case 1024:
		        setContentView(R.layout.i800_cai3);
				break;
			default:
		        setContentView(R.layout.i800_cai3);
				break;
		} 
        long	endXMLTime =	Debug.threadCpuTimeNanos(); 
		Log.i("info", "执行setContentView(R.layout.i800_cai3)用时"+(endXMLTime - startTime) +"纳秒");  
        
		db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
      
        find_id();
        key_things();
        init_bigclass();
        init_smallclass();
        init_all_cai();    	//计算全部菜品数量！用于翻页！    
 //       init_grid(1);		//显示菜品详细的gridview第一页
//        Ref_Cai_Grid_List();
        init_data();		//初始化从上一页获取的导入数据
        Refe_list();		//刷新新增菜品列表
        Refe_list_yidian(); //刷新已点菜品列表
        TABLE_NUM.setText(Table_Num);
        SELL_NUM.setText(Sell_Num);
        Total_Money();
       

        //创建一个ArrayAdapter  封装数组
        longfly.development.ArrayAdapter<String>aa = new longfly.development.ArrayAdapter<String>(
        		this, 
        		android.R.layout.simple_dropdown_item_1line,
        		CaiDish_Name
        	);
    
        ACLT_NAME.setAdapter(aa);

		longfly.development.ArrayAdapter<String> bb_code = new longfly.development.ArrayAdapter<String>(
        		this, 
        		android.R.layout.simple_dropdown_item_1line,
        		CaiDish_Code
        	);
    
        ACLT_CODE.setAdapter(bb_code);
        
        long	endTime =	Debug.threadCpuTimeNanos(); 
		Log.i("info", "初始化数据用时"+(endTime - endXMLTime) +"纳秒");  

		ALLPAGE.setText(   all_page  );

    }


    /*************大类开始*********************************/
	private void init_bigclass() {
		//从数据库获取大类数量
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_bigsort";
		
		String	sql= "select * from "+TABLE_NAME;     

		try {
			Cursor  cursor = db.rawQuery( sql, null);
			fill_hashmap_bigclass(cursor);
			inflateList_bigclass(cursor);	
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void inflateList_bigclass(Cursor cursor) {
		 //将数据与adapter集合起来
		list_adapter = new SimpleAdapter(
      		this, 
			list, 
			R.layout.i800_caiclass_item 
			, new String[]{"cName"}
			, new int[]{R.id.grid_caiclass_text }
		);
		
		//显示数据
		BIG_CLASS.setAdapter(list_adapter);
	}



	private ArrayList fill_hashmap_bigclass(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) 
		{
			i++;
          map=new HashMap<String,String>();
          
          map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
          map.put("cName", cursor.getString(cursor.getColumnIndex("cName")));
          map.put("cCode", cursor.getString(cursor.getColumnIndex("cCode")));
			
		  list.add(map);
		}
		
		return list;		
	}
    /*************大类结束*********************************/

	
    /*************小类开始*********************************/
	private void init_smallclass(String big_class) {
		//从数据库获取大类数量
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_smallsort";
		
		String	sql= "select * from "+TABLE_NAME+" where cBigCode=?";     
		String[] BigClass = { big_class };
		try {
 	 	    Cursor  cursor = db.rawQuery("select * from lfcy_base_smallsort where cBigCode = ?", BigClass);

			fill_hashmap_smallclass(cursor);
			inflateList_smallclass(cursor);	
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void init_smallclass() {
		//从数据库获取大类数量
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_smallsort";
		String	sql= "select * from lfcy_base_smallsort where cBigCode=?";     
		String[] BigClass = { "10001" };
		try {
			Cursor  cursor = db.rawQuery( sql, null);
			fill_hashmap_smallclass(cursor);
			inflateList_smallclass(cursor);	
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void inflateList_smallclass(Cursor cursor) {
		 //将数据与adapter集合起来
		list_adapter = new SimpleAdapter(
      		this, 
			list, 
			R.layout.i800_caismallclass_item 
			, new String[]{"cName"}
			, new int[]{R.id.grid_caiclass_text }
		);
		
		//显示数据
		SMALL_CLASS.setAdapter(list_adapter);
	}



	private ArrayList fill_hashmap_smallclass(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) 
		{
			i++;
          map=new HashMap<String,String>();
          
          map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
          map.put("cBigCode", cursor.getString(cursor.getColumnIndex("cBigCode")));
          map.put("cName"	, cursor.getString(cursor.getColumnIndex("cName")));
          map.put("cCode"	, cursor.getString(cursor.getColumnIndex("cCode")));
			
		  list.add(map);
		}
		
		return list;		
	}
    /*************小类结束*********************************/
	
	
	private void init_data() {
		Bundle bundle=getIntent().getExtras();
		try {
			  //获取Bundle的信息
			Table_Num = bundle.getString("table_number");
			Sell_Num  = bundle.getString("sell_number");
			UserCode = bundle.getString("UserCode");			
		} catch (Exception e) {
			  //获取Bundle的信息
			Table_Num = "0";
			Sell_Num  = "0";
			UserCode  = "0";		
		}
	}


	private void init_grid(int pageID ) {
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_dishinf";
		int PageSize = 16 ;
		
		String sql= "select * from " + TABLE_NAME +     
				" Limit "+String.valueOf(PageSize)+ " Offset " +String.valueOf( (pageID-1)*PageSize); 
		try {
			Cursor  cursor = db.rawQuery( sql, null);
			while (cursor.moveToNext()) {
				Grid_Item_Cai_SQL picture = new Grid_Item_Cai_SQL();
				picture.setName(cursor.getString(cursor
						.getColumnIndex("cDishName")));		//菜品名称
				picture.setCode(cursor.getString(cursor
						.getColumnIndex("cDishCode")));		//菜品编码
				picture.setStyle(cursor.getString(cursor
						.getColumnIndex("iDishStyle")));	//菜品类型 推荐新品还是什么别的			
				picture.setPrice(cursor.getString(cursor
						.getColumnIndex("fDishPrice")));	//菜品价格
				picture.setDian(cursor.getString(cursor
						.getColumnIndex("iDishDian")));		//该菜是否已被点
				picture.setPath(cursor.getString(cursor
						.getColumnIndex("cDishName2")));	//该菜的菜品图片
				mylist.add(picture);
			}
			NOWPAGE.setText(""+pageID);
			if (pageID == 1)
				ALLPAGE.setText(   String.valueOf( cursor.getCount()/PageSize + 1 )   );
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		adapterr = new GridItemCaiSQLAdapter(mylist, this ,Dispay_widthPixels);// 自定义适配器
		GRID_VIEW_CAI.setAdapter(adapterr);
	}

	//计算全部菜品的数量！用于翻页
	private void init_all_cai() {
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_dishinf";
		int PageSize = 16 ;
		
		String sql= "select * from " + TABLE_NAME ; 
		try {
			Cursor  cursor = db.rawQuery( sql, null);
			while (cursor.moveToNext()) {

			}
			all_page =  String.valueOf( cursor.getCount()/PageSize + 1 ) ;
			ALLPAGE.setText(   all_page  );
			CaiDish_Name = new String[cursor.getCount()];
			CaiDish_Code = new String[cursor.getCount()];
			cursor.close();

			Cursor  cursor_new = db.rawQuery( sql, null);
			int num=0;
			while (cursor_new.moveToNext()) {
				CaiDish_Name[num] = cursor_new.getString(cursor_new.getColumnIndex("cDishName"));
				CaiDish_Code[num] = cursor_new.getString(cursor_new.getColumnIndex("cDishCode"));
				num++;
			}			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void get_list_from(int style , String reason) {
		String TABLE_NAME = "lfcy_base_dishinf";
		String	sql,dish_name = null,dish_code = null ,dish_price = null,
				table_number,sell_number,cai_unitname = null,cai_unitcode =null;
		switch (style) {
		case 1:
			sql= "select * from "+TABLE_NAME+" where cDishName=?"; 					
			break;
		case 2:
			sql= "select * from "+TABLE_NAME+" where cDishCode=?"; 					
			break;
		default:
			sql= "select * from "+TABLE_NAME+" where cDishName=?"; 					
			break;
		}
		String[] refer = {reason};
		try {
			Cursor  cursor = db.rawQuery( sql, refer);
			while (cursor.moveToNext()) {
				dish_name  = cursor.getString(cursor.getColumnIndex("cDishName"));
				dish_code  = cursor.getString(cursor.getColumnIndex("cDishCode"));
				dish_price = cursor.getString(cursor.getColumnIndex("fDishPrice"));
				cai_unitname = cursor.getString(cursor.getColumnIndex("cDishUnitName"));
				cai_unitcode = cursor.getString(cursor.getColumnIndex("cDishUnitCode"));
			}	
			
			Toast.makeText(Cai.this, dish_name + "  "+ dish_code, Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			// TODO: handle exception
		}

        table_number	= Table_Num;
        sell_number 	= Sell_Num;
        try {
			insert_SellDishData_New(db, table_number ,sell_number, dish_name,dish_code,dish_price,"1",cai_unitname,cai_unitcode);
        } catch (Exception e) {
			// TODO: handle exception
        }

		        
        Toast toast = Toast.makeText(Cai.this, 
        		dish_name+" "+dish_code+" "+dish_price+"￥", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        View toastView = toast.getView();
        //创建一个imageview
        ImageView	image = new ImageView(Cai.this);
        image.setImageResource(R.drawable.other_1);
        //创建一个linearLayout容器
        LinearLayout ll = new LinearLayout(Cai.this);
        //向Linearlayout中添加图片和原有的view
        ll.addView(image);
        ll.addView(toastView);
        toast.setView(ll);
        toast.show();	
	}
	
	private void init_grids(String[] condition ) {
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_dishinf";
		int PageSize = 16 ;
		
		try {	
			String[]	Sql_String_condition = condition;	
			
			Cursor  cursor = db.rawQuery( "select * from lfcy_base_dishinf where iDishStyle = ?" , Sql_String_condition );
			while (cursor.moveToNext()) {
				Grid_Item_Cai_SQL picture = new Grid_Item_Cai_SQL();
				picture.setName(cursor.getString(cursor
						.getColumnIndex("cDishName")));		//菜品名称
				picture.setCode(cursor.getString(cursor
						.getColumnIndex("cDishCode")));		//菜品编码
				picture.setStyle(cursor.getString(cursor
						.getColumnIndex("iDishStyle")));	//菜品类型 推荐新品还是什么别的			
				picture.setPrice(cursor.getString(cursor
						.getColumnIndex("fDishPrice")));	//菜品价格
				picture.setDian(cursor.getString(cursor
						.getColumnIndex("iDishDian")));		//该菜是否已被点
				picture.setPath(cursor.getString(cursor
						.getColumnIndex("cDishName2")));
				mylist.add(picture);
			}
			NOWPAGE.setText(""+pageID);
			if (pageID == 1)
				ALLPAGE.setText(   String.valueOf( cursor.getCount()/PageSize + 1 )   );
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		adapterr = new GridItemCaiSQLAdapter(mylist, this,Dispay_widthPixels);// 自定义适配器
		GRID_VIEW_CAI.setAdapter(adapterr);
	}

	private void init_grids(int style , String[] condition) {
		// 用于初始化
		mylist = new ArrayList<Grid_Item_Cai_SQL>();
		String TABLE_NAME = "lfcy_base_dishinf";
		int PageSize = 16 ;
		
		try {	
			String[]	Sql_String_condition = condition;	
			Cursor  cursor ;
			
			switch (style) {
			case 1:		//大类
				cursor = db.rawQuery( "select * from lfcy_base_dishinf where cDishClass1 = ?" , Sql_String_condition );				
				break;
			case 2:		//小类
				cursor = db.rawQuery( "select * from lfcy_base_dishinf where cDishClass2 = ?" , Sql_String_condition );				
				break;
			case 3:
				cursor = db.rawQuery( "select * from lfcy_base_dishinf where iDishStyle = ?" , Sql_String_condition );				
				break;
			default:
				cursor = db.rawQuery( "select * from lfcy_base_dishinf where iDishStyle = ?" , Sql_String_condition );				
				break;
			}
			while (cursor.moveToNext()) {
				Grid_Item_Cai_SQL picture = new Grid_Item_Cai_SQL();
				picture.setName(cursor.getString(cursor
						.getColumnIndex("cDishName")));		//菜品名称
				picture.setCode(cursor.getString(cursor
						.getColumnIndex("cDishCode")));		//菜品编码
				picture.setStyle(cursor.getString(cursor
						.getColumnIndex("iDishStyle")));	//菜品类型 推荐新品还是什么别的			
				picture.setPrice(cursor.getString(cursor
						.getColumnIndex("fDishPrice")));	//菜品价格
				picture.setDian(cursor.getString(cursor
						.getColumnIndex("iDishDian")));		//该菜是否已被点
				picture.setPath(cursor.getString(cursor
						.getColumnIndex("cDishName2")));	//该菜菜品图片路径
				picture.setUnit(cursor.getString(cursor
						.getColumnIndex("cDishUnitName")));	//该菜菜品单位
				mylist.add(picture);
			}
			NOWPAGE.setText(""+pageID);
			if (pageID == 1)
				ALLPAGE.setText(   String.valueOf( cursor.getCount()/PageSize + 1 )   );
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		adapterr = new GridItemCaiSQLAdapter(mylist, this,Dispay_widthPixels);// 自定义适配器
		GRID_VIEW_CAI.setAdapter(adapterr);
	}
	
	private	void	Refe_list(){
        //右边的已点菜列表   
    	try {
	        //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
			String[] sell_number = {Sell_Num};
			Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf_new where cSellNo= ?", sell_number);
			fill_hashmap(cursor);
			inflateList(cursor);			
 		} catch (Exception e) {
 			// TODO: handle exception
 		}	
	}

	private	void	Refe_list_yidian(){
        //右边的已点菜列表   
    	try {
	        //cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
			String[] sell_number = {Sell_Num};
			Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf where cSellNo= ?", sell_number);
			fill_hashmap(cursor);
			inflateList_yidian(cursor);			
 		} catch (Exception e) {
 			// TODO: handle exception
 		}	
	}
	
	private void inflateList(Cursor cursor) {
		 //将数据与adapter集合起来
		list_adapter = new SimpleAdapter(
      		this, 
			list, 
			R.layout.imx_cai_list 
			, new String[]{"num" ,"cDishName" , "cDishQty","fDishPrice","cDishUnitName" }
			, new int[]{R.id.diglog_cai_line ,R.id.diglog_cai_name , R.id.diglog_cai_number, R.id.diglog_cai_price , R.id.diglog_cai_unitr }
		);
		
		//显示数据
		LIST_NEW.setAdapter(list_adapter);
	}
	
	private void inflateList_yidian(Cursor cursor) {
		 //将数据与adapter集合起来
		list_adapter = new SimpleAdapter(
       		this, 
			list, 
			R.layout.imx_cai_list 
			, new String[]{"num" ,"cDishName" , "cDishQty","fDishPrice","cDishUnitName"}
			, new int[]{R.id.diglog_cai_line ,R.id.diglog_cai_name , R.id.diglog_cai_number, R.id.diglog_cai_price , R.id.diglog_cai_unitr }
		);
		
		//显示数据
		LIST_DIANCAI.setAdapter(list_adapter);
	}



	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) 
		{
			i++;
           map=new HashMap<String,String>();
           map.put("num", ""+i);
           
           map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
           map.put("cDishName", cursor.getString(cursor.getColumnIndex("cDishName")));
           map.put("cDishCode", cursor.getString(cursor.getColumnIndex("cDishCode")));
           map.put("fDishPrice", cursor.getString(cursor.getColumnIndex("fDishPrice")));
           map.put("cDishQty",  cursor.getString(cursor.getColumnIndex("cDishQty")));
           map.put("cDishUnitName",  cursor.getString(cursor.getColumnIndex("cDishUnitName")));
           
		   list.add(map);
		}
		
		return list;		
	}
	
	private void key_things() {
		ACLT_NAME.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, View view, int position,
        		long id) {
		        String username = ACLT_NAME.getText().toString();
		        get_list_from(1 , username );
            	if ( SWITCH_yidian )
 		   			my_AnimationSet(1);				
 	            Refe_list();
		        Total_Money();	        
		        //按名称查找
		        //把菜品信息加入新增列表
	        }
        });
		
		ACLT_CODE.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, View view, int position,
        		long id) {
		        String username = ACLT_CODE.getText().toString();
		        get_list_from(2 , username );

            	if ( SWITCH_yidian )
 		   			my_AnimationSet(1);
 	            
            	Refe_list();
 	            Total_Money();	        
		        //按名称查找
		        //把菜品信息加入新增列表
	        }
        });
		
		BTN_SWIFT_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		my_AnimationSet(1);
		   	}
	    });	
		BTN_SWIFT_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		my_AnimationSet(2);			
		   	}
	    });	
		
		// TODO Auto-generated method stub
		 LIST_NEW.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
            	CAI_list_number = position;
 				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(CAI_list_number);
                Share_Table_Name =	String.valueOf(map.get("cDishName"));
                Share_Cai_QTY 	 =	String.valueOf(map.get("cDishQty"));
                Share_Cai_Code	 =	String.valueOf(map.get("cDishCode"));
		   	    System.out.println(" 1  Share_Cai_Code = "+ Share_Cai_Code );
                String[] Sql_String_condition = {Share_Cai_Code};
                Cursor cursor = db.rawQuery( "select fDishPrice from lfcy_base_dishinf where cDishCode = ?" , Sql_String_condition );				
				while(cursor.moveToNext()) 
				{
					Share_Cai_ALLPrice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
				}	   	    		
				showDialog(DIALOG_FIND);              
             }  
         }); 

		BIG_CLASS.setOnItemClickListener(new OnItemClickListener()   
         {   
             @SuppressWarnings("unchecked")
			public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {               	
  				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
  				bigclass =	String.valueOf(map.get("cCode"));   
  				System.out.println("smallclass == "+ bigclass);
  				
            	Ref_Cai_Grid_List();
            	
                try {
                	BIG_ITem.setBackgroundResource(R.drawable.table_number_up);             
				} catch (Exception e) {
					// TODO: handle exception
				}
				BIG_ITem = v;
				BIG_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
				
				//取消模糊查询功能
		   		LAY_ACLT_PART_NAME.setVisibility(View.GONE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.GONE);             
		   	}  
         });  
		
		SMALL_CLASS.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
              	String smallclass;
  				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
  				smallclass =	String.valueOf(map.get("cCode"));   
  				System.out.println("smallclass == "+ smallclass);
            	String[] nima = {smallclass};
            	init_grids(2,nima);
            	
                try {
                    //SMALL_ITem.setBackgroundColor(Color.argb(255, 63, 156, 4) );              
                	SMALL_ITem.setBackgroundResource(R.drawable.table_number_up);             
				} catch (Exception e) {
					// TODO: handle exception
				}
				SMALL_ITem = v;
				SMALL_ITem.setBackgroundColor(Color.argb(255, 22, 175, 251)); 
             }  
         });  
		GRID_VIEW_CAI.setOnItemClickListener(new OnItemClickListener()   
         {   
             public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
             {   
            	if ( SWITCH_yidian )
 		   			my_AnimationSet(1);
 		   		cai_control(position);
            	Total_Money();
             }  
         });  
		 
		FRONTPAGE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		Integer intObj = new Integer( NOWPAGE.getText().toString() );
		   	    int i = intObj.intValue();	
		   	    if(i == 1)
		   	    {
		   	    	Toast.makeText(Cai.this, "已经是最前一页了", Toast.LENGTH_SHORT).show();
		   	    }
		   	    else
		   	    {	
		   	    	init_grid( i-1);
		   	    	NOWPAGE.setText(""+(i-1));
		   	    }	
		   	}
	    });	
		NEXTPAGE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		Integer intObj = new Integer( NOWPAGE.getText().toString() );
		   	    int i = intObj.intValue();	

		   		intObj = new Integer( ALLPAGE.getText().toString() );
		   	    int j = intObj.intValue();	
		   	    
		   	    if(i == j)
		   	    {
		   	    	Toast.makeText(Cai.this, "已经是最后一页了", Toast.LENGTH_SHORT).show();
		   	    }
		   	    else
		   	    {	
		   	    	init_grid( i+1);
		   	    	NOWPAGE.setText(""+(i+1));
		   	    }				   	 
		   	}
	    });		

		
		ZNDC.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
	    		Intent intent_page_new = new Intent();
    			intent_page_new.setClass(Cai.this, Diancan_help.class);
	    		startActivity(intent_page_new);	
				overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);	   	
		    }
	    });	
		
		BTN_CAI_XIADAN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		//应该搞一个线程！
		   		try {
			   		Save_SharePreference();					
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(Cai.this, "下单失败，请检查数据是否设置正确或重新下单", Toast.LENGTH_SHORT).show();
		   		}	
		   	}
	    });	
		
		BTN_CAI_GUAQI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		db.close();
		   		finish();
		   	}
	    });	
		
		BTN_CAI_JIEZHANG.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
   	    		showDialog(DIALOG_CHECK);              

		   		//打印已点菜的菜单
		   		
		   		//改变桌台状态

		   	}
	    });	
		
		BTN_ACLT_PART_NAME.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.VISIBLE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.GONE);
		   	}
	    });			
		BTN_ACLT_PART_NAME_OUT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.GONE);
		   	}
	    });	
		BTN_ACLT_PART_CODE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.GONE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.VISIBLE);
		   	}
	    });	
		BTN_ACLT_PART_CODE_OUT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_CODE.setVisibility(View.GONE);
		   	}
	    });	
		
		BTN_ACLT_PART_NAME_SWITCH.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.GONE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.VISIBLE);		   	
		   	}
	    });	
		BTN_ACLT_PART_NAME_SWITCH2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.GONE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.VISIBLE);		   	
		   	}
	    });	
		BTN_ACLT_PART_CODE_SWITCH.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.VISIBLE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.GONE);		   	
		   	}
	    });	
		BTN_ACLT_PART_CODE_SWITCH2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		LAY_ACLT_PART_NAME.setVisibility(View.VISIBLE);
		   		LAY_ACLT_PART_CODE.setVisibility(View.GONE);		   	
		   	}
	    });	
	}
	
	private	void my_AnimationSet( int mycase){
		AnimationSet animationSet_in = new AnimationSet(true); 
		TranslateAnimation translateAnimation_in = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation_in.setStartOffset(400);
		translateAnimation_in.setDuration(400); 
		animationSet_in.addAnimation(translateAnimation_in); 	
		
		AnimationSet animationSet_out = new AnimationSet(true); 
		TranslateAnimation translateAnimation_out = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation_out.setDuration(400); 
		animationSet_out.addAnimation(translateAnimation_out); 		
		
		switch (mycase) {
		case 1:		//首页出   尾页进
			PART_1.startAnimation(animationSet_out);				
			PART_1.setVisibility(View.GONE);		
			SWITCH_yidian = false;
			PART_2.setVisibility(View.VISIBLE);			
			PART_2.startAnimation(animationSet_in);				
			break;
		case 2:		//首页进  尾页出 
			PART_2.startAnimation(animationSet_out);				
			PART_2.setVisibility(View.GONE);			
			SWITCH_yidian = true;
			PART_1.setVisibility(View.VISIBLE);			
			PART_1.startAnimation(animationSet_in);				
			break;
		default:
			break;
		}
	}
	
	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	protected void  Total_Money() {
		// TODO Auto-generated method stub
		try {
			String[] sell_number = {Sell_Num};
			Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf where cSellNo= ?", sell_number);
			float	Total_All = 0;
			while(cursor.moveToNext()) 
			{
				String Sprice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
//				Integer intObj = new Integer(Sprice);
//			    int i = intObj.intValue();	
			    float f = Float.parseFloat(Sprice);
			    Total_All = Total_All + f;
			}		
			TOTAL_ALL.setText(""+Total_All);			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}


	protected void cai_control(int position) {
		// TODO Auto-generated method stub
		try {
	 		 String	cai_name,cai_price,cai_code,table_number,sell_number,cai_unitname,cai_unitcode;
	         cai_name 	=	mylist.get(position).getCaiName();
	         cai_price 	=	mylist.get(position).getCaiPrice();
	         cai_code 	=	mylist.get(position).getCaiCode();
	         cai_unitname = mylist.get(position).getUnit();
	         cai_unitcode = mylist.get(position).getUnit();
	         table_number	= Table_Num;
	         sell_number 	= Sell_Num;
	         System.out.println(cai_name + cai_code+ cai_price +"￥"+sell_number+ table_number);

	         //如果有重复的！叠加菜品数量！并且updata
 //  			 sql_control sqlc = new sql_control();					
//	         boolean  dian_or_no = sqlc.sql_item_only_one(db, "lfcy_base_sellinf_new", "cDishCode", cai_code);
	 		
	         boolean	dian_or_no = false;
			String name[] = {sell_number,cai_code};
			try {
	    		Cursor  cursor = db.rawQuery(
	    				"select * from "+ "lfcy_base_sellinf_new" +" where "+ "cSellNo" +" = ?" +"and cDishCode = ?", 
	    				name);
	    		int num = cursor.getCount();

	    		if(num == 0)
	    			dian_or_no = true;	//true是没有重复
	    		else
	    			dian_or_no = false;		//true是有重复	
			} catch (Exception e) {
				dian_or_no = false;			//true是有重复
			}
	         
	         
	         if (dian_or_no) {
				insert_SellDishData_New(db, table_number ,sell_number, cai_name,cai_code,cai_price,"1",cai_unitname,cai_unitcode);				
			 } else {
				String refname[] = {sell_number,cai_code};
	    		System.out.println("cai_qty satrt ");	    		
	    		
				Cursor  cursor = db.rawQuery(
	    				"select cDishQty from lfcy_base_sellinf_new  where cSellNo = ? and cDishCode = ?", 
	    				refname );
	    		System.out.println("cai_qty end ");	    		
	    		cursor.moveToFirst();
	    		String recorsend = cursor.getString(0).toString();	//
	    	    Integer intObj = new Integer(recorsend);
	    	    int i = intObj.intValue();	

//	    	    Integer intObj_price = new Integer(cai_price);
//	    	    int pricell = intObj_price.intValue() * (i+1);	
	    	    float pricell = Float.parseFloat(cai_price);
	   			ContentValues cv = new ContentValues();
	   		   	//存储修改的数据
	   		   	cv.put("iTableNumber" 	, table_number);
	   		   	cv.put("cSellNo" 		, sell_number);
	   		   	cv.put("cDishName" 		, cai_name);
	   		   	cv.put("cDishCode" 		, cai_code);
	   		   	cv.put("fDishPrice" 	, pricell*(i+1));
	   		   	cv.put("cDishQty" 		, ""+(i+1));
	   		   	cv.put("cDishUnitName" 	, cai_unitname);
	   		   	cv.put("cDishUnitCode" 	, cai_unitcode);
	   		   	String[] contion = {cai_code,sell_number}; 
			   	db.update("lfcy_base_sellinf_new",cv,"cDishCode = ? and cSellNo =?",contion);
			}
	         			
	         Refe_list(); 			
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(Cai.this, "温馨提示：最左边可以上下滑动 显示菜品分类 ", Toast.LENGTH_SHORT).show();
		}
 
	}


	protected void insert_SellDishData(SQLiteDatabase db, String table_number,
			String sell_number, String cai_name, String cai_code,
			String cai_price, String cai_number,String cai_unit_name,
			String cai_unit_code) {
		// TODO Auto-generated method stub  	
    	int mYear,mMonth,mDay,mHour,mMinute,mSecond;
    	//获取桌台号和即时时间    
   		final Calendar c = 	Calendar.getInstance();
        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
        mMonth 	= c.get(Calendar.MONTH)+ 1;	//获取当前月份
        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
        mSecond = c.get(Calendar.SECOND);	//获取当前的秒钟数		   		

   		String mmHour,mmMinute,mmSecond;

   		if(mHour < 10)
   			mmHour = "0"+mHour;
   		else
   			mmHour = ""+mHour;

   		if(mMinute < 10)
   			mmMinute = "0"+mMinute;
   		else
   			mmMinute = ""+mMinute;

   		if(mSecond < 10)
   			mmSecond = "0"+mSecond;
   		else
   			mmSecond = ""+mSecond;

   		String	system_date	= mYear+"-"+mMonth+"-"+	mDay;
   		String	system_time	= mmHour+":"+mmMinute+":"+mmSecond;
   		String  str = system_date + " "+system_time;
		db.execSQL("insert into lfcy_base_sellinf values(null,?,?,?,?,?,?,?,?,?)", 
				new String[]{
					table_number ,sell_number ,	cai_name ,cai_code ,
					cai_price , cai_number ,cai_unit_name , cai_unit_code,str }
				);
	}

	protected void insert_SellDishData_New(SQLiteDatabase db, String table_number,
			String sell_number, String cai_name, String cai_code,
			String cai_price, String cai_number,String cai_unit_name,
			String cai_unit_code) {
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_sellinf_new values(null,?,?,?,?,?,?,?,?,null)", 
				new String[]{
					table_number ,sell_number ,	cai_name ,cai_code ,
					cai_price , cai_number ,cai_unit_name , cai_unit_code }
				);
	}
	
	protected void remove_cai(SQLiteDatabase db, String	id) {
		// TODO Auto-generated method stub
   		try {
	   			db.delete("lfcy_base_sellinf_new", " _id =?", new String[]{id});
   			} catch (Exception e) {
					
   		}
	}

	private void find_id() {
		// TODO Auto-generated method stub
		GRID_VIEW_CAI	=	(GridView) findViewById(R.id.cai_gridview);
		LIST_DIANCAI	=	(ListView) findViewById(R.id.cai_listView_diancai);
		LIST_NEW	=	(ListView) findViewById(R.id.cai_listView_diancai_new);
		FRONTPAGE		=	(Button) findViewById(R.id.cai_page_front);
		NEXTPAGE		=	(Button) findViewById(R.id.cai_page_next);
		ZNDC			=	(Button) findViewById(R.id.cai_diancanhelp);
		NOWPAGE  		=	(TextView) findViewById(R.id.cai_page_now);
		ALLPAGE			=	(TextView) findViewById(R.id.cai_page_all);
		TOTAL_ALL		=	(TextView) findViewById(R.id.cai_tablemoney_text);
		TABLE_NUM		=	(TextView) findViewById(R.id.cai_tablenum);
		SELL_NUM		=	(TextView) findViewById(R.id.cai_sellnum);

		BTN_CAI_XIADAN		=	(Button) findViewById(R.id.cai_xiadan);
		BTN_CAI_GUAQI		=	(Button) findViewById(R.id.cai_guaqi);
		BTN_CAI_JIEZHANG	=	(Button) findViewById(R.id.cai_jiezhang);
		ACLT_NAME	=	(AutoCompleteTextView) findViewById(R.id.autocomplete_cainame);
		ACLT_CODE	=	(AutoCompleteTextView) findViewById(R.id.autocomplete_caicode);
		LAY_ACLT_PART_NAME	=	(LinearLayout) findViewById(R.id.lay_ac_name);
		LAY_ACLT_PART_CODE	=	(LinearLayout) findViewById(R.id.lay_ac_code);
		BTN_ACLT_PART_NAME		=	(Button) findViewById(R.id.cai_search_name);
		BTN_ACLT_PART_NAME_OUT	=	(Button) findViewById(R.id.lay_ac_name_out);
		BTN_ACLT_PART_CODE		=	(Button) findViewById(R.id.cai_search_code);
		BTN_ACLT_PART_CODE_OUT	=	(Button) findViewById(R.id.lay_ac_code_out);
		BTN_ACLT_PART_NAME_SWITCH = (LinearLayout) findViewById(R.id.autocomplete_switch_name);
		BTN_ACLT_PART_CODE_SWITCH = (LinearLayout) findViewById(R.id.autocomplete_switch_code);
		BTN_ACLT_PART_NAME_SWITCH2 = (Button) findViewById(R.id.autocomplete_switch_name2);
		BTN_ACLT_PART_CODE_SWITCH2 = (Button) findViewById(R.id.autocomplete_switch_code2);
		BIG_CLASS	=	(ListView) findViewById(R.id.cai_bigclass_list);
		SMALL_CLASS	=	(GridView) findViewById(R.id.cai_smallclass_list);
/*
		BTN_STYLE_1		=	(Button) findViewById(R.id.cai_style_button_1);
		BTN_STYLE_2		=	(Button) findViewById(R.id.cai_style_button_2);
		BTN_STYLE_3		=	(Button) findViewById(R.id.cai_style_button_3);
		BTN_STYLE_4		=	(Button) findViewById(R.id.cai_style_button_4);
		BTN_STYLE_5		=	(Button) findViewById(R.id.cai_style_button_5);
		BTN_STYLE_6		=	(Button) findViewById(R.id.cai_style_button_6);
		BTN_STYLE_7		=	(Button) findViewById(R.id.cai_style_button_7);
		BTN_STYLE_8		=	(Button) findViewById(R.id.cai_style_button_8);
*/		
		BTN_SWIFT_1		=	(Button) findViewById(R.id.cai_button_swift_1);
		BTN_SWIFT_2		=	(Button) findViewById(R.id.cai_button_swift_2);
		
		PART_1	=	(LinearLayout) findViewById(R.id.cai_yidian_part_1);
		PART_2	=	(LinearLayout) findViewById(R.id.cai_yidian_part_2);
	}
	

	
	/***      初始一个对话框！ 命名为buildDialog      ***/
    private Dialog buildCheckDialog(Context context) {
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("结账消费管理");  
        LayoutInflater inflater = LayoutInflater.from(Cai.this);
        View view = null ;
        switch (Dispay_widthPixels) {
			case 1024:
		        view = inflater.inflate(R.layout.i800dialog_cai_check, null);			
				break;
			case 800:
		        view = inflater.inflate(R.layout.i800dialog_cai_check, null);			
				break;
			default:
		        view = inflater.inflate(R.layout.i800dialog_cai_check, null);			
				break;
		}
        builder.setView(view);
        builder.setCancelable(true);
 
        
        DAILOG_CHECK_TEXT_TAIHAO		=	(TextView) view.findViewById(R.id.dialog_check_tx_taihao);
        DAILOG_CHECK_TEXT_DANHAO		=	(TextView) view.findViewById(R.id.dialog_check_tx_danhao);
    	DAILOG_CHECK_TEXT_KAITAISHIJIAN =	(TextView) view.findViewById(R.id.dialog_check_tx_kaitaishijian);
    	DAILOG_CHECK_TEXT_RENSHU 		=	(TextView) view.findViewById(R.id.dialog_check_renshu);
    	DAILOG_CHECK_TEXT_KAITAIREN 	=	(TextView) view.findViewById(R.id.dialog_check_kaitairen);
    	DAILOG_CHECK_TEXT_XIAOFEI 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_xiaofei);
    	DAILOG_CHECK_TEXT_JIASHOU 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_jiashou);
    	DAILOG_CHECK_TEXT_ZHEKOU 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_zhekou);
    	DAILOG_CHECK_TEXT_ZONGJI 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_zongji);
    	DAILOG_CHECK_TEXT_YINGSHOU 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_yingshou);
    	DAILOG_CHECK_TEXT_XIANSJIN 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_xianjin);
    	DAILOG_CHECK_TEXT_ZHAOLING 		=	(TextView) view.findViewById(R.id.dialog_check_block2_tx_zhaoling);
    	DAILOG_CHECK_TEXT_KAOHAO 		=	(TextView) view.findViewById(R.id.dialog_check_kahao);
    	DAILOG_CHECK_TEXT_JIFEN 		=	(TextView) view.findViewById(R.id.dialog_check_jifen);
    	DAILOG_CHECK_TEXT_KAYUKE 		=	(TextView) view.findViewById(R.id.dialog_check_yue);
        DAILOG_CHECK_TEXT_LEIXING		=	(TextView) view.findViewById(R.id.dialog_check_leixing);

        DAILOG_BTN_CHECK_0	=	(Button) view.findViewById(R.id.dialog_check_btn_control_0);
        DAILOG_BTN_CHECK_1	=	(Button) view.findViewById(R.id.dialog_check_btn_control_1);
        DAILOG_BTN_CHECK_2	=	(Button) view.findViewById(R.id.dialog_check_btn_control_2);
        DAILOG_BTN_CHECK_3	=	(Button) view.findViewById(R.id.dialog_check_btn_control_3);
        DAILOG_BTN_CHECK_4	=	(Button) view.findViewById(R.id.dialog_check_btn_control_4);
        DAILOG_BTN_CHECK_5	=	(Button) view.findViewById(R.id.dialog_check_btn_control_5);
        DAILOG_BTN_CHECK_6	=	(Button) view.findViewById(R.id.dialog_check_btn_control_6);
        DAILOG_BTN_CHECK_7	=	(Button) view.findViewById(R.id.dialog_check_btn_control_7);
        DAILOG_BTN_CHECK_8	=	(Button) view.findViewById(R.id.dialog_check_btn_control_8);
        DAILOG_BTN_CHECK_9	=	(Button) view.findViewById(R.id.dialog_check_btn_control_9);
        DAILOG_BTN_CHECK_CLEAR	=	(Button) view.findViewById(R.id.dialog_check_btn_control_clear);
        DAILOG_BTN_CHECK_DEL	=	(Button) view.findViewById(R.id.dialog_check_btn_control_del);

        DAILOG_BTN_CHECK_SK	=	(Button) view.findViewById(R.id.dialog_check_btn_control_card);
        DAILOG_BTN_CHECK	=	(Button) view.findViewById(R.id.dialog_check_button_enter_casio);	        
        dAILOG_BTN_EXIT		=	(Button) view.findViewById(R.id.dialog_check_button_exit);	        
        get_table_message(Table_Num);
        
    	DAILOG_CHECK_TEXT_TAIHAO.setText(Table_Num);
    	DAILOG_CHECK_TEXT_DANHAO.setText(Sell_Num);
    	DAILOG_CHECK_TEXT_KAITAIREN.setText(UserCode);
    	DAILOG_CHECK_TEXT_XIAOFEI.setText( TOTAL_ALL.getText().toString() );

//	    Integer intObj = new Integer(TOTAL_ALL.getText().toString());
//	    int tatal = intObj.intValue();	
	    float tatal = Float.parseFloat(TOTAL_ALL.getText().toString());
	    try {
//		    intObj = new Integer(DAILOG_CHECK_TEXT_JIASHOU.getText().toString());			
		} catch (Exception e) {
			// TODO: handle exception
//		    intObj = 2;
		}
//	    int jaishou = intObj.intValue();	
	    int jaishou = 0;	
	    
    	DAILOG_CHECK_TEXT_ZONGJI.setText(""+(tatal+jaishou));
    	DAILOG_CHECK_TEXT_YINGSHOU.setText(""+(tatal+jaishou));

		DAILOG_BTN_CHECK_SK.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_CHECK);  //关闭对话框 
		   	}
	    });
		dAILOG_BTN_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_CHECK);  //关闭对话框 
		   		finish();
		   	}
	    });
		DAILOG_BTN_CHECK.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		DAILOG_BTN_CHECK.setEnabled(false);
		   		try {
//				    Integer intObj = new Integer(DAILOG_CHECK_TEXT_YINGSHOU.getText().toString());
//				    int tatal = intObj.intValue();	
				    float tatal = Float.parseFloat(DAILOG_CHECK_TEXT_YINGSHOU.getText().toString());				    
//				    intObj = new Integer(DAILOG_CHECK_TEXT_XIANSJIN.getText().toString());
//				    int xianjin = intObj.intValue();
				    float xianjin = Float.parseFloat(DAILOG_CHECK_TEXT_XIANSJIN.getText().toString());
				    if(xianjin >= tatal )
				    {	
				    	DAILOG_CHECK_TEXT_ZHAOLING.setText(""+( xianjin - tatal));
				   		ContentValues cv = new ContentValues();
					   	//存储修改的数据
					   	cv.put("iTableState" ,3);
					   	cv.put("iTable_Person","");
					   	cv.put("iTabletime","");
					   	String[] contion = {Table_Num}; 
				   		try {
						   	//更新数据
						   	db.update("lfcy_base_tableinf",cv,"iTableNumber = ? ",contion);
						} catch (Exception e) {
							// TODO: handle exception
						}

						/**
						 * 根据销售单号，桌号，人数，操作员，销售金额，结账时间，生成结账数据
						 **/
						//查找店号，机号，操作员
			    		Cursor  cursor = db.rawQuery("select * from lfcy_base_posinf ", null);
			    		cursor.moveToFirst();
			    		String iPosMachineNum = cursor.getString(1).toString();	//机器号
			    		String iPosShopNum = cursor.getString(2).toString();	//门店号
			    		
						//获取结账日期和时间
			    		int	mYear,mMonth,mDay,mHour,mMinute;
			       		final Calendar c = 
			       			Calendar.getInstance();
			       			        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
			       			        mMonth 	= c.get(Calendar.MONTH)+1;	//获取当前月份
			       			        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
			       			        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
			       			        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
			       							
						//根据店号机号结账日期和流水生成一条结账单号
						String	Check_Code	=	iPosShopNum+iPosMachineNum+mYear+mMonth+mDay+mHour+mMinute;
			       			        
						//增加一条结账数据
     					//结账时间//操作员编码//门店号//机器号//桌台号//结账单号//销售单号（利用开台时间）
						//实际消费金额//服务费及加收金额//打折及直减金额//最终应收交易金额
						//最终实收交易金额  //找零金额//桌台容纳人数
						insert_CheckData(db ,
								mYear+"-"+mMonth+"-"+mDay+" "+mHour+":"+mMinute,  
								UserCode , 
								iPosShopNum ,
								iPosMachineNum , 
								DAILOG_CHECK_TEXT_TAIHAO.getText().toString() , 
								Check_Code  ,
								DAILOG_CHECK_TEXT_DANHAO.getText().toString() ,
								DAILOG_CHECK_TEXT_XIAOFEI.getText().toString(),
								DAILOG_CHECK_TEXT_JIASHOU.getText().toString(),
								DAILOG_CHECK_TEXT_ZHEKOU.getText().toString(),
								DAILOG_CHECK_TEXT_YINGSHOU.getText().toString(),
								DAILOG_CHECK_TEXT_XIANSJIN.getText().toString(),
								DAILOG_CHECK_TEXT_ZHAOLING.getText().toString(),
								DAILOG_CHECK_TEXT_RENSHU.getText().toString()								
							);

				   		/***/
				   		CheckTitle = "     欢迎光临“8090自助餐厅”";
				   		CheckTableNum = DAILOG_CHECK_TEXT_TAIHAO.getText().toString();
				   		CheckNum = Check_Code;
				   		CheckContent = "编号  菜品名称   数量   价格";
				   		CheckDanhao = DAILOG_CHECK_TEXT_DANHAO.getText().toString();
				   		CheckXiaoFeiZongJia = DAILOG_CHECK_TEXT_XIAOFEI.getText().toString();
				   		CheckJiaShou = DAILOG_CHECK_TEXT_JIASHOU.getText().toString();
				   		CheckYingShou = DAILOG_CHECK_TEXT_YINGSHOU.getText().toString();
				   		CheckXianJinShiSHou = DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
				   		CheckZhaoLing = DAILOG_CHECK_TEXT_ZHAOLING.getText().toString();
				   		CheckUser = UserCode;
				   		CheckDateTime = mYear+"-"+mMonth+"-"+ mDay +" "+mHour+":"+mMinute;
				   		/***/
				   		
						//启动一条线程！负责获取打印内容，打印格式，最终打印出来
				   		try {
				   			Save_CHECKSharePreference();					
				   		} catch (Exception e) {
							// TODO: handle exception
				   			Toast.makeText(Cai.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
				   		}	   				
						
				    }
				    else
				    	Toast.makeText(Cai.this, "请重新输入现金", Toast.LENGTH_SHORT).show();					
				} catch (Exception e) {
			    	Toast.makeText(Cai.this, "请输入现金后,再按结账按钮", Toast.LENGTH_SHORT).show();					
				}
				
				new Handler().postDelayed(new Runnable(){   
				    public void run() {   
				    //execute the task   
				    	Toast.makeText(Cai.this, "       找零"+ DAILOG_CHECK_TEXT_ZHAOLING.getText().toString()+"元       ", Toast.LENGTH_LONG).show();					
				   		removeDialog(DIALOG_CHECK);  //关闭对话框 
				   		db.close();
				   		finish();
				    }   
				 }, 3000);  
		   	}
	    });		
		DAILOG_BTN_CHECK_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  1	);		   	
		   	}
	    });
		DAILOG_BTN_CHECK_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  2	);		   	
		   	}
	    });
		DAILOG_BTN_CHECK_3.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 3	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_4.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 4	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_5.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 5	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_6.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  6);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_7.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num + 7	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_8.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  8	);		   	
		   	}
	    });		
		DAILOG_BTN_CHECK_9.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  9	);		   	
		   	}
	    });	
		DAILOG_BTN_CHECK_0.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText(num +  0	);		   	
		   	}
	    });			
		DAILOG_BTN_CHECK_CLEAR.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText("");		   	
		   	}
	    });			
		DAILOG_BTN_CHECK_DEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String	num	=	DAILOG_CHECK_TEXT_XIANSJIN.getText().toString();
		   		String	str =	num.substring(0,num.length()-1);
		   		DAILOG_CHECK_TEXT_XIANSJIN.setText( str	);		   	
		   	}
	    });			
		
		return builder.create();  
    } 
	
    protected void insert_CheckData(SQLiteDatabase db, 
    		String dtCheckDate,
			String cUserCode, 
			String iPosShopNum, 
			String iPosMachineNum,
			String iTableNumber, 
			String cPayNo, 
			String cSellNo,
			String fSaleSum,
			String fSvrSum,
			String fReduceSum, 
			String fTradeSum, 
			String fTradeEndSum,
			String fPayBackSum, 
			String iTable_Person) 
    {
			
		// TODO Auto-generated method stub
		db.execSQL("insert into lfcy_base_tablecheckinf values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", 
				new String[]{ 
					dtCheckDate,		//结账时间
					cUserCode ,			//操作员编码
					iPosShopNum , 		//门店号
					iPosMachineNum , 	//机器号
					iTableNumber  ,		//桌台号
					cPayNo , 			//结账单号
					cSellNo,			//销售单号（利用开台时间）
					fSaleSum,			//实际消费金额
					fSvrSum,			//服务费及加收金额
					fReduceSum,			//打折及直减金额
					fTradeSum,			//最终应收交易金额
					fTradeEndSum,		//最终实收交易金额
					fPayBackSum,		//找零金额
					iTable_Person		//桌台容纳人数
		 });
    }
    
	private void get_table_message(String table_Num2) {
		// TODO Auto-generated method stub
		String[] Contind	= {table_Num2};
		try {
			//根据桌号查询桌台消费信息
    		Cursor  cursor = db.rawQuery("select * from lfcy_base_tableinf where iTableNumber = ?", Contind);
    		cursor.moveToFirst();
    		int    TABLE_STYLE = cursor.getInt(2);
    		String iTabletime = cursor.getString(5).toString();
    		String iTable_Person = cursor.getString(7).toString();	
    		
    	    Integer intObj = new Integer(iTable_Person);
    	    int person = intObj.intValue();	
    		if(TABLE_STYLE == 1)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("大厅");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+(person*2));
    		}
    		if(TABLE_STYLE == 2)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("包厢");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+person*2);       	
    		}
    		if(TABLE_STYLE == 3)
    		{	
    			DAILOG_CHECK_TEXT_LEIXING.setText("卡座");
    			DAILOG_CHECK_TEXT_JIASHOU.setText(""+person*5);
    		}
    		DAILOG_CHECK_TEXT_KAITAISHIJIAN.setText(iTabletime);
        	DAILOG_CHECK_TEXT_RENSHU.setText(iTable_Person);

		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}


	/***      初始一个对话框！ 命名为buildDialog      ***/
    private Dialog buildDialog(Context context) {
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1);  
        builder.setTitle("已点菜品管理");  
        LayoutInflater inflater = LayoutInflater.from(Cai.this);
        View view = null ;
        switch (Dispay_widthPixels) {
			case 1024:
		        view = inflater.inflate(R.layout.dialog_cailist_control, null);			
				break;
			case 800:
		        view = inflater.inflate(R.layout.i800dialog_cai_control, null);			
				break;
			default:
		        view = inflater.inflate(R.layout.i800dialog_cai_control, null);			
		        break;
		}
        builder.setView(view);
        builder.setCancelable(true);
        
        
		DAILOG_BTN_CUICAI 	= (Button) view.findViewById(R.id.diglog_cai_cuicai);
		DAILOG_BTN_TUICAI 	= (Button) view.findViewById(R.id.diglog_cai_tuicai);
		DAILOG_BTN_HUANCAI 	= (Button) view.findViewById(R.id.diglog_cai_huancai);
		DAILOG_BTN_SHANCHU 	= (Button) view.findViewById(R.id.diglog_cai_shanchu);
		DAILOG_BTN_GAIJIA 	= (Button) view.findViewById(R.id.diglog_cai_gaijia);
		DAILOG_BTN_ZUOFA 	= (Button) view.findViewById(R.id.diglog_cai_zuofa);
		DAILOG_BTN_SHULIANG	= (Button) view.findViewById(R.id.diglog_cai_shuliang);
		DAILOG_BTN_FANHUI	= (Button) view.findViewById(R.id.diglog_cai_back);
		DAILOG_BTN_FRONT	= (Button) view.findViewById(R.id.diglog_cai_front);
		DAILOG_BTN_NEXT		= (Button) view.findViewById(R.id.diglog_cai_next);
		DAILOG_QTY_REDUCE   = (ImageView) view.findViewById(R.id.diglog_qty_reduce);
		DAILOG_QTY_ADD   	= (ImageView) view.findViewById(R.id.diglog_qty_add);
		DAILOG_QTY   		= (EditText) view.findViewById(R.id.diglog_qty);
		DAILOG_CAI_NAME 	= (TextView) view.findViewById(R.id.diglog_cai_name);

		DAILOG_CAI_NAME.setText(Share_Table_Name);
		DAILOG_QTY.setText(Share_Cai_QTY);
		
		DAILOG_QTY_REDUCE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String dish_qty_string = DAILOG_QTY.getText().toString();
		   	    Integer intObj = new Integer(dish_qty_string);
		   	    int i = intObj.intValue();			   	    
		   	    if ((i - 1)> 0) {
		   	    	DAILOG_QTY.setText(""+(i-1));
		   	    	
		   	    	ContentValues cv = new ContentValues();
		   	    	String sell_number = Sell_Num;
		   		   	cv.put("cSellNo" 		, sell_number);	
		   		   	
			   	    float intPrice = Float.parseFloat(Share_Cai_ALLPrice);
			   	    System.out.println("intPrice = "+ intPrice + "    i = " + i);
		   		   	cv.put("fDishPrice" 	, ""+intPrice*(i-1));
		   		   	cv.put("cDishQty" 		, ""+(i-1));
			   	    System.out.println("ALLPrice = "+ intPrice *(i-1) );
		   		   	String[] contion = {Share_Cai_Code,sell_number}; 
			   	    System.out.println("Share_Cai_Code = "+ Share_Cai_Code + " sell_number = " + sell_number);
		   		   	db.update("lfcy_base_sellinf_new",cv,"cDishCode = ? and cSellNo =?",contion);
				   	Refe_list();		//刷新已点菜品列表

				} else {

				}
		   	}

	    });

		DAILOG_QTY_ADD.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		String dish_qty_string = DAILOG_QTY.getText().toString();
		   	    Integer intObj = new Integer(dish_qty_string);
		   	    int i = intObj.intValue();	
		   		DAILOG_QTY.setText(""+(i+1));
	   	    	String sell_number = Sell_Num;
	   			ContentValues cv = new ContentValues();
	   		   	//存储修改的数据
	   		   	cv.put("cSellNo" 		, sell_number);		   		   		
		   	    float intPrice = Float.parseFloat(Share_Cai_ALLPrice);
	   		   	cv.put("fDishPrice" 	, ""+intPrice*(i+1));
	   		   	cv.put("cDishQty" 		, ""+(i+1));
	   		   	String[] contion = {Share_Cai_Code,sell_number}; 
		   	    System.out.println("Share_Cai_Code = "+ Share_Cai_Code + " sell_number = " + sell_number);

	   		   	db.update("lfcy_base_sellinf_new",cv,"cDishCode = ? and cSellNo =?",contion);
		        Refe_list();		//刷新已点菜品列表

		   	}

	    });
		
		DAILOG_BTN_CUICAI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });

		DAILOG_BTN_TUICAI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 

		   	}

	    });
		
		DAILOG_BTN_HUANCAI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 

		   	}

	    });
		
		DAILOG_BTN_SHANCHU.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
			   		//获取listview行数
	 				HashMap<String,String> map = (HashMap<String,String>)LIST_NEW.getItemAtPosition(CAI_list_number);
	                String id =	String.valueOf(map.get("_id"));		   		//根据行数得到该行的id值
			   		//删除对应id的那道菜
			   		remove_cai(db , id);
			        Refe_list();		//刷新已点菜品列表
	 				map = (HashMap<String,String>)LIST_NEW.getItemAtPosition(CAI_list_number);
	                Share_Table_Name =	String.valueOf(map.get("cDishName"));
	        		DAILOG_CAI_NAME.setText(Share_Table_Name);
	                Share_Cai_QTY =	String.valueOf(map.get("cDishQty"));
	        		DAILOG_QTY.setText(Share_Cai_QTY);
	                Share_Cai_Code=	String.valueOf(map.get("cDishCode"));
			   	    System.out.println("2  Share_Cai_Code = "+ Share_Cai_Code );

	                String[] Sql_String_condition = {Share_Cai_Code};
	                Cursor cursor = db.rawQuery( "select fDishPrice from lfcy_base_dishinf where cDishCode = ?" , Sql_String_condition );				
					while(cursor.moveToNext()) 
					{
						Share_Cai_ALLPrice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
					}	

	        		Total_Money();

//			   		removeDialog(DIALOG_FIND);  //关闭对话框 					
				} catch (Exception e) {
					// TODO: handle exception
	                Total_Money();
					Toast.makeText(Cai.this, "已删到最后一条！", Toast.LENGTH_LONG).show();
	        		DAILOG_CAI_NAME.setText("已删到最后一条！");
				}

		   	}

	    });		

		DAILOG_BTN_GAIJIA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}

	    });		
		
		DAILOG_BTN_ZUOFA.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 

		   	}

	    });		

		DAILOG_BTN_SHULIANG.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		removeDialog(DIALOG_FIND);  //关闭对话框 

		   	}

	    });		

		DAILOG_BTN_FANHUI.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		        Refe_list();		//刷新已点菜品列表
        		Total_Money();
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}
	    });
		
		DAILOG_BTN_FRONT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
		   			System.out.println(CAI_list_number);
	            	CAI_list_number = CAI_list_number - 1;
	            	if(CAI_list_number < 0)
	            		CAI_list_number = 0;
	 				HashMap<String,String> map = (HashMap<String,String>)LIST_NEW.getItemAtPosition(CAI_list_number);
	                Share_Table_Name =	String.valueOf(map.get("cDishName"));
	        		DAILOG_CAI_NAME.setText(Share_Table_Name); 					
	                Share_Cai_QTY =	String.valueOf(map.get("cDishQty"));
	        		DAILOG_QTY.setText(Share_Cai_QTY);
	                Share_Cai_Code=	String.valueOf(map.get("cDishCode"));
			   	    System.out.println("3  Share_Cai_Code = "+ Share_Cai_Code );

			   	    String[] Sql_String_condition = {Share_Cai_Code};
	                Cursor cursor = db.rawQuery( "select fDishPrice from lfcy_base_dishinf where cDishCode = ?" , Sql_String_condition );				
					while(cursor.moveToNext()) 
					{
						Share_Cai_ALLPrice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
					}
		   		} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Cai.this, "已是第一条", Toast.LENGTH_SHORT).show();
				}
		   	}
	    });
		DAILOG_BTN_NEXT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{
		   		try {
		   			System.out.println(CAI_list_number);
	            	CAI_list_number = CAI_list_number + 1;
	 				HashMap<String,String> map = (HashMap<String,String>)LIST_NEW.getItemAtPosition(CAI_list_number);
	                Share_Table_Name =	String.valueOf(map.get("cDishName"));
	        		DAILOG_CAI_NAME.setText(Share_Table_Name);					
	                Share_Cai_QTY =	String.valueOf(map.get("cDishQty"));
	        		DAILOG_QTY.setText(Share_Cai_QTY);
	                Share_Cai_Code=	String.valueOf(map.get("cDishCode"));
			   	    System.out.println("4 Share_Cai_Code = "+ Share_Cai_Code );

	                String[] Sql_String_condition = {Share_Cai_Code};
	                Cursor cursor = db.rawQuery( "select fDishPrice from lfcy_base_dishinf where cDishCode = ?" , Sql_String_condition );				
					while(cursor.moveToNext()) 
					{
						Share_Cai_ALLPrice = cursor.getString(cursor.getColumnIndex("fDishPrice"));
					}
		   		} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(Cai.this, "已是最后一条", Toast.LENGTH_SHORT).show();
	            	CAI_list_number = CAI_list_number - 1;
				}

		   	}
	    });
		
        return builder.create();  
    } 
    
    @Override 
    protected Dialog onCreateDialog(int id) {  
        // TODO Auto-generated method stub  
        if(id==101){  
        	return this.buildDialog(Cai.this);  
        }
        if(id==102){  
        	return this.buildCheckDialog(Cai.this);  
        }
        else{  
            return null;  
        }  
    } 

    @Override 
    protected void onPrepareDialog(int id, Dialog dialog) {  
        // TODO Auto-generated method stub  
        super.onPrepareDialog(DIALOG_FIND, dialog);  
    } 
    /***      以上是初始一个对话框！       ***/	
    
    
	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在处理下单                ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}
	
	/***************     以下是一个带进度条的对话框    **************************************/
	ProgressDialog progressDialog;

	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Cai.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     try {
		     progressDialog.show();
			
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(Cai.this, "请先设置打印机端口", Toast.LENGTH_SHORT).show();
		}
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
			handler.sendEmptyMessage(1);	//进度条进度
			//获取结账日期和时间
    		int	mYear,mMonth,mDay,mHour,mMinute;
       		final Calendar c = 
       			Calendar.getInstance();
       			        mYear	= c.get(Calendar.YEAR); 	//获取当前年份
       			        mMonth 	= c.get(Calendar.MONTH)+1;	//获取当前月份
       			        mDay 	= c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
       			        mHour 	= c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数
       			        mMinute = c.get(Calendar.MINUTE);	//获取当前的分钟数		   		
      
			CheckDateTime = mYear+"-"+mMonth+"-"+ mDay +" "+mHour+":"+mMinute;
	   		
	   		//把LIST_NEW里的清单取出来,并发送给厨打   				   		
	   		SerialPort mSerialPort = null;		//串口设备描述
	   		  
	   	    int	   TTY_BPS = 9600;			//串口的初始信息！只在安装后第一次有用！以后会从xml中读出配置信息
	   		String TTY_DEV = "/dev/ttymxc2";
	   		String	SPF_Prt1_bt	=	"SPF_Prt1_bt";
	   		String	SPF_Prt2_bt	=	"SPF_Prt2_bt";
	   		String	SPF_Prt3_bt	=	"SPF_Prt3_bt";			   		
	   		String	SPF_Com_1	=	"SPF_Com_1";
			String	SPF_Com_2	=	"SPF_Com_2";
			String	SPF_Com_3	=	"SPF_Com_3";
	   		String[] btl_String = getResources().getStringArray(R.array.botelv);

			SharedPreferences settings = getSharedPreferences("Prt_Setup_SharedPreferences", 0);		
			String COM_1 = settings.getString(SPF_Com_1, null);
			String COM_2 = settings.getString(SPF_Com_2, null);
			String COM_3 = settings.getString(SPF_Com_3, null);
	        Log.i("info", "COM = "+ COM_1 + COM_2 + COM_3);  

			if (COM_1.trim().equals("ChuDa") ||COM_1.trim().equals("BingYong")) {
				TTY_DEV = "/dev/ttymxc4";
				TTY_BPS = settings.getInt(SPF_Prt1_bt, 0);
			    Integer intObj = new Integer(btl_String[TTY_BPS]);
			    TTY_BPS = intObj.intValue();	
			} else {
				if (COM_2.trim().equals("ChuDa") ||COM_2.trim().equals("BingYong") ) {
					TTY_DEV = "/dev/ttymxc3";
					TTY_BPS = settings.getInt(SPF_Prt2_bt, 0);
				    Integer intObj = new Integer(btl_String[TTY_BPS]);
				    TTY_BPS = intObj.intValue();
				} else {
					if (COM_3.trim().equals("ChuDa")|| COM_3.trim().equals("BingYong")) {
						TTY_DEV = "/dev/ttymxc2";
						TTY_BPS = settings.getInt(SPF_Prt3_bt, 0);
					    Integer intObj = new Integer(btl_String[TTY_BPS]);
					    TTY_BPS = intObj.intValue();
					} else {
						
					}
				}
			}
	        Log.i("info", "TTY_DEV = "+ TTY_DEV);  
			try {
				mSerialPort = new SerialPort(new File(TTY_DEV), TTY_BPS, 0);
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}			
			try {
				mOutputStream = mSerialPort.getOutputStream();					
			} catch (Exception e) {
				// TODO: handle exception
			}

	        handler.sendEmptyMessage(55);
	        get_SharePerference();

			String	SPF_ET_1_1	=	"SPF_ET_1_1";
			String	SPF_ET_1_2	=	"SPF_ET_1_2";
			String	SPF_ET_1_3	=	"SPF_ET_1_3";
			String	SPF_ET_2_1	=	"SPF_ET_2_1";
			String	SPF_ET_2_2	=	"SPF_ET_2_2";
			String	SPF_ET_2_3	=	"SPF_ET_2_3";
			String	SPF_ET_2_4	=	"SPF_ET_2_4";
			CheckContent = "行     菜品名称          数量";
			print_String(Check_b1_1,Check_b3_1,Check_b3_2,settings.getString(SPF_ET_1_1, null));
			print_String(Check_b1_2,false,false,settings.getString(SPF_ET_1_2, null));
			print_String(true,false,Check_b3_9," 桌台：" + Table_Num +"   单号  "+ Sell_Num );
			print_String(true,false,false,"===============================");
			print_String(true,false,false,CheckContent);
			try {
				String[] sell_number = {Sell_Num};
					Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf_new where cSellNo= ?", sell_number);
				int number = 0;
				while(cursor.moveToNext()) 
				{
					number ++;
					String iTableNumber = cursor.getString(cursor.getColumnIndex("iTableNumber"));
					String SdishName 	= cursor.getString(cursor.getColumnIndex("cDishName"));
					String cai_code 	= cursor.getString(cursor.getColumnIndex("cDishCode"));
					String Sprice 	 	= cursor.getString(cursor.getColumnIndex("fDishPrice"));
					String Snum		 	= cursor.getString(cursor.getColumnIndex("cDishQty"));
					String cai_unitName	= cursor.getString(cursor.getColumnIndex("cDishUnitName"));
					String cai_unitCode	= cursor.getString(cursor.getColumnIndex("cDishUnitCode"));

					System.out.println(iTableNumber+ Sell_Num + SdishName+ cai_code + Sprice +  Snum);					
	   				print_String(true,Check_b3_7,Check_b3_8,number+"    "+ SdishName +"     "+Snum + cai_unitName);
	
	   				insert_SellDishData(db,  iTableNumber,
							Sell_Num,  SdishName,  cai_code,
							Sprice,  Snum ,cai_unitName ,cai_unitCode);
				}		
			print_String(true,false,false,"===============================");
			print_String(Check_b3_5,false,Check_b3_6," 时间:"+ CheckDateTime);
			print_String(Check_b1_3,Check_b3_3,Check_b3_4,settings.getString(SPF_ET_1_3, null));
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
				mSerialPort.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			/*
	   		//送厨打
	        try {
				mSerialPort = new SerialPort(new File(TTY_DEV), TTY_BPS, 0);					
				mOutputStream = mSerialPort.getOutputStream();	
				
		   		byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
		   		1
   				print_String(cancel_to_normal,"     欢迎光临“8090自助餐厅”");
   				print_String(cancel_to_normal," 桌台：" + Table_Num +"   单号  "+ Sell_Num );
   				print_String(cancel_to_normal,"===============================");
   				print_String(cancel_to_normal,"编号  菜品名称   数量   价格");
   				try {
   					String[] sell_number = {Sell_Num};
   					Cursor  cursor = db.rawQuery("select * from lfcy_base_sellinf_new where cSellNo= ?", sell_number);
   					int number = 0;
   					while(cursor.moveToNext()) 
   					{
   						number ++;
   						String iTableNumber = cursor.getString(cursor.getColumnIndex("iTableNumber"));
   						String SdishName 	= cursor.getString(cursor.getColumnIndex("cDishName"));
   						String cai_code 	= cursor.getString(cursor.getColumnIndex("cDishCode"));
   						String Sprice 	 	= cursor.getString(cursor.getColumnIndex("fDishPrice"));
   						String Snum		 	= cursor.getString(cursor.getColumnIndex("cDishQty"));

   						System.out.println(iTableNumber+ Sell_Num + SdishName+ cai_code+
   								Sprice +  Snum);
   						
   		   				print_String(cancel_to_normal,number+"    "+ SdishName +"     "+Snum +"    "+Sprice+"元" );

   		   				insert_SellDishData(db,  iTableNumber,
   								Sell_Num,  SdishName,  cai_code,
   								Sprice,  Snum);
   					}		
	   				print_String(cancel_to_normal,"===============================");
	   				print_String(cancel_to_normal," ");
	   				print_String(cancel_to_normal," ");
	   				print_String(cancel_to_normal," ");
	   				print_String(cancel_to_normal," ");
	   				print_String(cancel_to_normal," ");
   					mSerialPort.close();
   				} catch (Exception e) {
   					// TODO: handle exception
	   				print_String(cancel_to_normal,"===========出故障了===============");
   				}	   				
   				
			} catch (Exception e) {
				// TODO: handle exception
			}
	        handler.sendEmptyMessage(85);
	   		
	   		//打印新增菜的菜品信息
	   		
	   		//厨房打印
	   		
	   		//把数据传给LIST_DIANCAI
	   		
	   		//清除LIST_NEW并刷新LIST_DIANCAI
	   		 * 
	   		 */
			
	   		try {
	   			db.delete("lfcy_base_sellinf_new", " cSellNo =?", new String[]{Sell_Num});
   			} catch (Exception e) {
					
   			}		   		
	   		//界面转到已点菜品	
			db.close();
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    

	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(boolean print_if,boolean weight,boolean height,  String in_String) {
		// TODO Auto-generated method stub
		if (print_if) {
			int i;
			byte[] prt_code_buffer;
	   		byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
	   		byte[] double_height    = {0x0a, 0x1b, 0x21,0x10};//倍高
	   		byte[] double_weight    = {0x0a, 0x1b, 0x21,0x20};//倍宽
	   		byte[] double_hweight   = {0x0a, 0x1b, 0x21,0x30};//倍高倍宽
	   		
	   		if (weight) {
				if (height) {
					prt_code_buffer = double_hweight;	//又倍宽 又倍高
				} else {
					prt_code_buffer = double_weight;	//又倍宽 但是不倍高
				}
			} else {
				if (height) {
					prt_code_buffer = double_height;	//不倍宽 但是倍高
				} else {
					prt_code_buffer = cancel_to_normal;	//不倍宽 也不倍高
				}
			}
	   		
			CharSequence t =  (CharSequence)in_String;
			char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
			for (i=0; i<t.length(); i++) {
				text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
			}
			try {
				byte[] buffer = prt_code_buffer;//倍高倍宽;
				mOutputStream.write(buffer);
				mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
			} catch (IOException e) {
				e.printStackTrace();
			}				
		}
	}
	

	protected void Save_CHECKSharePreference() {
		// TODO Auto-generated method stub
		progressDialogCHECK("正在处理结账打印           ", "请稍等......");		
		insert_ThreadCHECK thread= new insert_ThreadCHECK();
     	thread.start();		
	}	
	/***************     以下是一个带进度条的对话框    **************************************/
	ProgressDialog progressDialogCHECK;

	private void progressDialogCHECK(String title, String message) {
		// TODO Auto-generated method stub
		progressDialogCHECK = new ProgressDialog(Cai.this);
		progressDialogCHECK.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialogCHECK.setMessage(message);
		progressDialogCHECK.setTitle(title);
		progressDialogCHECK.setProgress(0);
		progressDialogCHECK.setMax(100);
		progressDialogCHECK.setCancelable(false);
		progressDialogCHECK.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handlerCHECK = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialogCHECK.cancel();
    	       }
    		  progressDialogCHECK.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_ThreadCHECK extends Thread{
        public void run(){      	
			handlerCHECK.sendEmptyMessage(1);	//进度条进度

			
			SerialPort mSerialPort = null;		//串口设备描述

	   	    int	   TTY_BPS = 9600;			//串口的初始信息！只在安装后第一次有用！以后会从xml中读出配置信息
	   		String TTY_DEV = "/dev/ttymxc2";
	   		String	SPF_Prt1_bt	=	"SPF_Prt1_bt";
	   		String	SPF_Prt2_bt	=	"SPF_Prt2_bt";
	   		String	SPF_Prt3_bt	=	"SPF_Prt3_bt";			   		
	   		String	SPF_Com_1	=	"SPF_Com_1";
			String	SPF_Com_2	=	"SPF_Com_2";
			String	SPF_Com_3	=	"SPF_Com_3";

			
			
	   		String[] btl_String = getResources().getStringArray(R.array.botelv);

			SharedPreferences settings = getSharedPreferences("Prt_Setup_SharedPreferences", 0);		
			String	SPF_ET_1_1	=	"SPF_ET_1_1";
			String	SPF_ET_1_2	=	"SPF_ET_1_2";
			String	SPF_ET_1_3	=	"SPF_ET_1_3";
			String	SPF_ET_2_1	=	"SPF_ET_2_1";
			String	SPF_ET_2_2	=	"SPF_ET_2_2";
			String	SPF_ET_2_3	=	"SPF_ET_2_3";
			String	SPF_ET_2_4	=	"SPF_ET_2_4";
			
			String COM_1 = settings.getString(SPF_Com_1, null);
			String COM_2 = settings.getString(SPF_Com_2, null);
			String COM_3 = settings.getString(SPF_Com_3, null);
	        Log.i("info", "COM = "+ COM_1 + COM_2 + COM_3);  

			if (COM_1.trim().equals("ZhuDa") ||COM_1.trim().equals("BingYong")) {
				TTY_DEV = "/dev/ttymxc4";
				TTY_BPS = settings.getInt(SPF_Prt1_bt, 0);
			    Integer intObjj = new Integer(btl_String[TTY_BPS]);
			    TTY_BPS = intObjj.intValue();	
			} else {
				if (COM_2.trim().equals("ZhuDa") ||COM_2.trim().equals("BingYong") ) {
					TTY_DEV = "/dev/ttymxc3";
					TTY_BPS = settings.getInt(SPF_Prt2_bt, 0);
				    Integer intObjj = new Integer(btl_String[TTY_BPS]);
				    TTY_BPS = intObjj.intValue();
				} else {
					if (COM_3.trim().equals("ZhuDa")|| COM_3.trim().equals("BingYong")) {
						TTY_DEV = "/dev/ttymxc2";
						TTY_BPS = settings.getInt(SPF_Prt3_bt, 0);
					    Integer intObjj = new Integer(btl_String[TTY_BPS]);
					    TTY_BPS = intObjj.intValue();
					} else {
						
					}
				}
			}
	        Log.i("info", "TTY_DEV = "+ TTY_DEV); 
	        
			try {
				mSerialPort = new SerialPort(new File(TTY_DEV), TTY_BPS, 0);
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}					
			mOutputStream = mSerialPort.getOutputStream();	
	   		
	   		get_SharePerference();
	   		
			print_String(Check_b1_4,Check_b2_1,Check_b2_2,settings.getString(SPF_ET_2_1, null));
			print_String(Check_b1_5,false,false,settings.getString(SPF_ET_2_2, null));
			print_String(Check_b1_6,false,false,settings.getString(SPF_ET_2_3, null));
			print_String(true,false,false," 桌台：" + CheckTableNum  );
			print_String(true,false,false," 结账单号  "+ CheckNum );
			print_String(true,false,false,"===============================");
			print_String(true,false,false,CheckContent);
			try {
				String[] sell_number = {CheckDanhao};
				Cursor cursor = db.rawQuery("select * from lfcy_base_sellinf where cSellNo= ?", sell_number);
				int number = 0;
				while(cursor.moveToNext()) 
				{
					number ++;
					String SdishName = cursor.getString(cursor.getColumnIndex("cDishName"));
					String Snum		 = cursor.getString(cursor.getColumnIndex("cDishQty"));
					String Sprice 	 = cursor.getString(cursor.getColumnIndex("fDishPrice"));
					String cai_unitname = cursor.getString(cursor.getColumnIndex("cDishUnitName"));
					String cai_unitcode = cursor.getString(cursor.getColumnIndex("cDishUnitCode"));
	   				print_String(true,false,false,number+"    "+ SdishName +"     "+Snum + cai_unitname+"    "+Sprice+"元" );
				}		
			print_String(true,false,false,"===============================");
			print_String(true,false,false," 总价：	     "+ CheckXiaoFeiZongJia +"元");				   				
			print_String(true,false,false," 加收：	     "+ CheckJiaShou +"元");
			print_String(true,false,false," 应收：	     "+ CheckYingShou +"元" );
			print_String(true,Check_b2_9,Check_b2_10," 现金：	     "+ CheckXianJinShiSHou +"元" );
			print_String(true,Check_b2_11,Check_b2_12," 找零：	     "+ CheckZhaoLing +"元" );
			print_String(Check_b2_5,false,Check_b2_6," 操作员："+ CheckUser);
			print_String(Check_b2_7,false,Check_b2_8," 时间:"+ CheckDateTime);
			print_String(Check_b1_7,false,false,settings.getString(SPF_ET_2_4, null));
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
			print_String(true,false,false," ");
				mSerialPort.close();
			} catch (Exception e) {
				// TODO: handle exception
			}

			handlerCHECK.sendEmptyMessage(100);
        }
    };

	private void get_SharePerference() {
		SharedPreferences settings = getSharedPreferences("Prt_Setup_SharedPreferences", 0);		
		
		
		String	SPF_CHECKBOX_1_1	=	"SPF_CHECKBOX_1_1";
		String	SPF_CHECKBOX_1_2	=	"SPF_CHECKBOX_1_2";
		String	SPF_CHECKBOX_1_3	=	"SPF_CHECKBOX_1_3";
		String	SPF_CHECKBOX_1_4	=	"SPF_CHECKBOX_1_4";
		String	SPF_CHECKBOX_1_5	=	"SPF_CHECKBOX_1_5";
		String	SPF_CHECKBOX_1_6	=	"SPF_CHECKBOX_1_6";
		String	SPF_CHECKBOX_1_7	=	"SPF_CHECKBOX_1_7";

		String	SPF_CHECKBOX_2_1	=	"SPF_CHECKBOX_2_1";
		String	SPF_CHECKBOX_2_2	=	"SPF_CHECKBOX_2_2";
		String	SPF_CHECKBOX_2_3	=	"SPF_CHECKBOX_2_3";
		String	SPF_CHECKBOX_2_4	=	"SPF_CHECKBOX_2_4";
		String	SPF_CHECKBOX_2_5	=	"SPF_CHECKBOX_2_5";
		String	SPF_CHECKBOX_2_6	=	"SPF_CHECKBOX_2_6";
		String	SPF_CHECKBOX_2_7	=	"SPF_CHECKBOX_2_7";
		String	SPF_CHECKBOX_2_8	=	"SPF_CHECKBOX_2_8";
		String	SPF_CHECKBOX_2_9	=	"SPF_CHECKBOX_2_9";
		String	SPF_CHECKBOX_2_10	=	"SPF_CHECKBOX_2_10";
		String	SPF_CHECKBOX_2_11	=	"SPF_CHECKBOX_2_11";
		String	SPF_CHECKBOX_2_12	=	"SPF_CHECKBOX_2_12";

		String	SPF_CHECKBOX_3_1	=	"SPF_CHECKBOX_3_1";
		String	SPF_CHECKBOX_3_2	=	"SPF_CHECKBOX_3_2";
		String	SPF_CHECKBOX_3_3	=	"SPF_CHECKBOX_3_3";
		String	SPF_CHECKBOX_3_4	=	"SPF_CHECKBOX_3_4";
		String	SPF_CHECKBOX_3_5	=	"SPF_CHECKBOX_3_5";
		String	SPF_CHECKBOX_3_6	=	"SPF_CHECKBOX_3_6";
		String	SPF_CHECKBOX_3_7	=	"SPF_CHECKBOX_3_7";
		String	SPF_CHECKBOX_3_8	=	"SPF_CHECKBOX_3_8";
		String	SPF_CHECKBOX_3_9	=	"SPF_CHECKBOX_3_9";
		String	SPF_CHECKBOX_3_10	=	"SPF_CHECKBOX_3_10";
		String	SPF_CHECKBOX_3_11	=	"SPF_CHECKBOX_3_11";
		String	SPF_CHECKBOX_3_12	=	"SPF_CHECKBOX_3_12";
		
//		settings.getString(SPF_ET_1_1, null) ;
//		settings.getString(SPF_ET_1_2, null) ;
//		settings.getString(SPF_ET_1_3, null) ;
//	    CheckTitle = settings.getString(SPF_ET_2_1, null) ;
//		settings.getString(SPF_ET_2_2, null) ;
//		settings.getString(SPF_ET_2_3, null) ;
//		settings.getString(SPF_ET_2_4, null) ;
	
		Check_b1_1 = settings.getBoolean(SPF_CHECKBOX_1_1, false);
		Check_b1_2 = settings.getBoolean(SPF_CHECKBOX_1_2, false);
		Check_b1_3 = settings.getBoolean(SPF_CHECKBOX_1_3, false);
		Check_b1_4 = settings.getBoolean(SPF_CHECKBOX_1_4, false);
		Check_b1_5 = settings.getBoolean(SPF_CHECKBOX_1_5, false);
		Check_b1_6 = settings.getBoolean(SPF_CHECKBOX_1_6, false);
		Check_b1_7 = settings.getBoolean(SPF_CHECKBOX_1_7, false);

		Check_b2_1 = settings.getBoolean(SPF_CHECKBOX_2_1, false);
		Check_b2_2 = settings.getBoolean(SPF_CHECKBOX_2_2, false);
		Check_b2_3 = settings.getBoolean(SPF_CHECKBOX_2_3, false);
		Check_b2_4 = settings.getBoolean(SPF_CHECKBOX_2_4, false);
		Check_b2_5 = settings.getBoolean(SPF_CHECKBOX_2_5, false);
		Check_b2_6 = settings.getBoolean(SPF_CHECKBOX_2_6, false);
		Check_b2_7 = settings.getBoolean(SPF_CHECKBOX_2_7, false);
		Check_b2_8 = settings.getBoolean(SPF_CHECKBOX_2_8, false);
		Check_b2_9 = settings.getBoolean(SPF_CHECKBOX_2_9, false);
		Check_b2_10= settings.getBoolean(SPF_CHECKBOX_2_10, false);
		Check_b2_11= settings.getBoolean(SPF_CHECKBOX_2_11, false);
		Check_b2_12= settings.getBoolean(SPF_CHECKBOX_2_12, false);

		Check_b3_1 = settings.getBoolean(SPF_CHECKBOX_3_1, false);
		Check_b3_2 = settings.getBoolean(SPF_CHECKBOX_3_2, false);
		Check_b3_3 = settings.getBoolean(SPF_CHECKBOX_3_3, false);
		Check_b3_4 = settings.getBoolean(SPF_CHECKBOX_3_4, false);
		Check_b3_5 = settings.getBoolean(SPF_CHECKBOX_3_5, false);
		Check_b3_6 = settings.getBoolean(SPF_CHECKBOX_3_6, false);
		Check_b3_7 = settings.getBoolean(SPF_CHECKBOX_3_7, false);
		Check_b3_8 = settings.getBoolean(SPF_CHECKBOX_3_8, false);
		Check_b3_9 = settings.getBoolean(SPF_CHECKBOX_3_9, false);
		Check_b3_10= settings.getBoolean(SPF_CHECKBOX_3_10, false);
		Check_b3_11= settings.getBoolean(SPF_CHECKBOX_3_11, false);
		Check_b3_12= settings.getBoolean(SPF_CHECKBOX_3_12, false);
	}
	/***************     以上是一个进度条    **************************************/

	
	
	
	/***************     以下是一个带进度条的对话框    
	 * **************************************/
	protected void Ref_Cai_Grid_List() {
		// TODO Auto-generated method stub
		progresRefCaiGridDialog("查询                ", "请稍等......");		
		Ref_CAI_Grid_Thread thread= new Ref_CAI_Grid_Thread();
     	thread.start();		
	}
	
	ProgressDialog progresRefGridDialog;
	
	private void progresRefCaiGridDialog(String title, String message) {
		// TODO Auto-generated method stub
		progresRefGridDialog = new ProgressDialog(Cai.this);
		progresRefGridDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progresRefGridDialog.setMessage(message);
		progresRefGridDialog.setTitle(title);
		progresRefGridDialog.setProgress(0);
		progresRefGridDialog.setMax(100);
		progresRefGridDialog.setCancelable(false);
		progresRefGridDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler_RefCaiGrid = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
   	    		  GRID_VIEW_CAI.setAdapter(adapterr);
  				
  				  //显示数据
  				  SMALL_CLASS.setAdapter(list_adapter);
    			  progresRefGridDialog.cancel();
    	       }
    		  progresRefGridDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class Ref_CAI_Grid_Thread extends Thread{
        public void run(){
        	
        	handler_RefCaiGrid.sendEmptyMessage(1);	//进度条进度
        	String[] nima = {bigclass};
//        	init_grids(1,nima);
    		// 用于初始化
    		mylist = new ArrayList<Grid_Item_Cai_SQL>();
    		String TABLE_NAME = "lfcy_base_dishinf";
    		int PageSize = 16 ;
    		
    		try {	
    			String[]	Sql_String_condition = nima;	
    			Cursor  cursor ;
    		
    			if (bigclass == null || bigclass.length()< 1) {
    				String sql= "select * from " + TABLE_NAME +     
    				" Limit "+String.valueOf(PageSize)+ " Offset " +String.valueOf( (pageID-1)*PageSize); 

    				cursor = db.rawQuery( sql , null );						
				} else {
    				cursor = db.rawQuery( "select * from lfcy_base_dishinf where cDishClass1 = ?" , Sql_String_condition );				

				}
/*
    			switch (1) {
    			case 1:		//大类
    				cursor = db.rawQuery( "select * from lfcy_base_dishinf where cDishClass1 = ?" , Sql_String_condition );				
    				break;
    			case 2:		//小类
    				cursor = db.rawQuery( "select * from lfcy_base_dishinf where cDishClass2 = ?" , Sql_String_condition );				
    				break;
    			case 3:
    				cursor = db.rawQuery( "select * from lfcy_base_dishinf where iDishStyle = ?" , Sql_String_condition );				
    				break;
    			case 0:		//大类
    				String sql= "select * from " + TABLE_NAME +     
    				" Limit "+String.valueOf(PageSize)+ " Offset " +String.valueOf( (pageID-1)*PageSize); 

    				cursor = db.rawQuery( sql , null );				
    				break;
    			default:
    				cursor = db.rawQuery( "select * from lfcy_base_dishinf where iDishStyle = ?" , Sql_String_condition );				
    				break;
    			}
 */ 
    			while (cursor.moveToNext()) {
    				Grid_Item_Cai_SQL picture = new Grid_Item_Cai_SQL();
    				picture.setName(cursor.getString(cursor
    						.getColumnIndex("cDishName")));		//菜品名称
    				picture.setCode(cursor.getString(cursor
    						.getColumnIndex("cDishCode")));		//菜品编码
    				picture.setStyle(cursor.getString(cursor
    						.getColumnIndex("iDishStyle")));	//菜品类型 推荐新品还是什么别的			
    				picture.setPrice(cursor.getString(cursor
    						.getColumnIndex("fDishPrice")));	//菜品价格
    				picture.setDian(cursor.getString(cursor
    						.getColumnIndex("iDishDian")));		//该菜是否已被点
    				picture.setPath(cursor.getString(cursor
    						.getColumnIndex("cDishName2")));	//该菜菜品图片路径
    				picture.setUnit(cursor.getString(cursor
    						.getColumnIndex("cDishUnitName")));	//该菜菜品单位
    				mylist.add(picture);
    			}
    			NOWPAGE.setText(""+pageID);
    			if (pageID == 1)
    				ALLPAGE.setText(   String.valueOf( cursor.getCount()/PageSize + 1 )   );
    			cursor.close();
    		} catch (Exception e) {
    			// TODO: handle exception
    		}

    		adapterr = new GridItemCaiSQLAdapter(mylist, Cai.this,Dispay_widthPixels);// 自定义适配器
        	
        	//init_smallclass(bigclass);

    		//从数据库获取大类数量
    		// 用于初始化
    		String TABLE_NAMEE = "lfcy_base_smallsort";
    		
    		String	sql= "select * from "+TABLE_NAMEE+" where cBigCode=?";     
    		String[] BigClass = { bigclass };
    		try {
     	 	    Cursor  cursor = db.rawQuery("select * from lfcy_base_smallsort where cBigCode = ?", BigClass);

    			fill_hashmap_smallclass(cursor);

    			list_adapter = new SimpleAdapter(
    		      		Cai.this, 
    					list, 
    					R.layout.i800_caismallclass_item 
    					, new String[]{"cName"}
    					, new int[]{R.id.grid_caiclass_text }
    				);

    		} catch (Exception e) {
    			// TODO: handle exception
    		}
     	
	        //修改数据        
        	handler_RefCaiGrid.sendEmptyMessage(5);

        	handler_RefCaiGrid.sendEmptyMessage(100);
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
    
}