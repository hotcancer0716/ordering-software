package longfly.development;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import longfly.development.POS_Setup.insert_Thread;

import casio.serial.SerialPort;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class About extends Activity {
    /** Called when the activity is first created. */

	TextView	TEST;
	Button		TEST_BTN;
	Button		TEST_EXIT;
	Button		TEST_SAVE;
	Spinner		PRT_BTL;
	Spinner		PRT_DUANKOU;

	private SerialPort mSerialPort = null;		//串口设备描述
	protected OutputStream mOutputStream;		//串口输出描述
	ProgressDialog progressDialog;
	
	  
    int	   TTY_BPS = 0;			//串口的初始信息！只在安装后第一次有用！以后会从xml中读出配置信息
	String TTY_DEV = null;
	int		prt_btl,prt_duankou;	
    private ArrayAdapter adapter_prt_btl;  
    private ArrayAdapter adapter_prt_duankou;  
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
       find_id();
       key_things();
       init_view();
	}

	private void key_things() {
		// TODO Auto-generated method stub
		TEST_BTN.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{				   						
		   		SerialPort mSerialPort = null;		//串口设备描述
		   		  
		        try {
					mSerialPort = new SerialPort(new File(TTY_DEV), TTY_BPS, 0);					
					mOutputStream = mSerialPort.getOutputStream();	
					
	   				Start_Print_Test();
	   				
				} catch (Exception e) {
					// TODO: handle exception
				}		   		
		   	}
	    });	
		TEST_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
		TEST_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
		PRT_BTL.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt_btl = postion;
            	int pBTL = 0;
            	switch (prt_btl) {
				case 0:
					pBTL = 9600;
					break;
				case 1:
					pBTL = 14400;
					break;
				case 2:
					pBTL = 19200;
					break;
				case 3:
					pBTL = 115200;
					break;					
				case 4:
					pBTL = 256000;
					break;					
				default:
					pBTL = 9600;
					break;
				}
            	TTY_BPS = pBTL;
            	TEST.setText("你选择的波特率是"+ pBTL);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });
		PRT_DUANKOU.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt_duankou = postion;
            	String pDKH = null;
            	switch (prt_duankou) {
				case 0:
					pDKH = "/dev/ttymxc0";
					break;
            	case 1:
					pDKH = "/dev/ttymxc1";
					break;
				case 2:
					pDKH = "/dev/ttymxc2";
					break;
				case 3:
					pDKH = "/dev/ttymxc3";
					break;
				case 4:
					pDKH = "/dev/ttymxc4";
					break;					
				case 5:
					pDKH = "/dev/ttymxc5";
					break;					
				case 6:
					pDKH = "/dev/ttymxc6";
					break;	
				case 7:
					pDKH = "/dev/ttymxc7";
					break;									
				default:
					pDKH = "/dev/ttymxc1";
					break;
				}
            	TTY_DEV = pDKH;
            	TEST.setText("你选择的端口号是"+ pDKH);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });		
	}


	private void init_view() {
		// TODO Auto-generated method stub
        //将可选内容与ArrayAdapter连接起来   
		adapter_prt_btl= ArrayAdapter.createFromResource(this, R.array.botelv, android.R.layout.simple_spinner_item); 
 	    //设置下拉列表的风格   
		adapter_prt_btl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		PRT_BTL.setAdapter(adapter_prt_btl);    

/**********************************************************************************/		        
		adapter_prt_duankou= ArrayAdapter.createFromResource(this, R.array.duankou, android.R.layout.simple_spinner_item); 
		adapter_prt_duankou.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT_DUANKOU.setAdapter(adapter_prt_duankou);    
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		TEST		=	(TextView) findViewById(R.id.about_test);
		TEST_BTN	=	(Button) findViewById(R.id.about_btn_test);
		TEST_SAVE	=	(Button) findViewById(R.id.other_setup_save);
		TEST_EXIT	=	(Button) findViewById(R.id.other_setup_exit);
		
		PRT_BTL		=	(Spinner) findViewById(R.id.customer_spinner_1);
		PRT_DUANKOU	=	(Spinner) findViewById(R.id.customer_spinner_2);
	}
	
	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	protected void Start_Print_Test() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}	
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(About.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
	   		byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
	   		byte[] double_h_double_w = { 0x0a, 0x1b, 0x21, 0x30 };//倍高倍宽;

	   		for(int i = 0 ;i<=100;i++)
	   		{
		   		print_String(cancel_to_normal,"===============================");			   		
		   		print_String(double_h_double_w,"第"+i+"行！");			   		
				print_String(cancel_to_normal,"广东韶关龙飞数码科技有限公司");
				print_String(cancel_to_normal,"   Android系统智能收银终端    ");
				print_String(cancel_to_normal,"     欢迎光临“8090自助餐厅”");
				print_String(cancel_to_normal,"12345 abcde ABCDE !@#$% 00");
				print_String(cancel_to_normal,"===============================");
        	
				handler.sendEmptyMessage(i);	//进度条进度	   			
	   		}
//	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

  
    
}