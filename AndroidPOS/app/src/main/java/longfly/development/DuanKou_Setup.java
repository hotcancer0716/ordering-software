package longfly.development;

import java.io.IOException;
import java.io.OutputStream;

import casio.serial.SerialPort;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.System;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class DuanKou_Setup extends Activity {
    /** Called when the activity is first created. */

	Button		PRT_SETUP_SAVE;
	Button		PRT_SETUP_CANCEL;

	Spinner		PRT1_BTL;
	Spinner		PRT2_BTL;
	Spinner		PRT3_BTL;
	Spinner		PRT1_DUANKOU;
	Spinner		PRT2_DUANKOU;
	Spinner		PRT3_DUANKOU;
	Spinner		PRT_USB;
	
	CheckBox	USB_SWICH;
	LinearLayout	LN_LAYOUT;
	private SerialPort mSerialPort = null;		//串口设备描述
	protected OutputStream mOutputStream;		//串口输出描述

    private ArrayAdapter adapter_prt1_btl;  
    private ArrayAdapter adapter_prt2_btl;  
    private ArrayAdapter adapter_prt3_btl;  
    private ArrayAdapter adapter_prt1_duankou;  
    private ArrayAdapter adapter_prt2_duankou;  
    private ArrayAdapter adapter_prt3_duankou;  
    private ArrayAdapter adapter_usb_prt;  

  	int		prt1_btl,prt2_btl,prt3_btl,prt1_duankou,prt2_duankou,prt3_duankou;
	int 	prt_usb;
  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;
	String	SPF_Prt1_bt	=	"SPF_Prt1_bt";
	String	SPF_Prt2_bt	=	"SPF_Prt2_bt";
	String	SPF_Prt3_bt	=	"SPF_Prt3_bt";
	String	SPF_Prt1_DK	=	"SPF_Prt1_DK";
	String	SPF_Prt2_DK	=	"SPF_Prt2_DK";
	String	SPF_Prt3_DK	=	"SPF_Prt3_DK";
	String	SPF_Prt_USB	=	"SPF_Prt_USB";
	String	SPF_Com_1	=	"SPF_Com_1";
	String	SPF_Com_2	=	"SPF_Com_2";
	String	SPF_Com_3	=	"SPF_Com_3";
	String	SPF_CHECKBOX_USB_PRT =	"SPF_CHECKBOX_USB_PRT";

	String  COM_1,COM_2,COM_3;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_duankou_setup);
        
       find_id();
       init_view();
       
	   settings = getSharedPreferences("Prt_Setup_SharedPreferences", 0);		
       get_SharePerference();
       key_things();
	}


	private void key_things() {
		// TODO Auto-generated method stub
		PRT_SETUP_SAVE.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(DuanKou_Setup.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		PRT_SETUP_CANCEL.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
		
		PRT1_BTL.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt1_btl = postion;
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });
		PRT2_BTL.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt2_btl = postion;
                Log.i("info", "postion = "+ postion);  
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });	
		PRT3_BTL.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt3_btl = postion;
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });
		PRT1_DUANKOU.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt1_duankou = postion;
            	switch (postion) {
				case 0:
					COM_1 = "null";
					break;				
            	case 1:
					COM_1 = "ChuDa";
					break;
				case 2:
					COM_1 = "ZhuDa";
					break;
				case 3:
					COM_1 = "DuKa";
					break;					
				case 4:
					COM_1 = "Cheng";
					break;					
				case 5:
					COM_1 = "BingYong";
					break;	
				default:
					break;
				}
            	if (COM_1 == COM_2) {
            		PRT2_DUANKOU.setSelection(0); 
            		COM_2 = "null";
				}
            	if (COM_1 == COM_3) {
            		PRT3_DUANKOU.setSelection(0); 
            		COM_3 = "null";
				}
            	if (COM_1.trim().equals("BingYong")) {
					if (COM_2.trim().equals("ZhuDa") || COM_2.trim().equals("ChuDa")) {
	            		PRT2_DUANKOU.setSelection(0); 
	            		COM_2 = "null";
					}
					if (COM_3.trim().equals("ZhuDa") || COM_3.trim().equals("ChuDa")) {
	            		PRT3_DUANKOU.setSelection(0); 
	            		COM_3 = "null";
					}
            	}
            	if (COM_1.trim().equals("ZhuDa") || COM_1.trim().equals("ChuDa") ) {
					if (COM_2.trim().equals("BingYong")) {
	            		PRT2_DUANKOU.setSelection(0); 
	            		COM_2 = "null";						
					}
					if (COM_3.trim().equals("BingYong")) {
	            		PRT3_DUANKOU.setSelection(0); 
	            		COM_3 = "null";						
					}
            	}
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });	
		PRT2_DUANKOU.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt2_duankou = postion;
            	switch (postion) {
				case 0:
					COM_2 = "null";
					break;				
				case 1:
					COM_2 = "ChuDa";
					break;
				case 2:
					COM_2 = "ZhuDa";
					break;
				case 3:
					COM_2 = "DuKa";
					break;					
				case 4:
					COM_2 = "Cheng";
					break;	
				case 5:
					COM_2 = "BingYong";
				default:
					break;
				}
            	if (COM_2 == COM_1) {
            		PRT1_DUANKOU.setSelection(0); 
            		COM_1 = "null";
				}
            	if (COM_2 == COM_3) {
            		PRT3_DUANKOU.setSelection(0); 
            		COM_3 = "null";
				}   
            	if (COM_2.trim().equals("BingYong")) {
					if (COM_1.trim().equals("ZhuDa") || COM_1.trim().equals("ChuDa")) {
	            		PRT1_DUANKOU.setSelection(0); 
	            		COM_1 = "null";
					}
					if (COM_3.trim().equals("ZhuDa") || COM_3.trim().equals("ChuDa")) {
	            		PRT3_DUANKOU.setSelection(0); 
	            		COM_3 = "null";
					}
            	}   
            	if (COM_2.trim().equals("ZhuDa") || COM_2.trim().equals("ChuDa") ) {
					if (COM_1.trim().equals("BingYong")) {
	            		PRT1_DUANKOU.setSelection(0); 
	            		COM_1 = "null";						
					}
					if (COM_3.trim().equals("BingYong")) {
	            		PRT3_DUANKOU.setSelection(0); 
	            		COM_3 = "null";						
					}
            	}            	
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });	
		PRT3_DUANKOU.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt3_duankou = postion;
            	switch (postion) {
				case 0:
					COM_3 = "null";
					break;
            	case 1:
					COM_3 = "ChuDa";
					break;
				case 2:
					COM_3 = "ZhuDa";
					break;
				case 3:
					COM_3 = "DuKa";
					break;					
				case 4:
					COM_3 = "Cheng";
					break;	
				case 5:
					COM_3 = "BingYong";
				default:
					break;
				}
            	
            	if (COM_3 == COM_1) {
            		PRT1_DUANKOU.setSelection(0); 
            		COM_1 = "null";
				}
            	if (COM_3 == COM_2) {
            		PRT2_DUANKOU.setSelection(0); 
            		COM_2 = "null";
				}              	
            	if (COM_3.trim().equals("BingYong")) {
					if (COM_1.trim().equals("ZhuDa") || COM_1.trim().equals("ChuDa")) {
	            		PRT1_DUANKOU.setSelection(0); 
	            		COM_1 = "null";
					}					
					if (COM_2.trim().equals("ZhuDa") || COM_2.trim().equals("ChuDa")) {
	            		PRT2_DUANKOU.setSelection(0); 
	            		COM_2 = "null";
					}
            	}
            	if (COM_3.trim().equals("ZhuDa") || COM_3.trim().equals("ChuDa") ) {
					if (COM_1.trim().equals("BingYong")) {
	            		PRT1_DUANKOU.setSelection(0); 
	            		COM_1 = "null";						
					}
            		if (COM_2.trim().equals("BingYong")) {
	            		PRT2_DUANKOU.setSelection(0); 
	            		COM_2 = "null";						
					}
            	}            	
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });

		PRT_USB.setOnItemSelectedListener(new OnItemSelectedListener()
		{
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                int postion, long id) 
            {
            	prt_usb = postion;
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
         });
		
		USB_SWICH.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if ( USB_SWICH.isChecked() ) {
					LN_LAYOUT.setVisibility(View.VISIBLE);
				} else {
					LN_LAYOUT.setVisibility(View.GONE);
				}
				
			}
		});
	}

	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}

	private void get_SharePerference() {
		// TODO Auto-generated method stub
		prt1_btl = settings.getInt(SPF_Prt1_bt, 1);
		PRT1_BTL.setSelection(prt1_btl); 
        Log.i("info", "prt1_btl = "+ prt1_btl);  

		prt2_btl = settings.getInt(SPF_Prt2_bt, 0) ;
		PRT2_BTL.setSelection(prt2_btl); 
        Log.i("info", "prt2_btl = "+ prt2_btl);  
		
		prt3_btl = settings.getInt(SPF_Prt3_bt, 0) ;
		PRT3_BTL.setSelection(prt3_btl); 
        Log.i("info", "prt3_btl = "+ prt3_btl);  
		
		prt1_duankou = settings.getInt(SPF_Prt1_DK, 0) ;
		PRT1_DUANKOU.setSelection(prt1_duankou); 
        Log.i("info", "prt1_duankou = "+ prt1_duankou);  
		
		prt2_duankou = settings.getInt(SPF_Prt2_DK, 0) ;
		PRT2_DUANKOU.setSelection(prt2_duankou); 
        Log.i("info", "prt2_duankou = "+ prt2_duankou);  
		
		prt3_duankou = settings.getInt(SPF_Prt3_DK, 0) ;
		PRT3_DUANKOU.setSelection(prt3_duankou); 
        Log.i("info", "prt3_duankou = "+ prt3_duankou);  
		
		prt_usb = settings.getInt(SPF_Prt_USB, 0) ;
		PRT_USB.setSelection(prt_usb); 
		
		USB_SWICH.setChecked(settings.getBoolean(SPF_CHECKBOX_USB_PRT, false));	
		
		COM_1 = settings.getString(SPF_Com_1, null);
		COM_2 = settings.getString(SPF_Com_2, null);
		COM_3 = settings.getString(SPF_Com_3, null);
		
        Log.i("info", "COM = "+ COM_1 + COM_2 + COM_3);  

	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		PRT_SETUP_SAVE		=	(Button) findViewById(R.id.print_setup_save);
		PRT_SETUP_CANCEL	=	(Button) findViewById(R.id.print_setup_cancel);
		
		PRT1_BTL	=	(Spinner) findViewById(R.id.print_setup_pt1_btl);
		PRT2_BTL	=	(Spinner) findViewById(R.id.print_setup_pt2_btl);
		PRT3_BTL	=	(Spinner) findViewById(R.id.print_setup_pt3_btl);
		PRT1_DUANKOU	=	(Spinner) findViewById(R.id.print_setup_pt1_duankou);
		PRT2_DUANKOU	=	(Spinner) findViewById(R.id.print_setup_pt2_duankou);
		PRT3_DUANKOU	=	(Spinner) findViewById(R.id.print_setup_pt3_duankou);

		USB_SWICH	=	(CheckBox) findViewById(R.id.print_setup_usb_checkbox);
		LN_LAYOUT	=	(LinearLayout) findViewById(R.id.print_setup_pt_usb);
		PRT_USB		= 	(Spinner) findViewById(R.id.print_setup_usb_pt);
	}

	private void init_view() {
		// TODO Auto-generated method stub
        //将可选内容与ArrayAdapter连接起来   
		adapter_prt1_btl= ArrayAdapter.createFromResource(this, R.array.botelv, android.R.layout.simple_spinner_item); 
 	    //设置下拉列表的风格   
		adapter_prt1_btl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        //将adapter2 添加到spinner中  
		PRT1_BTL.setAdapter(adapter_prt1_btl);    

/**********************************************************************************/		        
		adapter_prt2_btl= ArrayAdapter.createFromResource(this, R.array.botelv, android.R.layout.simple_spinner_item); 
		adapter_prt2_btl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT2_BTL.setAdapter(adapter_prt2_btl);    
/**********************************************************************************/		        
		adapter_prt3_btl= ArrayAdapter.createFromResource(this, R.array.botelv, android.R.layout.simple_spinner_item); 
		adapter_prt3_btl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT3_BTL.setAdapter(adapter_prt3_btl);    
/**********************************************************************************/		        
		adapter_prt1_duankou= ArrayAdapter.createFromResource(this, R.array.device, android.R.layout.simple_spinner_item); 
		adapter_prt1_duankou.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT1_DUANKOU.setAdapter(adapter_prt1_duankou);    
/**********************************************************************************/		        
		adapter_prt2_duankou= ArrayAdapter.createFromResource(this, R.array.device, android.R.layout.simple_spinner_item); 
		adapter_prt2_duankou.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT2_DUANKOU.setAdapter(adapter_prt2_duankou);    
/**********************************************************************************/		        
		adapter_prt3_duankou= ArrayAdapter.createFromResource(this, R.array.device, android.R.layout.simple_spinner_item); 
		adapter_prt3_duankou.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT3_DUANKOU.setAdapter(adapter_prt3_duankou);    
/**********************************************************************************/		        
		adapter_usb_prt = ArrayAdapter.createFromResource(this, R.array.device, android.R.layout.simple_spinner_item); 
		adapter_usb_prt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
		PRT_USB.setAdapter(adapter_usb_prt);    
/**********************************************************************************/		        
		if ( USB_SWICH.isChecked() ) {
			LN_LAYOUT.setVisibility(View.VISIBLE);
		} else {
			LN_LAYOUT.setVisibility(View.GONE);
		}
	}
 
   
	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(DuanKou_Setup.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据      
	        handler.sendEmptyMessage(5);
	        editor.putInt(SPF_Prt1_bt, prt1_btl);
	        editor.putInt(SPF_Prt2_bt, prt2_btl);
	        editor.putInt(SPF_Prt3_bt, prt3_btl);
	        editor.putInt(SPF_Prt1_DK, prt1_duankou);
	        editor.putInt(SPF_Prt2_DK, prt2_duankou);
	        editor.putInt(SPF_Prt3_DK, prt3_duankou);   	        
	        handler.sendEmptyMessage(55);
	        editor.putString(SPF_Com_1, COM_1); 	        
	        editor.putString(SPF_Com_2, COM_2); 	        
	        editor.putString(SPF_Com_3, COM_3); 	        
	        Log.i("info", "COM = "+ COM_1 + COM_2 + COM_3);  
	        handler.sendEmptyMessage(85);
	        editor.putBoolean(SPF_CHECKBOX_USB_PRT, USB_SWICH.isChecked());	        
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}