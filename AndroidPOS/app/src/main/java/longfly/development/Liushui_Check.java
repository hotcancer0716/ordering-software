package longfly.development;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker.OnTimeChangedListener;

public class Liushui_Check extends Activity{

	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

	/********以下是对话框内的控件**************************/
	Button		DIALOG_BTN_OK;
	Button		DIALOG_BTN_CANCEL;
	TextView	DIALOG_TX_TIME;
	DatePicker  DIALOG_DATEPICKER;
	TimePicker  DIALOG_TIMEPICKER;
	/****************************************************/	
	Button	LIUSHUI_CHECKE,LIUSHUI_EXIT,LIUSHUI_START,LIUSHUI_END;
	ListView	LS_LIUSHUI;
	TextView	TITLE;
	LinearLayout LNEARLAYOUT;
	String SQL_Table = "lfcy_base_sellinf_liushui";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_liushui_check);
        
       find_id();
       key_things();
 	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
 	   LNEARLAYOUT.setVisibility(View.VISIBLE);
 	   TITLE.setText("收银流水查询");
	}

	private void key_things() {
		// TODO Auto-generated method stub
		LIUSHUI_CHECKE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   	   	String start_time,end_time;
		   	   	start_time = LIUSHUI_START.getText().toString();
		   	   	end_time = LIUSHUI_END.getText().toString();
		   		String[] refer = {start_time , end_time};
				Cursor  cursor = db.rawQuery("select * from "+SQL_Table+" where cOpenTableTime > ? and cOpenTableTime < ?",refer);
				fill_hashmap(cursor);
				inflateList(cursor);				
			}
	    });	
		LIUSHUI_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		db.close();
		   		finish();
		   	}
	    });	
		LIUSHUI_START.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		START_END = true;
                showDialog(DIALOG_FIND);
		   	}
	    });			
		LIUSHUI_END.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		START_END = false;
                showDialog(DIALOG_FIND);
		   	}
	    });	
		
		LS_LIUSHUI.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
			{
				// TODO Auto-generated method stub
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(position);
                String CaiCode 	=	String.valueOf(map.get("_id"));
				
		   		sql_control sqlc = new sql_control();
		   		sqlc.remove_table_sample(db, CaiCode);
		   		refch_list(); 	
			}
		});		
	}

	protected void refch_list() {
   		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
		Cursor  cursor = db.rawQuery("select * from "+SQL_Table, null);
		fill_hashmap(cursor);
		inflateList(cursor);			
	}

	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            
            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("_num", ""+i);
            map.put("cOpenTableNo", cursor.getString(cursor.getColumnIndex("cOpenTableNo")));
            map.put("iTableNumber", cursor.getString(cursor.getColumnIndex("iTableNumber")));
            map.put("cOpenTableTime",  cursor.getString(cursor.getColumnIndex("cOpenTableTime")));
		
			list.add(map);
		}
		
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_liushui_check 
				, new String[]{	"_num" 	, "cOpenTableTime" ,"cOpenTableNo" 	, "iTableNumber" 
			}
        		, new int[]{R.id.liushui_check_1 , R.id.liushui_check_2 ,R.id.liushui_check_3,R.id.liushui_check_4
			}
        );
		
		//显示数据
        LS_LIUSHUI.setAdapter(adapter);
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		LIUSHUI_CHECKE 	= (Button) findViewById(R.id.liuchui_check_chaxun);
		LIUSHUI_EXIT 	= (Button) findViewById(R.id.liuchui_check_exit);
		LIUSHUI_START 	= (Button) findViewById(R.id.liuchui_check_start);
		LIUSHUI_END 	= (Button) findViewById(R.id.liuchui_check_end);
		LS_LIUSHUI		= (ListView) findViewById(R.id.liushui_check_list);
		TITLE			= (TextView) findViewById(R.id.liushui_title);
		LNEARLAYOUT		= (LinearLayout) findViewById(R.id.liushui_style_1);
	}
	
	
	/***      初始化时间对话框！ 命名为buildDialog      ***/
   	int	now_year,now_month,now_day,now_hour,now_minute;
	int sYear,sMounth,sDay,sHour,sMinute;	//起始时间记录，和起始按钮的时间要一致
	int eYear,eMounth,eDay,eHour,eMinute;	//结束时间记录，和结束按钮的时间要一致
	boolean	Init_Dialog_S = true;
	boolean	Init_Dialog_E = true;
	boolean	START_END;
   	private int DIALOG_FIND=101;
	private Dialog buildDialog(Context context) {  	
		// TODO Auto-generated method stub
        final AlertDialog.Builder builder =   new AlertDialog.Builder(context);  
        //下面是设置对话框的属性
        builder.setIcon(R.drawable.other_1); 
        if (START_END) {
            builder.setTitle("选择起始时间");  
		} else {
	        builder.setTitle("选择结束时间");  
		}
        LayoutInflater inflater = LayoutInflater.from(Liushui_Check.this);
        View view = inflater.inflate(R.layout.i800dialog_datetime_choose, null);			
        
		if (START_END) {
			if (Init_Dialog_S) {
	            Calendar calc = Calendar.getInstance();
	            now_year  = calc.get(Calendar.YEAR);
	            now_month = calc.get(Calendar.MONTH);
	            now_day   = calc.get(Calendar.DAY_OF_MONTH);
	            now_hour  = calc.get(Calendar.HOUR);
	            now_minute= calc.get(Calendar.MINUTE);						
			} else {
	            now_year  = sYear;
	            now_month = sMounth;
	            now_day   = sDay;
	            now_hour  = sHour;
	            now_minute= sMinute;
			}
		} else {
			if (Init_Dialog_E) {
	            Calendar calc = Calendar.getInstance();
	            now_year  = calc.get(Calendar.YEAR);
	            now_month = calc.get(Calendar.MONTH);
	            now_day   = calc.get(Calendar.DAY_OF_MONTH);
	            now_hour  = calc.get(Calendar.HOUR);
	            now_minute= calc.get(Calendar.MINUTE);						
			} else {
	            now_year  = eYear;
	            now_month = eMounth;
	            now_day   = eDay;
	            now_hour  = eHour;
	            now_minute= eMinute;
			}
		}

        builder.setView(view);
        builder.setCancelable(false);
        
		DIALOG_BTN_OK 		= (Button) view.findViewById(R.id.dialog_time_ok);
		DIALOG_BTN_CANCEL 	= (Button) view.findViewById(R.id.dialog_time_cancel);	
		DIALOG_TX_TIME		= (TextView) view.findViewById(R.id.dialog_time_tx);
		DIALOG_TIMEPICKER	= (TimePicker) view.findViewById(R.id.dialog_time_pick);
		DIALOG_DATEPICKER	= (DatePicker) view.findViewById(R.id.dialog_date_pick);
		
		DIALOG_DATEPICKER.init(now_year, now_month, now_day, new OnDateChangedListener() {
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				now_year  = year;
				now_month = monthOfYear;
				now_day   = dayOfMonth;		
			}
		});
		
		DIALOG_TIMEPICKER.setOnTimeChangedListener(new OnTimeChangedListener() {		
			@Override
			public void onTimeChanged(TimePicker view, int hour, int minute) {
				// TODO Auto-generated method stub
				now_hour = hour;
				now_minute = minute;
			}
		});
		
		
		DIALOG_BTN_OK.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
				if (START_END) {
		              sYear		=	now_year;
		              sMounth	=	now_month;
		              sDay		=	now_day;
		              sHour		=	now_hour;
		              sMinute	=	now_minute;
		              showData(sYear,sMounth+1,sDay,sHour,sMinute);
				} else {
		              eYear		=	now_year;
		              eMounth	=	now_month;
		              eDay		=	now_day;
		              eHour		=	now_hour;
		              eMinute	=	now_minute;
		              showData(eYear,eMounth+1,eDay,eHour,eMinute);
				}
				if (START_END) {
					Init_Dialog_S = false;					
				} else {
					Init_Dialog_E = false;
				}
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}
	    });
		
		DIALOG_BTN_CANCEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		removeDialog(DIALOG_FIND);  //关闭对话框 
		   	}
	    });
		
        return builder.create();  
    } 
    
	protected void showData(int year2, int month2, int day2, int hour2,
			int minute2) {
		// TODO Auto-generated method stub
		String mMounth,mDay,mHour,mMinute;
		
		if (month2<10) {
			mMounth = "0"+month2;
		}else {
			mMounth = ""+month2;
		}
		
		if (day2<10) {
			mDay = "0"+day2;
		}else {
			mDay = ""+day2;
		}
		if (hour2<10) {
			mHour = "0"+hour2;
		}else {
			mHour = ""+hour2;
		}	
		if (minute2<10) {
			mMinute = "0"+minute2;
		}else {
			mMinute = ""+minute2;
		}		
		DIALOG_TX_TIME.setText("您设置的时间是:         "+year2+"年"+ mMounth+"月"+
				mDay+"日"+mHour+"点"+mMinute+"分");
		
   		if (START_END) {
   			LIUSHUI_START.setText( year2+"-"+mMounth+"-"+mDay +" "+mHour+":"+mMinute );
		} else {
			LIUSHUI_END.setText( year2+"-"+mMounth+"-"+mDay +" "+mHour+":"+mMinute );
		}
	}

	@Override 
    protected Dialog onCreateDialog(int id) {  
        // TODO Auto-generated method stub  
        if(id==101){  
        	return this.buildDialog(Liushui_Check.this);  
        }
        else{  
            return null;  
        }  
    } 

    @Override 
    protected void onPrepareDialog(int id, Dialog dialog) {  
        // TODO Auto-generated method stub  
        super.onPrepareDialog(DIALOG_FIND, dialog);  
    } 
    /***      以上是初始一个对话框！       ***/	
    
    
}
