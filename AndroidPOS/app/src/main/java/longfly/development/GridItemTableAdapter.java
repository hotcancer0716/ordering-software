package longfly.development;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridItemTableAdapter extends BaseAdapter  
{  
  
    private LayoutInflater inflater;   
    private List<Grid_Item_Table> gridItemList;   
   
    public GridItemTableAdapter(String[] titles, int[] all_number,  String[] time, int[] state,  Context context)   
    {   
        super();   
        gridItemList = new ArrayList<Grid_Item_Table>();   
        inflater = LayoutInflater.from(context);   
        for (int i = 0; i < all_number.length; i++)   
        {   
        	Grid_Item_Table picture = new Grid_Item_Table( titles[i], all_number[i], time[i] , state[i]);   
            gridItemList.add(picture);   
        }   
    }   
    @Override  
    public int getCount( )  
    {  
        if (null != gridItemList)   
        {   
            return gridItemList.size();   
        }   
        else  
        {   
            return 0;   
        }   
    }  
  
    @Override  
    public Object getItem( int position )  
    {  
        return gridItemList.get(position);   
    }  
  
    @Override  
    public long getItemId( int position )  
    {  
        return position;   
    }  
  
    @Override  
    public View getView( int position, View convertView, ViewGroup parent )  
    {  
        ViewHolder viewHolder;   
        if (convertView == null)   
        {   
            convertView = inflater.inflate(R.layout.imx_grid_table_item, null);   
            viewHolder = new ViewHolder();   
            viewHolder.TABLE_NUMBER = (TextView) convertView.findViewById(R.id.table_number);   
            viewHolder.TABLE_BACKGROUND = (ImageView) convertView.findViewById(R.id.grid_table_background);  
            viewHolder.START_TIME = (TextView) convertView.findViewById(R.id.table_start_time);   
            convertView.setTag(viewHolder);   
        } else  
        {   
            viewHolder = (ViewHolder) convertView.getTag();   
        }   
        viewHolder.TABLE_NUMBER.setText(gridItemList.get(position).getNumber());  
        viewHolder.START_TIME.setText(gridItemList.get(position).getTime());   
        viewHolder.TABLE_BACKGROUND.setImageResource(gridItemList.get(position).getTableState());   
        return convertView;   
    } 
}