package longfly.development;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import casio.serial.SerialPort;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class Service_Test extends Service{

	private	int	count=0;
	private	boolean	quit;
	
	private	MyBinder binder = new MyBinder();

	private SerialPort mSerialPort = null;		//串口设备描述
	protected OutputStream mOutputStream;		//串口输出描述
	
	public class MyBinder extends Binder{
		public	int	getCount()
		{
			return count;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		System.out.println("3.Service is bindered");
		return binder;
	}

	public	void	onCreate(){
		super.onCreate();
/*		
		new Thread(){
			@Override
			public	void run(){
				while(!quit)
				{
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						// TODO: handle exception
					}
					count++;
					System.out.println("count = "+ count);
					
					if( count==10 |count== 20)
					{
						String TTY_DEV = "/dev/ttymxc2";
						try {
							mSerialPort = new SerialPort(new File(TTY_DEV), 9600, 0);
						} catch (SecurityException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					
						mOutputStream = mSerialPort.getOutputStream();
						
						byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
						print_String(cancel_to_normal,"广东韶关龙飞数码科技有限公司");

					}	
				}	
			}
		}.start();
*/
		new Thread(){
			@Override
			public	void run(){
				try {
					System.out.println("control_wifi");					
					control_wifi();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();		
		System.out.println("1.Service is Created");

	}

	
	@Override
	public	boolean	onUnbind(Intent intent){
		System.out.println("4.Service is Unbinded");
		return true;
	}
	
	@Override
	public	int	onStartCommand(Intent intent,int flags ,int startId )
	{
		System.out.println("2.Service is Started");
		/*************************************************
		try {
			control_wifi();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("control_wifi is failed");
			e.printStackTrace();
		}
		*************************************************/
		
		return START_STICKY;
		
	}
	
	@Override
	public	void onDestroy(){
		super.onDestroy();
		this.quit = true;
		System.out.println("5.Service is Destroyed");
	}
	
	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	private void control_wifi() throws IOException
	{
		System.out.println("net is wait");

		// 创建一个ServerSocket，用于监听客户端Socket的连接请求
		ServerSocket ss = new ServerSocket(30000);
		// 采用循环不断接受来自客户端的请求
		while (true)
		{
			System.out.println("net is ready");
			// 每当接受到客户端Socket的请求，服务器端也对应产生一个Socket
			Socket s = ss.accept();
			System.out.println("net is accept");
			
 			// 将Socket对应的输出流包装成PrintStream
			PrintStream ps = new PrintStream(s.getOutputStream());
			// 进行普通IO操作
			ps.println("On or Off");

			//接收
			BufferedReader br = new BufferedReader(
					new InputStreamReader(s.getInputStream()));
					// 进行普通IO操作
			String line = br.readLine();

			Integer intObj = new Integer(line);
			int i = intObj.intValue();
			//i就是最终接收到的信号标志码！用来辨认是下单还是厨打！

			String TTY_DEV = "/dev/ttymxc2";
			try {
				mSerialPort = new SerialPort(new File(TTY_DEV), 9600, 0);
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
			mOutputStream = mSerialPort.getOutputStream();
			
			byte[] cancel_to_normal = {0x0a, 0x1b, 0x21,0x00};//取消倍高倍宽
			
			switch (i) {
			case 0:
				print_String(cancel_to_normal,"广东韶关龙飞数码科技有限公司");					
				break;
			case 1:
				print_String(cancel_to_normal,"沐溪工业园 19路公交车");	
				break;
			case 2:
				print_String(cancel_to_normal,"武江区沙洲尾碧水花城第一期");	
				break;				
			default:
				print_String(cancel_to_normal,"嵌入式Linux AndroidPOS");	
				break;
			}

			// 关闭输出流，关闭Socket
			br.close();
			ps.close();
			s.close();
		}
	}
	
}