package longfly.development;

public class Grid_Item_Table {
 
        private String  table_number;
        private	int		all_table;
        private int 	table_state; 
        private String 	Table_State; 
        private String  start_time;
    	private int 	imageId; //图片ID(位置)
    	private	String	table_person;
    	private	String	table_person_number;
    	private	String	table_place;
    	
        
        public Grid_Item_Table() 
        { 
            super(); 
        } 
      
        public Grid_Item_Table(String number, int all_number ,String time ,int state) 
        { 
            super(); 
            this.table_number = number; 
            this.all_table = all_number; 
            this.start_time = time;
            this.table_state = state; 
        } 
     
        public String getTime( )
        {
            return start_time;
        }

        public String getNumber() 
        { 
            return table_number; 
        } 
     
        public int getTableState() 
        { 
            return table_state; 
        } 
        public String  getSringTableState() 
        { 
            return Table_State; 
        }
        
        public int getAllTableNumber() 
        { 
            return all_table; 
        }   
        
    	public void setImageId(int imageId) {
    		this.imageId = imageId;
    	}
    	public void setNumber(String  number) {
            this.table_number = number; 
    	}
    	public void setTime(String  time) {
            this.start_time = time; 
    	}    
    	public void setTableState(String  state) {
            this.Table_State = state; 
    	}   
        public String getPlace() 
        { 
            return table_place; 
        }     	
 
    	public void setTablePlace(String  place) {
            this.table_place = place; 
    	} 
    	
        public String getPersonNumber() 
        { 
            return table_person_number; 
        }     	
 
    	public void setTablePersonNumber(String  table_person_number) {
            this.table_person_number = table_person_number; 
    	} 
    	
        public String setTable_PersonNumber() 
        { 
            return table_person; 
        }     	
 
    	public void setTable_PersonNumber(String  table_person) {
            this.table_person = table_person; 
    	} 
     	
} 