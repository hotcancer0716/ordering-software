package longfly.development;

/**
 * 全局资源类
 * 
 * @author WLei
 * 
 */
public class Gloable {

	public static String[] TITLES = new String[] { "商城", "测试1", "测试2", "测试3" };

	public static Integer[] IMAGES = new Integer[] { R.drawable.button1_up,
			R.drawable.button1_up, R.drawable.button1_up, R.drawable.button1_up };

	public static String DB_NAME = "pictureDB";
	public static String TABLE_NAME = "picture";
	public static int DB_VERSION = 1;

}
