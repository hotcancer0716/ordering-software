package longfly.development;

public class Grid_Item_Table_SQL {
 
        private String  table_number;
        private	int		all_table;
        private int 	table_state; 
        private String  start_time;
        
        public Grid_Item_Table_SQL() 
        { 
            super(); 
        } 
     
        public Grid_Item_Table_SQL(String number, int all_number ,String time ,int state) 
        { 
            super(); 
            this.table_number = number; 
            this.all_table = all_number; 
            this.start_time = time;
            this.table_state = state; 
        } 
     
        public String getTime( )
        {
            return start_time;
        }

        public String getNumber() 
        { 
            return table_number; 
        } 
     
        public int getTableState() 
        { 
            return table_state; 
        } 
        public int getAllTableNumber() 
        { 
            return all_table; 
        }         
    } 