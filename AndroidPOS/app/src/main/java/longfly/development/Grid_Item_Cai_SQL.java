package longfly.development;

public class Grid_Item_Cai_SQL {
 
		private	int		cai_style_pic;	//样式图片
        private String 	cai_style;		//样式内容
        private	int		cai_pic;		//菜品图片
        private int 	cai_number; 	//菜品图片的顺序号
        private String  cai_name;		//菜品名称
        private String  cai_price;		//菜品售价
        private String  cai_code;		//菜品编码
        private String 	cai_dian; 	    //1是已点   0是没点
        private String 	cai_pic_path; 	    //1是已点   0是没点
        private String 	cai_unit; 	    //菜品编码

        public Grid_Item_Cai_SQL() 
        { 
            super(); 
        } 
     
        public Grid_Item_Cai_SQL(int cai_style_pic, String cai_style ,
        		int		cai_pic,int		cai_number ,
        		String cai_name ,String cai_price , 
        		String cai_code,String cai_pic_path ,String cai_unit) 
        { 
            super(); 
            this.cai_style_pic  = cai_style_pic; 
            this.cai_style 		= cai_style; 
            this.cai_pic 		= cai_pic;
            this.cai_number 	= cai_number; 
            this.cai_name 		= cai_name; 
            this.cai_price 		= cai_price; 
            this.cai_code 		= cai_code; 
            this.cai_pic_path 	= cai_pic_path; 
            this.cai_unit 		= cai_unit; 
        } 
     
        public String getStyle( )
        {
            return cai_style;
        }

        public int getNumber() 
        { 
            return cai_number; 
        } 
        
        public int getStylePic() 
        { 
            return cai_style_pic; 
        }
        
        public int getPic() 
        { 
            return cai_pic; 
        } 
        
        public String getCaiName() 
        { 
            return cai_name; 
        } 
        
        public String getCaiPrice() 
        { 
            return cai_price; 
        }    
        
        public String getCaiCode() 
        { 
            return cai_code; 
        }       
        public String getDian() 
        { 
            return cai_dian; 
        }
        public String getPath() 
        { 
            return cai_pic_path; 
        }        
        public String getUnit() 
        { 
            return cai_unit; 
        }   
        
    	public void setStyle(String  cai_style) {
            this.cai_style = cai_style; 
    	}        
    	public void setCai(int  cai_pic) {
            this.cai_pic = cai_pic; 
    	} 
    	public void setNumber(int  cai_number) {
            this.cai_number = cai_number; 
    	}    	
    	public void setName(String  cai_name) {
            this.cai_name = cai_name; 
    	}  
    	public void setPrice(String  cai_price) {
            this.cai_price = cai_price; 
    	}  
    	public void setCode(String  cai_code) {
            this.cai_code = cai_code; 
    	}  
    	public void setDian(String  cai_dian) {
            this.cai_dian = cai_dian; //1是已点   0是没点
    	}     	
    	public void setPath(String  cai_path) {
            this.cai_pic_path = cai_path; 
    	} 
    	public void setUnit(String  cai_unit) {
            this.cai_unit = cai_unit; 
    	} 
} 