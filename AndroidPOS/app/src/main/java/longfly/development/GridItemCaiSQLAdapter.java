package longfly.development;

import java.util.List;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class GridItemCaiSQLAdapter extends BaseAdapter {

	List<Grid_Item_Cai_SQL> pictures;
	Context context;
	int Dispay_widthPixels;
	public GridItemCaiSQLAdapter(List<Grid_Item_Cai_SQL> pic_list, Context context ,int dispay_widthPixels) {
		super();
		this.pictures = pic_list;
		this.context = context;
		this.Dispay_widthPixels	=	dispay_widthPixels;
	}

	@Override
	public int getCount() {
		if (null != pictures) {
			return pictures.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return pictures.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolderCai viewHolder;
		if (convertView == null   || convertView.getTag() == null ) {
			switch (Dispay_widthPixels) {
			case 800:
				convertView = inflater.inflate(R.layout.i800_grid_cai_item, null);				
				break;
			case 1024:
				convertView = inflater.inflate(R.layout.imx_grid_cai_item, null);				
				break;
			default:
				convertView = inflater.inflate(R.layout.i800_grid_cai_item, null);				
				break;
			}
            viewHolder = new ViewHolderCai();   
            viewHolder.CAI_BACKGROUND 	= (ImageView) convertView.findViewById(R.id.grid_cai_background);  
            viewHolder.CAI_STYLE_IMAGE 	= (ImageView) convertView.findViewById(R.id.grid_cai_style_pic);  
            viewHolder.CAI_CAI_DIAN 	= (ImageView) convertView.findViewById(R.id.grid_cai_dian);  
            viewHolder.CAI_NAME 		= (TextView) convertView.findViewById(R.id.grid_cai_name);   
            viewHolder.CAI_CODE 		= (TextView) convertView.findViewById(R.id.grid_cai_code);   
            viewHolder.CAI_PRICE 		= (TextView) convertView.findViewById(R.id.grid_cai_price);   
            viewHolder.CAI_STYLE 		= (TextView) convertView.findViewById(R.id.grid_cai_style);   
            viewHolder.CAI_UNIT 		= (TextView) convertView.findViewById(R.id.grid_cai_unit);   
			convertView.setTag(viewHolder);

	        viewHolder.CAI_NAME.setText(pictures.get(position).getCaiName());  
	        viewHolder.CAI_PRICE.setText(pictures.get(position).getCaiPrice());  
	        viewHolder.CAI_CODE.setText(pictures.get(position).getCaiCode());  
	        viewHolder.CAI_UNIT.setText(pictures.get(position).getUnit());  
	        String Cai_State = pictures.get(position).getStyle();
	        int i = 0;        
	        try {
	            Integer intObj = new Integer(Cai_State);
	            i = intObj.intValue();				
			} catch (Exception e) {
				i=0;
		    }

	        switch (i) {
				case 1:
			        viewHolder.CAI_STYLE_IMAGE.setImageResource(R.drawable.griditem_cai_style_1);
			        viewHolder.CAI_STYLE.setText("推荐");     
			        break;
				case 2:
			        viewHolder.CAI_STYLE_IMAGE.setImageResource(R.drawable.griditem_cai_style_2);
			        viewHolder.CAI_STYLE.setText("特价");  	        
					break;
				case 3:
			        viewHolder.CAI_STYLE_IMAGE.setImageResource(R.drawable.griditem_cai_style_3);
			        viewHolder.CAI_STYLE.setText("新品");  	        
					break;			
				case 4:
			        viewHolder.CAI_STYLE_IMAGE.setImageResource(R.drawable.griditem_cai_style_4);
			        viewHolder.CAI_STYLE.setText("招牌");  	        
					break;
				case 6:
			        viewHolder.CAI_STYLE_IMAGE.setImageResource(R.drawable.griditem_cai_style_5);
			        viewHolder.CAI_STYLE.setText("沽清");  	        
					break;	
				default:
					break;
			}

	        String picturePath = pictures.get(position).getPath();
	        if (picturePath.length() < 5) {
//		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.table_free);  			
		        viewHolder.CAI_BACKGROUND.setBackgroundColor(R.color.a_wlei_blue) ;			
			} else {
				viewHolder.CAI_BACKGROUND.setImageBitmap(BitmapFactory.decodeFile(picturePath));
			}
		
		} else {
			viewHolder = (ViewHolderCai) convertView.getTag();
		}

		

/*
        String CAI_Code = pictures.get(position).getCaiCode();
        try {
            Integer intObj = new Integer(CAI_Code);
            i = intObj.intValue();				
		} catch (Exception e) {
			i=0;		
	    }
		

		switch (i) {
			case 10001:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_1);  
				break;		
			case 10002:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_2);  
				break;	
			case 10003:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_3);  
				break;				
			case 10004:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_4);  
				break;				
			case 10005:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_5);  
				break;	
			case 10006:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_6);  
				break;					
			case 10007:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_7);  
				break;					
			case 10008:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_8);  
				break;					
			case 10009:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_9);  
				break;					
			case 10010:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_10);  
				break;					
			case 10011:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_11);  
				break;	
			case 10012:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_12);  
				break;					
			case 10013:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_13);  
				break;					
			case 10014:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_14);  
				break;					
			case 10015:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_15);  
				break;					
			case 10016:
		        viewHolder.CAI_BACKGROUND.setImageResource(R.drawable.cai_16);  
				break;					
				
			default:
				break;
		}
        
        String CAI_Dian = pictures.get(position).getDian();
        try {
            Integer   intObj = new Integer(CAI_Dian);
            i = intObj.intValue();			
		} catch (Exception e) {
			i=0;		
	    }
	
        switch (i) {
			case 1:
		        viewHolder.CAI_CAI_DIAN.setImageResource(R.drawable.cai_dian);  
				break;		
			default:
				break;
		}
*/        
		return convertView;
	}
}

