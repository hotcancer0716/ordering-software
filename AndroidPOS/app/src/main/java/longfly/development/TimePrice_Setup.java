package longfly.development;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class TimePrice_Setup extends Activity {
	
	TextView	TP_CAI_NAME;
	Button		TP_CAI_EXIT;	
	ListView	TP_CAI_LIST;
	EditText	TP_CAI_EDIT_PRICE;
	Button		TP_CAI_BTN_SAVE;
	View		ITem;
	
	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

    int	list_position=0; 
    String ID;
    final	String Activity_Table = "lfcy_base_dishinf";
    String CAI_CODE;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_timeprice_setup);
        
        find_id();
        key_things();

        //初始化数据库 显示在listview
  	    db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null); 
    	init_taset_list();
   }

	/*******************大类列表开始*****************************************/
	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;            
			map=new HashMap<String,String>();
            map.put("num", ""+i);            
            map.put("_id", 	 cursor.getString(cursor.getColumnIndex("_id")));
            map.put("cDishName", cursor.getString(cursor.getColumnIndex("cDishName")));
            map.put("cDishCode", cursor.getString(cursor.getColumnIndex("cDishCode")));
            map.put("fDishPrice", cursor.getString(cursor.getColumnIndex("fDishPrice")));
		
			list.add(map);
		}
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.i800_cai_list 
				, new String[]{	"num" 		  , "cDishName" ,"cDishCode" ,"fDishPrice"}
				, new int[]{R.id.table_list_1 , R.id.table_list_2 ,R.id.table_list_3 ,R.id.table_list_4}
        );
		
		//显示数据
        TP_CAI_LIST.setAdapter(adapter);
	}
	
	private void init_taset_list() {
  		//cursor里面存了所有从数据库里读出的数据！这时还仅仅是数据！跟显示没有半毛钱关系！
 	   Cursor  cursor = db.rawQuery("select * from "+ Activity_Table, null);
 	   fill_hashmap(cursor);
 	   inflateList(cursor);			
	}
	/*******************大类列表结束*****************************************/



	private void key_things() {
		TP_CAI_EXIT.setOnClickListener(new OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	

		TP_CAI_LIST.setOnItemClickListener(new OnItemClickListener()   
        {   
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   
            {   
            	list_position = position;
				HashMap<String,String> map = (HashMap<String,String>)parent.getItemAtPosition(list_position);
				String	cName  =	String.valueOf(map.get("cDishName"));
                String	cCode  =	String.valueOf(map.get("cDishCode"));
                String	cPrice  =	String.valueOf(map.get("fDishPrice"));
                ID =	String.valueOf(map.get("_id")); 
                try {
                    ITem.setBackgroundColor(Color.argb(255, 235, 235, 235));              
				} catch (Exception e) {
					// TODO: handle exception
				}
                ITem = v;
                ITem.setBackgroundColor(Color.argb(255, 22, 175, 251));              
        		
                TP_CAI_NAME.setText(cName);
                TP_CAI_EDIT_PRICE.setHint(cPrice) ;               
                CAI_CODE = cCode;
            }  
        }); 
		
	
		TP_CAI_BTN_SAVE.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
		   		String	price	=	TP_CAI_EDIT_PRICE.getText().toString();
		   		
				//判断编辑框是否为空	
				if ( TP_CAI_EDIT_PRICE.getText().toString() == null || TP_CAI_EDIT_PRICE.getText().toString().length() <= 0 )
				{
					Toast.makeText(TimePrice_Setup.this, "时价不能为空 ", Toast.LENGTH_SHORT).show();				
				} 
				else 
				{
		   			ContentValues cv = new ContentValues();
		   		   	//存储修改的数据
		   		   	cv.put("fDishPrice"		, price);
		   		   	String[] contion = {CAI_CODE}; 
				   	db.update(Activity_Table,cv,"cDishCode = ? ",contion);
						init_taset_list();
				}
			}
		});

	}
	
	


	private void find_id() {
		// TODO Auto-generated method stub
		TP_CAI_EXIT		= (Button) findViewById(R.id.timeprice_cai_exit);
		TP_CAI_BTN_SAVE	= (Button) findViewById(R.id.timeprice_cai_save);		
		TP_CAI_LIST  	= (ListView) findViewById(R.id.timeprice_cai_list);
		TP_CAI_EDIT_PRICE = (EditText) findViewById(R.id.timeprice_cai_price);		
		TP_CAI_NAME 	= (TextView) findViewById(R.id.timeprice_cai_name);

	}
   
}