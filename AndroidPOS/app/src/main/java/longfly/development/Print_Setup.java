package longfly.development;

import java.io.IOException;
import java.io.OutputStream;

import casio.serial.SerialPort;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class Print_Setup extends Activity {
    /** Called when the activity is first created. */

	Button		PRT_SETUP_SAVE;
	Button		PRT_SETUP_CANCEL;

	EditText	PRT_ET_1_1;
	EditText	PRT_ET_1_2;
	EditText	PRT_ET_1_3;
	EditText	PRT_ET_2_1;
	EditText	PRT_ET_2_2;
	EditText	PRT_ET_2_3;
	EditText	PRT_ET_2_4;
	
	CheckBox	PRT_CHECKBOX_1_1;
	CheckBox	PRT_CHECKBOX_1_2;
	CheckBox	PRT_CHECKBOX_1_3;
	CheckBox	PRT_CHECKBOX_1_4;
	CheckBox	PRT_CHECKBOX_1_5;
	CheckBox	PRT_CHECKBOX_1_6;
	CheckBox	PRT_CHECKBOX_1_7;

	CheckBox	PRT_CHECKBOX_2_1,PRT_CHECKBOX_2_2,PRT_CHECKBOX_2_3,PRT_CHECKBOX_2_4,PRT_CHECKBOX_2_5,PRT_CHECKBOX_2_6,PRT_CHECKBOX_2_7,PRT_CHECKBOX_2_8,PRT_CHECKBOX_2_9,PRT_CHECKBOX_2_10,PRT_CHECKBOX_2_11,PRT_CHECKBOX_2_12;
	CheckBox	PRT_CHECKBOX_3_1,PRT_CHECKBOX_3_2,PRT_CHECKBOX_3_3,PRT_CHECKBOX_3_4,PRT_CHECKBOX_3_5,PRT_CHECKBOX_3_6,PRT_CHECKBOX_3_7,PRT_CHECKBOX_3_8,PRT_CHECKBOX_3_9,PRT_CHECKBOX_3_10,PRT_CHECKBOX_3_11,PRT_CHECKBOX_3_12;

	private SerialPort mSerialPort = null;		//串口设备描述
	protected OutputStream mOutputStream;		//串口输出描述
	
  	//定义SharedPreferences对象  
	SharedPreferences settings; 
	ProgressDialog progressDialog;
	
	String	SPF_ET_1_1	=	"SPF_ET_1_1";
	String	SPF_ET_1_2	=	"SPF_ET_1_2";
	String	SPF_ET_1_3	=	"SPF_ET_1_3";
	String	SPF_ET_2_1	=	"SPF_ET_2_1";
	String	SPF_ET_2_2	=	"SPF_ET_2_2";
	String	SPF_ET_2_3	=	"SPF_ET_2_3";
	String	SPF_ET_2_4	=	"SPF_ET_2_4";
	
	String	SPF_CHECKBOX_1_1	=	"SPF_CHECKBOX_1_1";
	String	SPF_CHECKBOX_1_2	=	"SPF_CHECKBOX_1_2";
	String	SPF_CHECKBOX_1_3	=	"SPF_CHECKBOX_1_3";
	String	SPF_CHECKBOX_1_4	=	"SPF_CHECKBOX_1_4";
	String	SPF_CHECKBOX_1_5	=	"SPF_CHECKBOX_1_5";
	String	SPF_CHECKBOX_1_6	=	"SPF_CHECKBOX_1_6";
	String	SPF_CHECKBOX_1_7	=	"SPF_CHECKBOX_1_7";

	String	SPF_CHECKBOX_2_1	=	"SPF_CHECKBOX_2_1";
	String	SPF_CHECKBOX_2_2	=	"SPF_CHECKBOX_2_2";
	String	SPF_CHECKBOX_2_3	=	"SPF_CHECKBOX_2_3";
	String	SPF_CHECKBOX_2_4	=	"SPF_CHECKBOX_2_4";
	String	SPF_CHECKBOX_2_5	=	"SPF_CHECKBOX_2_5";
	String	SPF_CHECKBOX_2_6	=	"SPF_CHECKBOX_2_6";
	String	SPF_CHECKBOX_2_7	=	"SPF_CHECKBOX_2_7";
	String	SPF_CHECKBOX_2_8	=	"SPF_CHECKBOX_2_8";
	String	SPF_CHECKBOX_2_9	=	"SPF_CHECKBOX_2_9";
	String	SPF_CHECKBOX_2_10	=	"SPF_CHECKBOX_2_10";
	String	SPF_CHECKBOX_2_11	=	"SPF_CHECKBOX_2_11";
	String	SPF_CHECKBOX_2_12	=	"SPF_CHECKBOX_2_12";

	String	SPF_CHECKBOX_3_1	=	"SPF_CHECKBOX_3_1";
	String	SPF_CHECKBOX_3_2	=	"SPF_CHECKBOX_3_2";
	String	SPF_CHECKBOX_3_3	=	"SPF_CHECKBOX_3_3";
	String	SPF_CHECKBOX_3_4	=	"SPF_CHECKBOX_3_4";
	String	SPF_CHECKBOX_3_5	=	"SPF_CHECKBOX_3_5";
	String	SPF_CHECKBOX_3_6	=	"SPF_CHECKBOX_3_6";
	String	SPF_CHECKBOX_3_7	=	"SPF_CHECKBOX_3_7";
	String	SPF_CHECKBOX_3_8	=	"SPF_CHECKBOX_3_8";
	String	SPF_CHECKBOX_3_9	=	"SPF_CHECKBOX_3_9";
	String	SPF_CHECKBOX_3_10	=	"SPF_CHECKBOX_3_10";
	String	SPF_CHECKBOX_3_11	=	"SPF_CHECKBOX_3_11";
	String	SPF_CHECKBOX_3_12	=	"SPF_CHECKBOX_3_12";

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_printsetup);
        
       find_id();
       key_things();
       
	   settings = getSharedPreferences("Prt_Setup_SharedPreferences", 0);		
       get_SharePerference();
	}


	private void key_things() {
		// TODO Auto-generated method stub
		PRT_SETUP_SAVE.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		try {
			   		Save_SharePreference();					
			   		finish();
		   		} catch (Exception e) {
					// TODO: handle exception
		   			Toast.makeText(Print_Setup.this, "保存失败，请检查数据是否设置正确", Toast.LENGTH_SHORT).show();
		   		}							
		   	}
	    });	

		PRT_SETUP_CANCEL.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		finish();
		   	}
	    });	
	}

	protected void Save_SharePreference() {
		// TODO Auto-generated method stub
		progressDialog("正在保存设置 ", "请稍等......");		
     	insert_Thread thread= new insert_Thread();
     	thread.start();		
	}

	private void get_SharePerference() {
		
		PRT_ET_1_1.setText(  settings.getString(SPF_ET_1_1, null) );
		PRT_ET_1_2.setText(  settings.getString(SPF_ET_1_2, null) );
		PRT_ET_1_3.setText(  settings.getString(SPF_ET_1_3, null) );
		PRT_ET_2_1.setText(  settings.getString(SPF_ET_2_1, null) );
		PRT_ET_2_2.setText(  settings.getString(SPF_ET_2_2, null) );
		PRT_ET_2_3.setText(  settings.getString(SPF_ET_2_3, null) );
		PRT_ET_2_4.setText(  settings.getString(SPF_ET_2_4, null) );
	
		PRT_CHECKBOX_1_1.setChecked(settings.getBoolean(SPF_CHECKBOX_1_1, false));
		PRT_CHECKBOX_1_2.setChecked(settings.getBoolean(SPF_CHECKBOX_1_2, false));
		PRT_CHECKBOX_1_3.setChecked(settings.getBoolean(SPF_CHECKBOX_1_3, false));
		PRT_CHECKBOX_1_4.setChecked(settings.getBoolean(SPF_CHECKBOX_1_4, false));
		PRT_CHECKBOX_1_5.setChecked(settings.getBoolean(SPF_CHECKBOX_1_5, false));
		PRT_CHECKBOX_1_6.setChecked(settings.getBoolean(SPF_CHECKBOX_1_6, false));
		PRT_CHECKBOX_1_7.setChecked(settings.getBoolean(SPF_CHECKBOX_1_7, false));

		PRT_CHECKBOX_2_1.setChecked(settings.getBoolean(SPF_CHECKBOX_2_1, false));
		PRT_CHECKBOX_2_2.setChecked(settings.getBoolean(SPF_CHECKBOX_2_2, false));
		PRT_CHECKBOX_2_3.setChecked(settings.getBoolean(SPF_CHECKBOX_2_3, false));
		PRT_CHECKBOX_2_4.setChecked(settings.getBoolean(SPF_CHECKBOX_2_4, false));
		PRT_CHECKBOX_2_5.setChecked(settings.getBoolean(SPF_CHECKBOX_2_5, false));
		PRT_CHECKBOX_2_6.setChecked(settings.getBoolean(SPF_CHECKBOX_2_6, false));
		PRT_CHECKBOX_2_7.setChecked(settings.getBoolean(SPF_CHECKBOX_2_7, false));
		PRT_CHECKBOX_2_8.setChecked(settings.getBoolean(SPF_CHECKBOX_2_8, false));
		PRT_CHECKBOX_2_9.setChecked(settings.getBoolean(SPF_CHECKBOX_2_9, false));
		PRT_CHECKBOX_2_10.setChecked(settings.getBoolean(SPF_CHECKBOX_2_10, false));
		PRT_CHECKBOX_2_11.setChecked(settings.getBoolean(SPF_CHECKBOX_2_11, false));
		PRT_CHECKBOX_2_12.setChecked(settings.getBoolean(SPF_CHECKBOX_2_12, false));

		PRT_CHECKBOX_3_1.setChecked(settings.getBoolean(SPF_CHECKBOX_3_1, false));
		PRT_CHECKBOX_3_2.setChecked(settings.getBoolean(SPF_CHECKBOX_3_2, false));
		PRT_CHECKBOX_3_3.setChecked(settings.getBoolean(SPF_CHECKBOX_3_3, false));
		PRT_CHECKBOX_3_4.setChecked(settings.getBoolean(SPF_CHECKBOX_3_4, false));
		PRT_CHECKBOX_3_5.setChecked(settings.getBoolean(SPF_CHECKBOX_3_5, false));
		PRT_CHECKBOX_3_6.setChecked(settings.getBoolean(SPF_CHECKBOX_3_6, false));
		PRT_CHECKBOX_3_7.setChecked(settings.getBoolean(SPF_CHECKBOX_3_7, false));
		PRT_CHECKBOX_3_8.setChecked(settings.getBoolean(SPF_CHECKBOX_3_8, false));
		PRT_CHECKBOX_3_9.setChecked(settings.getBoolean(SPF_CHECKBOX_3_9, false));
		PRT_CHECKBOX_3_10.setChecked(settings.getBoolean(SPF_CHECKBOX_3_10, false));
		PRT_CHECKBOX_3_11.setChecked(settings.getBoolean(SPF_CHECKBOX_3_11, false));
		PRT_CHECKBOX_3_12.setChecked(settings.getBoolean(SPF_CHECKBOX_3_12, false));
	
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		PRT_SETUP_SAVE		=	(Button) findViewById(R.id.print_setup_save);
		PRT_SETUP_CANCEL	=	(Button) findViewById(R.id.print_setup_cancel);

		PRT_ET_1_1	=	(EditText) findViewById(R.id.print_setup_1_edit_1);
		PRT_ET_1_2	=	(EditText) findViewById(R.id.print_setup_1_edit_2);
		PRT_ET_1_3	=	(EditText) findViewById(R.id.print_setup_1_edit_3);
		PRT_ET_2_1	=	(EditText) findViewById(R.id.print_setup_2_edit_1);
		PRT_ET_2_2	=	(EditText) findViewById(R.id.print_setup_2_edit_2);
		PRT_ET_2_3	=	(EditText) findViewById(R.id.print_setup_2_edit_3);
		PRT_ET_2_4	=	(EditText) findViewById(R.id.print_setup_2_edit_4);
		
		PRT_CHECKBOX_1_1	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_1);
		PRT_CHECKBOX_1_2	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_2);
		PRT_CHECKBOX_1_3	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_3);
		PRT_CHECKBOX_1_4	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_4);
		PRT_CHECKBOX_1_5	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_5);
		PRT_CHECKBOX_1_6	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_6);
		PRT_CHECKBOX_1_7	=	(CheckBox) findViewById(R.id.print_setup_1_checkbox_7);

		PRT_CHECKBOX_2_1	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_1);
		PRT_CHECKBOX_2_2	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_2);
		PRT_CHECKBOX_2_3	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_3);
		PRT_CHECKBOX_2_4	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_4);
		PRT_CHECKBOX_2_5	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_5);
		PRT_CHECKBOX_2_6	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_6);
		PRT_CHECKBOX_2_7	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_7);
		PRT_CHECKBOX_2_8	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_8);
		PRT_CHECKBOX_2_9	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_9);
		PRT_CHECKBOX_2_10	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_10);
		PRT_CHECKBOX_2_11	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_11);
		PRT_CHECKBOX_2_12	=	(CheckBox) findViewById(R.id.print_setup_2_checkbox_12);

		PRT_CHECKBOX_3_1	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_1);
		PRT_CHECKBOX_3_2	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_2);
		PRT_CHECKBOX_3_3	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_3);
		PRT_CHECKBOX_3_4	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_4);
		PRT_CHECKBOX_3_5	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_5);
		PRT_CHECKBOX_3_6	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_6);
		PRT_CHECKBOX_3_7	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_7);
		PRT_CHECKBOX_3_8	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_8);
		PRT_CHECKBOX_3_9	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_9);
		PRT_CHECKBOX_3_10	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_10);
		PRT_CHECKBOX_3_11	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_11);
		PRT_CHECKBOX_3_12	=	(CheckBox) findViewById(R.id.print_setup_3_checkbox_12);

	}
 
   
	//我的打印机打印字符串的方法！！第一个参数是命令字！倍高倍宽之类！第二个参数是要打印的字符串！
	protected void print_String(byte[] prt_code_buffer, String in_String) {
		// TODO Auto-generated method stub
		int i;
		CharSequence t =  (CharSequence)in_String;
		char[] text = new char[t.length()];	//声明一个和所输入字符串同样长度的字符数组
		for (i=0; i<t.length(); i++) {
			text[i] = t.charAt(i);		//把CharSequence中的charAt传入刚声明的字符数组中
		}
		try {
			byte[] buffer = prt_code_buffer;//倍高倍宽;
			mOutputStream.write(buffer);
			mOutputStream.write(new String(text).getBytes("gb2312"));	//把字符数组变成byte型发送
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	
	
	/***************     以下是一个带进度条的对话框    **************************************/
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Print_Setup.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
        	
			handler.sendEmptyMessage(1);	//进度条进度

	        //获得SharedPreferences 的Editor对象  
	        SharedPreferences.Editor editor = settings.edit();  
	        //修改数据        
	        handler.sendEmptyMessage(5);
	        editor.putString(SPF_ET_1_1, String.valueOf( PRT_ET_1_1.getText().toString()));  
	        editor.putString(SPF_ET_1_2, String.valueOf( PRT_ET_1_2.getText().toString()));  
	        editor.putString(SPF_ET_1_3, String.valueOf( PRT_ET_1_3.getText().toString()));  
	        editor.putString(SPF_ET_2_1, String.valueOf( PRT_ET_2_1.getText().toString()));  
	        editor.putString(SPF_ET_2_2, String.valueOf( PRT_ET_2_2.getText().toString()));  
	        editor.putString(SPF_ET_2_3, String.valueOf( PRT_ET_2_3.getText().toString()));  
	        editor.putString(SPF_ET_2_4, String.valueOf( PRT_ET_2_4.getText().toString()));  
	        handler.sendEmptyMessage(25);
	        editor.putBoolean(SPF_CHECKBOX_1_1, PRT_CHECKBOX_1_1.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_2, PRT_CHECKBOX_1_2.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_3, PRT_CHECKBOX_1_3.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_4, PRT_CHECKBOX_1_4.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_5, PRT_CHECKBOX_1_5.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_6, PRT_CHECKBOX_1_6.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_1_7, PRT_CHECKBOX_1_7.isChecked());	        
	        handler.sendEmptyMessage(50);
	        editor.putBoolean(SPF_CHECKBOX_2_1, PRT_CHECKBOX_2_1.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_2, PRT_CHECKBOX_2_2.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_3, PRT_CHECKBOX_2_3.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_4, PRT_CHECKBOX_2_4.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_5, PRT_CHECKBOX_2_5.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_6, PRT_CHECKBOX_2_6.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_7, PRT_CHECKBOX_2_7.isChecked());	
	        editor.putBoolean(SPF_CHECKBOX_2_8, PRT_CHECKBOX_2_8.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_9, PRT_CHECKBOX_2_9.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_10, PRT_CHECKBOX_2_10.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_11, PRT_CHECKBOX_2_11.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_2_12, PRT_CHECKBOX_2_12.isChecked());	        
	        handler.sendEmptyMessage(75);
	        editor.putBoolean(SPF_CHECKBOX_3_1, PRT_CHECKBOX_3_1.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_2, PRT_CHECKBOX_3_2.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_3, PRT_CHECKBOX_3_3.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_4, PRT_CHECKBOX_3_4.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_5, PRT_CHECKBOX_3_5.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_6, PRT_CHECKBOX_3_6.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_7, PRT_CHECKBOX_3_7.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_8, PRT_CHECKBOX_3_8.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_9, PRT_CHECKBOX_3_9.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_10, PRT_CHECKBOX_3_10.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_11, PRT_CHECKBOX_3_11.isChecked());	        
	        editor.putBoolean(SPF_CHECKBOX_3_12, PRT_CHECKBOX_3_12.isChecked());	        
	        
	        editor.commit();    	//很重要！用于保存数据！不用commit保存是写不进文件的！     	
	        handler.sendEmptyMessage(100);
	        finish();
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
    
}