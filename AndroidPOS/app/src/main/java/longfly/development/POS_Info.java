package longfly.development;

import org.w3c.dom.Text;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class POS_Info extends Activity {
    /** Called when the activity is first created. */
	Button		INIFO_EXIT,INFO_BTN_2,INFO_BTN_3;
	TextView	INFO_TITLE,INFO_PART_1,INFO_PART_2,INFO_PART_3;
	TextView	INFO_PART_1_TEXT,INFO_PART_2_TEXT,INFO_PART_3_TEXT;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_info);
        
        find_id();
        key_things();
        init_view();
    }

	private void init_view() {
		// TODO Auto-generated method stub
		INFO_TITLE.setText("系统信息");
		INFO_PART_1.setText("版本信息");
		INFO_PART_2.setVisibility(View.GONE);
		INFO_PART_3.setVisibility(View.GONE);
		INFO_PART_1_TEXT.setText("v20131225-1");
		INFO_PART_2_TEXT.setVisibility(View.GONE);
		INFO_PART_3_TEXT.setVisibility(View.GONE);
		INFO_BTN_2.setVisibility(View.GONE);	
		INFO_BTN_3.setVisibility(View.GONE);		
	}

	private void key_things() {
		// TODO Auto-generated method stub
		INIFO_EXIT.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				finish();
			}
		});
	}

	private void find_id() {
		// TODO Auto-generated method stub
		INIFO_EXIT = (Button) findViewById(R.id.info_exit);
		INFO_BTN_2 = (Button) findViewById(R.id.info_btn_2);
		INFO_BTN_3 = (Button) findViewById(R.id.info_btn_3);		
		INFO_TITLE = (TextView) findViewById(R.id.info_title);
		INFO_PART_1 = (TextView) findViewById(R.id.info_part_1);
		INFO_PART_2 = (TextView) findViewById(R.id.info_part_2);
		INFO_PART_3 = (TextView) findViewById(R.id.info_part_3);
		INFO_PART_1_TEXT = (TextView) findViewById(R.id.info_part_1_text);
		INFO_PART_2_TEXT = (TextView) findViewById(R.id.info_part_2_text);
		INFO_PART_3_TEXT = (TextView) findViewById(R.id.info_part_3_text);
	}
}