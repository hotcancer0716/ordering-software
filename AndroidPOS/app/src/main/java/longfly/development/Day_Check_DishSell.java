package longfly.development;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker.OnTimeChangedListener;

public class Day_Check_DishSell extends Activity{

	SQLiteDatabase db;
    private ArrayList<HashMap<String,String>> list=null;
    private HashMap<String,String>map=null;
	SimpleAdapter adapter;

	/****************************************************/	
	Button	LIUSHUI_EXIT ,BTN_PART_1,BTN_PART_2,BTN_PART_3;
	ListView	LS_LIUSHUI;
	HorizontalScrollView LAYPART_1,LAYPART_2,LAYPART_3;
	
	String SQL_Table = "lfcy_day_check_dish";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.i800_daycheck_dishsell);
        
       find_id();
       key_things();
 	   db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString()+"/Android_POS.db3", null);
 	   
 	   //获取今天的日期
	  Cursor  cursor = db.rawQuery("select * from lfcy_day_check_dish order by cDishQty DESC",null);
	  fill_hashmap(cursor);
	  inflateList(cursor);	
	}

	private void init_btn(int state){
		BTN_PART_1.setBackgroundResource(R.drawable.table_number_up);
		BTN_PART_2.setBackgroundResource(R.drawable.table_number_up);
		BTN_PART_3.setBackgroundResource(R.drawable.table_number_up);

		
		LAYPART_1.setVisibility(View.GONE);
		LAYPART_2.setVisibility(View.GONE);
		LAYPART_3.setVisibility(View.GONE);

		
		AnimationSet animationSet = new AnimationSet(true); 
		TranslateAnimation translateAnimation = new TranslateAnimation( 
				Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0f, 
				Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0f); 
		translateAnimation.setDuration(200); 
		animationSet.addAnimation(translateAnimation); 
		
		switch (state) {
		case 1:
			BTN_PART_1.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYPART_1.setVisibility(View.VISIBLE);			
			LAYPART_1.startAnimation(animationSet);			
			break;
		case 2:
			BTN_PART_2.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYPART_2.setVisibility(View.VISIBLE);			
			LAYPART_2.startAnimation(animationSet);			
			break;	
		case 3:
			BTN_PART_3.setBackgroundResource(R.drawable.btn_blue_up);			
			LAYPART_3.setVisibility(View.VISIBLE);			
			LAYPART_3.startAnimation(animationSet);			
			break;	
		default:
			break;
		}
		
	}
	
	private void key_things() {

		LIUSHUI_EXIT.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
		   		db.close();
		   		finish();
		   	}
	    });	
		
		LS_LIUSHUI.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
			{
				//变蓝就好
			}
		});		
	
		BTN_PART_1.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
//		   		LAYPART_1.setVisibility(View.VISIBLE);
//		   		LAYPART_2.setVisibility(View.GONE);
//		   		BTN_PART_1.setBackgroundResource(R.drawable.table_background);
//		   		BTN_PART_2.setBackgroundResource(R.drawable.table_number_up);
		   		init_btn(1);
		   	}
	    });	
		
		BTN_PART_2.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
//		   		LAYPART_1.setVisibility(View.GONE);
//		   		LAYPART_2.setVisibility(View.VISIBLE);
//		   		BTN_PART_1.setBackgroundResource(R.drawable.table_number_up);
//		   		BTN_PART_2.setBackgroundResource(R.drawable.table_background);
		   		init_btn(2);
		   	}
	    });	

		BTN_PART_3.setOnClickListener(new Button.OnClickListener()
	    {
		   	@Override
		   	public void onClick(View v)
		   	{	
//		   		LAYPART_1.setVisibility(View.GONE);
//		   		LAYPART_2.setVisibility(View.VISIBLE);
//		   		BTN_PART_1.setBackgroundResource(R.drawable.table_number_up);
//		   		BTN_PART_2.setBackgroundResource(R.drawable.table_background);
		   		init_btn(3);
		   	}
	    });	
	}

	//创建并新建一个Hashmap
	private ArrayList fill_hashmap(Cursor cursor) {
	    list=new ArrayList<HashMap<String,String>>();
		int i=0;
		while(cursor.moveToNext()) {
			i++;
            map=new HashMap<String,String>();
            
            map.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
            map.put("_num", ""+i);
            map.put("cDishName", cursor.getString(cursor.getColumnIndex("cDishName")));
            map.put("cDishCode",  cursor.getString(cursor.getColumnIndex("cDishCode")));
            map.put("fDishPrice",  cursor.getString(cursor.getColumnIndex("fDishPrice")));
            map.put("cDishQty",  cursor.getString(cursor.getColumnIndex("cDishQty")));
            map.put("cDishUnitName",  cursor.getString(cursor.getColumnIndex("cDishUnitName")));
            map.put("cDishUnitCode",  cursor.getString(cursor.getColumnIndex("cDishUnitCode")));
            map.put("fDishAllPrice",  cursor.getString(cursor.getColumnIndex("fDishAllPrice")));
			list.add(map);
		}
		
		return list;
	}
	
	private void inflateList(Cursor cursor)
	{
		//将数据与adapter集合起来
        adapter = new SimpleAdapter(
        		this, 
				list, 
				R.layout.imx_timesell_rank 
				, new String[]{	"_num" 			, 
						"cDishName"		,"cDishCode"		, "fDishPrice"	,
						"cDishQty" ,"cDishUnitName"	,"fDishAllPrice"
			}
        		, new int[]{R.id.text_1 , R.id.text_2 ,R.id.text_3,R.id.text_4,R.id.text_5,R.id.text_6,R.id.text_7
			}
        );
		
		//显示数据
        LS_LIUSHUI.setAdapter(adapter);
	}
	
	private void find_id() {
		// TODO Auto-generated method stub
		LIUSHUI_EXIT 	= (Button) findViewById(R.id.liuchui_check_exit);
		LS_LIUSHUI		= (ListView) findViewById(R.id.liushui_check_list);
		
		BTN_PART_1	= (Button) findViewById(R.id.daycheck_sellreport);
		BTN_PART_2	= (Button) findViewById(R.id.daycheck_myreport);
		BTN_PART_3	= (Button) findViewById(R.id.daycheck_workerreport);
		
		LAYPART_1	= (HorizontalScrollView) findViewById(R.id.part_1);
		LAYPART_2	= (HorizontalScrollView) findViewById(R.id.part_2);
		LAYPART_3	= (HorizontalScrollView) findViewById(R.id.part_3);
	}
	
	
	/***************     以下是一个带进度条的对话框    **************************************/
	ProgressDialog progressDialog;
	private void progressDialog(String title, String message) {
		// TODO Auto-generated method stub
	     progressDialog = new ProgressDialog(Day_Check_DishSell.this);
	     progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	     progressDialog.setMessage(message);
	     progressDialog.setTitle(title);
	     progressDialog.setProgress(0);
	     progressDialog.setMax(100);
	     progressDialog.setCancelable(false);
	     progressDialog.show();
	}

	//用来查询进度条的值！大于100就把进度条Cancel掉
    Handler handler = new Handler(){
    	  @Override
    	  public void handleMessage(Message msg) {
    		  // TODO Auto-generated method stub
    		  if(msg.what>=100){
    			  progressDialog.cancel();
				  Cursor  cursor = db.rawQuery("select * from lfcy_day_check_dish order by cDishQty DESC",null);
				  fill_hashmap(cursor);
				  inflateList(cursor);	
    	       }
    		  progressDialog.setProgress(msg.what);
    		  super.handleMessage(msg);
    	  }
    };

	
//创建一个进程！用来后台加载数据
    class insert_Thread extends Thread{
        public void run(){
			handler.sendEmptyMessage(1);	//进度条进度
			

			
			handler.sendEmptyMessage(100);
        }
    };
	
	/***************     以上是一个进度条    **************************************/

    
}
